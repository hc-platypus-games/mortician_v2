﻿Shader "Idle/UnlitSprite"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_Opacity("Opacity", Range(0,1)) = 1
        
        _Ambient("Ambient", Range(0,1)) = 1
        _AmbientMask("Ambient Mask", Vector) = (1,1,1,0)
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex VertexProgram
            #pragma fragment FragmentProgram
			#pragma multi_compile_instancing

            #define _AMBIENT_LIGHT

            #include "ShaderFunctions.cginc"
			
			// struct appdata_t
			// {
			// 	float4 vertex   : POSITION;
			// 	float4 color    : COLOR;
			// 	float2 uv : TEXCOORD0;
			// };
			//
			// struct v2f
			// {
			// 	float4 vertex   : SV_POSITION;
			// 	fixed4 color    : COLOR;
			// 	float2 uv  : TEXCOORD0;
			// };

			fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                Helpers h = GetHelpers(i);
                fixed4 color = tex2D(_MainTex, i.uv) * TINT4;
            	color.a *= INST(_Opacity);
            	color.rgb *= color.a;

                AMBIENT_LIGHT(color.rgb, 1, h);
                
                return color;
            }
			
			// v2f VertexProgram(appdata_t IN)
			// {
			// 	v2f OUT;
			// 	OUT.vertex = UnityObjectToClipPos(IN.vertex);
			// 	OUT.uv = IN.uv;
			// 	OUT.color = IN.color * half4(INST(_Color), 1);
			// 	return OUT;
			// }
			//
			// fixed4 FragmentProgram(v2f i) : SV_Target
			// {
			// 	fixed4 color = tex2D(_MainTex, i.uv) * i.color;
			// 	color.rgb *= color.a;
			//
			// 	AMBIENT_LIGHT(color, 1, h);
			// 	
			// 	return color;
			// }
		ENDCG
		}
	}
}