﻿Shader "Idle/UnlitTransparentColor"
{
    Properties
    {
        _Color("Tint", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0, 1)) = 1
        
        _MainTex ("Texture", 2D) = "white" {}
        _Power("Power", Float) = 1
        
        _ZOffset("ZOffset", Float) = -1
    }
    SubShader
    {
        Tags { 
            "RenderType"="Transparent"
            "Queue"="AlphaTest+50"
        }
        Cull Back
        Offset 0, [_ZOffset]
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram

            #include "ShaderFunctions.cginc"

            half _Power;

            fixed4 FragmentProgram (Interpolators i) : SV_Target
            {
                fixed4 main_tex = tex2D(_MainTex, i.uv);
                //clip(main_tex.r - 0.01);
                main_tex.a = pow(INST(_Opacity) * main_tex.r, _Power);
                main_tex.rgb = INST(_Color);
                return main_tex;
            }
            ENDCG
        }
    }
}
