﻿Shader "Idle/StandardWindow"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Color2("Off Color", Color) = (0,0,0,0)
        _Idx("Idx", Range(0, 1)) = 1
        
        
        _NormalLight("Light", Vector) = (1,1,1,1)
        _NormalColorX("Color X", Color) = (1,1,1,1)
        _NormalColorY("Color Y", Color) = (1,1,1,1)
        _NormalColorZ("Color Z", Color) = (1,1,1,1)
        
        _Ambient("Ambient", Range(0,1)) = 1
        _AmbientMask("Ambient Mask", Vector) = (1,1,1,0)
    }
    
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        Pass
        {
            CGPROGRAM
            
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing

            #define _AMBIENT_LIGHT
            #define _IDX

            #include "ShaderFunctions.cginc"

            half3 _Color2;

            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);
                Helpers h = GetHelpers(i);

                half3 color = _Color2;

                AMBIENT_LIGHT(color, 1, h);

                color = lerp(color, INST(_Color), INST(_Idx));
                
                return half4(color, 1);
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}
