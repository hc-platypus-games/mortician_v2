﻿Shader "Idle/StandardPropsBlinding"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0,1)) = 1
        _MainTex("Color Tex", 2D) = "white" {}
        _BlindSpeed("Blind Speed", Range(50,500)) = 100
        _BlindIntensity("Blind Intensity", Range(0, 0.5)) = 0.5
        
        [HideInInspector]
        _EmissionTex("Emission Map", 2D) = "black" {}
        _Emission("Emission", Float) = 1
        
        _NormalLight("Light", Vector) = (1,1,1,1)
        _NormalColorX("Color X", Color) = (1,1,1,1)
        _NormalColorY("Color Y", Color) = (1,1,1,1)
        _NormalColorZ("Color Z", Color) = (1,1,1,1)
        
        _Ambient("Ambient", Range(0,1)) = 1
        _AmbientMask("Ambient Mask", Vector) = (1,1,1,0)
    }
    
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        Pass
        {
            CGPROGRAM
            
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing

            #define _NORMAL_LIGHT
            #define _AMBIENT_LIGHT
            #define _EMISSION

            #include "ShaderFunctions.cginc"

            float _BlindSpeed;
            float _BlindIntensity;
            
            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);

                half4 src_tex = tex2D(_MainTex, i.uv);
                half4 main_tex = src_tex;
                
                DitherClip(i.screen_pos, main_tex.a * INST(_Opacity));
                Helpers h = GetHelpers(i);

                // apply lighting
                NORMAL_LIGHT(main_tex.rgb, h);

                // apply emissive
                AMBIENT_LIGHT(main_tex.rgb, 1, h);

                main_tex.rgb = lerp(main_tex.rgb, src_tex, (i.color.g) * INST(_Emission));
                
                
                // tint
                main_tex.rgb *= INST(_Color);
                float blind = cos(_Time * _BlindSpeed) * _BlindIntensity + _BlindIntensity;
                main_tex.rgb += blind;
                return main_tex;
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}
