﻿Shader "Idle/StandardSimple"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0,1)) = 1
        
        _NormalLight("Light", Vector) = (1,1,1,1)
        _NormalColorX("Color X", Color) = (1,1,1,1)
        _NormalColorY("Color Y", Color) = (1,1,1,1)
        _NormalColorZ("Color Z", Color) = (1,1,1,1)
        
        _Ambient("Ambient", Range(0,1)) = 1
        _AmbientMask("Ambient Mask", Vector) = (1,1,1,0)
    }
    
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        Pass
        {
            CGPROGRAM
            
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing

            #define _NORMAL_LIGHT
            #define _AMBIENT_LIGHT

            #include "ShaderFunctions.cginc"

            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);
                
                DitherClip(i.screen_pos, INST(_Opacity));
                Helpers h = GetHelpers(i);

                half3 color = INST(_Color);

                NORMAL_LIGHT(color, h);
                AMBIENT_LIGHT(color, 1, h);
                
                return half4(color, 1);
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}
