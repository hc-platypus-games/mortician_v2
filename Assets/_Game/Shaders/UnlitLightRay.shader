﻿Shader "Idle/UnlitLightRay"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0, 1)) = 1
        
        _Width("Width", Range(0, 1)) = 0.5
        _HalfWidth("Half Width", Range(0, 0.5)) = 0.1
        
        _TopPosition("Top Position", Range(0, 1)) = 0
        _TopPower("Top Contrast", Float) = 1
        _BottomPosition("Bottom Position", Range(0, 1)) = 0
        _BottomPower("Bottom Contrast", Float) = 1

    }
    SubShader
    {
        Tags { 
            "RenderType"="Transparent"
            "Queue"="AlphaTest+50"
        }
        Cull Back
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing

            #define _LIGHT_RAY

            #include "ShaderInputs.cginc"
            #include "ShaderUtils.cginc"

            struct VertexData
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Interpolators
            {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
                float4 screenPos : TEXCOORD1;

                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            Interpolators VertexProgram (VertexData v)
            {
                Interpolators i;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, i);
                
                i.pos = UnityObjectToClipPos(v.vertex);
                i.uv = v.uv;
                i.screenPos = ComputeScreenPos(i.pos);
                return i;
            }

            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                half x = i.uv.x;
                half y = i.uv.y;

                //cone
                half width_gradient = abs(x * 2 - 1);

                half y_lift = (1-y) * (1 - INST(_Width)) + INST(_Width);
                half cone = step( y_lift * (1-INST(_HalfWidth)), width_gradient);
                half halfCone = 1 - step(y_lift, width_gradient);
                halfCone *= cone;
                cone = 1- cone;
                

                //top grad
                half topGradient;
                topGradient = Fit(y, 1.001, INST(_TopPosition), 0, 1);
                topGradient = saturate(topGradient);
                topGradient = pow(topGradient, INST(_TopPower));

                half baseGradient;
                baseGradient = Fit(1-y, 1.001, INST(_BottomPosition), 0, 1);
                baseGradient = saturate(baseGradient);
                baseGradient = pow(baseGradient, INST(_BottomPower));


                half c = (cone + halfCone * 0.35) * baseGradient * topGradient;
  
                
                fixed4 col = fixed4(INST(_Color), c * INST(_Opacity));
                
                return col;
            }
            ENDCG
        }
    }
}
