﻿Shader "Hidden/IdleLUT" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}

		_LUT("LUT", 2D) = "white" {}
        _LUTIntensity("Contribution", Range(0, 1)) = 1
	}
	SubShader 
	{
		Cull Off ZWrite Off ZTest Always
		Pass
		{
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			half4 _MainTex_ST;

			sampler2D _LUT;
            float4 _LUT_TexelSize;
            fixed _LUTIntensity;
			fixed _ColorsAmount;

			float4 applyLUT(float4 color) : COLOR
			{
				color = saturate(color);
				float i = color.b * 63;
				float x = floor(i % 8);
				float y = floor(i / 8);

				x = (x * 64) + (color.r * 63);
				y = (y * 64) + (color.g * 63);

				x /= 512;
				y /= 512;

				float4 cc = tex2D(_LUT, float2(x + 0.5/512, 1-(y+ 0.5/512)));

				float4 c = lerp(color, cc, _LUTIntensity);
				return c;
				return pow(c, 2.2);
			}

			float4 frag(v2f_img i) : COLOR
			{
				float4 color = tex2D(_MainTex, i.uv);
				return applyLUT(color);
			}

			ENDCG
		} 
	}
}    