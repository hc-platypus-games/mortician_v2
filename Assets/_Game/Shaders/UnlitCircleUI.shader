﻿Shader "Idle/UnlitCircleUI"
{
    Properties
    {
        _Color("Color1", Color) = (1,1,1,1)
        _Color2("Color2", Color) = (0,0,0,1)
        
        _Progress("Progress", Range(0, 1)) = 1
        
        _Width("Circle Width", Range(0, 1)) = 0.2
        _Smooth("Circle Smooth", Range(0, 0.2)) = 0.05
        
        _LineCount("Lines Count", Range(0, 50)) = 1
        _LineWidth("Line Width", Range(0.05, 0.95)) = 0.5
        _LineSmooth("Line Smooth", Range(0, 1)) = 0
        _LineSpeed("Speed", Range(-10, 10)) = 1
    }
    SubShader
    {
        Tags { 
            "RenderType"="Transparent"
            "Queue"="Transparent"
        }
        Cull Back
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            #define PI 3.141592653589793238462
            #define PI2 6.283185307179586476924

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            half3 _Color;
            half3 _Color2;
            half _Width;
            half _LineSpeed;
            half _Smooth;
            int _LineCount;
            half _Progress;
            half _LineSmooth;
            half _LineWidth;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                half2 uv = i.uv - 0.5;
                half2 st;
                st.x = sqrt(uv.x*uv.x + uv.y * uv.y);
                st.y = (atan2(uv.y, uv.x) + PI) / PI2;

                half steps = sin(st.y * PI * _LineCount*2 + _Time.w * _LineSpeed) * 0.5 + 0.5;
                steps = smoothstep(steps, steps+_LineSmooth, _LineWidth);
                half circle = smoothstep(st.r,st.r+_Smooth, 0.5) * (1-smoothstep(st.r-_Smooth, st.r, 0.5-_Width*0.5));
                half3 color = _LineCount > 0 ? lerp(_Color, _Color2, steps) : _Color;

                // half progress_grad = frac(st.y*(1/_Progress));
                // half progress = smoothstep(progress_grad, progress_grad+_ProgressSmooth, 0.5);
                half progress = step(1-_Progress, st.y);
                // half progress = _Progress
                // half progress = smoothstep(st.y-_ProgressSmooth, st.y+_ProgressSmooth, );
                return fixed4(color, circle * progress);
            }
            ENDCG
        }
    }
}
