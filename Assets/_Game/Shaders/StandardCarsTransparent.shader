﻿Shader "Idle/StandardCarsTransparent"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0,1)) = 1
        _MainTex("Color Tex", 2D) = "white" {}
        
        [HideInInspector]
        _EmissionTex("Emission Map", 2D) = "black" {}
        _Emission("Emission", Float) = 1
        _Pulse("Pulse Emission", Float) = 0
        
        _NormalLight("Light", Vector) = (1,1,1,1)
        _NormalColorX("Color X", Color) = (1,1,1,1)
        _NormalColorY("Color Y", Color) = (1,1,1,1)
        _NormalColorZ("Color Z", Color) = (1,1,1,1)
        
        _Ambient("Ambient", Range(0,1)) = 1
        _AmbientMask("Ambient Mask", Vector) = (1,1,1,0)
        
        _Rim("Rim", Range(0, 1)) = 0
        _RimPower("Rim Contrast", Range(0.5, 8)) = 2

        _ZOffset("ZOffset", Float) = 0
    }
    
    SubShader
    {
        Tags { 
            "RenderType"="Transparent" 
            "Queue"="Transparent"
        }
        Cull Back
        Offset 0, [_ZOffset]
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite On

        
        Pass
        {
            CGPROGRAM
            
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing

            #define _NORMAL_LIGHT
            #define _AMBIENT_LIGHT
            #define _EMISSION
            #define _PULSE
            #define _RIM

            #include "ShaderFunctions.cginc"

            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);

                half4 src_tex = tex2D(_MainTex, i.uv);
                half4 main_tex = src_tex;
                
                //DitherClip(i.screen_pos, main_tex.a * INST(_Opacity));
                Helpers h = GetHelpers(i);

                NORMAL_LIGHT(main_tex.rgb, h);

                AMBIENT_LIGHT(main_tex.rgb, 1, h);

                half pulse = i.color.b * INST(_Pulse);// * abs(sin(_Time * _PulseSpeed));
                main_tex.rgb = lerp(main_tex.rgb, src_tex, i.color.g * INST(_Emission) + pulse);

                // tint
                main_tex.rgb *= INST(_Color);

                // rim
                half ndotl = dot(i.world_normal, normalize(i.view_dir));
                half rim = pow(1 - ndotl, INST(_RimPower)) * INST(_Rim);
                main_tex.rgb += rim;

                main_tex.a *= INST(_Opacity);

                //main_tex.rgb = normalize(i.view_dir);
                
                return main_tex;
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}
