﻿Shader "Idle/RoomBox"
{
    Properties
    {
        _Color("Light Color", Color) = (1,1,1,1)
        _Color2("Shadow Color", Color) = (0,0,0,1)
        _Rim("Intensity", Range(0, 3)) = 0
        _RimPower("Contrast", Range(0.1, 3)) = 1
        
        _NormalLight("Light", Vector) = (1,1,1,1)
        _NormalColorX("Color X", Color) = (1,1,1,1)
        _NormalColorY("Color Y", Color) = (1,1,1,1)
        _NormalColorZ("Color Z", Color) = (1,1,1,1)
        
        _Ambient("Ambient", Range(0,1)) = 1
        _AmbientMask("Ambient Mask", Vector) = (1,1,1,0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing

            #define _NORMAL_LIGHT
            #define _AMBIENT_LIGHT
            #define _RIM

            #include "ShaderFunctions.cginc"

            half3 _Color2;

            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);
                
                Helpers h = GetHelpers(i);

                //half pulse = abs(sin(_Time * 130) + (i.worldPos.x + i.worldPos.y*0)) * 0.5 + 0.5;
                half pulse = abs(sin(_Time * 100) + sin(_Time * 30 + 2 + i.world_pos)) * 0.25 + 0.5;
                //half pulse = 1;

                half power = INST(_RimPower);
                half mask = pow(i.color.r, power) ;

                half3 color = lerp(_Color2, 0, mask);

                NORMAL_LIGHT(color, h);
                AMBIENT_LIGHT(color, 1, h);

                color += INST(_Color) * INST(_Rim) * mask * pulse;
                
                return half4(color, 1);
            }

            ENDCG
        }
    }
}
