﻿Shader "Idle/StandardFull"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0,1)) = 1
        _MainTex("Color Tex", 2D) = "white" {}
        
        _EmissionTex("Emission Map", 2D) = "black" {}
        _Emission("Emission", Float) = 1
        
        _LightTex("Light(R) GI(G) Paint(B)", 2D) = "white" {}
        _LightColor("Light Color", Color) = (1,1,1,1)
        _ShadowColor("Shadow Color", Color) = (1,1,1,1)
        _IndirectColor("Indirect Color", Color) = (0,0,0,0)
        _Indirect("Indirect", Float) = 0
        
        _NormalLight("Normal Light", Vector) = (1,1,1,1)
        _NormalColorX("X Color", Color) = (1,1,1,1)
        _NormalColorY("Y Color", Color) = (1,1,1,1)
        _NormalColorZ("Z Color", Color) = (1,1,1,1)
        
        _Ambient("Ambient", Range(0,1)) = 1
        _AmbientMask("Ambient Mask", Vector) = (1,1,1,1)
    }
    
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing
            
            #define _NORMAL_LIGHT
            #define _TEXTURE_LIGHT
            #define _AMBIENT_LIGHT
            #define _EMISSION

            #include "ShaderFunctions.cginc"

            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);

                half4 main_tex = tex2D(_MainTex, i.uv);
                half4 light_tex = tex2D(_LightTex, i.uv);
                
                DitherClip(i.screen_pos, main_tex.a * INST(_Opacity));
                Helpers h = GetHelpers(i);

                // apply lighting
                NORMAL_LIGHT(main_tex.rgb, h);
                TEXTURE_LIGHT(main_tex.rgb, light_tex.r);

                // apply emissive
                AMBIENT_LIGHT(main_tex.rgb, light_tex.r, h);
                TEXTURE_GI(main_tex.rgb, light_tex.g);
                EMISSION(main_tex.rgb, i);

                // tint
                main_tex.rgb *= INST(_Color);
                
                return main_tex;
            }


            ENDCG
        }
    }
    FallBack "Diffuse"
}
