﻿Shader "Idle/Road"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0,1)) = 1
        _MainTex("Color Tex", 2D) = "white" {}
        
        _NormalLight("Light", Vector) = (1,1,1,1)
        _NormalColorX("Color X", Color) = (1,1,1,1)
        _NormalColorY("Color Y", Color) = (1,1,1,1)
        _NormalColorZ("Color Z", Color) = (1,1,1,1)
        
        _Ambient("Ambient", Range(0,1)) = 1
        _AmbientMask("Ambient Mask", Vector) = (1,1,1,0)
        
        _TileTexture("Tile", 2D) = "white" {}
        _TileMult("Tile Mult", Range(0,1)) = 0.35
        
        _Dirt("Dirt", Color) = (1,1,1,1)
    }
    
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        Pass
        {
            CGPROGRAM
            
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing
            #pragma target 3.0

            #define _NORMAL_LIGHT
            #define _AMBIENT_LIGHT
            #define _TRIPLANAR

            #include "ShaderFunctions.cginc"

            sampler2D _TileTexture;
            float4 _TileTexture_ST;
            half _TileMult;
            half3 _Dirt;

            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);

                half4 main_tex = tex2D(_MainTex, i.uv);
                DitherClip(i.screen_pos, main_tex.a * INST(_Opacity));
                Helpers h = GetHelpers(i);
                

                 // half tile_tex_1 = TriplanarTex(i, _TileTexture, _TileTexture_ST);
                 // half tile_tex_2 = TriplanarTex(i, _TileTexture, _TileTexture_ST * half4(2, 2, 1, 1));
                 // half tile_tex = lerp(tile_tex_1, tile_tex_2, i.color.r);
                 // tile_tex = lerp(0.2176, tile_tex, i.color.r + i.color.g + i.color.b);
                 // tile_tex = pow(tile_tex, .454);
                 // tex.rgb = OverlayBlend(tex.rgb, tile_tex.rrr, _TileMult);

                main_tex.rgb = lerp(main_tex.rgb, main_tex.rgb * _Dirt, i.color.r*i.color.r);

                NORMAL_LIGHT(main_tex.rgb, h);
                AMBIENT_LIGHT(main_tex.rgb, 1, h);
                
                main_tex.rgb *= INST(_Color);
                return main_tex;
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}
