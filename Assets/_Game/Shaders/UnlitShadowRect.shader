﻿Shader "Idle/UnlitShadowRect"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0,1)) = 1
        
        _Smooth("Smooth", Range(-0.5,0.5)) = 0
        _Power("Power", Range(0.2, 8)) = 1
        
        _ZOffset("ZOffset", Float) = 0
    }
    
    SubShader
    {
        Tags { 
            "RenderType"="Transparent"
            "Queue"="AlphaTest+50"
        }
        Cull Back
        Offset 0, [_ZOffset]
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off
        
        Pass
        {
            CGPROGRAM
            
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing

            #define _RECT

            #include "ShaderFunctions.cginc"

            half _Smooth;
            half _Power;


            half2 Fit2(half2 value, half2 from1, half2 to1, half2 from2, half2 to2) {
                half2 c = (value - from1) / (to1 - from1) * (to2 - from2) + from2;
                return c;
            }

            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);

                half2 pos_min = INST(_PosMin).xz;
                half2 pos_max = INST(_PosMax).xz;
                half2 pos = i.world_pos.xz;

                half2 scale = 1 / (pos_max - pos_min);
                half2 d = 1 - min(pos - pos_min, pos_max - pos);
                d = Fit2(d, -_Smooth*2, 1, -1, 1);
                half sd = length( max(0, d) + min(0, max(d.x, d.y))) * max(sign(d.x), sign(d.y));
                sd = pow(1-saturate(sd), _Power);

                //d = smoothstep(0, _Smooth, d);
                //d = pow(d, _Power) * INST(_Opacity);

                //return fixed4(sd.xx, 0, 1);
                return fixed4(INST(_Color), sd * INST(_Opacity));
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}
