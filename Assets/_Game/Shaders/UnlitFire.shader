﻿Shader "Idle/UnlitFire"
{
    Properties
    {
        _Color("Color1", Color) = (1,1,1,1)
        _Color2("Color2", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0,1)) = 1
        
        _MainTex("Noise Tex", 2D) = "white" {}
    }
    SubShader
    {
        Tags { 
            "RenderType"="Opaque"
        }
        Cull Back
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing

            #include "ShaderFunctions.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
                float4 screenPos : TEXCOORD1;
            };

            half3 _Color2;

            v2f VertexProgram (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.screenPos = ComputeScreenPos(o.pos);
                return o;
            }

            fixed4 FragmentProgram (v2f i) : SV_Target
            {
                half2 uv_off = i.uv - half2(0, _Time.y * 0.5);

                half circle = 1- distance(half2(0.5, 0.2), i.uv) * 1.5;
                half noise = pow(tex2D(_MainTex, uv_off), 2.2);
                noise = (noise - 0.1) * (1-pow(i.uv.y, 2)) * 1.5 * pow(circle, 2);
                half v1 = 1- step(noise, 0.05);
                half v2 = 1 - step(noise, 0.1);

                half3 res = lerp(INST(_Color), _Color2, v2);
                res = lerp(_Color2, res, i.uv.y + 0.5);
                DitherClip(i.screenPos, v1 * INST(_Opacity));
                return half4(res, 1);
            }
            ENDCG
        }
    }
}
