﻿Shader "Idle/CharacterTransparent"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Opacity("Opacity", Range(0,1)) = 1
        _MainTex("Color Tex", 2D) = "white" {}
        
        [HideInInspector] _EmissionTex("Emission Map", 2D) = "black" {}
        _Emission("Emission", Float) = 1
        _EmissionTint("Emission Tint", Color) = (1,1,1,1)
        
        _NormalLight("Light", Vector) = (1,1,1,1)
        _NormalColorX("Color X", Color) = (1,1,1,1)
        _NormalColorY("Color Y", Color) = (1,1,1,1)
        _NormalColorZ("Color Z", Color) = (1,1,1,1)
        
        _Ambient("Ambient", Range(0,1)) = 1
        _AmbientMask("Ambient Mask", Vector) = (1,1,1,0)
        
        _Rim("Rim", Range(0, 1)) = 0.5
        _RimPower("Rim Contrast", Range(0.5, 8)) = 2
        
        _ShadowThreshold("Shadow Threshold", Range(-1, 1)) = 0.2
        _ShadowGain("Shadow Gain", Range(0,1)) = 0.85
        _ShadowNormal("Shadow Normal", Vector) = (0, 1, 1.8)
        
        _ZOffset("ZOffset", Float) = -1
    }
    
    SubShader
    {
        Tags { 
            "RenderType"="Transparent"
            "Queue"="Transparent"
             }
        
        Cull Back
        Offset 0, [_ZOffset]
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite On
        
        Pass
        {
            CGPROGRAM
            
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_instancing

            #define _NORMAL_LIGHT
            #define _AMBIENT_LIGHT
            #define _EMISSION
            #define _RIM

            #include "ShaderFunctions.cginc"

            half3 _EmissionTint;
            half _ShadowThreshold;
            half _ShadowGain;
            half3 _ShadowNormal;

            fixed4 FragmentProgram(Interpolators i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);

                half4 main_tex = tex2D(_MainTex, i.uv);
                
                //DitherClip(i.screen_pos, main_tex.a * INST(_Opacity));
                Helpers h = GetHelpers(i);

                // apply lighting
                NORMAL_LIGHT(main_tex.rgb, h);

                // apply emissive
                AMBIENT_LIGHT(main_tex.rgb, 1, h);
                //EMISSION(main_tex.rgb, i);

                main_tex.rgb += main_tex.rgb * i.color.r * INST(_Emission) * _EmissionTint;

                // rim
                half rim = 1.0 - saturate(dot(normalize(i.view_dir), i.world_normal));
                half3 rim_color = main_tex.rgb + pow(rim, INST(_RimPower)) * INST(_Rim);

               // half shadow_top = (i.world_normal.y + 0.5) > 0 ? 1 : 0;
                //half shadow_rim = (rim) > 0.5 ? 0 : 1;
                // half shadow = max(i.world_normal.y + 0.5, 1-rim);
                // shadow = shadow > 0.35 ? 0 : 1;
                // shadow = saturate(shadow);
                half shadow_dot = dot(normalize(i.world_normal), normalize(_ShadowNormal));
                half shadow = shadow_dot + _ShadowThreshold > 0 ? 0 : 1;
                main_tex.rgb = lerp(rim_color, rim_color * _ShadowGain, shadow);

                // tint
                main_tex.rgb *= INST(_Color);
                main_tex.a *= INST(_Opacity);
                
                return main_tex;
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}
