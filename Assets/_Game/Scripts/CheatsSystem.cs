using System;
using _Game.Scripts.Enums;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems.Base;
using UnityEngine;
using Zenject;

namespace _Game.Scripts
{
    public class CheatsSystem : ITickableSystem
    {
        [Inject] private GameFlags _flags;
        [Inject] private GameSystem _game;
        
        public void Tick(float deltaTime)
        {
            if (_game.GamePaused) return;
            
            Pause();
            
            if (Input.GetKeyDown(KeyCode.F11)) CheckTimeScale(3f);
            if (Input.GetKeyDown(KeyCode.F12)) CheckTimeScale(10f);
            if (Input.GetKeyDown(KeyCode.S)) AddSoft();
            if (Input.GetKeyDown(KeyCode.H)) AddHard();
            if (Input.GetKeyDown(KeyCode.T)) AddTokens();
        }

        public static void CheckTimeScale(float value) => Time.timeScale = Time.timeScale > 1f ? 1f : value;
        private static bool IsKeyDown(KeyCode keyCode) => Input.GetKeyDown(keyCode);
        
        private static void Pause()
        {
            if (!IsKeyDown(KeyCode.P)) return;
            Time.timeScale = Time.timeScale >= 1 ? 0 : 1;
        }
        
        public void AddSoft()
        {
            _game.AddCurrency(GameParamType.Soft, 10000000, GetCurrencyPlace.Cheats);
        }
        
        public void AddHard()
        {
            _game.AddCurrency(GameParamType.Hard, 1000, GetCurrencyPlace.Cheats);
        }
        
        public void AddTokens()
        {
            _game.AddCurrency(GameParamType.Tokens, 1000, GetCurrencyPlace.Cheats);
        }
        
        public void RemoveAllPurchases()
        {
            _flags.Remove(GameFlag.AllAdsRemoved);
        }
    }
}