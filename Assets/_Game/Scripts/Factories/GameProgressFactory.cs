using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems;
using Sirenix.Serialization;

namespace _Game.Scripts.Factories
{
    public class GameProgressFactory : ITickableSystem
    {
        private readonly GameProgress.Pool _progressPool;
        public List<GameProgress> Progresses { get;} = new();
        
        public List<GameProgress> SavedProgresses { get;} = new();

        public GameProgressFactory(GameProgress.Pool progressPool)
        {
            _progressPool = progressPool;
        }

        public GameProgress CreateProgress(IGameProgress owner, GameParamType type, float target, bool looped = true, bool updatable = true, bool saved = false)
        {
            var progress = saved 
                ? SavedProgresses.Find(p => p.Owner == owner && p.Type == type)
                : Progresses.Find(p => p.Owner == owner && p.Type == type);
            
            if (progress != null) return progress;
            progress = _progressPool.Spawn(owner, type, target, looped, updatable);

            if (saved)
            {
                SavedProgresses.Add(progress);
            }
            else
            {
                Progresses.Add(progress);
            }
            
            return progress;
        }

        public void RemoveProgress(GameProgress progress)
        {
            _progressPool.Despawn(progress);
            Progresses.Remove(progress);
        }

        public GameProgress GetProgress(IGameProgress owner, GameParamType type)
        {
            var savedParam = Progresses.FirstOrDefault(p => p.Owner == owner && p.Type == type);
            if (savedParam != null) return savedParam;
            var param = SavedProgresses.FirstOrDefault(p => p.Owner == owner && p.Type == type);
            return param;
        }
        
        public GameProgress GetProgress<T>(GameParamType type)
        {
            var savedProgress = Progresses.FirstOrDefault(p => p.Owner is T && p.Type == type);
            if (savedProgress != null) return savedProgress;
            var progress = SavedProgresses.FirstOrDefault(p => p.Owner is T && p.Type == type);
            return progress;
        }
        
        public void Tick(float deltaTime)
        {
            foreach (var progress in Progresses)
            {
                progress.Tick(deltaTime);
            }
        }
    }
}