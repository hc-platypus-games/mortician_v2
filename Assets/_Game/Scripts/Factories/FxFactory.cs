using System.Collections.Generic;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Pool;
using _Game.Scripts.Systems;
using _Game.Scripts.View.Fx;
using UnityEngine;
using Zenject;
using Random = _Game.Scripts.Tools.Random;

namespace _Game.Scripts.Factories
{
    public class FxFactory
    {

        private FxPool _pool;
        private List<BaseFx> _activeFx;
        
        public class FxPool : MultiPrefabPool<BaseFx.FxType, BaseFx>
        {
            [Inject]
            public FxPool(DiContainer container, MemoryPoolSettings settings, BaseFx.Factory factory) : base(container, settings, factory)   {   }
        }

        private void ShowFx(BaseFx fx, Vector3 position)
        {
            fx.transform.position = position;
            InvokeSystem.StartInvoke(RemoveFirstFx, 5f);
        }

        public BaseFx SpawnFx(BaseFx.FxType type, Vector3 position)
        {
            var fx = _pool.Spawn(type);
            ShowFx(fx, position);
            return fx;
        }

        private void RemoveFirstFx()
        {
            
        }
		
        /*public TFx ShowFx<TFx>(Vector3 position) where TFx : BaseFx
        {
            var fx = Pool.Pop<TFx>(_map.View.transform);
            fx.transform.position = position;
            _activeFx.Add(fx);
            _map.StartInvoke(this, RemoveFx, 5f);
            return fx;
        }

        public TFx ShowFxWithText<TFx>(string value, Vector3 position) where TFx : BaseFx
        {
            var fx = ShowFx<TFx>(position);
            ShowFx<TextFx>(position).Init(value);
            return fx;
        }
		
        public TFx ShowUIFx<TFx>(Vector3 position) where TFx : BaseFx
        {
            var fx = Pool.Pop<TFx>(_canvas.transform);
            fx.transform.position = position;
            _activeFx.Add(fx);
            _map.StartInvoke(this, RemoveFx, 5f);
            return fx;
        }

        private void RemoveFx()
        {
            _activeFx[0]?.PushToPool();
            _activeFx.RemoveAt(0);
        }*/
    }
}
