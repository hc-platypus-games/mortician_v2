using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Core;
using _Game.Scripts.Enums;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Save.SaveStructures;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.View.MapPoints.Clickable;
using _Game.Scripts.View.MapPoints.Events;
using Zenject;

namespace _Game.Scripts.Factories
{
    public class LevelEventsFactory
    {
        public Action OnEventCreated,
            OnEventCalled,
            OnEventTaken;
        
        [Inject] private SceneData _sceneData;
        [Inject] private GameProgressFactory _progresses;
        [Inject] private GameParamFactory _params;
        [Inject] private UIFactory _ui;

        private readonly LevelSystem _levels;
        
        private List<MapEventPointView> _mapEvents = new();
        private List<ClickableEventView> _clickableEvents = new();
        public List<MapEventPointView> EventsQueue { get; } = new();
        public List<MapEventPointView> EventsCalledQueue { get; } = new();
        public List<MapEventPointView> MapEvents => _mapEvents;

        public int CallsCount => EventsCalledQueue.Count;

        public LevelEventsFactory(LevelSystem levels)
        {
            _levels = levels;
            _levels.OnLoadedLevel += OnLevelLoaded;
            _levels.OnDestroyLevel += OnDestroyLevel;
        }

        public void Init()
        {
            _params.GetParam<GameSystem>(GameParamType.Soft).UpdatedEvent += OnUpdateBadges;
        }

        private void OnUpdateBadges()
        {
            _ui.UpdateBadges(BadgeNotificationType.CorpsePoint);
        }

        public void LoadFromData(MapEventData data)
        {
            var point = _mapEvents.FirstOrDefault(i => i.Config.Id == data.Id);
            if(!point) return;
            point.LoadFromSave(data);
            if (data.Bought)
            {            
                if (data.Called)
                {
                    AddEventToCalled(point);
                    point.Capacity.Change(1);
                }
                else
                {
                    AddEventToQueue(point);
                }
            }
        }

        private void OnLevelLoaded()
        {
            _mapEvents = _sceneData.GetComponentsInChildren<MapEventPointView>(true).ToList();
            _clickableEvents = _sceneData.GetComponentsInChildren<ClickableEventView>(true).ToList();
            foreach (var item in _clickableEvents)
            {
                item.Init();
            }
            //InvokeSystem.StartInvoke(SpawnClickableTrash, 15f);
        }

        private void OnDestroyLevel()
        {
        }

        public void AddTutorialEvent()
        {
            var eventPoint = _mapEvents[0];
            AddEventToQueue(eventPoint);
        }
        
        public void AddEventToQueue(MapEventPointView pointView)
        {
            EventsQueue.Add(pointView);
            OnEventCreated?.Invoke();
        }

        public MapEventPointView GetEventFromQueue()
        {
            if (EventsQueue.Count == 0) return null;
            var newEvent = EventsQueue[0];
            EventsQueue.RemoveAt(0);
            return newEvent;
        }

        public void AddEventToCalled(MapEventPointView pointView)
        {
            var eventsFromPoint = EventsQueue.Count(i => i == pointView)
                                  + EventsCalledQueue.Count(i => i == pointView);
            if(eventsFromPoint >= _progresses.GetProgress(pointView, GameParamType.Capacity).TargetValue)
            {
                EventsQueue.Remove(pointView);
            }
            EventsCalledQueue.Add(pointView);
            OnEventCalled?.Invoke();
        }

        public MapEventPointView GetEventFromCalled()
        {
            if (EventsCalledQueue.Count == 0) return null;
            var newEvent = EventsCalledQueue.FirstOrDefault(i => i.IsAvailable);
            if (!newEvent) return null;
            var progress = _progresses.GetProgress(newEvent, GameParamType.Capacity);
            progress.Change(-1);
            progress.Play();
            EventsCalledQueue.Remove(newEvent);
            OnEventTaken?.Invoke();
            newEvent.IsAvailable = false;
            return newEvent;
        }

        public List<MapEventPointView> GetCorpsePoints()
        {
            return _mapEvents;
        }

        public float GetCorpsesCount()
        {
            return _mapEvents.Sum(c => c.Capacity.CurrentValue);
        }

        public void OnFreeEventPoint(MapEventPointView point)
        {
            point.IsAvailable = true;
            if(EventsCalledQueue.Count > 0) OnEventCalled?.Invoke();
        }

        public int GetAvailablePoints()
        {
            return _mapEvents.Count(p => !p.IsBought && p.CanBuy());
        }

        private void SpawnClickableTrash()
        {
            var trash = _clickableEvents.Where(i => i is ClickableTrashView).RandomValue() as ClickableTrashView;
            if(trash) trash.Spawn();
            InvokeSystem.StartInvoke(SpawnClickableTrash, 15f);
        }

        public ClickableTrashView GetClickableTrash()
        {
            var trash = _clickableEvents.Where(i => i is ClickableTrashView && i.gameObject.activeSelf).RandomValue() as ClickableTrashView;
            return trash;
        }
    }
}
