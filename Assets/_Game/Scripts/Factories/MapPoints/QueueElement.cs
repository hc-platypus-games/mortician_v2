using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints;

namespace _Game.Scripts.Factories.MapPoints
{
    public class QueueElement
    {
        public delegate void QueueEvent(QueueElement element);

        private readonly QueueEvent _onAvailablePoint, _onComplete;
        private readonly CharacterViewBase _character;
        private readonly PointBaseView[] _points;

        public bool SameCharacter(CharacterViewBase characterView) => _character == characterView;
        public bool SameCharacter(QueueElement otherElement) => _character == otherElement._character;

        public QueueElement(CharacterViewBase character, QueueEvent onComplete, params PointBaseView[] targetPoints)
        {
            _character = character;
            _onComplete = onComplete;
            _points = targetPoints;
            foreach (var point in _points)
            {
                point.OnAvailable += OnFreePoint;
            }
        }

        public QueueElement(QueueEvent onAvailablePoint, QueueEvent onComplete, params PointBaseView[] targetPoints)
        {
            _onComplete = onComplete;
            _points = targetPoints;
            _onAvailablePoint = onAvailablePoint;
            foreach (var point in _points)
            {
                point.OnAvailable += OnCustomFreePoint;
            }
        }

        private void OnFreePoint()
        {
            if (_character.TryExitQueue())
            {
                foreach (var point in _points)
                {
                    point.OnAvailable -= OnFreePoint;
                }
                
                _onComplete?.Invoke(this);
            }
        }

        private void OnCustomFreePoint()
        {
            foreach (var point in _points)
            {
                point.OnAvailable -= OnCustomFreePoint;
            }
            _onComplete?.Invoke(this);
            _onAvailablePoint?.Invoke(this);
        }

        public void Destroy()
        {
            foreach (var point in _points)
            {
                point.OnAvailable -= OnFreePoint;
            }
        }
    }
}
