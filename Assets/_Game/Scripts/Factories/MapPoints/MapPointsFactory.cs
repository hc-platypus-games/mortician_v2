using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Core;
using _Game.Scripts.Enums;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using _Game.Scripts.View.Room;
using UnityEngine;
using Zenject;
using Random = _Game.Scripts.Tools.Random;

namespace _Game.Scripts.Factories.MapPoints
{
    public class MapPointsFactory
    {
        [Inject] private SceneData _sceneData;
        
        private readonly LevelSystem _levels;
        
        private List<PointBaseView> _all = new();
        private readonly List<GravePlacePointView> _gravePoints = new();
        private readonly List<QueueElement> _queueElements = new();

        public MapPointsFactory(LevelSystem levels)
        {
            _levels = levels;
            _levels.OnLoadedLevel += OnLevelLoaded;
        }
        
        private void OnLevelLoaded()
        {
            _all = _sceneData.GetComponentsInChildren<PointBaseView>(true).ToList();
            foreach (var point in _all)
            {
                point.Init();
            }
            
            var graves = GetAllPoints(Type.CorpsePlace, RoomType.Cemetery);
            foreach (var grave in graves)
            {
                _gravePoints.Add((GravePlacePointView) grave);
            }
        }

        public PointBaseView GetPoint(Type type, CharacterType characterType = CharacterType.Default, RoomType roomType = RoomType.None, bool takeSeat = false, params PointBaseView[] points)
        {
            if (points.Length == 0)
            {
                
                if (roomType == RoomType.None)
                {
                    points = _all.ToArray();
                } 
                else
                {
                    points = _all.Where(i => i.RoomType == roomType).ToArray();
                }
            }

            var point = points?.FirstOrDefault(i => i.Type == type && i.CharacterType.HasFlag(characterType) && i.IsAvailable);
            if (point && takeSeat) point.IsAvailable = false;
            return point;
        }

        public PointBaseView[] GetAllPoints(Type type, RoomType room = RoomType.None, bool availableOnly = false, params PointBaseView[] points)
        {
            if (points.Length == 0) points = _all.ToArray();
            if (room != RoomType.None)
            {
                points = _all.Where(i => i.RoomType == room).ToArray();
            }
            if(availableOnly) return points.Where(i => i.Type == type && i.IsAvailable).ToArray();
            return points.Where(i => i.Type == type).ToArray();
        }

        public PointBaseView[] GetAllPoints(Type type, CharacterType characterType, RoomType room = RoomType.None, bool availableOnly = false)
        {
            var points = _all.Where(i => i.CharacterType.HasFlag(characterType)).ToArray();
            return GetAllPoints(type, room, availableOnly, points);
        }

        public PointBaseView GetPoint(Type type, RoomType room, bool takeSeat = false, params PointBaseView[] points)
        {
            if (points.Length == 0) points = _all.ToArray();
            var point = points.FirstOrDefault(i => i.Type == type && i.RoomType == room && i.IsAvailable);
            if (point && takeSeat) point.IsAvailable = false;
            return point;
        }

        public PointBaseView GetRandomPoint(Type type, bool availableOnly = false, params PointBaseView[] points)
        {
            if (points.Length == 0) points = _all.Where(i => i.Type == type && i.IsAvailable).ToArray();
            if (points.Length == 0) return null;
            if (availableOnly) return points.Where(i => i.Type == type && i.IsAvailable).RandomValue();
            return points.Where(i => i.Type == type).RandomValue();
        }

        public PointBaseView GetRandomPoint(Type type, RoomType room, params PointBaseView[] points)
        {
            if (points.Length == 0) points = _all.Where(i => i.Type == type && i.RoomType == room && i.IsAvailable).ToArray();
            if (points.Length == 0) return null;
            return points[Random.Range(0, points.Length)];
        }

        public PointBaseView GetRoomPoint(CharacterViewBase character, RoomView room, bool takeSeat = true)
        {
            var point = GetPoint(Type.WorkPlace, character.Config.Type, RoomType.None, false, room.AllPoints);
            if (!point)
            {
                point = GetPoint(Type.Queue, character.Config.Type, RoomType.None, false, room.AllPoints);
                if(point && takeSeat) point.IsAvailable = false;
                character.CacheRoom(room);
                if (NeedQueue(character))
                {
                    AddToQueue(
                        new QueueElement(
                            character,
                            RemoveQueue,
                            GetAllPoints(Type.WorkPlace, character.Config.Type, room.Type)
                        )
                    );
                }
                else
                {
                    FreePoint(character.CharacterPoint);
                    character.SetTargetPoint(point);
                }
            }
            else
            {
                character.CacheRoom(room);
                if(takeSeat) point.IsAvailable = false;
            }
            return point;
        }

        private bool NeedQueue(CharacterViewBase owner)
        {
            foreach (var queueElement in _queueElements)
            {
                if (queueElement.SameCharacter(owner))
                {
                    return false;
                }
            }
            return true;
        }

        private bool AddToQueue(QueueElement element)
        {
            _queueElements.Add(element);
            return true;
        }

        private void RemoveQueue(QueueElement element)
        {
            _queueElements.Remove(element);
        }

        public void RemoveFromQueue(CharacterViewBase character)
        {
            for (var i = 0; i < _queueElements.Count; i++)
            {
                if (_queueElements[i].SameCharacter(character))
                {
                    _queueElements[i].Destroy();
                    _queueElements.RemoveAt(i);
                    i--;
                }
            }
        }

        public PointBaseView GetExitPoint(out PointBaseView secondPoint)
        {
            var points = GetAllPoints(Type.Exit, availableOnly: true);
            secondPoint = points.Length > 1 ? points[1] : null;
            points[0].IsAvailable = false;
            return points[0];
        }

        public void FreePoint(PointBaseView point)
        {
            if(point == null) return;
            point.IsAvailable = true;
        }

        public GravePlacePointView GetGravePlace(GravePlacePointView.State state, bool takeSeat, QueueElement.QueueEvent onAvailablePoint, List<GravePlacePointView> excludeGraves = null)
        {            
            var graves = excludeGraves == null ? _gravePoints : _gravePoints.Except(excludeGraves);
            var point = graves.OrderBy(i => i.GraveCares).FirstOrDefault(i => i.IsAvailableWhen(state));
             if (point == null && onAvailablePoint != null)
            {
                AddToQueue(
                    new QueueElement(
                        onAvailablePoint, 
                        RemoveQueue, 
                        GetAllPoints(Type.CorpsePlace, RoomType.Cemetery)
                    )
                );
                return null;
            }

            if(takeSeat && point) point.IsAvailable = false;
            return point;
        }

        public GravePlacePointView GetGravePlace(int id)
        {
            if (id >= _gravePoints.Count || id < 0) return null;
            return _gravePoints[id];
        }

        public Transform GetTutorialGrave()
        {
            return _gravePoints[0].transform;
        }

        public BaseRoomPointView GetOrderCorpsePlace(CharacterView character, RoomType roomType)
        {
            var points = GetAllPoints(Type.RoomCycle, roomType);
            var fullPoint = points.FirstOrDefault(i => ((CorpseOfferCyclePointView) i).IsFull);
            if (fullPoint)
            {
                return fullPoint as BaseRoomPointView;
            }
            var point = GetPoint(Type.RoomCycle, roomType, true, points);
            if (point == null)
            {
                AddToQueue(
                    new QueueElement(
                        character, 
                        RemoveQueue, 
                        GetAllPoints(Type.RoomCycle, roomType)
                    )
                );
                return null;
            }

            point.IsAvailable = false;
            return (BaseRoomPointView) point;
        }

        public Transform GetFocusPoint(RoomType roomType, bool tutorial = false)
        {
            PointBaseView[] points = null;
            if (tutorial)
            {
                points = GetAllPoints(Type.TutorialCameraFocus, roomType);
            }

            if (points == null || points.Length == 0)
            {
                points = GetAllPoints(Type.CameraFocus, roomType);
            }

            if (points == null || points.Length == 0) return null;
            return points[0].transform;
        }
        
        public PointBaseView GetStoragePoint(QueueElement.QueueEvent onAvailablePoint, RoomType roomType)
        {
            var point = GetPoint(Type.CorpsePlace, roomType);
            if (!point)
            {
                AddToQueue(
                    new QueueElement(
                        onAvailablePoint,
                        RemoveQueue,
                        GetAllPoints(Type.CorpsePlace, roomType)
                    )
                );
            }
            else
            {
                point.IsAvailable = false;
            }
            return point;
        }
    }
}
