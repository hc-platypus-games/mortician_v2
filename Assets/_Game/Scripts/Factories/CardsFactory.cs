﻿using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Save;
using _Game.Scripts.Systems.Tutorial;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Cards;
using Zenject;

namespace _Game.Scripts.Factories
{
    public class CardsFactory
    {
        private readonly CardUI.Factory _cardsFactory;
        private readonly List<CardUI> _cards = new();

        [Inject] private TutorialSystem _tutorial;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private SaveSystem _saveSystem;
        [Inject] private CalculationComponent _calculation;
        [Inject] private UIFactory _ui;

        public List<CardUI> All => _cards;

        public CardsFactory(CardUI.Factory cardsFactory)
        {
            _cardsFactory = cardsFactory;
        }
        
        public void Init()
        {
            var cards = _balance.DefaultBalance.Cards;
            foreach (var config in cards)
            {
                var card = _cardsFactory.Create();
                card.Init(config);
                card.UpdateEvent += OnUpdateCard;
                _cards.Add(card);
            }
        }

        public void AddCard(int id, int amount)
        {
            var card = _cards.FirstOrDefault(c => c.Id == id);
            if (card == null) return;
            card.IncreaseAmount(amount);
        }

        private void OnUpdateCard(CardUI card)
        {
            _saveSystem.Save();
            _ui.UpdateBadges(BadgeNotificationType.Cards);
        }
        
        public List<CardsWindow.RewardConfig> GetBoxItems(int id, int min, int max)
        {
            var items = new List<CardsWindow.RewardConfig>();
            var list = new List<int>();
            
            var regions = new List<int> { 1 };
            
            for (int i = 0; i < id; i++)
            {
                if (_tutorial.CurrentTutorialId == 100)
                {
                    items.Add(new CardsWindow.RewardConfig
                    {
                        Type = GameParamType.Card,
                        Amount = Random.Range(min, max),
                        RewardId = 3,
                        NewCard = IsNewCard(3)
                    });
                    break;
                }
                
                var randomItems = GetItems(id, list, regions);
                var randomCard = GetRandomCard(randomItems, id);
               
                var cardId = 0;
                if (randomCard < 0)
                {
                    cardId = id == 3 ? 1 : 5;
                    var amountCard1 = Random.Range(min, max);
                    var charCard = GetCard(cardId);
                    if (charCard.Level == 0) amountCard1 = 1;
                    
                    items.Add(new CardsWindow.RewardConfig
                    {
                        Type = GameParamType.Card,
                        Amount = amountCard1,
                        RewardId = cardId,
                        NewCard = IsNewCard(cardId)
                    });
                    
                    list.Add(cardId);
                    continue;
                }
            
                cardId = randomItems[randomCard].Id;
                items.Add(new CardsWindow.RewardConfig
                {
                    Type = GameParamType.Card,
                    Amount = Random.Range(min, max),
                    RewardId = cardId,
                    NewCard = IsNewCard(cardId)
                });
                
                list.Add(cardId);
            }

            var softRandom = Random.Range(_balance.DefaultBalance.SoftMinWeight, _balance.DefaultBalance.SoftMaxWeight);
            var type = softRandom < _balance.DefaultBalance.SoftWeight ? GameParamType.Soft : GameParamType.Tokens;
            var amount = type == GameParamType.Tokens ? Random.Range(id * 5, 40) : _calculation.GetIncomeByTime(id * _balance.DefaultBalance.CardSoftReward);
            if (type == GameParamType.Soft && amount <= 0)
            {
                amount = id * 150;
            }
            
            items.Add(new CardsWindow.RewardConfig
            {
                Type = type,
                Amount = amount
            });
            
            return items;
        }

        private bool IsNewCard(int cardId)
        {
            var card = GetCard(cardId);
            if (card == null) return true;
            return !card.IsExists;
        }

        public CardUI GetCard(int id)
        {
            return _cards.FirstOrDefault(data => data.Id == id);
        }
        
        public CardUI GetCardByRoomType(RoomType type)
        {
            return _cards.FirstOrDefault(data => data.Config.RoomType == type);
        }
        
        private List<CardConfig> GetItems(int id, List<int> list, List<int> regions)
        {
            var itemsArray = new List<CardConfig>();
            
            switch (id)
            {
                case 1:
                    itemsArray = _balance.DefaultBalance.Cards.Where(c =>
                        c.RareBoxWeight > 0 && 
                        regions.Contains(c.Region) && !list.Contains(c.Id)).ToList();
                    break;
                
                case 2:
                    itemsArray = _balance.DefaultBalance.Cards.Where(c =>
                        c.EpicBoxWeight > 0 &&
                        regions.Contains(c.Region) && !list.Contains(c.Id)).ToList();
                    break;
                
                case 3:
                    itemsArray = _balance.DefaultBalance.Cards.Where(c =>
                        c.LegendaryBoxWeight > 0 &&
                        regions.Contains(c.Region) && !list.Contains(c.Id)).ToList();
                    
                    break;
            }

            return itemsArray;
        }
        
        private int GetRandomCard(List<CardConfig> items, int id)
        {
            if (items.Count == 0) return -1;
            
            var weightSum = 0;
            var randomResult = 0;
            var min = 0;
            
            switch (id)
            {
                case 0:
                    return 3;
                
                case 1:
                    weightSum = items.Sum(i => i.RareBoxWeight);
                    min = items.Min(m => m.RareBoxWeight);
                    randomResult = Random.Range(min, weightSum);
                    break;
                
                case 2:
                    weightSum = items.Sum(i => i.EpicBoxWeight);
                    min = items.Min(m => m.EpicBoxWeight);
                    randomResult = Random.Range(min, weightSum);
                    break;
                    
                case 3:
                    weightSum = items.Sum(i => i.LegendaryBoxWeight);
                    min = items.Min(m => m.LegendaryBoxWeight);
                    randomResult = Random.Range(min, weightSum);
                    break;
            }
            
            var prevWeights = 0f;
            var itemIndex = 0;
            
            foreach (var item in items)
            {
                switch (id)
                {
                    case 1:
                        if (randomResult - prevWeights <= item.RareBoxWeight) return itemIndex;
                        prevWeights += item.RareBoxWeight;
                        break;
                
                    case 2:
                        if (randomResult - prevWeights <= item.EpicBoxWeight) return itemIndex;
                        prevWeights += item.EpicBoxWeight;
                        break;
                
                    case 3:
                        if (randomResult - prevWeights <= item.LegendaryBoxWeight) return itemIndex;
                        prevWeights += item.LegendaryBoxWeight;
                        break;
                }
                
                itemIndex++;
            }
        
            return itemIndex;
        }

        public int CanUpgradeCards()
        {
            return _cards.Count(c => c.CouldBeUpgraded());
        }
    }
}