using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.View.Room;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Factories
{
    public class RoomsFactory
    {
        public Action ItemUpgradeEvent;

        [Inject] private GameFlags _flags;
        
        private readonly DiContainer _container;
        private readonly GameBalanceConfigs _balance;
        private readonly LevelSystem _levels;

        private readonly List<RoomView> _rooms = new();
        private readonly List<RoomItemView> _roomItems = new();

        public RoomsFactory(DiContainer container, GameBalanceConfigs balance, LevelSystem levels)
        {
            _container = container;
            _balance = balance;
            _levels = levels;
            _levels.OnLoadedLevel += OnLevelLoaded;
            _levels.OnDestroyLevel += OnDestroyLevel;
        }

        private void OnLevelLoaded()
        {
             UpdateRooms();
             UpdateRoomItems();
        }

        private void OnDestroyLevel()
        {
            
        }

        private void UpdateRooms()
        {
            var rooms = _levels.CurrentLevel.GetComponentsInChildren<RoomView>();
            foreach (var room in rooms)
            {
                var config = _balance.DefaultBalance.Rooms.FirstOrDefault(r => r.RoomType == room.Type);
                if (config == null) continue;
                _rooms.Add(room);
                _container.BindInstance(room.Init(config));
            }
        }

        private void UpdateRoomItems()
        {
            var items = _levels.CurrentLevel.GetComponentsInChildren<RoomItemView>(true);
            foreach (var item in items)
            {
                var config = _balance.DefaultBalance.Items.FirstOrDefault(r => r.RoomType == item.RoomType 
                                                                                            && r.ItemId == item.Id);
                
                if (config == null)
                {
                    Debug.Log($"(0003) Balance config for item {item.Id} from room {item.RoomType} not found");
                    continue;
                }
                
                _container.BindInstance(item);
                
                item.Init(config);
                item.UpgradeEvent += OnUpgradeRoomItem;
                item.OnLevelLoaded();
                
                _roomItems.Add(item);
            }
        }

        private void OnUpgradeRoomItem(RoomItemView item)
        {
            ItemUpgradeEvent?.Invoke();
        }

        public List<RoomItemView> GetRoomItems(RoomType roomType)
        {
            return _roomItems.FindAll(r => r.RoomType == roomType);
        }

        public void Destroy()
        {
            _rooms.Clear();
            _roomItems.Clear();
        }

        public RoomItemView GetRoomItem(RoomType type, int id)
        {
            return _roomItems.FirstOrDefault(i => i.RoomType == type && i.Id == id);
        }

        public float GetIncome()
        {
            return _roomItems.Sum(i => i.RoomType != RoomType.Cemetery && i.Level > 0 ? i.GetParamValue(GameParamType.Income) : 0);
        }

        public int GetCapRoomExp(RoomView room)
        {
            return _roomItems
                .Where(i => i.RoomType == room.Type 
                            && i.Config.RequiredLevel == room.Level 
                            && !i.Config.BoughtForHard)
                .Sum(i => i.Config.Cap);
        }
        
        public List<RoomItemView> GetCurrentRoomItems(RoomView room)
        {
            return _roomItems
                .Where(i => i.RoomType == room.Type 
                            && i.Config.RequiredLevel == room.Level).ToList();
        }
        
        public int GetRoomExp(RoomView room)
        {
            return _roomItems
                .Where(i => i.RoomType == room.Type 
                            && i.Config.RequiredLevel >= room.Level 
                            && !i.Config.BoughtForHard)
                .Sum(i => i.Level);
        }
        
        public float GetParamCurrentValue(RoomType type, GameParamType paramType)
        {
            switch (paramType)
            {
                case GameParamType.CycleDelay:
                    
                    if (_flags.Has(GameFlag.TutorialFinished) == false)
                    {
                        return _balance.DefaultBalance.TutorialCycleDelay;
                    }
                    var roomCd = _rooms.FirstOrDefault(r => r.Type == type)?.Config.CycleDelay ?? 0;
                    var itemsCd = _roomItems.Where(i => i.RoomType == type && i.Level > 0)
                                                .Sum(i => i.GetParamValue(paramType));
                    return roomCd - itemsCd;
                
                case GameParamType.Seats:
                    return _roomItems.Where(i => i.RoomType == type && i.Level > 0)
                        .Sum(i => i.Config.Seats);
                
                default:
                    return _roomItems.Where(i => i.RoomType == type && i.Level > 0)
                        .Sum(i => i.GetParamValue(paramType));
            }
        }

        public List<RoomView> GetRooms()
        {
            return _rooms;
        }
        
        public List<RoomItemView> GetRoomItems()
        {
            return _roomItems;
        }
        
        public RoomView[] GetRooms(RoomCategory category)
        {
            return _rooms.Where(i => i.Category == category).ToArray();
        }

        public RoomView GetRoom(RoomType type)
        {
            return _rooms.FirstOrDefault(i => i.Type == type);
        }

        public float GetRoomItemValue(RoomType roomType, GameParamType paramType, int level)
        {
            return _roomItems.Where(r => r.RoomType == roomType)
                .Sum(i => i.GetParamLevelValue(paramType, false, level));
        }
    }
}