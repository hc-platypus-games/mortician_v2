using System.Collections.Generic;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.View;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.Vehicle;
using Zenject;

namespace _Game.Scripts.Factories
{
    public class VehicleFactory : ITickableSystem
    {
        [Inject] private RoomsFactory _rooms;
        [Inject] private DiContainer _container;
        [Inject] private GameFlags _flags;

        private LevelSystem _levelSystem;
        private bool _backgroundCarsInitialized;
        
        private readonly CarView.Pool _pool;
        private readonly List<VehicleViewBase> _cars = new();
        
        public VehicleFactory(CarView.Pool pool, LevelSystem levelSystem)
        {
            _pool = pool;
            _levelSystem = levelSystem;
            _levelSystem.OnLoadedLevel += OnLoadedLevel;
            _levelSystem.OnDestroyLevel += OnDestroyLevel;
        }

        private void OnLoadedLevel()
        {
            var cars = _levelSystem.CurrentLevel.GetComponentsInChildren<VehicleViewBase>(true);
            foreach (var transport in cars)
            {
                transport.Init();
                _cars.Add(transport);
            }

            if (_flags.Has(GameFlag.TutorialFinished))
            {
                InitBackgroundCars();
            }
            else
            {
                _flags.OnFlagSet += OnFlagSet;
            }
        }

        private void OnFlagSet(GameFlag flag)
        {
            if(flag != GameFlag.TutorialFinished) return;
            InitBackgroundCars();
        }

        private void InitBackgroundCars()
        {
            if(_backgroundCarsInitialized) return;
            _backgroundCarsInitialized = true;
            for (int i = 0; i < _levelSystem.CurrentLevel.BackgroundCars; i++)
            {
                InvokeSystem.StartInvoke(SpawnBackgroundCar, i * 2f);
            }
        }

        private void SpawnBackgroundCar()
        {
            var car = SpawnVehicle();
            car.AddBehaviour(new BackgroundCarBehaviour());
        }
        
        private void OnDestroyLevel()
        {
            
        }
        
        public void Tick(float deltaTime)
        {
            if(_cars.Count == 0) return;
            foreach (var car in _cars)
            {
                car.Tick(deltaTime);
            }
        }
        
        public CarView SpawnVehicle()
        {
            var car = _pool.Spawn();
            _cars.Add(car);
            car.Activate();
            return car;
        }

        public CarView FindCar(RoomType roomType, int itemId)
        {
            var item = _rooms.GetRoomItem(roomType, itemId);
            if (item == null) return null;
            return item.GetComponentInChildren<CarView>();
        }

        public void RemoveVehicle(CarView car)
        {
            _pool.Despawn(car);
            _cars.Remove(car);
        }
    }
}
