using _Game.Scripts.View.MapPoints;
using UnityEngine;

namespace _Game.Scripts.Factories.CorpseFactory
{
    public class CorpseObject
    {
        public int SkinId { get; private set; }
        public PointBaseView AttachedPoint { get; private set; }
        public bool IsGold { get; private set; }
        
        public CorpseObject Init(int id, bool gold = false)
        {
            IsGold = gold;
            SkinId = id;
            return this;
        }

        public void AttachTransform(PointBaseView target)
        {
            AttachedPoint = target;
        }
    }
}
