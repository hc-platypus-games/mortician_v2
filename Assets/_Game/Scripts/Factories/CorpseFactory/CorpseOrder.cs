﻿using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using UnityEngine;

namespace _Game.Scripts.Factories.CorpseFactory
{
    public enum CorpseOrderState
    {
        None,
        New,
        Preparing,
        InProgress,
        Completed
    }

    public class CorpseOrder
    {
        private readonly RoomType _roomType;
        private CorpseOrderState _state;
        private readonly List<Reward> _rewards;
        
        public GameProgress Progress { get; private set; }
        public string Text { get; private set; }
        public Sprite Avatar { get; private set; }
        public int AvatarId { get; private set; }
        
        public RoomType RoomType => _roomType;
        public CorpseOrderState State => _state;
        public List<Reward> Rewards => _rewards;

        public CorpseOrder(RoomType type, List<Reward> rewards, string text, int avatarId, Sprite avatar, CorpseOrderState state = CorpseOrderState.New)
        {
            _roomType = type;
            _state = state;
            _rewards = rewards;
            Text = text;
            Avatar = avatar;
            AvatarId = avatarId;
        }
        
        public void Start()
        {
            _state = CorpseOrderState.Preparing;
        }

        public void Complete()
        {
            _state = CorpseOrderState.Completed;
        }

        public bool IsHardRewardOrder()
        {
            return _rewards.Count(r => r.Type == GameParamType.Hard) > 0;
        }
        
        public bool IsTokensRewardOrder()
        {
            return _rewards.Count(r => r.Type == GameParamType.Tokens) > 0;
        }

        public string GetRewardText(int id)
        {
            if (id >= _rewards.Count)
            {
                return "";
            }

            var reward = _rewards[id];
            if (reward.Amount == 0) return "";
            
            switch (reward.Type)
            {
                case GameParamType.Soft:
                    return $"<sprite name=Soft>{reward.Amount}";

                case GameParamType.Hard:
                    return $"<sprite name=Hard>{reward.Amount}";
                
                case GameParamType.Tokens:
                    return $"<sprite name=Tokens>{reward.Amount}";
            }
            
            return "";
        }

        public void SetProgress(GameProgress progress)
        {
            Progress = progress;
            _state = CorpseOrderState.InProgress;
        }
    }
}