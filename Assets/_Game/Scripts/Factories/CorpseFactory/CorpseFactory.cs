using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Save.SaveStructures;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.View.Corpse;
using _Game.Scripts.View.MapPoints;
using UnityEngine;
using Zenject;
using Type = _Game.Scripts.View.MapPoints.Type;

namespace _Game.Scripts.Factories.CorpseFactory
{
    public class CorpseFactory : IGameProgress
    {
        public Action DeliveredCorpseAdded;
        public Action EmbalmedCorpseAdded;
        public Action CremationOrderAdded,
                      CryogenicOrderAdded;

        private readonly List<CorpseOrder> _orders = new();

        [Inject] private GameProgressFactory _progresses;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private CalculationComponent _calculation;
        [Inject] private MapPointsFactory _mapPoints;
        [Inject] private RoomsFactory _rooms;
        [Inject] private GameResources _resources;
        [Inject] private GameFlags _flags;
        [Inject] private UIFactory _uiFactory;
        
        private readonly CorpseView.Pool _pool;
        
        public List<CorpseObject> DeliveredCorpses { get; } = new();
        public List<CorpseObject> EmbalmedCorpses { get; } = new();
        public List<CorpseObject> CemeteryCorpses { get; } = new();
        public List<GameProgress> CemeteryProgresses { get; } = new();

        public List<CorpseOrder> SavableOrders => _orders;

        public GameProgress OrdersTimer { get; private set; }
        
        public CorpseFactory(CorpseView.Pool pool)
        {
            _pool = pool;
        }

        public void Init()
        {
            OrdersTimer = _progresses.CreateProgress(this, GameParamType.CorpseOrderTimer, _balance.DefaultBalance.CorpseOrderDelay);
            OrdersTimer.CompletedEvent += CreateOrders;

            _flags.OnFlagSet += OnSetFlag;
        }

        private void OnSetFlag(GameFlag flag)
        {
            switch (flag)
            {
                case GameFlag.CorpseOrdersOpened:
                    CreateOrders();
                    break;
            }
        }

        public void AddCorpseToDelivered(CorpseObject corpse)
        {
            DeliveredCorpses.Add(corpse);
            DeliveredCorpseAdded?.Invoke();
        }
        
        public void AddCorpseToEmbalmed(CorpseObject corpse)
        {
            EmbalmedCorpses.Add(corpse);
            EmbalmedCorpseAdded?.Invoke();
        }

        public CorpseObject GetCorpseFromDelivered()
        {
            var corpse = DeliveredCorpses.FirstOrDefault();
            if(corpse == null) return null;
            DeliveredCorpses.Remove(corpse);
            return corpse;
        }

        public CorpseObject GetCorpseFromEmbalmed()
        {
            var corpse = EmbalmedCorpses.FirstOrDefault();
            if(corpse == null) return null;
            EmbalmedCorpses.Remove(corpse);
            return corpse;
        }

        public CorpseOrder GetCorpseOrder(RoomType roomType)
        {
            return _orders.FirstOrDefault(i => i.RoomType == roomType && i.State is CorpseOrderState.InProgress or CorpseOrderState.Preparing);
        }

        private void CreateOrders()
        {
            var maxOrders = _balance.DefaultBalance.MaxCorpseOrder;
            if (maxOrders <= _orders.Count)
            {
                OrdersTimer.Pause();
                return;
            }

            var cremationRoom = _rooms.GetRoom(RoomType.Cremation).Level > 0;
            var cryogenicRoom = _rooms.GetRoom(RoomType.Cryogenic).Level > 0;
            
            var roomTypes = new List<RoomType>();
            if(cremationRoom) roomTypes.Add(RoomType.Cremation);
            if(cryogenicRoom) roomTypes.Add(RoomType.Cryogenic);
            
            if(roomTypes.Count == 0) return;

            var roomType = roomTypes.RandomValue();
            var rewardTypes = new List<GameParamType>
            {
                //GameParamType.Hard,
                GameParamType.Tokens,
                GameParamType.Soft
            };

            var hardOffers = _orders.Count(o => o.IsHardRewardOrder());
            if (hardOffers > 0) rewardTypes.Remove(GameParamType.Hard);

            var tokenOffers = _orders.Count(o => o.IsTokensRewardOrder());
            if (tokenOffers > 0) rewardTypes.Remove(GameParamType.Tokens);

            var type = rewardTypes.RandomValue();
            var rewards = new List<Reward>();
            
            switch (type)
            {
                case GameParamType.Soft:
                    rewards.Add(new Reward
                    {
                        Type = GameParamType.Soft,
                        Amount = GetAmount(GameParamType.Soft)
                    });
                    break;
                
                case GameParamType.Hard:
                    rewards.Add(new Reward
                    {
                        Type = GameParamType.Hard,
                        Amount = GetAmount(GameParamType.Hard)
                    });
                    break;
                
                case GameParamType.Tokens:
                    rewards.Add(new Reward
                    {
                        Type = GameParamType.Soft,
                        Amount = GetAmount(GameParamType.Soft)
                    });
                    rewards.Add(new Reward
                    {
                        Type = GameParamType.Tokens,
                        Amount = GetAmount(GameParamType.Tokens)
                    });
                    break;
            }

            if (rewards.Count == 0) return;

            var avatarId = _resources.GetCharacterIconId();
            var order = new CorpseOrder(roomType, rewards, GetText(), avatarId, _resources.GetCharacterIconSprite(avatarId));
            _orders.Add(order);
            _uiFactory.UpdateBadges(BadgeNotificationType.CorpseOrders);
            
            if (maxOrders <= _orders.Count)
            {
                OrdersTimer.Pause();
            }

            float GetAmount(GameParamType paramType)
            {
                switch (paramType)
                {
                    case GameParamType.Soft:
                        var value = _calculation.GetIncomeByTime(_balance.DefaultBalance.SoftRewardForCorpseOrder);
                        return value == 0 ? 1000 : value;
                    case GameParamType.Hard:
                        break;
                    case GameParamType.Tokens:
                        return _balance.DefaultBalance.TokensRewardForCorpseOrder;
                }

                return 0;
            }
            
            string GetText()
            {
                switch (roomType)
                {
                    case RoomType.Cremation:
                        return _resources.CremationsTexts.RandomValue();
                    
                    case RoomType.Cryogenic:
                        return _resources.CryogenicsTexts.RandomValue();
                    
                    default:
                        return "";
                }
            }
        }



        public List<CorpseOrder> GetNewCorpseOrders()
        {
            return _orders.FindAll(o => o.State == CorpseOrderState.New);
        }

        public List<CorpseOrder> GetActiveCorpseOrders()
        {
            return _orders.FindAll(o => o.State is CorpseOrderState.InProgress 
                or CorpseOrderState.Preparing 
                or CorpseOrderState.Completed);
        }

        public void RemoveCorpseOrder(CorpseOrder currentOrder)
        {
            _orders.Remove(currentOrder);
            OrdersTimer.Play();
            _uiFactory.UpdateBadges(BadgeNotificationType.CorpseOrders);
        }

        public void StartOrder(CorpseOrder currentOrder)
        {
            currentOrder.Start();
            
            switch (currentOrder.RoomType)
            {
                case RoomType.Cremation:
                    CremationOrderAdded?.Invoke();
                    break;
                
                case RoomType.Cryogenic:
                    CryogenicOrderAdded?.Invoke();
                    break;
            }
            _uiFactory.UpdateBadges(BadgeNotificationType.CorpseOrders);
        }

        public CorpseView SpawnCorpse(Transform parent)
        {
            var corpse = _pool.Spawn();
            corpse.Activate();
            Transform corpseTransform;
            (corpseTransform = corpse.transform).SetParent(parent);
            corpseTransform.localPosition = Vector3.zero;
            corpseTransform.localRotation = Quaternion.identity;
            corpseTransform.localScale = Vector3.one;
            return corpse;
        }

        public void RemoveCorpse(CorpseView corpseView)
        {
            corpseView.OnDestroy();
            corpseView.Deactivate();
            _pool.Despawn(corpseView);
        }

        public void LoadFromData(CorpseData item)
        {
            var corpse = new CorpseObject().Init(item.SKinId, item.IsGold);
            PointBaseView point;
            switch (item.State)
            {
                case CorpseState.Delivered:
                    point = _mapPoints.GetPoint(Type.CorpsePlace, RoomType.Garage, true);
                    if(!point) break;
                    DeliveredCorpses.Add(corpse);
                    SpawnCorpse(point.transform).ShowSkin(item.SKinId);
                    corpse.AttachTransform(point);
                    break;
                case CorpseState.Embalmed:
                    point = _mapPoints.GetPoint(Type.CorpsePlace, RoomType.Embalming, true);
                    if(!point) break;
                    EmbalmedCorpses.Add(corpse);
                    SpawnCorpse(point.transform).ShowSkin(item.SKinId);
                    corpse.AttachTransform(point);
                    break;
                case CorpseState.WaitInGrave:
                    point = _mapPoints.GetGravePlace(GravePlacePointView.State.Wait, false, null);
                    if(!point) break;
                    ((GravePlacePointView) point).SetCorpse(corpse);
                    corpse.AttachTransform(point);
                    break;
            }
        }

        public void LoadFromData(CorpseOrderData corpseOrder)
        {
            _orders.Add(new CorpseOrder(corpseOrder.RoomType, 
                corpseOrder.Rewards, 
                corpseOrder.Text, 
                corpseOrder.Sprite,
                _resources.GetCharacterIconSprite(corpseOrder.Sprite),
                corpseOrder.State));
        }

        public void LoadFromData(List<GameProgress> data)
        {
            for (var i = 0; i < data.Count; i++)
            {
                var progress = data[i];
                CemeteryProgresses[i].Change(progress.CurrentValue);
                if (progress.IsActive)
                {
                    _mapPoints.GetGravePlace(i).StartReset();
                }
            }
        }

        public int GetAvailableOrders()
        {
            return _orders.Count(o => o.State is CorpseOrderState.New or CorpseOrderState.Completed);
        }
    }
}
