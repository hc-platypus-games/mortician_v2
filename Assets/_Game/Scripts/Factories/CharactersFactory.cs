using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Save.SaveStructures;
using _Game.Scripts.Tools;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.Vehicle;
using UnityEngine;
using UnityEngine.AI;
using Zenject;
using Type = _Game.Scripts.View.MapPoints.Type;

namespace _Game.Scripts.Factories
{
    public class CharactersFactory : ITickableSystem
    {
        [Inject] private DiContainer _container;
        [Inject] private MapPoints.MapPointsFactory _mapPointsFactory;
        
        private readonly LevelSystem _levelSystem;
        
        private readonly CharacterView.Pool _pool;
        private readonly List<CharacterView> _chars = new();
        
        public List<CharacterData> Data { get; private set; } = new();
        public List<CharacterView> Characters => _chars;
        
        public CharactersFactory(CharacterView.Pool pool, LevelSystem levelSystem)
        {
            _pool = pool;
            _levelSystem = levelSystem;
            _levelSystem.OnLoadedLevel += OnLevelLoaded;
            _levelSystem.OnDestroyLevel += OnDestroyLevel;
        }

        private void OnLevelLoaded()
        {
            var porterPoints = _mapPointsFactory.GetAllPoints(Type.CorpsePorterIdle);
            var porterTransport = _levelSystem.CurrentLevel.GetComponentInChildren<CorpsePorterTransport>(true);
            if(porterTransport == null) return;
            for (int i = 0; i < porterPoints.Length; i++)
            {
                SpawnCharacter().Init(new CorpsePorterBehaviour(porterTransport, i));
            }

            InitBackgroundChars();
        }
        
        private void InitBackgroundChars()
        {
            for (int i = 0; i < _levelSystem.CurrentLevel.BackgroundCharacters; i++)
            {
                InvokeSystem.StartInvoke(SpawnBackgroundCharacter, i * 0.5f);
            }
        }
        
        private void OnDestroyLevel()
        {
            
        }

        public void LoadFromSave(List<CharacterData> data)
        {
            foreach (var item in data)
            {
                Data.Add(item);
            }
            InvokeSystem.StartInvoke(LoadFromData, 2f);
        }

        private void LoadFromData()
        {
            if (Data.Count == 0) return;
            var data = Data[0];
            Data.RemoveAt(0);
            SpawnWorker(data.RoomType, data.Type);
            InvokeSystem.StartInvoke(LoadFromData, 0.4f);
        }

        public CharacterView SpawnCharacter(CharacterConfig config = null)
        {
            config ??= new CharacterConfig();
            var character = _pool.Spawn();
            character.transform.position = Vector3.zero;
            character.GetComponent<NavMeshAgent>().agentTypeID = 0;
            character.SetConfig(config);
            _chars.Add(character);
            return character;
        }

        public CharacterView SpawnCharacter(CardConfig card)
        {
            var existCharacter = GetCharacter(card);
            if (existCharacter) return null;
            var character = SpawnCharacter();
            character.Config.Card = card;
            var behaviour = new CardCharacterBehaviour(card.RoomType);
            character.Init(behaviour);
            
            switch (card.RoomType)
            {
                case RoomType.Director:
                    character.ShowSkin(18);
                    break;
                case RoomType.Hall:
                    character.ShowSkin(19);
                    break;
                case RoomType.Garage:
                    character.ShowSkin(20);
                    break;
                case RoomType.Embalming:
                    character.ShowSkin(21);
                    break;
                case RoomType.FoodService:
                    character.ShowSkin(22);
                    break;
                case RoomType.Farewell:
                    character.ShowSkin(23);
                    break;
            }
            return character;
        }
        
        public CharacterView SpawnCorpseOrderVisitor()
        {
            var character = SpawnCharacter();
            var behaviour = new CorpseOrderVisitorBehaviour();
            character.Init(behaviour);
            _chars.Add(character);
            return character;
        }
        
        private void SpawnBackgroundCharacter()
        {
            var character = SpawnCharacter();
            character.Init(new BackgroundCharacterBehaviour());
        }

        public CharacterView InitCharacter(CharacterView character, EntityBehaviourBase behaviour)
        {
            _container.Inject(character);
            character.Init(behaviour);
            _chars.Add(character);
            return character;
        }

        public void RemoveCharacter(CharacterView character)
        {
            character.End();
            _pool.Despawn(character);
            _chars.Remove(character);
        }

        public void RemoveCharacter(CardConfig cardConfig)
        {
            var character = _chars.FirstOrDefault(i => i.Config.Card.Id == cardConfig.Id);
            if(!character) return;
            RemoveCharacter(character);
        }

        public void Tick(float deltaTime)
        {
            if(_chars.Count == 0) return;
            foreach (var character in _chars)
            {
                character.Tick(deltaTime);
            }
        }

        public CharacterView SpawnWorker(RoomType roomType, CharacterType type = CharacterType.Default)
        {
            var character = SpawnCharacter();
            character.Config.Savable = true;
            character.Config.Type = type;
            character.Init( new WorkerBehaviour(roomType));
            switch (roomType)
            {
                case RoomType.Hall:
                    character.ShowSkin(1);
                    break;
                case RoomType.Garage:
                    character.ShowSkin(2);
                    break;
                case RoomType.Embalming:
                    character.ShowSkin(3);
                    break;
                case RoomType.Farewell:
                    character.ShowSkin(4);
                    break;
                case RoomType.Cemetery:
                    if (type == CharacterType.Default)
                    {
                        character.ShowSkin(5);
                    }
                    else
                    {
                        character.ShowSkin(6);
                    }
                    break;
                case RoomType.Cremation:
                    character.ShowSkin(7);
                    break;
                case RoomType.Cryogenic:
                    character.ShowSkin(8);
                    break;
                case RoomType.FuneralVisitorParking:
                    break;
                case RoomType.Cleaner:
                    character.ShowSkin(26);
                    break;
            }
            return character;
        }

        public CharacterView GetCharacter(RoomType room)
        {
            return _chars.FirstOrDefault(i => i.Config.Room == room);
        }

        public CharacterView GetCharacter(CardConfig card)
        {
            return _chars.FirstOrDefault(i => i.Config.Card != null && i.Config.Card.Id == card.Id);
        }

        public CharacterView GetCharacter(CharacterType type)
        {
            return _chars.FirstOrDefault(i => i.Config.Type == type);
        }

        public int GetCharactersCount(RoomType room)
        {
            return _chars.Count(i => i.Config.Room == room);
        }

        public int GetCharactersCount(CharacterType type)
        {
            return _chars.Count(i => i.Config.Type == type);
        }

        public int GetCharactersCount(RoomType room, CharacterType type)
        {
            return _chars.Count(i => i.Config.Type == type && i.Config.Room == room);
        }

        public int GetAngryCharactersCount()
        {
            return _chars.Count(c => c.CurrentEmotion
                is UIFactory.EmotionType.Angry
                or UIFactory.EmotionType.WaitCorpse
                or UIFactory.EmotionType.WaitStorage
                or UIFactory.EmotionType.WaitTrolley);
        }

        public CharacterViewBase GetAngryCharacter()
        {
            return _chars.Where(c => c.CurrentEmotion
                is UIFactory.EmotionType.Angry
                or UIFactory.EmotionType.WaitCorpse
                or UIFactory.EmotionType.WaitStorage
                or UIFactory.EmotionType.WaitTrolley).RandomValue();
        }
    }
}
