using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Boosts;
using Zenject;

namespace _Game.Scripts.Factories
{
    /// <summary>
    /// Класс хранения параметров объектов
    /// </summary>
    public class GameParamFactory
    {
        [Inject] private BoostSystem _boosts;
        
        private readonly GameParam.Pool _paramsPool;
        private readonly List<GameParam> _params = new();
        private readonly List<GameParam> _savedParams = new();

        public GameParamFactory(GameParam.Pool paramsPool)
        {
            _paramsPool = paramsPool;
        }

        public GameParam CreateParam(IGameParam owner, GameParamType type, float value, bool saved = false)
        {
            var param = saved 
                ? _savedParams.Find(p => p.Owner == owner && p.Type == type)
                : _params.Find(p => p.Owner == owner && p.Type == type);
            
            if (param != null) return param;
            param = _paramsPool.Spawn(owner, type, value);

            if (saved)
            {
                _savedParams.Add(param);
            }
            else
            {
                _params.Add(param);
            }
            
            return param;
        }

        public void RemoveParam(GameParam param)
        {
            _paramsPool.Despawn(param);
            _params.Remove(param);
            _params.Remove(param);
        }
        
        public GameParam GetParam(IGameParam owner, GameParamType type)
        {
            var savedParam = _savedParams.FirstOrDefault(p => p.Owner == owner && p.Type == type);
            if (savedParam != null) return savedParam;
            var param = _params.FirstOrDefault(p => p.Owner == owner && p.Type == type);
            return param;
        }
        
        public GameParam GetParam<T>(GameParamType type)
        {
            var savedParam = _savedParams.FirstOrDefault(p => p.Owner is T && p.Type == type);
            if (savedParam != null) return savedParam;
            var param = _params.FirstOrDefault(p => p.Owner is T && p.Type == type);
            return param;
        }

        public float GetParamValue(IGameParam owner, GameParamType type)
        {
            var savedParam = _savedParams.FirstOrDefault(p => p.Owner == owner && p.Type == type)?.Value;
            if (savedParam != null) return savedParam.Value * BoostValue(type, owner);
            var param = _params.FirstOrDefault(p => p.Owner == owner && p.Type == type)?.Value ?? 0;
            return param * BoostValue(type, owner);
        }

        private float BoostValue(GameParamType type, IGameParam owner)
        {
            var value = _boosts.GetBoostValue(type, owner) * _boosts.GetBoostValue(type);
            return value == 0 ? 1 : value;
        }

        public void UpdateParamValue(IGameParam owner, GameParamType type, float value)
        {
            GetParam(owner, type)?.SetValue(value);
        }
        
        public float GetParamValue<T>(GameParamType type)
        {
            var savedParam = _savedParams.FirstOrDefault(p => p.Owner is T && p.Type == type)?.Value;
            if (savedParam != null) return savedParam.Value;
            var param = _params.FirstOrDefault(p => p.Owner is T && p.Type == type)?.Value ?? 0;
            return param;
        }
        
        public List<GameParam> GetSavedParams<T>()
        {
            return _savedParams.FindAll(p => p.Owner is T);
        }
    }
}