﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Offers;
using ModestTree;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Factories.Offers
{
    public class OffersFactory : ITickableSystem
    {
        [Inject] private DiContainer _container;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private WindowsSystem _windows;
        [Inject] private CharactersFactory _characters;
        [Inject] private GameFlags _flags;
        [Inject] private BoostSystem _boosts;

        private readonly MainWindowOffer.Pool _mainWindowItemsPool;
        private readonly List<MainWindowOffer> _mainWindowItems = new();
        private readonly List<AdPlacement> _playingOffers = new();

        private readonly List<BaseOffer> _storeOffers = new();
        private readonly List<RandomItem> _offerWeights = new();

        private Transform _offersContainer;
        
        public OffersFactory(MainWindowOffer.Pool mainWindowItemsPool)
        {
            _mainWindowItemsPool = mainWindowItemsPool;
        }
        
        public void Init()
        {
            var configs = _balance.DefaultBalance.Offers;
            var storeWindow = _windows.GetWindow<StoreWindow>();
            if (storeWindow == null) return;
            var offersUI = storeWindow.GetComponentsInChildren<BaseOffer>(true);
            if (configs.Count == 0 || offersUI.Length == 0) return;
            
            foreach (var offerUI in offersUI)
            {
                var config = configs.FirstOrDefault(c => c.Id == offerUI.Id);
                if (config == null) continue;
                offerUI.Init(config);
                
                _storeOffers.Add(offerUI);
                _container.BindInstance(offersUI);
            }

            _offersContainer = ((MainWindow)_windows.GetWindow<MainWindow>()).AdOffersContainer;

            var loading = _container.Resolve<EndLoadingComponent>();
            loading.OnEnd += OnLoadedLevel;
        }

        private void OnLoadedLevel()
        {
            if(_flags.Has(GameFlag.ManagerIsBought)) SpawnDirectorCharacter();
        }
        
        public void Tick(float deltaTime)
        {
            _storeOffers[0].Tick(deltaTime);
        }

        public StoreOffer GetOfferById(int id)
        {
            return _storeOffers.FirstOrDefault(o => o.Id == id) as StoreOffer;
        }

        public BaseOffer[] GetSavedOffers()
        {
            return _storeOffers.Where(o => o.Saved).ToArray();
        }

        public void StartMainWindowOffers()
        {
            if(_offerWeights.Count > 0) return;
            
            _offerWeights.Add(new RandomItem {Type = AdPlacement.ItemsUpgradeDiscount, RandomWeight = 10});
            _offerWeights.Add(new RandomItem {Type = AdPlacement.FastCash, RandomWeight = 50});
            if (!_flags.Has(GameFlag.FirstHardReceived))
            {
                _offerWeights.Add(new RandomItem {Type = AdPlacement.FirstHard, RandomWeight = 200});
            }

            foreach (var item in _offerWeights)
            {
                var placement = (AdPlacement) item.Type;
                var boost = _boosts.GetAdBoost(placement);
                if (boost == null) continue;
                if (!boost.IsActive) continue;
                
                var offer = _mainWindowItemsPool.Spawn(_offersContainer, placement);
                offer.OnOfferHided += DespawnMainWindowOffer;
                offer.OnOfferClicked += OnOfferClicked;
                _mainWindowItems.Add(offer);
                
                return;
            }
            
            InvokeSystem.CancelInvoke(PlayMainWindowOffer, true);
            InvokeSystem.StartInvoke(PlayMainWindowOffer, 1f);
        }

        private void OnOfferClicked(MainWindowOffer offer)
        {
            if(offer.Placement != AdPlacement.ItemsUpgradeDiscount) return;
            InvokeSystem.CancelInvoke(PlayMainWindowOffer, true);
            InvokeSystem.StartInvoke(PlayMainWindowOffer, (float)_balance.DefaultBalance.DelayBetweenOffers);
            if (!_playingOffers.Contains(offer.Placement)) _playingOffers.Add(offer.Placement);
        }

        private void PlayMainWindowOffer()
        {
            InvokeSystem.CancelInvoke(PlayMainWindowOffer, true);
            if (_flags.Has(GameFlag.TutorialFinished) == false)
            {
                InvokeSystem.StartInvoke(PlayMainWindowOffer, (float)_balance.DefaultBalance.DelayBetweenOffers);
                return;
            }
            
            var item = _offerWeights.RandomValue()?.Type;
            if (item == null) return;

            var placement = (AdPlacement) item;
            if (_playingOffers.Contains(placement))
            {
                InvokeSystem.StartInvoke(PlayMainWindowOffer, (float)_balance.DefaultBalance.DelayBetweenOffers);
                return;
            }
            var offer = _mainWindowItemsPool.Spawn(_offersContainer, placement);
            offer.OnOfferHided += DespawnMainWindowOffer;
            offer.OnOfferClicked += OnOfferClicked;
            _mainWindowItems.Add(offer);
            if(!_playingOffers.Contains(offer.Placement)) _playingOffers.Add(offer.Placement);
        }

        public void RemoveHardMainOffer()
        {
            var item = _offerWeights.FirstOrDefault(i => Equals(i.Type, AdPlacement.FirstHard));
            if (item != null) _offerWeights.Remove(item);
            _flags.Set(GameFlag.FirstHardReceived);
        }

        private void DespawnMainWindowOffer(MainWindowOffer offer)
        {
            _playingOffers.Remove(offer.Placement);
            offer.OnOfferHided -= DespawnMainWindowOffer;
            _mainWindowItemsPool.Despawn(offer);
            _mainWindowItems.Remove(offer);
            
            InvokeSystem.CancelInvoke(PlayMainWindowOffer, true);
            InvokeSystem.StartInvoke(PlayMainWindowOffer,
                _flags.Has(GameFlag.AllAdsRemoved)
                    ? (float)_balance.DefaultBalance.DelayBetweenOffersWithoutAds
                    : (float)_balance.DefaultBalance.DelayBetweenOffers);
        }

        public void SpawnDirectorCharacter()
        {
            _characters.SpawnCharacter(new CardConfig()
            {
                RoomType = RoomType.Director,
                Id = 0
            });
        }
    }
}
