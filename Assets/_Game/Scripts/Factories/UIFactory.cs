using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Core;
using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Room;
using _Game.Scripts.Ui.WorldSpace;
using _Game.Scripts.UI.WorldSpace;
using _Game.Scripts.View.Fx;
using _Game.Scripts.View.Room;
using UnityEngine;
using Zenject;
using Random = _Game.Scripts.Tools.Random;

namespace _Game.Scripts.Factories
{
    public class UIFactory
    {
        public enum EmotionType
        {
            Neutral,
            Sad,
            Angry,
            TouchReaction,
            WaitCorpse,
            WaitTrolley,
            WaitStorage,
        }
        
        [Inject] private DiContainer _container;
        [Inject] private SceneData _sceneData;
        [Inject] private WindowsSystem _windows;
        [Inject] private GameResources _resources;
        [Inject] private GameParamFactory _params;
        [Inject] private GameFlags _flags;
        
        private readonly RoomInfoItem.Pool _roomItemsPool;
        private readonly List<RoomInfoItem> _roomItems = new();
        
        private readonly MessageUI.Pool _messagePool;
        private readonly List<MessageUI> _messages = new();
        
        private readonly ResourceBubbleUI.Pool _resourceBubblesPool;
        private readonly List<ResourceBubbleUI> _resourceBubbles = new();
        
        private readonly Bubble.Pool _bubblePool;
        private readonly List<Bubble> _bubbles = new();
        
        private readonly ProgressBarView.Pool _progressBarsPool;
        private readonly List<ProgressBarView> _progressBars = new();
        
        private readonly TextFxView.Pool _textsPool;
        private readonly List<TextFxView> _texts = new();
        
        private readonly PoofFx.Pool _poofFxPool;
        private readonly List<PoofFx> _poofFxes = new();

        private BadgeNotification[] _badges;

        public UIFactory(RoomInfoItem.Pool itemPool, 
            Bubble.Pool bubblePool, 
            ProgressBarView.Pool progressBarsPool,
            MessageUI.Pool messagePool,
            ResourceBubbleUI.Pool resourceBubblesPool,
            TextFxView.Pool textsPool,
            PoofFx.Pool poofFxPool)
        {
            _roomItemsPool = itemPool;
            _bubblePool = bubblePool;
            _progressBarsPool = progressBarsPool;
            _messagePool = messagePool;
            _resourceBubblesPool = resourceBubblesPool;
            _textsPool = textsPool;
            _poofFxPool = poofFxPool;
        }

        public void Init()
        {
            _badges = _sceneData.UI.GetComponentsInChildren<BadgeNotification>(true);
            foreach (var badge in _badges)
            {
                _container.BindInstance(badge);
            }

            _flags.OnFlagSet += OnFlagSet;

            _params.GetParam<GameSystem>(GameParamType.Tokens).UpdatedEvent += UpdateCharacters;
        }

        private void OnFlagSet(GameFlag flag)
        {
            if (flag == GameFlag.TutorialFinished) UpdateBadges();
        }

        private void UpdateCharacters()
        {
            UpdateBadges(BadgeNotificationType.Cards);
        }

        public RoomInfoItem RoomItemSpawn(RectTransform parent, RoomItemView roomItem)
        {
            var item = _roomItemsPool.Spawn(parent, roomItem);
            _roomItems.Add(item);
            return item;
        }

        public void RemoveItem(RoomInfoItem item)
        {
            _roomItemsPool.Despawn(item);
            _roomItems.Remove(item);
        }

        public Bubble SpawnBubble()
        {
            var bubble = _bubblePool.Spawn();
            _bubbles.Add(bubble);
            bubble.SetParent(_sceneData.WorldSpaceCanvas).Init();
            return bubble;
        }

        public Bubble SpawnEmotion(EmotionType type, RoomType _targetRoomType = RoomType.None)
        {
            var bubble = SpawnBubble();
            Sprite sprite = _resources.GetSprite("");
            switch (type)
            {
                case EmotionType.Angry:
                    sprite = _resources.AngryEmotions.RandomValue();
                    if (Random.Range(0, 3) > 0)
                    {
                        switch (_targetRoomType)
                        {
                            case RoomType.ToiletService:
                                sprite = _resources.ToiletNeed.RandomValue();
                                break;
                            case RoomType.FoodService:
                                sprite = _resources.KitchenNeed.RandomValue();
                                break;
                            case RoomType.RestService:
                                sprite = _resources.RestNeed.RandomValue();
                                break;
                            case RoomType.Garage:
                                sprite = _resources.GetRoomIconSprite("PHONE");
                                bubble.Show(sprite);
                                return bubble;
                            case RoomType.Farewell:
                                sprite = _resources.GetSprite("zzz");
                                bubble.Show(sprite);
                                return bubble;
                        }
                    }
                    bubble.Show(sprite, false);
                    break;
                case EmotionType.Sad:
                    sprite = _resources.SadEmotions.RandomValue();
                    bubble.Show(sprite, false);
                    break;
                case EmotionType.Neutral:
                    sprite = _resources.NeutralEmotions.RandomValue();
                    bubble.Show(sprite, false);
                    break;
                case EmotionType.TouchReaction:
                    var textsLength = _resources.TouchReactionTexts.Length;
                    if (textsLength > 0)
                    {
                        bubble.ShowText( _resources.TouchReactionTexts.RandomValue().ToLocalized());
                    }
                    break;
                case EmotionType.WaitCorpse:
                    sprite = _resources.GetSprite("zzz");
                    bubble.Show(sprite);
                    break;
                case EmotionType.WaitStorage:
                    sprite = _resources.GetRoomIconSprite("BODY BOX");
                    bubble.Show(sprite);
                    break;
                case EmotionType.WaitTrolley:
                    sprite = _resources.GetRoomIconSprite("GURNEY");
                    bubble.Show(sprite);
                    break;
            }

            return bubble;
        }

        public void RemoveBubble(Bubble bubble)
        {
            bubble.Deactivate();
            _bubblePool.Despawn(bubble);
            _bubbles.Remove(bubble);
        }

        public ProgressBarView SpawnProgressBar()
        {
            var bubble = _progressBarsPool.Spawn();
            _progressBars.Add(bubble);
            bubble.SetParent(_sceneData.WorldSpaceCanvas).Init();
            return bubble;
        }

        public PoofFx SpawnPoofFx(Vector3 position)
        {
            var poof = _poofFxPool.Spawn();
            _poofFxes.Add(poof);
            poof.SetParent(null);
            poof.transform.position = position;
            InvokeSystem.StartInvoke(RemovePoofFx, 4f);
            return poof;
        }

        private void RemovePoofFx()
        {
            if(_poofFxes.Count == 0) return;
            var poof = _poofFxes[0];
            _poofFxPool.Despawn(poof);
            _poofFxes.Remove(poof);
        }

        public void SpawnTextFx(string text, Vector3 position, RectTransform parent = null)
        {
            var textFx = _textsPool.Spawn();
            _texts.Add(textFx);
            if (parent)
            {
                textFx.SetParent(parent).Show(text);
                textFx.RectTransform.anchoredPosition = position;
                textFx.transform.localScale = Vector3.one;
                textFx.transform.localRotation = Quaternion.identity;
            }
            else
            {
                textFx.SetParent(_sceneData.WorldSpaceCanvas).Show(text);
                textFx.transform.position = position;
                textFx.transform.localScale = Vector3.one * 0.5f;
                textFx.transform.localRotation = Quaternion.Euler(30f, -135f, 0f);
            }
            InvokeSystem.StartInvoke(RemoveTextFx, 2f);
        }
        
        private void RemoveTextFx()
        {
            var text = _texts[0];
            _textsPool.Despawn(text);
            _texts.Remove(text);
        }

        public void RemoveProgressBar(ProgressBarView bubble)
        {
            _progressBarsPool.Despawn(bubble);
            _progressBars.Remove(bubble);
        }

        public void SpawnMessage(string text)
        {
            var i = _messages.Count;
            foreach (var message in _messages)
            {
                message.Move(120 * (i + 1));
                i--;
            }
            
            var newMessage = _messagePool.Spawn(_sceneData.UI, text);
            newMessage.OnFinished += RemoveMessage;
            _messages.Add(newMessage);
        }

        private void RemoveMessage(MessageUI messageUI)
        {
            messageUI.OnFinished -= RemoveMessage;
            _messagePool.Despawn(messageUI);
            _messages.Remove(messageUI);
        }

        public void SpawnResourceBubble(GameParamType gameParam, float count, Vector3 pos = default)
        {
            GamePlayElement type;
            switch (gameParam)
            {
                case GameParamType.Soft:
                    type = GamePlayElement.Soft;
                    break;
                
                case GameParamType.Hard:
                    type = GamePlayElement.Hard;
                    break;
                
                case GameParamType.AdTickets:
                    type = GamePlayElement.AdTickets;
                    break;
                
                default:
                    return;
            }
            
            if (count == 0) return;
            
            var setup = _sceneData.ResourceBubbleSetups.Find(s => s.Type == type);
            if (setup == null) return;

            var startPos = pos == default ? Input.mousePosition : pos;
            var targetPos = _windows.GetGamePlayElement(type)?.GetComponent<RectTransform>().position?? Vector3.zero;
            if (targetPos == Vector3.zero) return;
            
            count = Mathf.Clamp((int) (count / setup.Conversion), 1, setup.Max);
            
            var isFirst = true;
            var delay = 0f;
            const float animDelay = 0.01f;

            while (count-- > 0)
            {
                var bubble = _resourceBubblesPool.Spawn(_sceneData.UI, startPos, targetPos, setup.Icon, delay);
                bubble.OnFinished += RemoveResourceBubble;
                _resourceBubbles.Add(bubble);
                
                delay += animDelay;

                if (!isFirst) continue;
                isFirst = false;
            }
        }

        private void RemoveResourceBubble(ResourceBubbleUI bubble)
        {
            bubble.OnFinished -= RemoveResourceBubble;
            _resourceBubblesPool.Despawn(bubble);
            _resourceBubbles.Remove(bubble);
        }

        public void UpdateBadges(BadgeNotificationType type = BadgeNotificationType.None)
        {
            if(_badges == null) return;
            
            if (type == BadgeNotificationType.None)
            {
                foreach (var badge in _badges)
                {
                    badge.Redraw();
                }
                return;
            }

            var element = _badges.FirstOrDefault(b => b.Type == type);
            element?.Redraw();
        }
    }
}