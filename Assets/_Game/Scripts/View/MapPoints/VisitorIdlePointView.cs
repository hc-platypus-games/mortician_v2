using _Game.Scripts.Factories;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.View.EntityBehaviour;
using Zenject;

namespace _Game.Scripts.View.MapPoints
{
    public class VisitorIdlePointView : PointBaseView
    {
        [Inject] private CharactersFactory _charactersFactory;
        
        public override void Init()
        {
            var character = _charactersFactory.SpawnCharacter();
            character.Init(new FuneralVisitorBehaviour(this));
            base.Init();
        }
    }
}
