using _Game.Scripts.View.EntityBehaviour;

namespace _Game.Scripts.View.MapPoints.PointsWithLogic
{
    public class EmbalmingTablePointView : BaseRoomPointView
    {
        public override EntityBehaviourBase GetPointLogic()
        {
            var behaviour = new EmbalmingLogic(this);
            Container.Inject(behaviour);
            return behaviour;
        }
    }
}
