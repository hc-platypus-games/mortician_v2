using System;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.View.EntityBehaviour;

namespace _Game.Scripts.View.MapPoints.PointsWithLogic
{
    public class FarewellWorkPointView : BaseRoomPointView
    {
        public Action CorpseAdded;
        
        private CorpseObject _corpseObject;

        public override bool IsAvailable => base.IsAvailable && _corpseObject == null;

        public override EntityBehaviourBase GetPointLogic()
        {
            return new FarewellRoomLogic(this);
        }

        public void AddCorpse(CorpseObject corpse)
        {
            _corpseObject = corpse;
            CorpseAdded?.Invoke();
        }

        public CorpseObject GetCorpse()
        {
            return _corpseObject;
        }

        public void ClearCorpse()
        {
            _corpseObject = null;
            OnAvailable?.Invoke();
        }
    }
}
