using _Game.Scripts.Factories;
using _Game.Scripts.View.EntityBehaviour;
using Zenject;

namespace _Game.Scripts.View.MapPoints.PointsWithLogic
{
    public class DriverWorkPointView : BaseRoomPointView
    {
        [Inject] private LevelEventsFactory _levelEvents;
        
        public override bool IsAvailable => base.IsAvailable && _levelEvents.EventsCalledQueue.Count > 0;
        
        public override void Init()
        {
            base.Init();
            _levelEvents.OnEventCalled += OnEventCalled;
        }

        private void OnEventCalled()
        {
            OnAvailable?.Invoke();
        }

        public override EntityBehaviourBase GetPointLogic()
        {
            return new FuneralDriverLogic(this);
        }
    }
}
