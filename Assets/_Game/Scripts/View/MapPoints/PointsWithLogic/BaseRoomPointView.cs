using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.UI.WorldSpace;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.Room;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.MapPoints.PointsWithLogic
{
    public class BaseRoomPointView : PointBaseView, IGameProgress
    {
        [Inject] protected RoomsFactory Rooms;
        [Inject] protected GameProgressFactory Progresses;
        [Inject] protected GameParamFactory Params;
        [Inject] private UIFactory _uiFactory;
        
        private ProgressBarView _progressBar;

        public RoomItemView Item { get; protected set; }
        public override bool IsAvailable => base.IsAvailable && Item?.Level > 0;
        public GameProgress Cycle { get; protected set; }

        public BaseRoomPointView()
        {
            type = Type.WorkPlace;
        }

        public override void Init()
        {
            Item ??= GetComponentInParent<RoomItemView>(true);
            if(!Item) return;
            Item.UpgradeEvent += OnItemUpgrade;
            Cycle = Progresses.CreateProgress(this, GameParamType.CycleDelay, Rooms.GetParamCurrentValue(roomType, GameParamType.CycleDelay));
            Cycle.Pause();
            Cycle.UpdatedEvent += UpdateProgressBars;
            Cycle.CompletedEvent += EndCycle;
            var param = Params.GetParam(Item, GameParamType.CycleDelay);
            if (param != null) param.UpdatedEvent += UpdateCycle;
            base.Init();
        }

        protected virtual void UpdateCycle()
        {
            Cycle.SetTarget(Rooms.GetParamCurrentValue(roomType, GameParamType.CycleDelay));
        }

        public override EntityBehaviourBase GetPointLogic()
        {
            if (!Item) return null;
            return new BaseRoomLogic(this);
        }

        protected void OnItemUpgrade(RoomItemView item)
        {
            if(IsAvailable) OnAvailable?.Invoke();
        }

        public virtual void StartCycle()
        {
            Cycle.Play();
        }

        public virtual void EndCycle()
        {
            Cycle.CompletedEvent -= EndCycle;
            Cycle.Pause();
            DeactivateProgressBar();
        }

        public void ActivateProgressBar(Vector3 position)
        {
            DeactivateProgressBar();
            _progressBar = _uiFactory.SpawnProgressBar();
            _progressBar.UpdateUI(0f);
            _progressBar.SetType(0);
            _progressBar.Activate();
            _progressBar.transform.position = position;
        }

        public void UpdateProgressBarPosition(Vector3 position)
        {
            if(_progressBar != null) _progressBar.transform.position = position;
        }

        public void DeactivateProgressBar()
        {
            if(!_progressBar) return;
            _uiFactory.RemoveProgressBar(_progressBar);
            _progressBar = null;
        }

        protected void UpdateProgressBars()
        {
            _progressBar?.UpdateUI(Cycle.ProgressValue);
        }
    }
}
