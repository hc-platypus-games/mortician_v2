using _Game.Scripts.View.EntityBehaviour;

namespace _Game.Scripts.View.MapPoints.PointsWithLogic
{
    public class CleanerWorkPoint : BaseRoomPointView
    {
        public override EntityBehaviourBase GetPointLogic()
        {
            return new CleanerLogic(this);
        }
    }
}
