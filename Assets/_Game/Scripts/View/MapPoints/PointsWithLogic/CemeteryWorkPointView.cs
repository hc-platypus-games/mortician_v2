using System;
using _Game.Scripts.Enums;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.Room;

namespace _Game.Scripts.View.MapPoints.PointsWithLogic
{
    public class CemeteryWorkPointView : BaseRoomPointView
    {
        public override void Init()
        {            
            base.Init();
            UpdateCycle();
        }

        protected override void UpdateCycle()
        {
            Cycle.SetTarget(Item.GetParamValue(GameParamType.CycleDelay));
        }

        public override EntityBehaviourBase GetPointLogic()
        {
            if (characterType == CharacterType.Gardener)
            {
                return new GardenerLogic(this);
            }
            
            return new CemeteryWorkLogic(this);
        }
    }
}
