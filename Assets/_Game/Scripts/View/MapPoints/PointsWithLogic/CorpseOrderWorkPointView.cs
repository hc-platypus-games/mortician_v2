using _Game.Scripts.View.EntityBehaviour;

namespace _Game.Scripts.View.MapPoints.PointsWithLogic
{
    public class CorpseOrderWorkPointView : BaseRoomPointView
    {
        public override bool IsAvailable => true;

        public override EntityBehaviourBase GetPointLogic()
        {
            var behaviour = new CorpseOrderRoomLogic(this, Item.RoomType);
            Container.Inject(behaviour);
            return behaviour;
        }
    }
}
