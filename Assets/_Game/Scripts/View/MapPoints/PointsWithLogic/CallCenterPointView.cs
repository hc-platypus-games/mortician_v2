using _Game.Scripts.View.EntityBehaviour;

namespace _Game.Scripts.View.MapPoints.PointsWithLogic
{
    public class CallCenterPointView : BaseRoomPointView
    {
        public override EntityBehaviourBase GetPointLogic()
        {
            var behaviour = new CallCenterLogic(this);
            Container.Inject(behaviour);
            return behaviour;
        }
    }
}
