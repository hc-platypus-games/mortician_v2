using _Game.Scripts.Factories;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using Zenject;

namespace _Game.Scripts.View.MapPoints
{
    public class CorpseOfferCyclePointView : BaseRoomPointView
    {
        private enum State
        {
            Empty,
            Working,
            Full
        }

        [Inject] private CharactersFactory _charactersFactory;

        private State _state;
        private CharacterView _orderVisitor;

        public bool IsEmpty => _state == State.Empty;
        public bool IsFull => _state == State.Full;
        public override bool IsAvailable => base.IsAvailable && (IsEmpty || IsFull);

        public override void Init()
        {
            base.Init();
        }

        public void StartWorking()
        {
            _state = State.Working;
            Cycle.CompletedEvent += OnCycleComplete;
        }

        private void OnCycleComplete()
        {
            Cycle.CompletedEvent -= OnCycleComplete;
            _state = State.Full;
            OnAvailable?.Invoke();
            _orderVisitor = _charactersFactory.SpawnCorpseOrderVisitor();
        }

        public CharacterView Clear()
        {
            _state = State.Empty;
            return _orderVisitor;
        }
    }
}
