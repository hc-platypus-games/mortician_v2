using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.View.Corpse;
using TMPro;
using Zenject;

namespace _Game.Scripts.View.MapPoints
{
    public class CorpseVisualizerView : PointBaseView
    {

        [Inject] private LoadingSystem _loading;
        
        public override bool IsAvailable => true;

        private TextMeshPro _text;

        public override void Init()
        {
            base.Init();
            _text = GetComponentInChildren<TextMeshPro>(true);
            
            var corpsePoints = GetComponentsInChildren<CorpseStorageView>(true).ToList();
            foreach (var point in corpsePoints)
            {
                point.OnClose += UpdateText;
            }

            _loading.OnLoadedGame += UpdateText;
        }

        private void UpdateText()
        {
            _text.text = GetComponentsInChildren<CorpseView>(true).Length.ToString();
        }
    }
}
