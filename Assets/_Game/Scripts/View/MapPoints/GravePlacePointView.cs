using System;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using _Game.Scripts.View.Room;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.MapPoints
{
    public class GravePlacePointView : BaseRoomPointView
    {
        public enum State
        {
            Wait,
            WaitCorpsePortering,
            ReadyToWork,
            Reset
        }

        public Action CycleEnded;

        [SerializeField] private GameObject[] _flowers;

        private State _state;
        private CorpseObject _currentCorpse;
        private GravePlaceVisualView[] _visuals;
        private PointBaseView[] _visitorsPoints;

        [Inject] private GameParamFactory _params;
        [Inject] private CorpseFactory _corpseFactory;
        
        public State CurrentState => _state;
        public int GraveCares { get; private set; }
        
        public GravePlacePointView()
        {
            type = Type.CorpsePlace;
        }

        public bool IsAvailableWhen(State state)
        {
            if (state == State.Reset && Cycle.LeftValue < 30f) return false;
            
            return base.IsAvailable && _state == state;
        } 

        public override void Init()
        {
            base.Init();
            UpdateCycle();
            _corpseFactory.CemeteryProgresses.Add(Cycle);
            Cycle.CompletedEvent += OnResetComplete;
            _visuals = GetComponentsInChildren<GravePlaceVisualView>(true);
            _visitorsPoints = GetComponentsInChildren<PointBaseView>(true)
                .Where(i => i.CharacterType.HasFlag(CharacterType.Visitor)).ToArray();
            foreach (var point in _visitorsPoints)
            {
                point.IsAvailable = false;
            }
            UpdateFlowers();
        }

        protected override void UpdateCycle()
        {
            Cycle.SetTarget(Item.GetParamValue(GameParamType.CycleDelay));
        }

        private void UpdateItem()
        {
            foreach (var visual in _visuals)
            {
                visual.Show(Item.Level, false);
            }

            UpdateFlowers();
        }

        private void UpdateFlowers()
        {
            for (int i = 0; i < _flowers.Length; i++)
            {
                _flowers[i].SetActive(GraveCares > i);
            }
        }

        public void PrepareForCorpse()
        {
            _state = State.WaitCorpsePortering;
            foreach (var point in _visitorsPoints)
            {
                point.IsAvailable = true;
            }
            UpdateItem();
        }

        public void SetCorpse(CorpseObject corpse)
        {
            _corpseFactory.CemeteryCorpses.Add(corpse);
            _currentCorpse = corpse;
            _state = State.ReadyToWork;
            UpdateItem();
        }

        public void StartReset()
        {
            _params.GetParam<GameSystem>(GameParamType.CorpsesProgress).Change(1);
            _corpseFactory.CemeteryCorpses.Remove(_currentCorpse);
            _state = State.Reset;
            IsAvailable = true;
            Cycle?.Play();
            CycleEnded?.Invoke();
            UpdateItem();
            foreach (var point in _visitorsPoints)
            {
                point.IsAvailable = false;
            }
        }

        public void GraveCare()
        {
            GraveCares++;
            UpdateFlowers();
        }

        private void OnResetComplete()
        {
            _state = State.Wait;
            Cycle?.Pause();
            IsAvailable = true;
            GraveCares = 0;
            UpdateItem();
        }
    }
}
