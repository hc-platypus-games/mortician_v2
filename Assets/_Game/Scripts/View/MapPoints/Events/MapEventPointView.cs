using System;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Save.SaveStructures;
using _Game.Scripts.Systems.Tutorial;
using _Game.Scripts.View.Vehicle;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.MapPoints.Events
{
    public class MapEventPointView : PointBaseView, IGameProgress, IGameParam
    {
        public Action OnCompleteCycle, OnCompleteAnimation;

        [SerializeField] private int _id;
        [SerializeField] private GameObject _assetsContainer;
        [SerializeField] private GameObject _trashContainer;
        [SerializeField] private GameObject _buildContainer;

        [Inject] private GameProgressFactory _progressFactory;
        [Inject] private LevelEventsFactory _eventFactory;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private GameParamFactory _params;
        [Inject] private GameSystem _game;
        [Inject] private TutorialSystem _tutorialSystem;

        private bool _isBought;

        public int Id => _id;
        public bool IsBought => _isBought;
        public MapEventPointConfig Config { get; private set; }
        public GameProgress CycleDelay { get; private set; }
        public GameProgress Capacity { get; private set; }
        public GameProgress Timer { get; private set; }
        public PointBaseView[] CharacterPoints { get; private set; }
        public override bool IsAvailable => base.IsAvailable && _isBought;

        public MapEventPointView()
        {
            type = Type.MapEvent;
        }

        public override void Init()
        {
            base.Init();

            Config = _balance.DefaultBalance.MapEventPoints.FirstOrDefault(p => p.Id == _id);
            
            CycleDelay = _progressFactory.CreateProgress(this, GameParamType.CycleDelay, Config?.CycleDelay ?? 0);
            CycleDelay.CompletedEvent += EndCycle;
            CycleDelay.Pause();
            
            Capacity = _progressFactory.CreateProgress(this, GameParamType.Capacity, Config?.Cap ?? 0, false,false);
            Timer = _progressFactory.CreateProgress(this, GameParamType.BuildingTimer, Config?.BuildingTime ?? 0, false);
            Timer.CompletedEvent += Buy;
            Timer.Pause();

            CharacterPoints = GetComponentsInChildren<PointBaseView>(true).Where(i => i != this).ToArray();

            if (Config?.StartLevel > 0) _isBought = true;
            if (_isBought) StartCycle();
            RedrawVisuals();
        }

        private void RedrawVisuals()
        {
            if(_buildContainer) _buildContainer.SetActive(Timer != null && Timer.IsActive);
            if(_trashContainer) _trashContainer.SetActive(!_isBought);
            if(_assetsContainer) _assetsContainer.SetActive(_isBought);
        }

        private void StartCycle()
        {
            if(_isBought == false) return;
            CycleDelay.Play();
        }

        public virtual void EndCycle()
        {
            if(_isBought == false) return;
            CycleDelay.Pause();
            OnCompleteCycle?.Invoke();

            if (Capacity.IsCompleted) return;
            if (gameObject.activeSelf)
            {
                Capacity.Change(1);
                _eventFactory.AddEventToQueue(this);
            }
            
            StartCycle();
        }

        public void Unlock()
        {
            if(_isBought) return;

            _params.GetParam<GameSystem>(GameParamType.Workers).Change(-1);
            
            Timer.Play();
            RedrawVisuals();
        }

        public void LoadFromSave(MapEventData data)
        {
            _isBought = data.Bought;
            if (data.Bought)
            {
                Capacity.Change(1);
                CycleDelay.Play(); 
            }
            else
            {
                var workers = _params.GetParam<GameSystem>(GameParamType.Workers);
                Timer.Change(data.BuildingTime);
                Timer.Play();
            }
            RedrawVisuals();
        }

        public void Buy()
        {
            if(_isBought) return;
            _params.GetParam<GameSystem>(GameParamType.Workers).Change(1);
            _isBought = true;
            RedrawVisuals();
            CycleDelay.Play();

            if (_id == 2)
            {
                _tutorialSystem.StartTutorial(102, 1);
            }
        }

        public virtual void OnCarArrived(CarView car)
        {
            EndEvent();
        }

        protected virtual void EndEvent()
        {
            OnCompleteAnimation?.Invoke();
            StartCycle();
        }

        public bool CanBuy()
        {
            return Config != null && _game.IsEnoughCurrency(GameParamType.Soft, Config.Price);
        }
    }
}
