using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.Vehicle;
using Zenject;

namespace _Game.Scripts.View.MapPoints.Events
{
    public class HospitalPointView : MapEventPointView
    {
        [Inject] private CharactersFactory _charactersFactory;
        private HospitalEventCharacterBehaviour _characterBehaviour;

        public override void Init()
        {
            base.Init();
            var character = GetComponentInChildren<CharacterView>();
            if (character != null)
            {
                if (character.Config == null) character.SetConfig(new CharacterConfig());
                
                _characterBehaviour = new HospitalEventCharacterBehaviour();
                _charactersFactory.InitCharacter(character, _characterBehaviour);
                character.SetTargetPoint(CharacterPoints[0]);
                _characterBehaviour.OnComplete += EndEvent;
            }
        }

        public override void OnCarArrived(CarView car)
        {
            if (_characterBehaviour == null)
            {
                EndEvent();
                return;
            }
            _characterBehaviour.StartEvent(car);
        }
    }
}
