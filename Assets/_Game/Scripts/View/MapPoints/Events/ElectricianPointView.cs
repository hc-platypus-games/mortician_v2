using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.Vehicle;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.MapPoints.Events
{
    public class ElectricianPointView : MapEventPointView
    {
        [SerializeField] private ParticleSystem _explosionFx;
        [SerializeField] private ParticleSystem _deadBodyFx;
        
        [Inject] private CharactersFactory _charactersFactory;
        [Inject] private CorpseFactory _corpseFactory;
        [Inject] private LoadingSystem _loading;
        [Inject] private MapPointsFactory _points;
        [Inject] private AppEventProvider _eventProvider;
        private ElectricianEventCharacterBehaviour _characterBehaviour;
        
        public override void Init()
        {
            base.Init();
            var character = GetComponentInChildren<CharacterView>(true);
            if (character != null)
            {
                if (character.Config == null) character.SetConfig(new CharacterConfig());
                
                _characterBehaviour = new ElectricianEventCharacterBehaviour();
                _charactersFactory.InitCharacter(character, _characterBehaviour);
                character.SetTargetPoint(CharacterPoints[0]);
                _characterBehaviour.SetPoints(CharacterPoints);
                _characterBehaviour.WorkBroke += OnWorkBroke;
                _characterBehaviour.AnimationEnd += OnAnimationEnd;
                _characterBehaviour.OnComplete += EndEvent;
            }

            _loading.OnLoadedGame += OnLoadedGame;
        }

        private void OnLoadedGame()
        {
            if (Capacity.CurrentValue > 0)
            {
                
                var spawnPoint = _points.GetPoint(Type.CorpsePlace, points: GetComponentsInChildren<PointBaseView>(true));
                if (!spawnPoint)
                {
                    return;
                }

                var corpse = _corpseFactory.SpawnCorpse(spawnPoint.transform);
                corpse.ShowSkin(0);
                OnAnimationEnd();
            }
        }

        public override void EndCycle()
        {
            base.EndCycle();
            if (CycleDelay.IsPaused) return;
            IsAvailable = false;
            _characterBehaviour.Spawn();
        }

        private void OnWorkBroke()
        {
            if(_explosionFx) _explosionFx.Play();
        }

        private void OnAnimationEnd()
        {
            if(_deadBodyFx) _deadBodyFx.Play();
            IsAvailable = true;
            InvokeSystem.StartInvoke(TriggerEvent, 0.5f);
        }

        private void TriggerEvent()
        {
            _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.ElectricianEventEnded);
        }

        public override void OnCarArrived(CarView car)
        {
            _characterBehaviour.OnCarArrived(car);
        }

        protected override void EndEvent()
        {
            base.EndEvent();
            if (_deadBodyFx)
            {
                _deadBodyFx.Stop();
                _deadBodyFx.Clear();
            }
        }
    }
}
