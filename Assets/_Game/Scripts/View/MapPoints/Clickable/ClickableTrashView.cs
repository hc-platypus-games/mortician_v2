using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.MapPoints.Clickable
{
    public class ClickableTrashView : ClickableEventView
    {
        [SerializeField] private RoomType _roomType;

        [Inject] private GameFlags _flags;
        [Inject] private GameSystem _game;
        [Inject] private RoomsFactory _rooms;
        [Inject] private CalculationComponent _calculation;
        [Inject] private UIFactory _uiFactory;

        private ParticleSystem _particle;

        public override void Init()
        {
            base.Init();
            _particle = GetComponentInChildren<ParticleSystem>(true);
            if(_particle) _particle.transform.SetParent(transform.parent);
            this.Deactivate();
        }

        public void Spawn()
        {
            if(_flags.Has(GameFlag.TutorialRunning)) return;
            if (_roomType != RoomType.None)
            {
                var room = _rooms.GetRoom(_roomType);
                if(room || room.Level == 0) return;
            }
            this.Activate();
        }

        public override bool Touch()
        {
            Clean(true);
            return true;
        }

        public void Clean(bool touch)
        {
            var reward = _calculation.GetIncomeByTime(touch ? 10f : 5f);
            _game.AddCurrency(GameParamType.Soft, reward, GetCurrencyPlace.ClickableEvent);
            this.Deactivate();
            if(_particle) _particle.Play();
            _uiFactory.SpawnTextFx($"+{reward.ToFormattedString()}", transform.position);
        }
    }
}
