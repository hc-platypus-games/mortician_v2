using System.Linq;
using _Game.Scripts.View.Room;

namespace _Game.Scripts.View.MapPoints.Clickable
{
    public class ClickableEventView : BaseView
    {
        public override void Init()
        {
            base.Init();
            var collider = GetComponentsInChildren<SelectionCollider>().FirstOrDefault(i => i.Type == SelectionColliderType.ClickableEvent);
            if (collider) collider.SetType(SelectionColliderType.ClickableEvent, this);
        }

        public virtual bool Touch()
        {
            return false;
        }
    }
}
