using System;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.Interfaces;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using _Game.Scripts.View.Room;
using _Game.Scripts.View.Vehicle;
using ModestTree;
using Zenject;

namespace _Game.Scripts.View.MapPoints
{
    public class VisitorParkingPointView : BaseRoomPointView
    {
        public Action CallVisitorsEvent,
            CarArrivedEvent,
            AllVisitorsReturned;
        
        [Inject] private VehicleFactory _vehicleFactory;
        [Inject] private MapPointsFactory _points;
        [Inject] private GameProgressFactory _progresses;
        [Inject] private RoomsFactory _rooms;

        private int _currentCharacters;
        private int _needCharacters;
        
        public override void Init()
        {
            base.Init();
            
            var myId = _points.GetAllPoints(Type.Parking, roomType).IndexOf(this);
            var car = GetComponentInChildren<CarView>(true);
            if (car == null) car = _vehicleFactory.SpawnVehicle();
            car.transform.SetParent(transform);
            car.Init(this);
            car.AddBehaviour(new VisitorCarBehaviour(myId));
            
            _needCharacters = _points.GetAllPoints(Type.WorkPlace, 
                points:GetComponentInParent<RoomItemView>(true).GetComponentsInChildren<PointBaseView>(true)).Length;
            _currentCharacters = 0;

            Cycle.CompletedEvent += OnCycleDelayComplete;
            Cycle.Play();
        }

        private void OnCycleDelayComplete()
        {
            if(!Item.IsBought) return;
            CallVisitorsEvent?.Invoke();
        }

        public void AddCharacter()
        {
            _currentCharacters++;
            if(_currentCharacters < _needCharacters) return;
            AllVisitorsReturned?.Invoke();
            _currentCharacters = 0;
        }
    }
}
