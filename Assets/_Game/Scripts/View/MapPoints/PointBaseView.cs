using System;
using _Game.Scripts.Enums;
using _Game.Scripts.View.EntityBehaviour;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.MapPoints
{
    public enum Type
    {
        None,
        WorkPlace,
        IdleWalk,
        Queue,
        ItemPlace,
        CardCharacter,
        CarCorpsePlace,
        EmbalmingCorpsePlace,
        Exit,
        Enter,
        MapEvent,
        VehicleEnter,
        Parking,
        CorpsePorterIdle,
        RESERVED,
        FarewellPlace,
        CorpsePlace,
        RoomCycle,
        CallTable,
        CharacterSpawn,
        CarSpawn,
        PayParking,
        TakeFlowers,
        CameraFocus,
        TutorialCameraFocus
    }
    
    public class PointBaseView : BaseView
    {
        public Action OnAvailable;
        public Action OnUnvailable;
        
        [SerializeField] protected Type type;
        [SerializeField] protected RoomType roomType;
        [SerializeField] protected CharacterType characterType;
        [SerializeField] private string _key;
        
        [Inject] protected DiContainer Container;
        
        private bool _isAvailable = true;
        private int _animationHash;

        public string Key => _key;
        public Type Type => type;
        public CharacterType CharacterType => characterType;
        public RoomType RoomType
        {
            get
            {
                return roomType;
            }
            set
            {
#if UNITY_EDITOR
                roomType = value;
#endif
            }
        }

        public int AnimationHash
        {
            get
            {
                if (_animationHash == 0)
                {
                    _animationHash = Animator.StringToHash(_key);
                }

                return _animationHash;
            }
        }

        public virtual bool IsAvailable
        {
            get => _isAvailable && gameObject.activeInHierarchy;
            set
            {
                bool wasAvailable = IsAvailable;
                _isAvailable = value;
                if (!wasAvailable && IsAvailable)
                {
                    OnAvailable?.Invoke();
                }

                if (wasAvailable && !IsAvailable)
                {
                    OnUnvailable?.Invoke();
                }
            }
        }

        public virtual EntityBehaviourBase GetPointLogic()
        {
            return null;
        }
    }
}
