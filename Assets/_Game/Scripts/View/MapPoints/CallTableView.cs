using _Game.Scripts.Factories;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.MapPoints
{
    public class CallTableView : PointBaseView
    {
        [SerializeField] private TextMeshPro _text;
    
        [Inject] private LevelEventsFactory _eventsFactory;

        public override void Init()
        {
            base.Init();
            _eventsFactory.OnEventCalled += UpdateTable;
            _eventsFactory.OnEventTaken += UpdateTable;
        }

        private void UpdateTable()
        {
            _text.text = _eventsFactory.CallsCount.ToString();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            _eventsFactory.OnEventCalled -= UpdateTable;
            _eventsFactory.OnEventTaken -= UpdateTable;
        }
    }
}
