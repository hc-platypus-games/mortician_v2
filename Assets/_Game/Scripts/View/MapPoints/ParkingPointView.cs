using _Game.Scripts.Factories;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using _Game.Scripts.View.Vehicle;
using Zenject;

namespace _Game.Scripts.View.MapPoints
{
    public class ParkingPointView : BaseRoomPointView
    {
        [Inject] private VehicleFactory _vehicleFactory;

        public ParkingPointView()
        {
            type = Type.Parking;
        }
        
        public override void Init()
        {
            base.Init();
            var car = GetComponentInChildren<CarView>(true);
            if (car == null) car = _vehicleFactory.SpawnVehicle();
            car.transform.SetParent(transform);
            car.Init(this);
        }
    }
}
