using _Game.Scripts.View.Room;
using UnityEngine;

namespace _Game.Scripts.View
{
    public class LevelView : MonoBehaviour
    {
        [Header("MapSettings")] 
        [SerializeField] private int _id;
        [SerializeField] private string _name;
        [SerializeField] private float _minCamSize;
        [SerializeField] private float _maxCamSize;
        [SerializeField] private Transform _boundsMin;
        [SerializeField] private Transform _boundsMax;
        [SerializeField] private Vector3 _defaultPos;
        [Header("Background")] 
        [SerializeField] private int _backgroundCars = 5;
        [SerializeField] private int _backgroundCharacters = 5;
        [SerializeField] private BuildingObjectView _buildingObjectView;

        public int Id => _id;
        public string LevelName => _name;
        public float MinCamSize => _minCamSize;
        public float MaxCamSize => _maxCamSize;
        public Vector3 BoundsMin => _boundsMin.localPosition;
        public Vector3 BoundsMax => _boundsMax.localPosition;
        public Vector3 DefaultPos => _defaultPos;
        public int BackgroundCars => _backgroundCars;
        public int BackgroundCharacters => _backgroundCharacters;
        public BuildingObjectView BuildingView => _buildingObjectView;
    }
}