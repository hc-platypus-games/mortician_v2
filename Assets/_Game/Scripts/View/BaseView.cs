using UnityEngine;

namespace _Game.Scripts.View
{
    public class BaseView : MonoBehaviour
    {
        protected virtual void Reset()
        {
            
        }

        public virtual void Init()
        {
            
        }

        public virtual void OnDestroy()
        {
            
        }
    }
}