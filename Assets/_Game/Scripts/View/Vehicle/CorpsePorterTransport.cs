using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.Corpse;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using _Game.Scripts.View.Scenarios;
using DG.Tweening;
using UnityEngine;
using Zenject;
using Type = _Game.Scripts.View.MapPoints.Type;

namespace _Game.Scripts.View.Vehicle
{
    public class CorpsePorterTransport : VehicleViewBase
    {
        private enum WorkState
        {
            Wait,
            Work,
            Queue
        }
        
        public Action TransportReset, GravePlaceFound, Complete;

        [Inject] private MapPointsFactory _mapPoints;
        [Inject] private CorpseFactory _corpseFactory;
        [Inject] private RoomsFactory _rooms;
        [Inject] private AppEventProvider _eventProvider;

        private WorkState _workState;
        private List<CorpsePorterBehaviour> _characters = new();
        private CorpseObject _currentCorpse;
        private CharacterItemView _coffin;
        private CorpseView _corpseView;
        private GravePlacePointView _currentGravePlace;
        private CharacterItemView[] _characterItems;
        private List<CharacterItemView> _takenItems = new();
        private PointBaseView _resetPoint;
        private bool _isFarewellCorpse;
        private FarewellCoffinAnimation _farewellCoffinAnimation;

        public GravePlacePointView GravePlacePointView => _currentGravePlace;

        public override void Init()
        {
            base.Init();
            _resetPoint = _mapPoints.GetPoint(Type.VehicleEnter, RoomType.CorpsePorter);
            ((FarewellWorkPointView)_mapPoints.GetAllPoints(Type.WorkPlace, RoomType.Farewell)[0]).CorpseAdded += FindCorpse;
            FindCorpse();
        }

        private void FindCorpse()
        {
            if (_currentCorpse != null) return;
            
            var room = _rooms.GetRoom(RoomType.CorpsePorter);
            room.UpgradedEvent -= FindCorpse;
            if (room.Level < 1)
            {
                room.UpgradedEvent += FindCorpse;
                return;
            }
            
            var point = (FarewellWorkPointView)_mapPoints.GetAllPoints(Type.WorkPlace, RoomType.Farewell)[0];
            _currentCorpse = point.GetCorpse();
            _corpseFactory.EmbalmedCorpseAdded -= FindCorpse;
            if (_currentCorpse == null)
            {
                _isFarewellCorpse = false;
                _currentCorpse = _corpseFactory.GetCorpseFromEmbalmed();
                if (_currentCorpse != null)
                {
                    SetTargetPoint(_currentCorpse.AttachedPoint);
                    Reset(_currentCorpse);
                    return;
                }
                
                _corpseFactory.EmbalmedCorpseAdded += FindCorpse;

                InvokeSystem.StartInvoke(FindCorpse, 3f);
                return;
            }

            _farewellCoffinAnimation = point.GetComponentInChildren<FarewellCoffinAnimation>(true);
            _isFarewellCorpse = true;
            SetTargetPoint(_currentCorpse.AttachedPoint);
            Reset(_currentCorpse);
        }

        public void Reset(CorpseObject corpse)
        {
            if(_workState == WorkState.Work) return;
            _workState = WorkState.Work;
            _currentCorpse = corpse;
            var resetPoint = _isFarewellCorpse ? _currentCorpse.AttachedPoint.transform : _resetPoint.transform;
            ForceMoveToPoint(resetPoint.position);
            transform.rotation = resetPoint.rotation;
            TransportReset?.Invoke();
            _characterItems ??= GetComponentsInChildren<CharacterItemView>(true);
        }

        public override void MoveToPoint(PointBaseView point, Action onReached = null)
        {
            if (Vector3.Distance(transform.position, point.transform.position) > 0.02f)
            {
                for (var i = 0; i < _characters.Count; i++)
                {
                    if (i < 2)
                    {
                        _characters[i].View.CarryCoffinRight();
                    }
                    else
                    {
                        _characters[i].View.CarryCoffinLeft();
                    }
                }
            }
            base.MoveToPoint(point, onReached ?? AnimateStop);
        }

        private void AnimateStop()
        {
            OnReached -= AnimateStop;
            if(_characters.Count == 0) return;
            foreach (var character in _characters)
            {
                character.View.Animate();
            }
        }

        public void AddCharacter(CorpsePorterBehaviour corpsePorterBehaviour, int id)
        {
            _characters.Add(corpsePorterBehaviour);
            if(_characters.Count < 4) return;
            
            _characters = _characters.OrderBy(o => o.Id).ToList();
            TakeCoffin();
        }

        private void TakeCoffin()
        {
            _coffin ??= _mapPoints.GetPoint(Type.CarCorpsePlace, RoomType.CorpsePorter)?
                .GetComponentInChildren<CharacterItemView>();
            if(!_coffin) return;
            if (_isFarewellCorpse)
            {
                _farewellCoffinAnimation.CloseCoffin();
                InvokeSystem.StartInvoke(TakeCorpseFromFarewell, 0.5f);
            }
            else
            {
                TakeItem(_coffin);
                InvokeSystem.StartInvoke(MoveToCorpse, 0.5f);
            }
        }

        private void TakeCorpseFromFarewell()
        {
            _coffin.transform.position = _farewellCoffinAnimation.CoffinPoint.position;
            var rotation = _farewellCoffinAnimation.CoffinPoint.rotation.eulerAngles;
            rotation.y -= 180f;
            _coffin.transform.rotation = Quaternion.Euler(rotation);
            _farewellCoffinAnimation.DeactivateCoffin();
            TakeItem(_coffin);
            TakeCorpse();
        }

        private void MoveToCorpse()
        {
            MoveToPoint(TargetPoint, TakeCorpse);
        }

        private void TakeCorpse()
        {
            OnReached -= TakeCorpse;
            if (_isFarewellCorpse)
            {
                _corpseView = TargetPoint.GetComponentInChildren<CorpseView>();
                TakeItem(_corpseView);
                var point = (FarewellWorkPointView)_mapPoints.GetAllPoints(Type.WorkPlace, RoomType.Farewell)[0];
                point.ClearCorpse();
                InvokeSystem.StartInvoke(FindGravePlace, 0.5f);
            }
            else
            {
                var storage = (CorpseStorageView)TargetPoint;
                storage.Open();
                _mapPoints.FreePoint(storage);
                _corpseView = storage.GetComponentInChildren<CorpseView>();
                TakeItem(_corpseView);
                InvokeSystem.StartInvoke(FindGravePlace, 0.5f);
            }
        }

        private void FindGravePlace()
        {
            var storage = TargetPoint as CorpseStorageView;
            if (storage != null) storage.Close();
            if (_corpseView != null)
            {
                _takenItems.Remove(_corpseView);
                _corpseFactory.RemoveCorpse(_corpseView);
                _corpseView = null;
            }
            _currentGravePlace = _mapPoints.GetGravePlace(GravePlacePointView.State.Wait, true, OnAvailableGrave);
            if (_currentGravePlace == null)
            {
                SetTargetPoint(_mapPoints.GetPoint(Type.Queue, RoomType.Cemetery));
                MoveToPoint(TargetPoint);
                OnReached += AnimateStop;
                return;
            }

            if (TargetPoint.Type == Type.Queue) _mapPoints.FreePoint(TargetPoint);
            MoveToPoint(_currentGravePlace, AnimatePutCoffin);
            _currentGravePlace.PrepareForCorpse();
            GravePlaceFound?.Invoke();
        }

        private void OnAvailableGrave(QueueElement element)
        {
            FindGravePlace();
        }

        private void AnimatePutCoffin()
        {
            OnReached -= AnimatePutCoffin;
            var point = _mapPoints.GetPoint(Type.ItemPlace, RoomType.Cemetery,false,
                _currentGravePlace.GetComponentsInChildren<PointBaseView>());
            if (!point)
            {
                PutCoffinToGrave();
                return;
            }

            PutItem(_coffin, point.transform);
            InvokeSystem.StartInvoke(PutCoffinToGrave, 0.5f);
        }

        private void PutCoffinToGrave()
        {
            _currentGravePlace.SetCorpse(_currentCorpse);
            _mapPoints.FreePoint(_currentGravePlace);
            _characters.Clear();
            _currentGravePlace = null;
            var coffinResetPoint = _mapPoints.GetPoint(Type.CarCorpsePlace, RoomType.CorpsePorter);
            _coffin.transform.SetParent(coffinResetPoint.transform);
            _coffin.transform.localPosition = Vector3.zero;
            _coffin.transform.localRotation = Quaternion.identity;
            _takenItems.Remove(_coffin);
            Complete?.Invoke();
            _workState = WorkState.Wait;
            _currentCorpse = null;
            FindCorpse();
            _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.CorpseDeliveredToGrave);
        }

        private CharacterItemView GetParentFor(string key, bool stack = true)
        {
            if (stack)
            {
                foreach (var itemPoint in _takenItems)
                {
                    if (itemPoint.Key == key) return itemPoint;
                }
            }
            
            foreach (var itemPoint in _characterItems)
            {
                if (itemPoint.Key == key) return itemPoint;
            }

            return null;
        }

        public void TakeItem(CharacterItemView item, string key = "")
        {
            var itemParent = GetParentFor(key.Length > 0 ? key : item.Key);
            if(itemParent == null) return;
            
            var items = item.GetComponentsInChildren<CharacterItemView>();
            foreach (var newItem in items)
            {
                _takenItems.Add(newItem);
            }
            item.SetParent(itemParent.transform);
            item.transform.DOLocalMove(Vector3.zero, 0.5f);
            item.transform.DOLocalRotate(Vector3.zero, 0.5f);
        }

        public void PutItem(CharacterItemView item, Transform parent = null)
        {
            if(_takenItems.Count == 0) return;
            var items = item.GetComponentsInChildren<CharacterItemView>();
            foreach (var newItem in items)
            {
                _takenItems.Remove(newItem);
            }
            
            item.SetParent(parent.transform);
            item.transform.DOLocalMove(Vector3.zero, 0.5f);
            item.transform.DOLocalRotate(Vector3.zero, 0.5f);
        }
    }
}
