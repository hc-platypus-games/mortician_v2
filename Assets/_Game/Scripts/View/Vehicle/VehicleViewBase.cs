using System;
using _Game.Scripts.View.MapPoints;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

namespace _Game.Scripts.View.Vehicle
{
    public abstract class VehicleViewBase : BaseView
    {
        protected enum CarState
        {
            Wait,
            OnEvent,
            MoveToExit
        }

        public Action OnReached, OnTouched;

        private bool _isMoving;
        
        protected CarState State;
        protected PointBaseView TargetPoint;
        
        protected NavMeshAgent NavMeshAgent;
        private const float RotationSpeed = 3.5f;
        private float _stoppingDistance = 0.01f;
        private PointBaseView _parkingPoint;

        public float Speed { get; private set; }
        
        public bool IsAvailable => State == CarState.Wait;
        public PointBaseView ParkingPoint => _parkingPoint;
        
        [Inject]
        private void Construct()
        {
            State = CarState.Wait;
            NavMeshAgent = GetComponent<NavMeshAgent>();
            Speed = NavMeshAgent.speed;
            NavMeshAgent.speed = 0;
        }

        public void Init(PointBaseView parking)
        {
            _parkingPoint = parking;
            ForceMoveToPoint(_parkingPoint.transform.position);
        }

        public virtual void Tick(float deltaTime)
        {
            CheckIfDestinationReached(deltaTime);
            RotateNavMeshAgent(deltaTime);
        }

        public void UpdateSpeed(float newSpeed)
        {
            Speed = newSpeed;
        }

        public void SetTargetPoint(PointBaseView pointBaseView)
        {
            if (pointBaseView == null) pointBaseView = _parkingPoint;
            TargetPoint = pointBaseView;
        }

        private void CheckIfDestinationReached(float deltaTime)
        {
            // if (TargetPoint)
            // {
            //     if(Vector3.Distance(TargetPoint.transform.position, NavMeshAgent.destination) > 0.5f) return;
            // }
            var distance = Vector3.Distance(NavMeshAgent.transform.position, NavMeshAgent.destination);
            if(distance < _stoppingDistance)
            {
                NavMeshAgent.enabled = false;
                _isMoving = false;
                if (TargetPoint == _parkingPoint) State = CarState.Wait;
                if (OnReached != null)
                {
                    var reachedEvent = OnReached;
                    OnReached = null;
                    reachedEvent.Invoke();
                }
                return;
            }
            
            if(!_isMoving) return;
            
            distance = Vector3.Distance(NavMeshAgent.transform.position, NavMeshAgent.steeringTarget);
            
            var offset = (NavMeshAgent.steeringTarget - transform.position).normalized * Speed * deltaTime;
            
            if (offset.magnitude > distance)
            {
                transform.position = NavMeshAgent.steeringTarget;
                return;
            }

            transform.position += offset;
        }
        
        private void RotateNavMeshAgent(float deltaTime)
        {
            if (NavMeshAgent == null) return;
            if(NavMeshAgent.enabled == false) return;
            if (Vector3.Distance(NavMeshAgent.transform.position, NavMeshAgent.steeringTarget) > _stoppingDistance)
            {
                NavMeshAgent.transform.rotation = Quaternion.Slerp(NavMeshAgent.transform.rotation,
                    Quaternion.LookRotation(NavMeshAgent.steeringTarget - NavMeshAgent.transform.position),
                    RotationSpeed * deltaTime);
            }
        }

        public void ResetPosition()
        {
            NavMeshAgent.enabled = false;
            transform.position = _parkingPoint.transform.position;
            transform.rotation = _parkingPoint.transform.rotation;
        }
        
        public void ForceMoveToPoint(Vector3 point = default)
        {
            var navAgentEnabled = NavMeshAgent.enabled;
            NavMeshAgent.enabled = false;
            if (point == default && TargetPoint != null)
            {
                var target = TargetPoint.transform;
                point = target.position;
                transform.rotation = target.rotation;
            }
            transform.position = point;
            NavMeshAgent.enabled = navAgentEnabled;
        }

        public virtual void MoveToPoint(PointBaseView point, Action onReached = null)
        {
            NavMeshAgent.enabled = true;
            TargetPoint = point;
            NavMeshAgent.SetDestination(point.transform.position);
            OnReached += onReached;
            SetStopDistance(0f);

            _isMoving = true;
        }
        
        public void MoveToCenter(float speed = 0f)
        {
            if (speed == 0f)
            {
                var childTransform = transform.GetChild(0);
                var localPosition = childTransform.localPosition;
                transform.GetChild(0).localPosition =
                    new Vector3(0f, localPosition.y, localPosition.z);
                return;
            }
            transform.GetChild(0).DOLocalMoveX(0f, 3f / speed);
        }

        public void MoveToRoadLine(float speed = 0f)
        {
            if (speed == 0f)
            {
                var childTransform = transform.GetChild(0);
                var localPosition = childTransform.localPosition;
                transform.GetChild(0).localPosition =
                    new Vector3(0.25f, localPosition.y, localPosition.z);
                return;
            }
            transform.GetChild(0).DOLocalMoveX(0.25f, 3f / speed);
        }

        public void SetStopDistance(float value)
        {
            NavMeshAgent.stoppingDistance = value;
            _stoppingDistance = value + 0.01f;
        }
    }
}
