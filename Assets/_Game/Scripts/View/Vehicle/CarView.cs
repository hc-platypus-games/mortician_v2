using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.View.Corpse;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.Other;
using UnityEngine;
using Zenject;
using Random = _Game.Scripts.Tools.Random;
using Type = _Game.Scripts.View.MapPoints.Type;

namespace _Game.Scripts.View.Vehicle
{
    public class CarView : VehicleViewBase
    {
        public Action SpawnCorpse;
        
        public enum SkinType
        {
            Default,
            Catafalque,
        }
        
        [SerializeField] private Animation _bodyAnimation;
        [SerializeField] private GameObject _backParent;
        [SerializeField] private bool _isPoolablePrefab;
        
        [Inject] private DiContainer _container;
        //[Inject] private CorpseFactory _corpseFactory;
        
        private List<EntityBehaviourBase> _behaviourList = new();

        private Animation _backAnimation;
        private PointBaseView _corpsePoint;
        private VisualView[] _visuals;

        private Ray _ray;
        private RaycastHit _hit;

        public bool IsPoolPrefab => _isPoolablePrefab;
        public PointBaseView DriverPoint { get; private set; }

        public class Pool : MonoMemoryPool<CarView>
        {
            protected override void Reinitialize(CarView car)
            {
                car.Reset();
            }
        }

        public override void Init()
        {
            InitVisuals();
            base.Init();
            _backAnimation = GetComponent<Animation>();
            _corpsePoint = GetComponentsInChildren<PointBaseView>().FirstOrDefault(i => i.Type == Type.CarCorpsePlace);
            DriverPoint = GetComponentsInChildren<PointBaseView>().FirstOrDefault(i => i.Type == Type.VehicleEnter);
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
            if (_behaviourList.Count > 0)
            {
                _behaviourList[0].Tick(deltaTime);
            }

            CheckDoors();
        }
        
        
        private void CheckDoors()
        {
            _ray.origin = transform.position + Vector3.up * 0.1f;
            _ray.direction = transform.forward;
            if (Physics.Raycast(_ray, out _hit,
                    1f, GameLayers.DOORS_MASK))
            {
                var door = _hit.collider.GetComponent<DoorView>();
                if (door) door.Open();
            }
        }

        public override void MoveToPoint(PointBaseView point, Action onReached = null)
        {
            StartEngine();
            base.MoveToPoint(point, onReached);
        }

        public void StartTravel()
        {
            MoveToPoint(TargetPoint);
        }

        public void AddBehaviour(EntityBehaviourBase behaviour)
        {
            _behaviourList.Add(behaviour);
            _container.Inject(behaviour);
            behaviour.AddOwner(this);
            behaviour.Init();
        }

        public void InsertBehaviour(EntityBehaviourBase behaviour)
        {
            _behaviourList.Insert(0, behaviour);
            _container.Inject(behaviour);
            behaviour.AddOwner(this);
            behaviour.Init();
        }

        public void RemoveBehaviour(EntityBehaviourBase behaviour)
        {
            _behaviourList.Remove(behaviour);
            behaviour.Destroy();
            NextBehaviourStep();
        }

        private void ClearBehaviour()
        {
            foreach (var behaviour in _behaviourList)
            {
                behaviour.Destroy();
            }
            _behaviourList.Clear();
        }

        public void NextBehaviourStep()
        {
            if (_behaviourList.Count == 0) return;

            _behaviourList[0].RunNextStep();
        }

        private void InitVisuals()
        {
            if (_visuals == null || _visuals.Length == 0)
            {
                _visuals = GetComponentsInChildren<VisualView>(true);
                foreach (var visual in _visuals)
                {
                    visual.Init();
                }
            }
        }

        public void SetSkin(SkinType type)
        {
            InitVisuals();
            switch (type)
            {
                case SkinType.Default:
                    _backParent.Deactivate();
                    UpdateVisuals(Random.Range(2, 5));
                    break;
                case SkinType.Catafalque:
                    _backParent.Activate();
                    UpdateVisuals(1);
                    break;
            }
        }

        private void UpdateVisuals(int id)
        {
            foreach (var visual in _visuals)
            {
                visual.Show(id);
            }
        }

        public void OpenBack()
        {
            _backAnimation?.Play("CarOpenBack");
        }

        public void CloseBack()
        {
            _backAnimation?.Play("CarCloseBack");
        }

        public void StartEngine()
        {
            _bodyAnimation?.Play("CarMoving");
        }

        public void StopEngine()
        {
            _bodyAnimation?.Stop("CarMoving");
        }
        
        public CorpseView GetCorpse()
        {
            if(_corpsePoint == null) return null;
            var corpse = _corpsePoint.GetComponentInChildren<CorpseView>();
            //if (!corpse) corpse = _corpseFactory.SpawnCorpse(_corpsePoint.transform);
            InvokeSystem.StartInvoke(CloseBack, 1f);
            return corpse;

        }
    }
}