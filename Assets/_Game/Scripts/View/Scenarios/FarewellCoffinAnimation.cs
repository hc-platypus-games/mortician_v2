using _Game.Scripts.Systems;
using _Game.Scripts.View.MapPoints;
using UnityEngine;

namespace _Game.Scripts.View.Scenarios
{
    public class FarewellCoffinAnimation : PointBaseView
    {
        [SerializeField] private VisualView[] _visuals;
        [SerializeField] private Transform _coffinPoint;
        
        private Animation _animation;
        public override bool IsAvailable => false;
        public Transform CoffinPoint => _coffinPoint;

        public override void Init()
        {
            base.Init();
            _animation = GetComponentInChildren<Animation>(true);
        }

        public void CloseCoffin()
        {
            _animation.Play("close");
        }

        public void DeactivateCoffin()
        {
            foreach (var visual in _visuals)
            {
                visual.SmoothHide(0f);
            }
            _animation.Play("open");

            InvokeSystem.StartInvoke(ActivateCoffin, 3f);
        }

        private void ActivateCoffin()
        {
            foreach (var visual in _visuals)
            {
                visual.SmoothAppear(1f);
            }
        }
    }
}
