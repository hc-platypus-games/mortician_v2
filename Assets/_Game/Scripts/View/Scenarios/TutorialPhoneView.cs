using _Game.Scripts.Enums;
using _Game.Scripts.Systems;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.Scenarios
{
    public class TutorialPhoneView : BaseView
    {
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private GameCamera _camera;
        
        public void StartAnimation()
        {
            var animation = GetComponent<Animation>();
            animation?.Play("phoneCall");
            InvokeSystem.StartInvoke(EndAnimation, 3f);
        }

        private void EndAnimation()
        {
            _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.PhoneCalled);
        }
    }
}
