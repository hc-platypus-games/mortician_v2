using _Game.Scripts.Factories;
using _Game.Scripts.Pool;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.Fx
{
    public class BaseFx : BaseView, IReinitializable
    {
        public enum FxType
        {
            Money,
            Text,
            ItemUpgrade,
            RoomUpgrade
        }

        [SerializeField] private FxType _type;

        private FxFactory.FxPool _pool;
        
        public class Factory : Factory<FxType, BaseFx>
        {

        }
        
        [Inject]
        public void Construct(FxFactory.FxPool pool)
        {
            _pool = pool;
        }
        
        public void Reinitialize()
        {
            
        }
    }
}
