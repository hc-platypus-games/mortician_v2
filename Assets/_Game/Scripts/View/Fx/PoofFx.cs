using _Game.Scripts.Factories;
using Zenject;

namespace _Game.Scripts.View.Fx
{
    public class PoofFx : BaseView
    {
        public class Pool : MonoMemoryPool<PoofFx>
        {
            protected override void Reinitialize(PoofFx item)
            {
                item.Reset();
            }
        }
    }
}
