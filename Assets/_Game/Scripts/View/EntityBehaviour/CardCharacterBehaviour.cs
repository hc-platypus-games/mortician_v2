using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class CardCharacterBehaviour : EntityBehaviourBase
    {
        private RoomType _roomType;
        
        public CardCharacterBehaviour(RoomType roomType)
        {
            IsRepeatable = true;
            _roomType = roomType;
        }

        public override EntityBehaviourBase Init()
        {
            base.Init();
            Character.OnTouched += DrawTouchReaction;
            return this;
        }

        private void DrawTouchReaction()
        {
            if(Character.IsHidden) return;
            
            Character.DrawEmotion(Random.Range(0, 3) > 1
                ? UIFactory.EmotionType.TouchReaction
                : Character.CurrentEmotion);
        }

        public override void RunNextStep()
        {
            base.RunNextStep();
            MoveToPoint();
        }

        private void MoveToPoint()
        {
            var point = Points.GetRandomPoint(Type.CardCharacter, _roomType);
            if(!point) return;
            Character.GoTo(point.transform, WaitInPoint, 0);
            Character.SetTargetPoint(point);
            if (Character.PointAnimation == 0)
            {
                Character.SetStopDistance(Random.Range(0f, 1f));
            }
        }

        private void WaitInPoint()
        {
            Character.OnReached -= WaitInPoint;
            if (Character.PointAnimation == 0)
            {
                InvokeSystem.StartInvoke(MoveToPoint, Random.Range(3f, 7f));
                return;
            }
            var playAnim = Random.Range(0, 2) > 0;
            if(playAnim) Character.Animate(Character.CharacterPoint.AnimationHash, moveToPoint: true);
            InvokeSystem.StartInvoke(End, Random.Range(3f, 7f));
        }
        
        public override void Destroy()
        {
            Character.OnReached -= WaitInPoint;
        }
    }
}
