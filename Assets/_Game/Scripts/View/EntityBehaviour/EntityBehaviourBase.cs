using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.Vehicle;
using Zenject;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class EntityBehaviourBase
    {
        protected bool IsRepeatable;
        
        protected MapPointsFactory Points;
        
        protected CharacterView Character;
        protected CarView Car;

        protected EntityBehaviourBase()
        {
        }
        
        [Inject]
        private void Construct(MapPointsFactory points)
        {
            Points = points;
        }

        public void AddOwner(CharacterView character)
        {
            Character = character;
        }
        
        public void AddOwner(CarView car)
        {
            Car = car;
        }
        
        public virtual EntityBehaviourBase Init()
        {
            return this;
        }

        public virtual void RunNextStep()
        {
            
        }

        public virtual void Tick(float deltaTime)
        {
            
        }

        public virtual void End()
        {
            if (Character)
            {
                if (IsRepeatable)
                {
                    Character.NextBehaviourStep();
                    return;
                }

                Points.RemoveFromQueue(Character);
                Character.RemoveBehaviour(this);
            }

            if (Car)
            {
                if (IsRepeatable)
                {
                    Init();
                }
                else
                {
                    Car.RemoveBehaviour(this);
                }
            }
        }

        public virtual void Destroy()
        {
            
        }
    }
}
