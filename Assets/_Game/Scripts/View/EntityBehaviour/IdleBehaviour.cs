using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class IdleBehaviour : EntityBehaviourBase
    {
        // можно удалить, не используется
        public IdleBehaviour()
        {
            IsRepeatable = true;
        }

        public override void RunNextStep()
        {
            base.RunNextStep();
            MoveToPoint();
        }

        private void MoveToPoint()
        {
            var point = Points.GetRandomPoint(Type.IdleWalk);
            if(!point) return;
            Character.GoTo(point.transform, WaitInPoint);
            Character.SetTargetPoint(point);
        }

        private void WaitInPoint()
        {
            Character.OnReached -= WaitInPoint;
            var playAnim = Random.Range(0, 2) > 0;
            if(playAnim) Character.Animate(Character.CharacterPoint.AnimationHash, moveToPoint: true);
            InvokeSystem.StartInvoke(End, Random.Range(3f, 7f));
        }
        
        public override void Destroy()
        {
            Character.OnReached -= WaitInPoint;
        }
    }
}
