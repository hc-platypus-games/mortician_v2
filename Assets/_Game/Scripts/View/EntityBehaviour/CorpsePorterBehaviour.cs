using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using _Game.Scripts.View.Vehicle;
using Zenject;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class CorpsePorterBehaviour : EntityBehaviourBase
    {
        [Inject] private CorpseFactory _corpseFactory;
        [Inject] private RoomsFactory _rooms;
        
        private CorpsePorterTransport _transport;
        private PointBaseView _transportPoint;
        private int _id;

        public CharacterView View => Character;
        public int Id => _id;
        
        public CorpsePorterBehaviour(CorpsePorterTransport transport, int id)
        {
            _transport = transport;
            _transport.TransportReset += OnCorpseFound;
            _transport.Complete += OnTransportComplete;
            _id = id;
            IsRepeatable = true;
        }

        public override EntityBehaviourBase Init()
        {
            var point = Points.GetPoint(Type.CorpsePorterIdle, Character.Config.Type, RoomType.None, true);
            Character.Config.Room = RoomType.CorpsePorter;
            Character.SetTargetPoint(point);
            Character.ForceMoveTo(point.transform.position);
            Character.Animate(Character.PointAnimation);
            _transportPoint = _transport.GetComponentsInChildren<PointBaseView>()[_id];
            Character.ShowSkin(0);
            Character.CacheRoom(_rooms.GetRoom(RoomType.Cemetery));
            return base.Init();
        }

        private void OnArrivedToIdle()
        {
            Character.Animate(Character.PointAnimation);
        }

        private void OnCorpseFound()
        {
            if(!_transportPoint) return;
            Points.FreePoint(Character.CharacterPoint);
            Character.SetTargetPoint(_transportPoint);
            Character.GoTo(_transportPoint.transform, OnArrivedToTransport);
        }

        private void OnArrivedToTransport()
        {
            Character.transform.SetParent(_transport.transform);
            Character.ForceMoveTo(Character.CharacterPoint.transform);
            _transport.AddCharacter(this, _id);
        }

        private void OnTransportComplete()
        {
            Character.transform.SetParent(null);
            Points.FreePoint(Character.CharacterPoint);
            var point = Points.GetPoint(Type.CorpsePorterIdle, Character.Config.Type, RoomType.None, true);
            Character.SetTargetPoint(point);
            Character.GoTo(point.transform, OnArrivedToIdle);
        }
    }
}
