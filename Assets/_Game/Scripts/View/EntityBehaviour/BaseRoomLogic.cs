using _Game.Scripts.View.MapPoints.PointsWithLogic;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class BaseRoomLogic : EntityBehaviourBase
    {
        protected BaseRoomPointView Point;
        
        public BaseRoomLogic(BaseRoomPointView pointRoomPointView)
        {
            Point = pointRoomPointView;
        }

        public override void RunNextStep()
        {
            StartRoomCycle();
        }

        protected virtual void StartRoomCycle()
        {
            if (Point == null) Point = Character.CharacterPoint.GetComponent<BaseRoomPointView>();
            Point.Cycle.CompletedEvent -= EndRoomCycle;
            Point.Cycle.CompletedEvent -= End;
            
            Point.Cycle.CompletedEvent += EndRoomCycle;
            Point.Cycle.CompletedEvent += End;
            Point.StartCycle();
            Character.RemoveBubble();
            Character.Animate(Character.PointAnimation);
            Point.ActivateProgressBar(Character.transform.position);
        }

        protected virtual void EndRoomCycle()
        {
            if (Point != null)
            {
                Point.Cycle.CompletedEvent -= EndRoomCycle;
                Point.Cycle.Pause();
                Point.DeactivateProgressBar();
            }
        }

        public override void End()
        {
            if (Point != null)
            {
                Point.Cycle.CompletedEvent -= End;
            }

            base.End();
        }

        public override void Destroy()
        {
            if (Point != null)
            {
                Point.Cycle.CompletedEvent -= EndRoomCycle;
                Point.Cycle.CompletedEvent -= End;
            }

            Character = null;
            Car = null;
            Point = null;
        }
    }
}
