using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using UnityEngine;
using Zenject;
using Random = _Game.Scripts.Tools.Random;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class CemeteryWorkLogic : BaseRoomLogic
    {
        [Inject] private UIFactory _uiFactory;
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private GameFlags _flags;
        [Inject] private GameSystem _game;
        
        private GravePlacePointView _currentGrave;
        private CharacterItemView _shovel;
        private PointBaseView _lastIdlePoint;
        
        public CemeteryWorkLogic(BaseRoomPointView pointRoomPointView) : base(pointRoomPointView)
        {
        }

        public override void RunNextStep()
        {
            StopWaiting();

            _currentGrave ??= Points.GetGravePlace(GravePlacePointView.State.ReadyToWork, false, null);
            if (!_currentGrave)
            {
                StartWaiting();
                return;
            }
            _currentGrave.IsAvailable = false;
            Points.FreePoint(Character.CharacterPoint);
            Points.RemoveFromQueue(Character);
            Character.SetTargetPoint(_currentGrave);
            Character.GoTo(_currentGrave.transform, StartRoomCycle);
            if (!_flags.Has(GameFlag.TutorialFinished))
            {
                Character.ForceMoveTo(_currentGrave.transform.position);
            }
        }

        protected override void StartRoomCycle()
        {
            _shovel ??= Point.GetComponentInChildren<CharacterItemView>();
            if (_shovel)
            {
                var shovelPoint = Character.GetParentFor(_shovel.Key);
                _shovel.SetParent(shovelPoint.transform);
                _shovel.transform.localPosition = Vector3.zero;
                Character.TakeItem(_shovel);
            }
            base.StartRoomCycle();
            Point.UpdateProgressBarPosition(Character.transform.position);
        }

        protected override void EndRoomCycle()
        {
            if(_currentGrave != null) _currentGrave.StartReset();
            base.EndRoomCycle();
            Points.FreePoint(Point);
            if (_shovel) Character.PutItem(_shovel, Point.transform);

            var reward = Point.Item.GetParamValue(GameParamType.Income);
            _game.AddCurrency(GameParamType.Soft, reward, GetCurrencyPlace.Graveyard);
            var position = Character.transform.position;
            Character.ShowMoneyFx(position);
            _uiFactory.SpawnTextFx($"+${(int)reward}", position);
            Character.SetTargetPoint(null);
            _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.CorpseBuried);
        }

        private void StartWaiting()
        {
            var targetPoints = Points.GetAllPoints(Type.CorpsePlace, RoomType.Cemetery);
            foreach (var point in targetPoints)
            {
                point.OnAvailable += RunNextStep;
            }
            MoveToIdlePoint();
        }

        private void StopWaiting()
        {
            var targetPoints = Points.GetAllPoints(Type.CorpsePlace, RoomType.Cemetery);
            foreach (var point in targetPoints)
            {
                point.OnAvailable -= RunNextStep;
            }
            InvokeSystem.CancelInvoke(MoveToIdlePoint);
        }
        
        private void MoveToIdlePoint()
        {
            var point = Points.GetRandomPoint(Type.IdleWalk, RoomType.Cemetery);
            if (!point)
            {
                InvokeSystem.StartInvoke(MoveToIdlePoint, Random.Range(3f, 7f));
                return;
            }

            Points.FreePoint(_lastIdlePoint);
            _lastIdlePoint = point;
            _lastIdlePoint.IsAvailable = false;
            Character.GoTo(_lastIdlePoint.transform, WaitInPoint, 0);
            Character.SetTargetPoint(_lastIdlePoint);
            if (Character.PointAnimation == 0)
            {
                Character.SetStopDistance(Random.Range(0f, 1f));
            }
        }

        private void WaitInPoint()
        {
            Character.OnReached -= WaitInPoint;
            if (Character.PointAnimation == 0)
            {
                InvokeSystem.StartInvoke(MoveToIdlePoint, Random.Range(3f, 7f));
                return;
            }
            var playAnim = Random.Range(0, 2) > 0;
            if(playAnim) Character.Animate(Character.CharacterPoint.AnimationHash, moveToPoint: true);
            InvokeSystem.StartInvoke(MoveToIdlePoint, Random.Range(3f, 7f));
        }
    }
}
