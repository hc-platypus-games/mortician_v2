using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.Vehicle;
using UnityEngine;
using UnityEngine.AI;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class BackgroundCarBehaviour : EntityBehaviourBase
    {
        private NavMeshAgent _navMeshAgent;
        private Transform _carTransform;
        private float _speed;

        private Ray _ray;

        public BackgroundCarBehaviour()
        {
            IsRepeatable = true;
        }

        public override EntityBehaviourBase Init()
        {
            _navMeshAgent = Car.GetComponent<NavMeshAgent>();
            _carTransform = Car.transform.GetChild(0);
            _speed = Car.Speed;
            
            var startPoint = Points.GetRandomPoint(Type.CarSpawn);
            var endPoint = Points.GetRandomPoint(Type.CarSpawn);
            Car.SetSkin(CarView.SkinType.Default);
            Car.ForceMoveToPoint(startPoint.transform.position);
            Car.MoveToRoadLine();
            Car.MoveToPoint(endPoint, End);
            return base.Init();
        }

        public override void Tick(float deltaTime)
        {
            _ray.origin = _carTransform.transform.position;
            _ray.direction = _carTransform.forward;
            if (Physics.Raycast(_ray,
                    0.2f + 0.5f * _speed, GameLayers.OBSTACLES_MASK))
            {
                var newSpeed = Car.Speed - 0.05f;
                if (newSpeed < 0) newSpeed = 0;
                Car.UpdateSpeed(newSpeed);
            }
            else
            {
                Car.UpdateSpeed(_speed);
            }
        }
    }
}
