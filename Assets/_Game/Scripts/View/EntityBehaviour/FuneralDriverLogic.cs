using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.Events;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using _Game.Scripts.View.Vehicle;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class FuneralDriverLogic : BaseRoomLogic
    {
        [Inject] private LevelEventsFactory _levelEventsFactory;
        [Inject] private CorpseFactory _corpseFactory;
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private GameFlags _flags;
        
        private MapEventPointView _currentEventPointView;
        private CarView _car;
        private float _speed = 1f;
        private bool _rotated;

        private bool _storageNeed;
        private PointBaseView _storagePoint;
        
        public FuneralDriverLogic(BaseRoomPointView pointRoomPointView) : base(pointRoomPointView)
        {
            _car = pointRoomPointView.Item.GetComponentInChildren<CarView>(true);
        }

        public override void RunNextStep()
        {
            if (_storageNeed)
            {
                FindStorage();
            }
            else
            {
                FindEvent();
            }
        }

        private void FindEvent()
        {
            _levelEventsFactory.OnEventCalled -= RunNextStep;
            _currentEventPointView = _levelEventsFactory.GetEventFromCalled();
            if (_currentEventPointView == null)
            {
                _levelEventsFactory.OnEventCalled += RunNextStep;
                return;
            }
            MoveToCar();
        }

        private void MoveToCar()
        {
            var point = _car.DriverPoint;
            Character.GoTo(point.transform, MoveToExit);
            if (!_flags.Has(GameFlag.TutorialFinished))
            {
                Character.HideVisual(false);
                Character.ForceMoveTo(point.transform);
            }
        }

        private void MoveToExit()
        {
            Character.HideVisual(false);
            _car.SetTargetPoint(Points.GetPoint(Type.Exit, RoomType.Garage));
            _car.StartTravel();
            _car.OnReached += MoveToEvent;
        }

        private void MoveToEvent()
        {
            _car.OnReached -= MoveToEvent;
            _car.SetTargetPoint(_currentEventPointView);
            _car.StartTravel();
            _car.OnReached += StartRoomCycle;
            _car.MoveToRoadLine(_speed);
        }

        protected override void StartRoomCycle()
        {
            _car.OnReached -= StartRoomCycle;
            if (_car.transform.InverseTransformPoint(_currentEventPointView.transform.position).x < 0)
            {
                _rotated = true;
                _car.transform.DORotate(
                    _car.transform.rotation.eulerAngles - Vector3.up * 180f, 1f / _speed);
                InvokeSystem.StartInvoke(StartEvent, 1f/_speed);
            }
            else
            {
                StartEvent();
            }
        }

        private void StartEvent()
        {
            _currentEventPointView.OnCarArrived(_car);
            _currentEventPointView.OnCompleteAnimation += EndEventAnimation;
        }

        public void EndEventAnimation()
        {
            _currentEventPointView.OnCompleteAnimation -= EndEventAnimation;
            _levelEventsFactory.OnFreeEventPoint(_currentEventPointView);
            MoveToParking();
        }

        private void MoveToParking()
        {
            _car.SetTargetPoint(Points.GetPoint(Type.Enter, RoomType.Garage));
            
            if (!_rotated)
            {
                var seq = DOTween.Sequence();
                seq.Append(_car.transform.DORotate(
                    _car.transform.rotation.eulerAngles - Vector3.up * 179f, 1f / _speed));
                seq.onComplete = StartMovingToParking;
            }
            else
            {
                StartMovingToParking();
            }
        }

        private void StartMovingToParking()
        {
            _car.StartTravel();
            _car.OnReached += MoveToParkingPlace;
        }

        private void MoveToParkingPlace()
        {
            _car.OnReached -= MoveToParkingPlace;
            _car.SetTargetPoint(null);
            _car.MoveToCenter(_speed);
            _car.StartTravel();
            _car.OnReached += ArriveToParking;
        }

        private void ArriveToParking()
        {
            _car.OnReached -= ArriveToParking;
            _car.ResetPosition();
            _car.StopEngine();
            Character.ForceMoveTo(_car.DriverPoint.transform.position);
            Character.ShowVisual(false);
            _storageNeed = true;
            _car.OpenBack();
            RunNextStep();
            _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.CorpseDelivered);
        }

        private bool FindStorage()
        {
            _storagePoint = Points.GetStoragePoint(OnAvailableStorage, RoomType.Garage);
            if (!_storagePoint)
            {
                Character.WaitingFor(UIFactory.EmotionType.WaitTrolley);
                return false;
            }
            MoveToTrolley();
            return true;
        }

        private void OnAvailableStorage(QueueElement queueElement)
        {
            if (FindStorage()) Points.RemoveFromQueue(Character);
        }


        private void MoveToTrolley()
        {
            Character.EndWaiting();
            Character.GoTo(_storagePoint.transform, TakeTrolley);
            Character.SetStopDistance(0.3f);
        }

        private void TakeTrolley()
        {
            Character.TakeItem(_storagePoint.GetComponentInChildren<CharacterItemView>());
            Character.ShowTrolley();
            MoveToTakeCorpse();
        }

        private void MoveToTakeCorpse()
        {
            var point = Points.GetPoint(Type.FarewellPlace, Character.Config.Type, RoomType.None, false, _car.GetComponentsInChildren<PointBaseView>());
            if(!point) return;
            Character.GoTo(point.transform, TakeCorpse);
            Character.SetStopDistance(0.3f);
        }

        private void TakeCorpse()
        {
            var corpseView = _car.GetCorpse();
            Character.TakeItem(corpseView, "trolley");
            InvokeSystem.StartInvoke(MoveToPutCorpse, 0.5f);
        }

        private void MoveToPutCorpse()
        {
            Character.GoTo(_storagePoint.transform, PutCorpse);
            Character.SetStopDistance(0.3f);
        }

        private void PutCorpse()
        {
            Character.HideTrolley();
            Character.PutItem(parent: _storagePoint.transform);
            var corpse = new CorpseObject();
            corpse.AttachTransform(_storagePoint);
            _corpseFactory.AddCorpseToDelivered(corpse);
            End();
            _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.CorpseDelivered);
        }
    }
}
