using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.Room;
using Zenject;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class CorpseOrderVisitorBehaviour : EntityBehaviourBase
    {
        private CharactersFactory _charactersFactory;
        
        private RoomView _mainRoom;
        private bool _orderGot;
        
        [Inject]
        private void Construct(RoomsFactory rooms, CharactersFactory charactersFactory)
        {
            _charactersFactory = charactersFactory;
            
            _mainRoom = rooms.GetRoom(RoomType.Hall);
        }

        public override EntityBehaviourBase Init()
        {
            base.Init();
            Character.Config.Type = CharacterType.Visitor;
            Character.TriggerAction += GetOrderResult;
            Character.ShowSkin(Random.Range(14, 18));
            Character.ShowVisual();
            return this;
        }

        private void GetOrderResult()
        {
            _orderGot = true;
        }

        public override void RunNextStep()
        {
            if (_orderGot)
            {
                MoveToExit();
            }
            else
            {
                MoveToRoom(_mainRoom);
            }
        }

        private void MoveToRoom(RoomView room)
        {
            var point = Points.GetRoomPoint(Character, room);
            if(!point) return;
            Character.SetTargetPoint(point);
            if (point.Type == Type.WorkPlace)
            {
                Character.GoTo(point.transform, Character.GetPointLogic);
            }
            else
            {
                Character.GoTo(point.transform);
            }
        }

        private void MoveToExit()
        {
            InvokeSystem.StartInvoke(DestroyCharacter, 5f);
            Points.FreePoint(Character.CharacterPoint);
            Character.GoTo(Points.GetPoint(Type.Exit).transform);
        }

        private void DestroyCharacter()
        {
            _charactersFactory.RemoveCharacter(Character);
        }
    }
}
