using _Game.Scripts.Enums;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.Vehicle;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class VisitorCarBehaviour : EntityBehaviourBase
    {
        private enum State
        {
            Wait,
            OnParking
        }

        private State _state;
        private VisitorParkingPointView _parkingPointView;
        private float _spawnOffset;

        public VisitorCarBehaviour(int id)
        {
            _spawnOffset = id * 2f;
        }
        
        public override EntityBehaviourBase Init()
        {
            Car.Deactivate();
            _parkingPointView = Car.ParkingPoint as VisitorParkingPointView;
            if (_parkingPointView != null)
            {
                _parkingPointView.CallVisitorsEvent += OnVisitorsCalled;
                _parkingPointView.AllVisitorsReturned += MoveToExit;
            }
            return base.Init();
        }

        private void OnVisitorsCalled()
        {
            if(_state != State.Wait) return;
            _state = State.OnParking;
            InvokeSystem.StartInvoke(Spawn, _spawnOffset);
        }

        private void Spawn()
        {
            var spawn = Points.GetPoint(Type.CarSpawn, RoomType.FuneralVisitorParking);
            if(!spawn) return;
            Car.SetSkin(CarView.SkinType.Default);
            Car.MoveToRoadLine();
            Car.Activate();
            Car.ForceMoveToPoint(spawn.transform.position);
            
            var enter = Points.GetPoint(Type.Enter, RoomType.FuneralVisitorParking);
            if(!enter) return;
            Car.SetTargetPoint(enter);
            Car.StartTravel();
            Car.OnReached += OnEnterParking;
        }

        private void OnEnterParking()
        {
            Car.OnReached -= OnEnterParking;
            Car.MoveToCenter(1f);
            Car.MoveToPoint(_parkingPointView, OnArrivedToParking);
        }

        private void OnArrivedToParking()
        {
            Car.OnReached -= OnArrivedToParking;
            Car.StopEngine();
            Car.ForceMoveToPoint();
            _parkingPointView.CarArrivedEvent?.Invoke();
        }

        private void MoveToExit()
        {
            var exit = Points.GetPoint(Type.Exit, RoomType.FuneralVisitorParking);
            if(!exit) return;
            Car.SetTargetPoint(exit);
            Car.StartTravel();
            InvokeSystem.StartInvoke(Hide, 2f);
        }

        private void Hide()
        {
            Car.Deactivate();
            _state = State.Wait;
        }
    }
}
