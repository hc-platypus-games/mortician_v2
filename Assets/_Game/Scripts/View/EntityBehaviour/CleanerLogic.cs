using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.Clickable;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using UnityEngine;
using Zenject;
using Random = _Game.Scripts.Tools.Random;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class CleanerLogic : BaseRoomLogic
    {
        [Inject] private LevelEventsFactory _levelEvents; 
        
        private int _trashCleaned;
        private ClickableTrashView _selectedTrash;
        private CharacterItemView _trolley;
        private Transform _trolleyPoint;
        private CharacterItemView _toolItem;
        private Transform _toolItemPoint;
        
        public CleanerLogic(BaseRoomPointView pointRoomPointView) : base(pointRoomPointView)
        {
        }

        public override void RunNextStep()
        {
            StopWaiting();
            _selectedTrash = _levelEvents.GetClickableTrash();
            if (_selectedTrash == null)
            {
                StartWaiting();
                return;
            }
            Character.GoTo(_selectedTrash.transform, ArriveToCleaning);
            Character.SetStopDistance(0.6f);
            Character.ShowTrolley();
            if (!_trolley)
            {
                _trolley = Point.GetComponentsInChildren<CharacterItemView>(true).FirstOrDefault(i => i.Key == "trolley");
                if (_trolley)
                {
                    _trolleyPoint = _trolley.transform.parent;
                    Character.TakeItem(_trolley);
                }
            }
        }

        private void StartWaiting()
        {
            MoveToIdlePoint();
        }

        private void StopWaiting()
        {
            InvokeSystem.CancelInvoke(RunNextStep);
        }
        
        private void MoveToIdlePoint()
        {
            var point = Points.GetRandomPoint(Type.IdleWalk, RoomType.Cleaner);
            Character.GoTo(point.transform, WaitInPoint, 0);
            Character.SetTargetPoint(point);
        }

        private void WaitInPoint()
        {
            Character.OnReached -= WaitInPoint;
            var playAnim = Random.Range(0, 2) > 0;
            if(playAnim) Character.Animate(Character.CharacterPoint.AnimationHash, moveToPoint: true);
            InvokeSystem.StartInvoke(RunNextStep, Random.Range(3f, 7f));
        }

        private void ArriveToCleaning()
        {
            var point = _selectedTrash.GetComponentInChildren<PointBaseView>();
            Character.PutItem(_trolley);
            _toolItem ??= _trolley.GetComponentsInChildren<CharacterItemView>(true).FirstOrDefault(i => i.Key == "left hand");
            if (_toolItem)
            {
                _toolItemPoint = _toolItem.transform.parent;
                Character.TakeItem(_toolItem);
            }

            Character.HideTrolley();
            Character.SetTargetPoint(point);
            Character.GoTo(point.transform, StartRoomCycle);
        }

        protected override void StartRoomCycle()
        {
            base.StartRoomCycle();
            Point.Cycle.CompletedEvent -= End;
        }

        protected override void EndRoomCycle()
        {
            base.EndRoomCycle();
            _selectedTrash.Clean(false);
            if (_toolItem)
            {
                Character.PutItem(_toolItem, _toolItemPoint);
            }

            Character.ShowTrolley();
            Character.TakeItem(_trolley);
            
            _trashCleaned++;
            if (_trashCleaned < 3)
            {
                RunNextStep();
                return;
            }
            
            Character.GoTo(Point.transform, End);
        }

        public override void End()
        {
            Character.HideTrolley();
            Character.PutItem(_trolley, _trolleyPoint);
            Points.FreePoint(Point);
            StopWaiting();
            base.End();
        }
    }
}
