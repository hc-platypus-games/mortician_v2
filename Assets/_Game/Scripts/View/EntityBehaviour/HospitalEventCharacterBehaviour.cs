using System;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Systems;
using _Game.Scripts.View.Corpse;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.Vehicle;
using Zenject;
using Type = _Game.Scripts.View.MapPoints.Type;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class HospitalEventCharacterBehaviour : EntityBehaviourBase
    {
        public Action OnComplete;
        
        [Inject] private CorpseFactory _corpseFactory;

        private PointBaseView _startPoint;
        private CarView _car;
        private CorpseView _corpseView;
        
        public void StartEvent(CarView car)
        {
            _startPoint = Character.CharacterPoint;
            _car = car;
            _corpseView = _corpseFactory.SpawnCorpse(Character.transform);
            _corpseView.ShowSkin(0);
            Character.TakeItem(_corpseView);
            Character.ShowTrolley();
            MoveToCar();
        }

        private void MoveToCar()
        {
            var point = Points.GetPoint(Type.FarewellPlace, Character.Config.Type, RoomType.None, false, 
                _car.GetComponentsInChildren<PointBaseView>(true));
            Character.GoTo(point.transform, OpenCar);
        }

        private void OpenCar()
        {
            _car.OpenBack();
            InvokeSystem.StartInvoke(PutCorpse, 0.5f);
        }

        private void PutCorpse()
        {
            var point = Points.GetPoint(Type.CarCorpsePlace, Character.Config.Type, RoomType.None, false, 
                _car.GetComponentsInChildren<PointBaseView>(true));
            Character.PutItem(_corpseView, point.transform);
            InvokeSystem.StartInvoke(CompleteEvent, 0.5f);
        }

        private void CompleteEvent()
        {
            _car.CloseBack();
            OnComplete?.Invoke();
            Return();
        }

        private void Return()
        {
            Character.GoTo(_startPoint.transform);
        }
    }
}
