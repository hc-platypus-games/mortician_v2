using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;
using UnityEngine.AI;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class BuilderWorkerBehaviour : EntityBehaviourBase
    {
        private readonly PointBaseView[] _points;
        private PointBaseView _lastIdlePoint;
        private int _navMashId;
        
        public BuilderWorkerBehaviour(PointBaseView[] points, int navmeshAgentId)
        {
            _points = points;
            IsRepeatable = true;
            _navMashId = navmeshAgentId;
        }

        public override void RunNextStep()
        {
            Character.GetComponent<NavMeshAgent>().agentTypeID = _navMashId;
            Character.ShowSkin(24);
            base.RunNextStep();
            WorkInPoint();
        }

        private void MoveToPoint()
        {
            var point = Points.GetRandomPoint(Type.WorkPlace, true, _points);
            if (!point)
            {
                InvokeSystem.StartInvoke(MoveToPoint, Random.Range(3f, 7f));
                return;
            }
            
            Points.FreePoint(_lastIdlePoint);
            _lastIdlePoint = point;
            _lastIdlePoint.IsAvailable = false;
            Character.GoTo(_lastIdlePoint.transform, WorkInPoint);
            Character.SetTargetPoint(_lastIdlePoint);
            Character.SetStopDistance(0.05f);
            if (Character.PointAnimation == 0)
            {
                Character.SetStopDistance(Random.Range(0f, 1f));
            }
        }

        private void WorkInPoint()
        {
            Character.OnReached -= WorkInPoint;
            if (Character.PointAnimation == 0)
            {
                InvokeSystem.StartInvoke(MoveToPoint, Random.Range(3f, 7f));
                return;
            }
            if(Character.CharacterPoint) Character.transform.LookAt(Character.CharacterPoint.transform);
            Character.Animate(Character.PointAnimation, moveToPoint: false);
            InvokeSystem.StartInvoke(MoveToPoint, Random.Range(3f, 7f));
        }
        
        public override void Destroy()
        {
            InvokeSystem.CancelInvoke(MoveToPoint, true);
            Character.OnReached -= WorkInPoint;
            base.Destroy();
        }
    }
}
