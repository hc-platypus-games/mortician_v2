using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Systems;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.Corpse;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using Zenject;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class FarewellRoomLogic : BaseRoomLogic
    {
        [Inject] private CorpseFactory _corpseFactory;

        private CorpseObject _currentCorpse;
        private CharacterItemView _trolley;
        private CharacterItemView _book;
        private CharacterItemView _corpseView;
        
        public FarewellRoomLogic(BaseRoomPointView pointRoomPointView) : base(pointRoomPointView)
        {
        }
        
        public override void RunNextStep()
        {
            _corpseFactory.EmbalmedCorpseAdded -= RunNextStep;
            _currentCorpse = _corpseFactory.GetCorpseFromEmbalmed();
            if (_currentCorpse != null)
            {
                MoveToGetCorpse();
            }
            else
            {
                Character.WaitingFor(UIFactory.EmotionType.WaitStorage);
                _corpseFactory.EmbalmedCorpseAdded += RunNextStep;
            }
        }

        private void MoveToGetCorpse()
        {
            Character.EndWaiting();
            var point = _currentCorpse.AttachedPoint;
            Character.GoTo(point.transform, OpenStorage);
            Character.SetStopDistance(0.3f);
            Character.ShowTrolley();
            _trolley = Point.GetComponentsInChildren<CharacterItemView>(true).FirstOrDefault(i => i.Key == "trolley");
            Character.TakeItem(_trolley);
        }

        private void OpenStorage()
        {
            var storage = (CorpseStorageView)_currentCorpse.AttachedPoint;
            storage.Open();
            InvokeSystem.StartInvoke(GetCorpse, 0.5f);
        }

        private void GetCorpse()
        {
            var storage = (CorpseStorageView)_currentCorpse.AttachedPoint;
            _corpseView = storage.GetComponentInChildren<CorpseView>();
            Character.TakeItem(_corpseView);
            Points.FreePoint(_currentCorpse.AttachedPoint);
            InvokeSystem.StartInvoke(storage.Close, 0.5f);
            InvokeSystem.StartInvoke(MoveToStartCycle, 0.5f);
        }

        private void MoveToStartCycle()
        {
            Character.GoTo(Character.CharacterPoint.transform, StartFarewell);
        }

        private void StartFarewell()
        {            
            Character.HideTrolley();
            var points = Point.GetComponentsInChildren<PointBaseView>(true);
            var corpsePoint = Points.GetPoint(Type.EmbalmingCorpsePlace, Character.Config.Type, RoomType.None, false, points);
            Character.PutItem(_corpseView, corpsePoint.transform);
            var trolleyPoint = Points.GetPoint(Type.ItemPlace, Character.Config.Type, RoomType.None, false, points);
            Character.PutItem(_trolley, trolleyPoint.transform);
            _book = Point.GetComponentsInChildren<CharacterItemView>(true).FirstOrDefault(i => i.Key == "hand");
            Character.TakeItem(_book);
            StartRoomCycle();
        }

        protected override void EndRoomCycle()
        {
            var points = Point.GetComponentsInChildren<PointBaseView>();
            var corpsePoint = Points.GetPoint(Type.EmbalmingCorpsePlace, Character.Config.Type, RoomType.None, false, points);
            var trolleyPoint = Points.GetPoint(Type.ItemPlace, Character.Config.Type, RoomType.None, false, points);
            Character.PutItem(_book, trolleyPoint.transform);
            _currentCorpse.AttachTransform(corpsePoint);
            ((FarewellWorkPointView) Character.CharacterPoint).AddCorpse(_currentCorpse);
            base.EndRoomCycle();
        }
    }
}
