using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.Systems;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.Corpse;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using _Game.Scripts.View.Room;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class EmbalmingLogic : BaseRoomLogic
    {
        [Inject] private CorpseFactory _corpseFactory;
        [Inject] private AppEventProvider _eventProvider;

        private Transform _deliveredCorpsePoint;
        private CorpseObject _currentCorpse;
        private CharacterItemView _trolley;
        private CharacterItemView _corpseView;
        private CorpseStorageView _storagePoint;

        private bool _corpseEmbalmed;
        
        public EmbalmingLogic(BaseRoomPointView pointRoomPointView) : base(pointRoomPointView)
        {
        }

        public override void RunNextStep()
        {
            if (_corpseEmbalmed)
            {
                FindStorage();
            }
            else
            {
                FindCorpse();
            }
        }

        private void FindCorpse()
        {
            _corpseFactory.DeliveredCorpseAdded -= RunNextStep;
            _currentCorpse = _corpseFactory.GetCorpseFromDelivered();
            if (_currentCorpse != null)
            {
                MoveToGetCorpse();
            }
            else
            {
                _corpseFactory.DeliveredCorpseAdded += RunNextStep;
                Character.WaitingFor(UIFactory.EmotionType.WaitCorpse);
            }
        }

        private void MoveToGetCorpse()
        {
            Character.EndWaiting();
            _deliveredCorpsePoint = _currentCorpse.AttachedPoint.transform;
            Character.ShowTrolley();
            _trolley = Point.GetComponentInChildren<CharacterItemView>();
            Character.TakeItem(_trolley);
            Character.GoTo(_deliveredCorpsePoint, GetCorpse);
            Character.SetStopDistance(0.4f);
        }

        private void GetCorpse()
        {
            _corpseView = _deliveredCorpsePoint.GetComponentInChildren<CorpseView>();
            Character.TakeItem(_corpseView);
            Points.FreePoint(_deliveredCorpsePoint.GetComponent<PointBaseView>());
            MoveToEmbalm();
        }

        private void MoveToEmbalm()
        {
            Character.GoTo(Character.CharacterPoint.transform, Embalm);
        }

        private void Embalm()
        {
            Character.HideTrolley();
            var points = Point.GetComponentsInChildren<PointBaseView>();
            var corpsePoint = Points.GetPoint(Type.EmbalmingCorpsePlace, RoomType.Embalming, false, points);
            _corpseView = Character.CurrentItems[1];
            Character.PutItem(1, corpsePoint.transform);
            _trolley = Character.CurrentItems[0];
            var trolleyPoint = Points.GetPoint(Type.ItemPlace, Character.Config.Type, RoomType.None, false, points);
            Character.PutItem(0, trolleyPoint.transform);
            StartRoomCycle();
            Point.Cycle.CompletedEvent -= End;
        }

        protected override void EndRoomCycle()
        {
            base.EndRoomCycle();
            Character.Animate();
            _corpseEmbalmed = true;
            FindStorage();
            InvokeSystem.StartInvoke(TriggerEvent, 0.5f);
        }
        
        private bool FindStorage()
        {
            _storagePoint = Points.GetStoragePoint(OnAvailableStorage, RoomType.Embalming) as CorpseStorageView;
            if (!_storagePoint)
            {
                Character.WaitingFor(UIFactory.EmotionType.WaitStorage);
                return false;
            }
            MoveToPutCorpse();
            return true;
        }

        private void OnAvailableStorage(QueueElement queueElement)
        {
            if (FindStorage()) Points.RemoveFromQueue(Character);
        }

        private void MoveToPutCorpse()
        {
            Character.EndWaiting();
            Character.ShowTrolley();
            Character.TakeItem(_trolley);
            Character.TakeItem(_corpseView);
            InvokeSystem.StartInvoke(MoveToStorage, 0.5f);
        }

        private void MoveToStorage()
        {
            _currentCorpse.AttachTransform(_storagePoint);
            Character.GoTo(_storagePoint.transform, OpenStorage);
            Character.SetStopDistance(0.3f);
        }

        private void OpenStorage()
        {
            _storagePoint.Open();
            InvokeSystem.StartInvoke(PuCorpse, 0.3f);
        }

        private void PuCorpse()
        {
            _corpseFactory.DeliveredCorpses.Remove(_currentCorpse);
            _corpseFactory.AddCorpseToEmbalmed(_currentCorpse);
            Character.PutItem(_corpseView, _storagePoint.transform);
            MoveReturnTrolley();
            
            InvokeSystem.StartInvoke(_storagePoint.Close, 0.5f);
            InvokeSystem.StartInvoke(TriggerEvent, 1f);
        }

        private void TriggerEvent()
        {
            _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.CorpseEmbalmed);
        }

        private void MoveReturnTrolley()
        {
            Character.GoTo(Point.transform, ReturnTrolley);
        }

        private void ReturnTrolley()
        {
            Character.HideTrolley();
            Character.PutItem(_trolley, Points.GetPoint(Type.ItemPlace, Character.Config.Type, RoomType.None, false, 
                Point.GetComponentsInChildren<PointBaseView>()).transform);
            End();
        }
    }
}
