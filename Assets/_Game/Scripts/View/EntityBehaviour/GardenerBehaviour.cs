using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using Zenject;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class GardenerLogic : BaseRoomLogic
    {
        [Inject] private GameSystem _game;
        [Inject] private UIFactory _uiFactory;

        private int _maxGraves = 3;
        
        private List<GravePlacePointView> _graves = new ();
        
        private GravePlacePointView _currentGrave;
        private PointBaseView _lastIdlePoint;
        
        public GardenerLogic(BaseRoomPointView pointRoomPointView) : base(pointRoomPointView)
        {
        }
        public override void RunNextStep()
        {
            StopWaiting();

            _currentGrave = Points.GetGravePlace(GravePlacePointView.State.Reset, true, null, _graves);
            if (!_currentGrave)
            {
                StartWaiting();
                return;
            }
            Points.FreePoint(Character.CharacterPoint);
            Points.RemoveFromQueue(Character);
            var point = Points.GetPoint(Type.WorkPlace, CharacterType.Gardener, points: _currentGrave.GetComponentsInChildren<PointBaseView>());
            Character.SetTargetPoint(point);
            Character.GoTo(point.transform, StartRoomCycle);
        }

        protected override void StartRoomCycle()
        {
            base.StartRoomCycle();
            _currentGrave.UpdateProgressBarPosition(Character.transform.position);
        }

        protected override void EndRoomCycle()
        {
            _currentGrave.GraveCare();
            base.EndRoomCycle();
        }

        public override void End()
        {
            Points.FreePoint(Point);
            Points.FreePoint(_currentGrave);
            _graves.Add(_currentGrave);
            var reward = Point.Item.GetParamValue(GameParamType.Income);
            _game.AddCurrency(GameParamType.Soft, reward, GetCurrencyPlace.Gardener);
            var position = Character.transform.position;
            Character.ShowMoneyFx(position);
            _uiFactory.SpawnTextFx($"+${(int)reward}", position);
            
            if (_graves.Count < _maxGraves)
            {
                RunNextStep();
                return;
            }
            
            var targetPoints = Points.GetAllPoints(Type.CorpsePlace, RoomType.Cemetery);
            foreach (var point in targetPoints)
            {
                point.OnAvailable -= RunNextStep;
            }
            base.End();
        }

        private void StartWaiting()
        {
            var targetPoints = Points.GetAllPoints(Type.CorpsePlace, RoomType.Cemetery);
            foreach (var point in targetPoints)
            {
                point.OnAvailable += RunNextStep;
            }
            MoveToIdlePoint();
        }

        private void StopWaiting()
        {
            var targetPoints = Points.GetAllPoints(Type.CorpsePlace, RoomType.Cemetery);
            foreach (var point in targetPoints)
            {
                point.OnAvailable -= RunNextStep;
            }
            InvokeSystem.CancelInvoke(MoveToIdlePoint);
        }
        
        private void MoveToIdlePoint()
        {
            var point = Points.GetRandomPoint(Type.IdleWalk, RoomType.Cemetery);
            if (!point)
            {
                InvokeSystem.StartInvoke(MoveToIdlePoint, Random.Range(3f, 7f));
                return;
            }

            Points.FreePoint(_lastIdlePoint);
            _lastIdlePoint = point;
            _lastIdlePoint.IsAvailable = false;
            Character.GoTo(_lastIdlePoint.transform, WaitInPoint, 0);
            Character.SetTargetPoint(_lastIdlePoint);
            if (Character.PointAnimation == 0)
            {
                Character.SetStopDistance(Random.Range(0f, 1f));
            }
        }

        private void WaitInPoint()
        {
            Character.OnReached -= WaitInPoint;
            if (Character.PointAnimation == 0)
            {
                InvokeSystem.StartInvoke(MoveToIdlePoint, Random.Range(3f, 7f));
                return;
            }
            var playAnim = Random.Range(0, 2) > 0;
            if(playAnim) Character.Animate(Character.CharacterPoint.AnimationHash, moveToPoint: true);
            InvokeSystem.StartInvoke(MoveToIdlePoint, Random.Range(3f, 7f));
        }
    }
}
