using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.Room;
using Zenject;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class WorkerBehaviour : EntityBehaviourBase
    {
        private bool _isWork = true;
        private readonly RoomType _mainRoomType;
        private RoomView _mainRoom;
        private RoomView[] _serviceRooms;
        private int _currentServiceRoom;

        public WorkerBehaviour(RoomType mainRoomType)
        {
            _mainRoomType = mainRoomType;
        }

        [Inject]
        private void Construct(RoomsFactory rooms)
        {
             _mainRoom = rooms.GetRoom(_mainRoomType);
             _serviceRooms = rooms.GetRooms(RoomCategory.Service);
        }

        public override EntityBehaviourBase Init()
        {
            Character.Config.Room = _mainRoom.Type;
            Character.OnTouched += DrawTouchReaction;
            return base.Init();
        }

        private void DrawTouchReaction()
        {
            if(Character.IsHidden) return;
            
            Character.DrawEmotion(Random.Range(0, 3) > 1
                ? UIFactory.EmotionType.TouchReaction
                : Character.CurrentEmotion);
        }

        public override void RunNextStep()
        {
            if (_isWork)
            {
                MoveToRoom(_mainRoom);
            }
            else
            {
                MoveToRoom(_serviceRooms[_currentServiceRoom]);
            }
        }

        private void MoveToRoom(RoomView room)
        {
            if (Character.debug)
            {
                        
            }
            var point = Points.GetRoomPoint(Character, room);
            if(!point) return;
            Character.SetTargetPoint(point);
            if (point.Type == Type.WorkPlace)
            {
                Character.GoTo(point.transform, Character.GetPointLogic, _isWork ? 2 : 1);
                if (!_isWork)
                {
                    _currentServiceRoom++;
                    if (_currentServiceRoom >= _serviceRooms.Length) _currentServiceRoom = 0;
                }
                _isWork = !_isWork;
            }
            else
            {
                Character.GoTo(point.transform, Character.WaitingInQueue, 1);
            }
        }
    }
}
