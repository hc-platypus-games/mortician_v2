using System;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Systems;
using _Game.Scripts.View.Corpse;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.Vehicle;
using DG.Tweening;
using UnityEngine;
using Zenject;
using Type = _Game.Scripts.View.MapPoints.Type;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class ElectricianEventCharacterBehaviour : EntityBehaviourBase
    {
        public Action WorkBroke, AnimationEnd, OnComplete;
        
        [Inject] private CorpseFactory _corpseFactory;

        private PointBaseView[] _eventPoints;
        private CarView _car;
        private CorpseView _corpseView;

        private static readonly int _explosion = Animator.StringToHash("explosion");

        public void SetPoints(PointBaseView[] points)
        {
            _eventPoints = points;
        }

        public void Spawn()
        {
            var spawnPoint = Points.GetPoint(Type.Enter, CharacterType.Default, RoomType.None, false, _eventPoints);
            var workPoint = Points.GetPoint(Type.WorkPlace, CharacterType.Default, RoomType.None, false, _eventPoints);
            if(!workPoint) return;
            if(!spawnPoint) return;
            
            Character.ShowSkin(24);
            Character.ShowVisual();
            Character.ForceMoveTo(spawnPoint.transform.position);
            Character.SetTargetPoint(workPoint);
            Character.GoTo(workPoint.transform, StartWork);
        }

        private void StartWork()
        {
            Character.Animate(Character.PointAnimation);
            InvokeSystem.StartInvoke(BreakWork, 1f);
        }

        private void BreakWork()
        {
            Character.Animate(_explosion);
            Character.transform.DOLocalMoveX(3.63f, 1f);
            Character.transform.DOLocalMoveY(0f, 0.8f).OnComplete(EndAnimation);
            WorkBroke?.Invoke();
        }

        private void EndAnimation()
        {
            Character.ShowSkin(25);
            AnimationEnd?.Invoke();
        }

        public void OnCarArrived(CarView car)
        {
            var spawnPoint = Points.GetPoint(Type.CorpsePlace, CharacterType.Default, RoomType.None, false, _eventPoints);
            if (!spawnPoint)
            {
                OnComplete?.Invoke();
                return;
            }

            _car = car;
            Character.HideVisual(false);
            _car.OpenBack();
            _corpseView = spawnPoint.GetComponentInChildren<CorpseView>();
            _corpseView ??= _corpseFactory.SpawnCorpse(spawnPoint.transform);
            _corpseView.ShowSkin(0);
            InvokeSystem.StartInvoke(PutCorpse, 0.5f);
        }

        private void PutCorpse()
        {
            var corpsePoint = Points.GetPoint(Type.CarCorpsePlace, Character.Config.Type, RoomType.None, false, 
                _car.GetComponentsInChildren<PointBaseView>(true));
            _corpseView.transform.SetParent(corpsePoint.transform);
            _corpseView.transform.DOLocalMove(Vector3.zero, 0.5f);
            _corpseView.transform.DOLocalRotate(Vector3.zero, 0.5f);
            _corpseView.transform.DOScale(Vector3.one, 0.5f);
            InvokeSystem.StartInvoke(Complete, 0.5f);
        }

        private void Complete()
        {
            _car.CloseBack();
            OnComplete?.Invoke();
        }
    }
}
