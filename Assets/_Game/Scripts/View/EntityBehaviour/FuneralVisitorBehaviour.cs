using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.Room;
using _Game.Scripts.View.Vehicle;
using UnityEngine;
using Zenject;
using Random = _Game.Scripts.Tools.Random;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class FuneralVisitorBehaviour : EntityBehaviourBase
    {
        private enum State
        {
            Waiting,
            MovingToFarewell,
            Farewell,
            Cemetery,
            MovingToParking,
        }

        [Inject] private RoomsFactory _rooms;
        [Inject] private Prefabs _prefabs;

        private LevelSystem _levelSystem;

        private readonly PointBaseView _startPoint;
        private State _state;
        private CorpsePorterTransport _corpsePorter;
        private GravePlacePointView _currentGrave;
        private GameObject _cryFx;
        
        protected internal FuneralVisitorBehaviour(PointBaseView startPoint)
        {
            IsRepeatable = true;
            _startPoint = startPoint;
        }
        
        [Inject]
        private void Construct(LevelSystem levelSystem)
        {
            _levelSystem = levelSystem;
        }

        public override EntityBehaviourBase Init()
        {
            _corpsePorter = _levelSystem.CurrentLevel.GetComponentInChildren<CorpsePorterTransport>(true);
            _corpsePorter.GravePlaceFound += CallToCemetery;
            Character.Config.Type = CharacterType.Visitor;
            Character.HideVisual();
            Character.ForceMoveTo(_startPoint.transform.position);
            
            var parking = _startPoint.GetComponentInParent<RoomItemView>()
                ?.GetComponentInChildren<VisitorParkingPointView>();
            if (parking != null)
            {
                parking.CarArrivedEvent += OnCarArrived;
            }

            var cryPoint = Character.GetParentFor("cry");
            if (cryPoint)
            {
                _cryFx = _prefabs.CopyPrefab<CryFxView>(cryPoint.transform).gameObject;
                _cryFx.Deactivate();
                _cryFx.transform.localPosition = Vector3.zero;
                _cryFx.transform.localRotation = Quaternion.identity;
                _cryFx.transform.localScale = Vector3.one * 0.35f;
            }
            return base.Init();
        }

        public override void RunNextStep()
        {
            switch (_state)
            {
                case State.MovingToFarewell:
                    GoFarewell();
                    break;
                case State.Farewell:
                    GoToCemetery();
                    break;
                case State.Cemetery:
                    FindGrave();
                    break;
            }
        }

        private void MoveToRoom(RoomView room)
        {
            var point = Points.GetRoomPoint(Character, room);
            if(!point) return;
            Character.SetTargetPoint(point);
            if (point.Type == Type.WorkPlace && point.RoomType == RoomType.Farewell)
            {
                Character.GoTo(point.transform, Farewell);
            }
            else
            {
                Character.GoTo(point.transform);
            }
        }

        private void OnCarArrived()
        {
            if(_state != State.Waiting) return;
            Character.ShowSkin(Random.Range(9, 14));
            Character.ShowVisual();
            _state = State.MovingToFarewell;
            GoPayParking();
        }

        private void GoPayParking()
        {
            var point = Points.GetPoint(Type.PayParking, _startPoint.RoomType);
            if (!point || point.IsAvailable == false)
            {
                GoTakeFlowers();
                return;
            }

            Character.GoTo(point.transform, PayParking);
        }

        private void PayParking()
        {
            GoTakeFlowers();
        }

        private void GoTakeFlowers()
        {
            var point = Points.GetPoint(Type.TakeFlowers, _startPoint.RoomType);
            if (!point || point.IsAvailable == false)
            {
                GoFarewell();
                return;
            }

            Character.GoTo(point.transform, TakeFlowers);
        }

        private void TakeFlowers()
        {
            GoFarewell();
        }

        private void GoFarewell()
        {
            _state = State.MovingToFarewell;
            var room = _rooms.GetRoom(RoomType.Farewell);
            var farewellIsExist = room.Level > 0;
            if (farewellIsExist)
            {
                MoveToRoom(room);
            }
            else
            {
                _state = State.Farewell;
                GoToCemetery();
            }
        }

        private void Farewell()
        {
            _state = State.Farewell;
            Character.Animate(Character.PointAnimation);
            _cryFx.Activate();
        }

        private void StayInQueue()
        {
            _state = State.Farewell;
        }

        private void CallToCemetery()
        {
            if(_state != State.Farewell) return;
            _state = State.Cemetery;
            _currentGrave = _corpsePorter.GravePlacePointView;
            Points.RemoveFromQueue(Character);
            var point = Points.GetPoint(Type.WorkPlace, Character.Config.Type, RoomType.Cemetery, true,
                _currentGrave.GetComponentsInChildren<PointBaseView>());
            if(!point) return;
            MoveToGrave(point);
        }

        private void GoToCemetery()
        {
            if(_state != State.Farewell) return;
            
            _cryFx.Deactivate();
            _state = State.Cemetery;
            var point = Points.GetPoint(Type.Queue, RoomType.Cemetery);
            Character.GoTo(point.transform, FindGrave);
            Character.SetStopDistance(1f);
        }

        private void FindGrave()
        {
            if(_state != State.Cemetery) return;
            Points.RemoveFromQueue(Character);
            var point = Points.GetRoomPoint(Character, _rooms.GetRoom(RoomType.Cemetery));
            if(point.Type == Type.Queue)
            {
                Points.FreePoint(Character.CharacterPoint);
                Character.SetTargetPoint(point);
                Character.GoTo(point.transform, StayInQueue);
                return;
            }

            MoveToGrave(point);
        }

        private void MoveToGrave(PointBaseView point)
        {
            Points.FreePoint(Character.CharacterPoint);
            _state = State.Cemetery;
            Character.SetTargetPoint(point);
            Character.GoTo(point.transform, WaitForEndBury);
        }

        private void WaitForEndBury()
        {
            _cryFx.Activate();
            Character.Animate(Character.PointAnimation);
            _currentGrave = Character.CharacterPoint.GetComponentInParent<GravePlacePointView>();
            _currentGrave.CycleEnded += MoveToCar;
        }

        private void MoveToCar()
        {
            _currentGrave.CycleEnded -= MoveToCar;
            _cryFx.Deactivate();
            _state = State.MovingToParking;
            Character.SetTargetPoint(_startPoint);
            Character.GoTo(_startPoint.transform, ArriveToCar);
        }

        private void ArriveToCar()
        {
            Character.HideVisual();
            var parking = _startPoint.GetComponentInParent<RoomItemView>()
                ?.GetComponentInChildren<VisitorParkingPointView>();
            if (parking != null)
            {
                parking.AddCharacter();
            }

            _state = State.Waiting;
            End();
        }
    }
}
