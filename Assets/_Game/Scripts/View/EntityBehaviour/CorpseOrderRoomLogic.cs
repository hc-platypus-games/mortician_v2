using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Systems;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.Corpse;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using Zenject;
using Random = _Game.Scripts.Tools.Random;
using Type = _Game.Scripts.View.MapPoints.Type;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class CorpseOrderRoomLogic : BaseRoomLogic
    {
        private enum State
        {
            None,
            FindOrder,
            FindCorpsePlace,
            FindCorpse,
            Working
        }
        
        [Inject] private CorpseFactory _corpseFactory;
        
        private CorpseOrder _corpseOrder;
        private CorpseObject _corpse;
        private CharacterItemView _trolley;
        private CorpseView _corpseView;
        private RoomType _roomType;
        private State _state;
        private CorpseOfferCyclePointView _cyclePointView;
        private CharacterView _orderClient;
        private CremationItemView _cremationView;
        
        public CorpseOrderRoomLogic(BaseRoomPointView pointRoomPointView, RoomType room) : base(pointRoomPointView)
        {
            _roomType = room;
            _state = State.FindOrder;
        }

        public override void RunNextStep()
        {
            switch (_state)
            {
                case State.None:
                case State.FindOrder:
                    _corpseOrder = _corpseFactory.GetCorpseOrder(_roomType);
                    if (_corpseOrder == null)
                    {
                        switch (_roomType)
                        {
                            case RoomType.Cremation:
                                _corpseFactory.CremationOrderAdded += OnOfferAdded;
                                break;
                            
                            case RoomType.Cryogenic:
                                _corpseFactory.CryogenicOrderAdded += OnOfferAdded;
                                break;
                        }
                        return;
                    }

                    _state = State.FindCorpsePlace;
                    RunNextStep();
                    break;
                
                case State.FindCorpsePlace:
                    StopWaiting();
                    var point = Points.GetOrderCorpsePlace(Character, _roomType);
                    if (!point)
                    {
                        StartWaiting();
                        if (_corpseOrder.State == CorpseOrderState.Preparing)
                        {
                            Character.WaitingFor(UIFactory.EmotionType.WaitStorage);
                        }
                        return;
                    }
                    Character.EndWaiting();
                    Point = point;
                    _cyclePointView = point as CorpseOfferCyclePointView;
                    if (_cyclePointView.IsFull)
                    {
                        _state = State.Working;
                        MoveToClear();
                        return;
                    }
                    _state = State.FindCorpse;
                    RunNextStep();
                    break;
                
                case State.FindCorpse:
                    StopWaiting();
                    _corpseFactory.CremationOrderAdded -= OnOfferAdded;
                    _corpseFactory.CryogenicOrderAdded -= OnOfferAdded;
                    _corpseFactory.EmbalmedCorpseAdded -= RunNextStep;
                    _corpse = _corpseFactory.GetCorpseFromEmbalmed();
                    if (_corpse != null)
                    {
                        MoveToGetCorpse();
                    }
                    else
                    {
                        _corpseFactory.EmbalmedCorpseAdded += RunNextStep;
                        StartWaiting();
                    }
                    break;
                case State.Working:
                    break;
            }
        }

        private void MoveToClear()
        {
            Character.GoTo(_cyclePointView.transform, OpenItemToGetCorpse);
            if (_roomType == RoomType.Cryogenic) Character.SetStopDistance(0.4f);
        }

        private void OpenItemToGetCorpse()
        {
            Character.ShowTrolley();
            var points = Point.Item.GetComponentsInChildren<PointBaseView>();
            _cremationView = Points.GetPoint(Type.EmbalmingCorpsePlace, Character.Config.Type, _roomType, false, points) as CremationItemView;
            _cremationView.EndCremation();
            _cremationView.Open();
            var trolleyPoint =
                Points.GetPoint(Type.ItemPlace, points: _cyclePointView.GetComponentsInChildren<PointBaseView>());
            _trolley = trolleyPoint.GetComponentInChildren<CharacterItemView>();
            _corpseView = _cremationView.GetComponentInChildren<CorpseView>();
            switch (_roomType)
            {
                case RoomType.Cremation:
                    _corpseView.ShowSkin(2);
                    break;
                            
                case RoomType.Cryogenic:
                    Character.TakeItem(_trolley);
                    _corpseView.ShowSkin(1);
                    break;
            }

            InvokeSystem.StartInvoke(GetCorpseFromCyclePoint, 0.5f);
        }

        private void GetCorpseFromCyclePoint()
        {
            Character.TakeItem(_corpseView);
            Points.FreePoint(_cyclePointView);
            _orderClient = _cyclePointView.Clear();
            InvokeSystem.StartInvoke(GoToClient, 0.5f);
            InvokeSystem.StartInvoke(GoToClient, 10f);
        }

        private void GoToClient()
        {
            _cremationView.Close();
            Character.GoTo(_orderClient.transform, GiveOrder);
            Character.SetStopDistance(0.25f);
        }

        private void GiveOrder()
        {
            Character.HideTrolley();
            var trolleyPoint =
                Points.GetPoint(Type.ItemPlace, points: _cyclePointView.GetComponentsInChildren<PointBaseView>());
            Character.PutItem(_trolley, trolleyPoint.transform);
            if (_corpseView != null)
            {
                Character.CurrentItems.Remove(_corpseView);
                _corpseFactory.RemoveCorpse(_corpseView);
            }
            Character.ShowMoneyFx(Character.transform.position, 0.5f);
            _orderClient.TriggerAction?.Invoke();
            _orderClient.NextBehaviourStep();
            End();
        }

        private void OnOfferAdded()
        {
            if(_state != State.FindOrder) return;
            _corpseFactory.CremationOrderAdded -= OnOfferAdded;
            _corpseFactory.CryogenicOrderAdded -= OnOfferAdded;
            RunNextStep();
        }
        
        private void MoveToGetCorpse()
        {
            _state = State.Working;
            var point = _corpse.AttachedPoint;
            Character.GoTo(point.transform, OpenStorage);
            Character.SetStopDistance(0.3f);
            Character.ShowTrolley();
            _trolley = Point.GetComponentInChildren<CharacterItemView>();
            Character.TakeItem(_trolley);
            _corpseOrder.SetProgress(Point.Cycle);
        }
        
        private void OpenStorage()
        {
            var storage = (CorpseStorageView)_corpse.AttachedPoint;
            storage.Open();
            InvokeSystem.StartInvoke(GetCorpse, 0.5f);
        }

        private void GetCorpse()
        {
            var storage = (CorpseStorageView)_corpse.AttachedPoint;
            Points.FreePoint(storage);
            _corpseView = storage.GetComponentInChildren<CorpseView>();
            Character.TakeItem(_corpseView);
            Points.FreePoint(_corpse.AttachedPoint);
            InvokeSystem.StartInvoke(storage.Close, 0.5f);
            InvokeSystem.StartInvoke(MoveToStartCycle, 0.5f);
        }

        private void MoveToStartCycle()
        {
            Character.SetTargetPoint(Point);
            Character.GoTo(Point.transform, StartOrderCycle);
        }

        private void StartOrderCycle()
        {
            Character.HideTrolley();
            
            OpenItem();
        }

        private void OpenItem()
        {
            var points = Point.Item.GetComponentsInChildren<PointBaseView>();
            _cremationView = Points.GetPoint(Type.EmbalmingCorpsePlace, Character.Config.Type, _roomType, false, points) as CremationItemView;
            _corpseView = Character.CurrentItems[1] as CorpseView;
            _cremationView.Open();
            InvokeSystem.StartInvoke(PutCorpseToStart, 0.5f);
        }

        private void PutCorpseToStart()
        {
            var points = Point.Item.GetComponentsInChildren<PointBaseView>();
            Character.PutItem(_corpseView, _cremationView.transform);
            _trolley = Character.CurrentItems[0];
            var trolleyPoint = Points.GetPoint(Type.ItemPlace, Character.Config.Type, _roomType, false, points);
            Character.PutItem(_trolley, trolleyPoint.transform);
            _cremationView.StartCremation();
            InvokeSystem.StartInvoke(StartCycle, 0.5f);
        }

        private void StartCycle()
        {
            _cyclePointView.StartWorking();
            StartRoomCycle();
            if(_cremationView.ProgressbarPosition) Point.UpdateProgressBarPosition(_cremationView.ProgressbarPosition.position);
            Point.Cycle.CompletedEvent -= End;
            Points.FreePoint(Point);
            _cremationView.Close();
            
            _state = State.FindCorpsePlace;
            RunNextStep();
        }

        protected override void EndRoomCycle()
        {
            _corpseOrder.Complete();
            base.EndRoomCycle();
        }

        private void StartWaiting()
        {
            var targetPoints = Points.GetAllPoints(Type.IdleWalk, _roomType);
            foreach (var point in targetPoints)
            {
                point.OnAvailable += RunNextStep;
            }
            MoveToIdlePoint();
        }

        private void StopWaiting()
        {
            var targetPoints = Points.GetAllPoints(Type.IdleWalk, _roomType);
            foreach (var point in targetPoints)
            {
                point.OnAvailable -= RunNextStep;
            }
            InvokeSystem.CancelInvoke(MoveToIdlePoint);
        }
        
        private void MoveToIdlePoint()
        {
            var point = Points.GetRandomPoint(Type.IdleWalk, _roomType);
            Character.GoTo(point.transform, WaitInPoint, 0);
            Character.SetTargetPoint(point);
        }

        private void WaitInPoint()
        {
            Character.OnReached -= WaitInPoint;
            var playAnim = Random.Range(0, 2) > 0;
            if(playAnim) Character.Animate(Character.CharacterPoint.AnimationHash, moveToPoint: true);
            InvokeSystem.StartInvoke(MoveToIdlePoint, Random.Range(3f, 7f));
        }
    }
}
