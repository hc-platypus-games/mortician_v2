using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints.Events;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class CallCenterLogic : BaseRoomLogic
    {
        [Inject] private LevelEventsFactory _levelEventsFactory;
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private GameFlags _flags;
        
        private MapEventPointView _currentEventPointView;
        private CharacterItemView _phone;
        private Transform _phoneParent;
        
        public CallCenterLogic(BaseRoomPointView pointRoomPointView) : base(pointRoomPointView)
        {
            
        }

        public override void RunNextStep()
        {
            Character.Animate(Character.CharacterPoint.AnimationHash);
            _phone ??= Point.GetComponentInChildren<CharacterItemView>();
            _phoneParent ??= _phone.transform.parent;
            Character.TakeItem(_phone);
            
            _levelEventsFactory.OnEventCreated -= RunNextStep;
            _currentEventPointView = _levelEventsFactory.GetEventFromQueue();
            if (_currentEventPointView == null)
            {
                _levelEventsFactory.OnEventCreated += RunNextStep;
                return;
            }
            StartRoomCycle();
            if (!_flags.Has(GameFlag.TutorialFinished))
            {
                Point.DeactivateProgressBar();
                Character.DrawBubble("CALL_CENTER_START".ToLocalized(), true, 1.5f);
                InvokeSystem.StartInvoke(DrawEndBubble, 3f);
            }
        }

        private void DrawEndBubble()
        {
            Character.DrawBubble("CALL_CENTER_END".ToLocalized(), true, 2f);
        }

        protected override void EndRoomCycle()
        {
            Character.PutItem(_phone, _phoneParent);
            _levelEventsFactory.AddEventToCalled(_currentEventPointView);
            base.EndRoomCycle();
            _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.CallEventEnded);
        }
    }
}
