using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;

namespace _Game.Scripts.View.EntityBehaviour
{
    public class BackgroundCharacterBehaviour : EntityBehaviourBase
    {
        public BackgroundCharacterBehaviour()
        {
            IsRepeatable = true;
        }

        public override void RunNextStep()
        {
            var startPoint = Points.GetRandomPoint(Type.CharacterSpawn);
            var endPoint = Points.GetRandomPoint(Type.CharacterSpawn);
            Character.ForceMoveTo(startPoint.transform.position);
            Character.ShowSkin(Random.Range(14, 18));
            Character.ShowVisual();
            Character.GoTo(endPoint.transform.position, 0);
            InvokeSystem.StartInvoke(End, 10f);
        }

        public override void End()
        {
            Character.HideVisual();
            InvokeSystem.StartInvoke(RunNextStep, 2f);
        }
    }
}
