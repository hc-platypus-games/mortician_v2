using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Systems.Base;
using Zenject;

namespace _Game.Scripts.View.Room
{
    public class CemeteryItemView : RoomItemView
    {
        [Inject] private GameFlags _flags;
        [Inject] private GameBalanceConfigs _balance;
        
        protected override void CreateParams()
        {
            base.CreateParams();
            UpdateCycleDelay();
        }

        protected override void UpdateParams()
        {
            base.UpdateParams();
            UpdateCycleDelay();
        }

        private void UpdateCycleDelay()
        {
            var cycle = _flags.Has(GameFlag.TutorialFinished)
                ? Math.GetValue(GameParamType.MorticianCycle, _level, Config.CycleDelay)
                : _balance.DefaultBalance.TutorialCycleDelay;
            Params.UpdateParamValue(this, GameParamType.CycleDelay, cycle);
        }
    }
}
