using System;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.Room
{
    [ExecuteInEditMode]
    public class RoomView : BaseView, IGameParam, IGameProgress
    {
        public Action UpgradedEvent;
        
        [SerializeField] private RoomCategory _category;
        [SerializeField] private RoomType _type;
        [SerializeField] private float _camFocusScale = 1.5f;
        [SerializeField] private GameObject _assetsContainer;
        [SerializeField] private GameObject _trashContainer;
        [SerializeField] private GameObject _buildContainer;
        [SerializeField] private Vector3 _buildingSize = Vector3.one;
        
        private int _level;

        [Inject] private GameProgressFactory _progresses;
        [Inject] private GameParamFactory _params;
        [Inject] private GameSystem _game;
        [Inject] private RoomsFactory _rooms;
        [Inject] private LevelSystem _levelSystem;
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private GameFlags _flags;
        [Inject] private UIFactory _uiFactory;

        private VisualView[] _visuals;
        
        public PointBaseView[] AllPoints { get; private set; }

        public RoomConfig Config { get; private set; }
        public RoomCategory Category => _category;
        public RoomType Type => _type;
        public int Level => _level;
        public float CamFocusScale => _camFocusScale;
        
        public RoomView Init(RoomConfig config)
        {
            Config = config;
            _level = Config.StartLevel;
            
            var colliders = GetComponentsInChildren<SelectionCollider>().Where (c => c.Type == SelectionColliderType.Room);
            if (colliders != null)
            {
                foreach (var col in colliders)
                {
                    col.SetType(SelectionColliderType.Room, this);
                }
            }
            _visuals = GetComponentsInChildren<VisualView>(true);
            
            foreach (var visual in _visuals)
            {
                visual.Init();
            }

            ShowsVisuals();
            
            AllPoints = GetComponentsInChildren<PointBaseView>(true).ToArray();
            
            RedrawVisualContainers();
            
            return this;
        }

        public void ShowsVisuals()
        {
            var level = Level;
            foreach (var visual in _visuals)
            {
                visual.Show(level, false);
            }

            var items = _rooms.GetRoomItems(_type);
            foreach (var item in items)
            {
                item.ShowsItem();
            }
        }

        public float UpgradePrice()
        {
            switch (Level)
            {
                case 0:
                    return Config.PriceLevel1;
                case 1:
                    return Config.PriceLevel2;
                case 2:
                    return Config.PriceLevel3;
            }

            return 0;
        }

        public float TimeToUpgrade()
        {
            switch (Level)
            {
                case 0:
                    return Config.TimerLevel1;
                case 1:
                    return Config.TimerLevel2;
                case 2:
                    return Config.TimerLevel3;
            }

            return 0;
        }

        public int GetBuildingProgressValue()
        {
            var progress = _progresses.GetProgress(this, GameParamType.BuildingTimer);
            if (progress == null || (!progress.IsActive && !progress.IsCompleted)) return 0;
            return (int)progress.CurrentValue;
        }

        private void RedrawVisualContainers()
        {
             if(_assetsContainer) _assetsContainer.SetActive(_level > 0);
            
            var progress = _progresses.GetProgress(this, GameParamType.BuildingTimer);
            if(_buildContainer) _buildContainer.SetActive(progress != null && progress.IsActive);
            if(_trashContainer) _trashContainer.SetActive(_level == 0 && progress is not {IsActive: true});
        }
        
        [ContextMenu("UpdateRoomType")]
        public void UpdateItemsAndPoints()
        {
            var items = GetComponentsInChildren<RoomItemView>(true);
            for (var i = 0; i < items.Length; i++)
            {
                var item = items[i];
                var id = i + 1;
                item.Id = id;
                item.RoomType = _type;
                item.name = $"item{id}";
            }

            var points = GetComponentsInChildren<PointBaseView>(true);
            foreach (var point in points)
            {
                point.RoomType = _type;
            }
#if UNITY_EDITOR
 
            var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
            if (prefabStage != null)
            {
                EditorSceneManager.MarkSceneDirty(prefabStage.scene);
            }
#endif
        }

        public void StartUpgrade()
        {
            if (!_game.IsEnoughCurrency(GameParamType.Soft, UpgradePrice())) return;
            var workers = _params.GetParam<GameSystem>(GameParamType.Workers);
            if (workers.Value <= 0)
            {
                _uiFactory.SpawnMessage("NOT_ENOUGH_WORKERS".ToLocalized());
                return;
            }
            
            var progress = _progresses.GetProgress(this, GameParamType.BuildingTimer);
            if(progress != null) return;

            workers.Change(-1);
            if(_level == 0) _levelSystem.CurrentLevel.BuildingView.Spawn(workers, transform.position, _buildingSize);
            progress = _progresses.CreateProgress(this, GameParamType.BuildingTimer, TimeToUpgrade(), false);
            progress.Play();
            progress.CompletedEvent += OnCompleteBuyTimer;
            
            _game.SpendCurrency(GameParamType.Soft, UpgradePrice(), 
                _level == 0 ? SpendCurrencyPlace.BuyRoomWindow : SpendCurrencyPlace.UpgradeRoom, SpendCurrencyItem.Room);
            
            RedrawVisualContainers();
        }

        private void OnCompleteBuyTimer()
        {
            var workers = _params.GetParam<GameSystem>(GameParamType.Workers);
            var maxWorkers = _params.GetParam<GameSystem>(GameParamType.MaxWorkers);
            workers.Change(1);
            if (workers.Value > maxWorkers.Value)
            {
                workers.SetValue(maxWorkers.Value);
            }
            
            // TODO delete if level 0
            if (!_flags.Has(GameFlag.TutorialFinished) || _level == 0)
            {
                Upgrade();
            }
        }

        public void Upgrade()
        {
            var progress = _progresses.GetProgress(this, GameParamType.BuildingTimer);
            if(!progress.IsCompleted) return;
            
            _level++;
            progress.SetTarget(TimeToUpgrade());
            switch (Type)
            {
                case RoomType.Cremation:
                case RoomType.Cryogenic:
                    _flags.Set(GameFlag.CorpseOrdersOpened);
                    break;
            }
            
            RedrawVisualContainers();
            // TODO delete if level 1
            if (_level > 1) InvokeSystem.StartInvoke(AnimateUpgrade, 0.5f);
            else ShowsVisuals();
                    
            UpgradedEvent?.Invoke();
            
            _eventProvider.TriggerEvent(AppEventType.Tasks,
                _level == 1 ? GameEvents.BuyRoom : GameEvents.UpgradeRoom, (int)_type);

            _eventProvider.TriggerEvent(AppEventType.Analytics,
                GameEvents.OpenRoom,
                _levelSystem.CurrentLevel.Id,
                _levelSystem.CurrentLevel.LevelName,
                (int)Config.RoomType, 
                $"room {_type}");
        }

        public void AnimateUpgrade()
        {
            if (_type != RoomType.Cemetery)
            {
                foreach (var item in _visuals)
                {
                    item.AnimateUpgrade(_level);
                } 
            }
            InvokeSystem.StartInvoke(ShowsVisuals, 0.2f);
        }

        public void SetRoomParams(int level, int buildingTime)
        {
            _level = level;
            if (buildingTime > 0)
            {
                var time = 0f;
                switch (_level)
                {
                    case 0:
                        time = Config.TimerLevel1;
                        break;
                    case 1:
                        time = Config.TimerLevel2;
                        break;
                    case 2:
                        time = Config.TimerLevel3;
                        break;
                }
                var progress = _progresses.CreateProgress(this, GameParamType.BuildingTimer, time, false);
                var workers = _params.GetParam<GameSystem>(GameParamType.Workers);
                if(_level == 0) _levelSystem.CurrentLevel.BuildingView.Spawn(workers, transform.position, _buildingSize);
                progress.Play();
                progress.CompletedEvent += OnCompleteBuyTimer;
                progress.Change(buildingTime);
            }
            UpgradedEvent?.Invoke();
            RedrawVisualContainers();
        }

        public bool CanUpgrade()
        {
            return _rooms.GetCapRoomExp(this) == _rooms.GetRoomExp(this) && _level < 3;
        }
    }
}