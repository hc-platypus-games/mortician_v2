using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.MapPoints;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

namespace _Game.Scripts.View.Room
{
    public class BuildingObjectView : MonoBehaviour
    {
        [SerializeField] private Transform _pointsContainer;
        [SerializeField] private Transform[] _props;
        
        [Inject] private CharactersFactory _charactersFactory;

        private PointBaseView[] _points;
        private GameParam _workers;
        private readonly List<CharacterView> _characters = new();
        
        public void Spawn(GameParam workers, Vector3 position, Vector3 scale = default)
        {
            _workers = workers;
            _workers.UpdatedEvent += OnUpdateWorkersParam;

            this.Activate();

            if (scale == default) scale = new Vector3(2f, 1f, 2f);

            transform.position = position;
            _pointsContainer.localScale = scale;

            var points = GetComponentsInChildren<PointBaseView>(true);

            var spawnPoints = points.Where(i => i.Type == Type.Enter);
            var workPoints = points.Where(i => i.Type == Type.WorkPlace);
            _points = workPoints as PointBaseView[] ?? workPoints.ToArray();

            var propLength = _props.Length;
            if (_points.Length < propLength) propLength = _points.Length;
            for (var i = 0; i < propLength; i++)
            {
                var prop = _props[i];
                prop.transform.position = _points[i].transform.position;
            }

            foreach (var point in spawnPoints)
            {
                var character = _charactersFactory.SpawnCharacter();
                character.ForceMoveTo(point.transform);
                character.Init(new BuilderWorkerBehaviour(_points, GetComponentInChildren<NavMeshSurface>(true).agentTypeID));
                _characters.Add(character);
            }
        }

        private void OnUpdateWorkersParam()
        {
            if(_workers.Value > 0f) Despawn();
        }

        private void Despawn()
        {
            for (var i = 0; i < _characters.Count;)
            {
                var character = _characters[i];
                _characters.Remove(character);
                _charactersFactory.RemoveCharacter(character);
            }

            this.Deactivate();
        }
    }
}
