using Unity.Collections;
using UnityEngine;

namespace _Game.Scripts.View.Room
{
    public enum SelectionColliderType
    {
        Room,
        Item,
        Character,
        ClickableEvent
    }

    public class SelectionCollider : MonoBehaviour
    {
        [SerializeField] private SelectionColliderType _type;

        private BaseView _view;
        public BaseView View => _view;

        public SelectionColliderType Type => _type;

        public void SetType(SelectionColliderType type, BaseView view)
        {
            _type = type;
            _view = view;
        }
    }
}