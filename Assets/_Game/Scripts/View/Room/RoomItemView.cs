using System;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Tools;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.Room
{
    public class RoomItemView : BaseView, IGameParam, IGameProgress
    {
        public Action<RoomItemView> UpgradeEvent;
        
        [SerializeField] private int _id;
        [SerializeField] private RoomType _roomType;
        [SerializeField] private bool _spawnCharacter;
       
        protected int _level;
        
        [Inject] protected GameParamFactory Params;
        [Inject] private GameProgressFactory _progresses;
        [Inject] private RoomsFactory _rooms;
        [Inject] private CharactersFactory _charactersFactory;
        [Inject] protected MathSystem Math;
        [Inject] private GameSystem _game;
        [Inject] private LevelSystem _levelSystem;
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private GameFlags _flags;
        [Inject] private BoostSystem _boosts;
        [Inject] private UIFactory _uiFactory;

        private VisualView[] _visuals;

        public RoomItemConfig Config { get; private set; }
        public bool IsBought => _level >= 1;
        
        public RoomType RoomType
        {
            get
            {
                return _roomType;
            }
            set
            {
#if UNITY_EDITOR
                _roomType = value;
#endif
            }
        }

        public int Id
        {
            get
            {
                return _id;
            }
            
            set
            {
#if UNITY_EDITOR
                _id = value;
#endif
            }
        }

        public int Level => _level;
        public float Price => Params.GetParamValue(this, GameParamType.Price);
        public RoomView Room { get; private set; }

        public RoomItemView Init(RoomItemConfig config)
        {
            Config = config;
            _level = config.StartLevel;
            _visuals = GetComponentsInChildren<VisualView>(true);
            Room = _rooms.GetRoom(_roomType);
            ShowsItem();

            CreateParams();
            _flags.OnFlagSet += OnFlagSet;
            if (config.StartLevel > 0 && _spawnCharacter)
            {
                Room.UpgradedEvent += UpdateAfterUpgrade;
            }
            
            var colliders = GetComponentsInChildren<SelectionCollider>(true)
                .Where(c => c.Type == SelectionColliderType.Item);
            foreach (var col in colliders)
            {
                col.SetType(SelectionColliderType.Item, this);
            }
            return this;
        }

        private void OnFlagSet(GameFlag flag)
        {
            switch (flag)
            {
                case GameFlag.TutorialFinished:
                    UpdateParams();
                    break;
            }
        }

        protected virtual void CreateParams()
        {
            switch (RoomType)
            {
                case RoomType.Hall:
                case RoomType.Embalming:
                case RoomType.Farewell:
                case RoomType.Garage:
                case RoomType.FoodService:
                case RoomType.RestService:
                case RoomType.ToiletService:
                case RoomType.FuneralVisitorParking:
                case RoomType.Cemetery:
                    Params.CreateParam(this, GameParamType.Income, Math.GetValue(GameParamType.Income, _level, Config.Income));
                    break;
            }
            Params.CreateParam(this, GameParamType.Seats, Math.GetValue(GameParamType.Seats, _level, Config.Seats));
            Params.CreateParam(this, GameParamType.CycleDelay, Math.GetValue(GameParamType.CycleDelay, _level, Config.CycleDelay));
            Params.CreateParam(this, GameParamType.Capacity, Config.Capacity);

            if (Config.BoughtForHard)
            {
                Params.CreateParam(this, GameParamType.Price, Config.Price);
            }
            else
            {
                Params.CreateParam(this, GameParamType.Price, Math.GetValue(GameParamType.Price, _level, Config.Price));   
            }
        }

        public virtual void OnLevelLoaded()
        {
            
        }

        public void ShowsItem(bool selected = false)
        {
            var room = _rooms.GetRoom(_roomType);
            var level = room.Level;
            if (_level == 0) level = 0;
            foreach (var visual in _visuals)
            {
                visual.Show(level, selected);
            }
        }

        public int GetBuildingProgressValue()
        {
            var progress = _progresses.GetProgress(this, GameParamType.BuildingTimer);
            if (progress == null || !progress.IsActive) return 0;
            return (int)progress.CurrentValue;
        }

        public GameProgress StartBuilding()
        {
            if (!_game.IsEnoughCurrency(GameParamType.Soft, Config.Price)) return null;
            
            var workers = Params.GetParam<GameSystem>(GameParamType.Workers);
            if (workers.Value <= 0)
            {
                _uiFactory.SpawnMessage("NOT_ENOUGH_WORKERS".ToLocalized());
                return null;
            }
            workers.Change(-1);
            _levelSystem.CurrentLevel.BuildingView.Spawn(workers, transform.position, new Vector3(2.5f, 1f, 2.5f));
            
            var progress = _progresses.CreateProgress(this, GameParamType.BuildingTimer, Config.BuildingTimer, false);
            progress.CompletedEvent += OnCompletedBuildingTimer;
            _game.SpendCurrency(GameParamType.Soft, Config.Price, SpendCurrencyPlace.BuyCemetery, SpendCurrencyItem.Cemetery);
            return progress;
        }

        private void OnCompletedBuildingTimer()
        {
            var workers = Params.GetParam<GameSystem>(GameParamType.Workers);
            var maxWorkers = Params.GetParam<GameSystem>(GameParamType.MaxWorkers);
            workers.Change(1);
            if (workers.Value > maxWorkers.Value)
            {
                workers.SetValue(maxWorkers.Value);
            }

            var progress = _progresses.GetProgress(this, GameParamType.BuildingTimer);
            progress.CompletedEvent -= OnCompletedBuildingTimer;
            progress.Pause();
            Upgrade(true);
        }

        public void Upgrade(bool isFree = false, bool isAdUpgrade = false)
        {
            var price = Price;
            
            if (!isFree)
            {
                if (Config.BoughtForHard)
                {
                    if(_game.IsEnoughCurrency(GameParamType.Hard, price) == false) return;
                }
                else
                {
                    if (!isAdUpgrade)
                    {
                        if(_game.IsEnoughCurrency(GameParamType.Soft, price) == false) return;
                    }
                }
            }

            _level++;
            UpdateAfterUpgrade();
            
            if (!isFree)
            {
                if (Config.BoughtForHard)
                {
                    _game.SpendCurrency(GameParamType.Hard, price, SpendCurrencyPlace.RoomInfoWindow, SpendCurrencyItem.RoomItem);
                }
                else
                {
                    if (!isAdUpgrade)
                    {
                        _game.SpendCurrency(GameParamType.Soft, price, SpendCurrencyPlace.RoomInfoWindow, SpendCurrencyItem.RoomItem);
                    }
                }
            }
            
            _eventProvider.TriggerEvent(AppEventType.Tasks,
                _level == 1 ? GameEvents.BuyRoomItem : GameEvents.UpgradeRoomItem, (int)_roomType, _id);
            
            _eventProvider.TriggerEvent(AppEventType.Tasks,
               GameEvents.ReachItemLevel, (int)_roomType, _id, (float)_level);
        }

        private void UpdateAfterUpgrade()
        {

            if (_level == 1 && Room.Level > 0 && _spawnCharacter && !_game.GameIsLoading)
            {
                _charactersFactory.SpawnWorker(_roomType);
            }

            UpdateParams();
            ShowsItem();
            UpgradeEvent?.Invoke(this);
        }

        protected virtual void UpdateParams()
        {
            Params.UpdateParamValue(this, GameParamType.Seats, Math.GetValue(GameParamType.Seats, _level, Config.Seats));
            Params.UpdateParamValue(this, GameParamType.Income, Math.GetValue(GameParamType.Income, _level, Config.Income));
            Params.UpdateParamValue(this, GameParamType.CycleDelay, Math.GetValue(GameParamType.CycleDelay, _level, Config.CycleDelay));
            Params.UpdateParamValue(this, GameParamType.Capacity, Config.Capacity);
           
            if (!Config.BoughtForHard)
            {
                Params.UpdateParamValue(this, GameParamType.Price, Math.GetValue(GameParamType.Price, _level, Config.Price));   
            }
        }

        public bool CanBuy()
        {
            return _game.IsEnoughCurrency(Config.BoughtForHard ? GameParamType.Hard : GameParamType.Soft, Price);
        }

        public bool IsEnoughLevel()
        {
            return _rooms.GetRoom(_roomType).Level >= Config.RequiredLevel;
        }
        
        public bool IsCapLevel()
        {
            return _level >= Config.Cap;
        }

        public float GetParamValue(GameParamType type)
        {
            return Params.GetParamValue(this, type);
        }
        
        public float GetParamLevelValue(GameParamType type, bool nextLevel = false, int level = -1)
        {
            float value = 0;
            switch (type)
            {
                case GameParamType.Income:
                    value = Config.Income;
                    break;
                
                case GameParamType.CycleDelay:
                case GameParamType.MorticianCycle:
                    value = Config.CycleDelay;
                    break;
                
                case GameParamType.Price:
                    value = Config.Price;
                    break;
                
                case GameParamType.Seats:
                    value = Config.Seats;
                    break;
                
                case GameParamType.Capacity:
                    value = Config.Capacity;
                    break;
            }

            var targetLevel = level >= 0 ? level : _level;
            targetLevel = nextLevel ? targetLevel + 1 : targetLevel;
            return Math.GetValue(type, targetLevel, value) * BoostValue(type);
        }

        private float BoostValue(GameParamType type)
        {
            var value = _boosts.GetBoostValue(type, Room) * _boosts.GetBoostValue(type);
            return value == 0 ? 1 : value;
        }
        
        public void SetRoomParams(int level, int buildingTime)
        {
            _level = level;
            if (buildingTime > 0)
            {
                var progress = _progresses.CreateProgress(this, GameParamType.BuildingTimer, Config.BuildingTimer, false);
                progress.Change(buildingTime);
                progress.Play();
                progress.CompletedEvent += OnCompletedBuildingTimer;
                var workers = Params.GetParam<GameSystem>(GameParamType.Workers);
                _levelSystem.CurrentLevel.BuildingView.Spawn(workers, transform.position, new Vector3(2.5f, 1f, 2.5f));
            }
            ShowsItem();
            UpdateParams();
        }
    }
}