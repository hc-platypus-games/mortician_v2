using _Game.Scripts.ScriptableObjects;
using Zenject;

namespace _Game.Scripts.View.Character
{
    public sealed class CharacterView : CharacterViewBase
    {
        public bool debug;

        public class Pool : MonoMemoryPool<CharacterView>
        {
            protected override void Reinitialize(CharacterView character)
            {
                character.Reset();
            }
        }

        public void SetConfig(CharacterConfig config)
        {
            Config = config;
        }
    }
}
