using System;
using System.Collections.Generic;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.UI.WorldSpace;
using _Game.Scripts.View.EntityBehaviour;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.Other;
using _Game.Scripts.View.Room;
using DG.Tweening;
using Unity.Collections;
using UnityEngine;
using UnityEngine.AI;
using Zenject;
using Random = _Game.Scripts.Tools.Random;
using Type = _Game.Scripts.View.MapPoints.Type;

namespace _Game.Scripts.View.Character
{
    public abstract class CharacterViewBase : BaseView, IGameParam
    {
        public enum AnimationState
        {
            Idle,
            Moving
        }

        public Action TriggerAction, OnTouched, OnReached;
        public static Action EmotionChanged;

        [SerializeField] [Range(0.55f, 1.1f)] private float _weight = 1f;
        [SerializeField] private Transform[] _weightTransforms;
        [SerializeField] private GameObject _shadow;
        [SerializeField] private GameObject _moneyFx;

        [ReadOnly] private float _stoppingDistance = 0.01f;
        [ReadOnly] private float _rotationSpeed = 5f;

        [Inject] private DiContainer _container;
        [Inject] private GameParamFactory _paramFactory;
        [Inject] private MapPointsFactory _pointsFactory;
        [Inject] private UIFactory _uiFactory;
        [Inject] private GameResources _resources;

        private RoomView _cachedTargetRoom;
        private NavMeshAgent _navMeshAgent;
        private Animator _animator;
        private AnimationState _animationState;
        private float _speedMultiplier;
        private Transform _moveTarget;
        private List<EntityBehaviourBase> _behaviourList = new();
        private Bubble _bubble;
        private VisualView[] _visuals;
        private CharacterItemView[] _characterItems;
        private List<CharacterItemView> _takenItems = new();

        private static readonly int _base = Animator.StringToHash("base");
        private static readonly int _speed = Animator.StringToHash("speed");
        private static readonly int _push = Animator.StringToHash("push");
        private static readonly int _opacity = Shader.PropertyToID("_Opacity");
        private static readonly int _carryCoffinLeft = Animator.StringToHash("carry_coffin_left");
        private static readonly int _carryCoffinRight = Animator.StringToHash("carry_coffin_right");
        private bool _isRunning;
        
        public CharacterConfig Config { get; protected set; }
        public bool IsHidden => !_animator.isActiveAndEnabled;
        public UIFactory.EmotionType CurrentEmotion { get; private set; }

        public PointBaseView CharacterPoint { get; private set; }
        public int PointAnimation => CharacterPoint ? CharacterPoint.AnimationHash : 0;
        public float MoveSpeed { get; private set; }
        public float CurrentMoveSpeed => Run > 2 ? MoveSpeed * 5f : MoveSpeed * (Run > 0 || _isRunning ? 2.5f : 1f);
        public int Run { get; private set; }

        public List<CharacterItemView> CurrentItems => _takenItems;

        [Inject]
        private void Construct()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            MoveSpeed = _navMeshAgent.speed;
            _animator = GetComponentInChildren<Animator>();
            _characterItems = GetComponentsInChildren<CharacterItemView>(true);
        }

        public void Init(EntityBehaviourBase behaviour = null)
        {
            if (behaviour == null) return;

            _visuals = GetComponentsInChildren<VisualView>(true);

            foreach (var visual in _visuals)
            {
                visual.Init();
            }
            
            behaviour.AddOwner((CharacterView)this);
            InsertBehaviour(behaviour);
            _behaviourList[0].Init().RunNextStep();
            UpdateWeight(0.8f);
            var selectionCollider = GetComponentInChildren<SelectionCollider>(true);
            if (selectionCollider) selectionCollider.SetType(SelectionColliderType.Character, this);
        }

        public void SetTargetPoint(PointBaseView pointBaseView)
        {
            CharacterPoint = pointBaseView;
        }

        public void Tick(float deltaTime)
        {
            CheckDestinationReached();
            RotateNavMeshAgent(deltaTime);
            CheckDoors();

            if (_animationState == AnimationState.Moving)
            {
                CheckObstacles(deltaTime);
                
                if(_bubble != null) _bubble.transform.position = transform.position;
            }
        }

        public void ShowItem(string key = "")
        {
            foreach (var item in _characterItems)
            {
                item.Show(key);
            }
        }

        public void ShowTrolley()
        {
            _animator.SetFloat(_push, 1f);
        }

        public void HideTrolley()
        {
            _animator.SetFloat(_push, 0f);
        }

        public void SetStopDistance(float value)
        {
            _navMeshAgent.stoppingDistance = value;
            _stoppingDistance = value + 0.01f;
        }

        private void UpdateWeight(float weight = 0)
        {
            if (weight == 0) weight = _weight;

            foreach (var weightTransform in _weightTransforms)
            {
                weightTransform.localScale = new Vector3(1f, weight, weight);
            }
        }

        public bool Touch()
        {
            if (OnTouched != null)
            {
                OnTouched.Invoke();
                return true;
            }
            return false;
        }

        public void ShowSkin(int id)
        {
            if (_visuals == null)
            {
                _visuals = GetComponentsInChildren<VisualView>(true);

                foreach (var visual in _visuals)
                {
                    visual.Init();
                }
            }
            
            foreach (var visual in _visuals)
            {
                visual.Show(id);
            }
        }

        public void ShowVisual(bool smooth = true)
        {
            if (!IsHidden) return;
            _animator.Activate();
            if (smooth)
            {
                foreach (var visual in _visuals)
                {
                    visual.SmoothAppear(1f);
                }
            }
            _shadow.Activate();
        }

        public void HideVisual(bool smooth = true)
        {
            if (IsHidden) return;
            if (!smooth)
            {
                HideAll();
            }
            else
            {
                foreach (var visual in _visuals)
                {
                    visual.SmoothHide(1f);
                }
                _shadow.Deactivate();
                InvokeSystem.StartInvoke(HideAll, 1f);
            }
        }

        private void HideAll()
        {
            _animator.Deactivate();
            _shadow.Deactivate();
        }

        private void RotateNavMeshAgent(float deltaTime)
        {
            if (_navMeshAgent.enabled == false) return;
            if (Vector3.Distance(
                    _navMeshAgent.transform.position,
                    _navMeshAgent.steeringTarget)
                > _stoppingDistance)
            {
                var target = _navMeshAgent.steeringTarget - _navMeshAgent.transform.position;
                _navMeshAgent.transform.rotation = Quaternion.Slerp(
                    _navMeshAgent.transform.rotation,
                    Quaternion.LookRotation(target),
                    _rotationSpeed * deltaTime);
            }
        }
        
        private void CheckDoors()
        {
            if (Physics.Raycast(transform.position + Vector3.up * 0.1f, transform.forward, out var hit,
                    0.4f, GameLayers.DOORS_MASK))
            {
                var door = hit.collider.GetComponent<DoorView>();
                if (door) door.Open();
            }
        }

        private void CheckObstacles(float deltaTime)
        {
            if (Physics.Raycast(transform.position, transform.forward, out var hit,
                    0.1f + 0.5f * MoveSpeed, GameLayers.OBSTACLES_MASK))
            {
                Debug.DrawLine(transform.position, hit.point);
                _speedMultiplier = 0.6f;
            }
            else
            {
                _speedMultiplier = 1f;
            }

            var speed = CurrentMoveSpeed * _speedMultiplier;
            if (speed > 0) _navMeshAgent.speed = speed;
        }

        public void ForceMoveTo(Vector3 point)
        {
            var navAgentEnabled = _navMeshAgent.enabled;
            _navMeshAgent.enabled = false;
            transform.position = point;
            _navMeshAgent.enabled = navAgentEnabled;
        }

        public void ForceMoveTo(Transform target = default)
        {
            if (target == default)
            {
                ForceMoveTo(_navMeshAgent.destination);
                return;
            }
            ForceMoveTo(target.position);
            transform.rotation = target.rotation;
        }

        public void SpeedUp()
        {
            Run = 3;
        }

        private void CheckDestinationReached()
        {
            if (_animationState != AnimationState.Moving)
                return;

            var currentDistance = Vector3.Distance(_navMeshAgent.transform.position, _navMeshAgent.destination);

            if (_isRunning && currentDistance < 5f)
            {
                _isRunning = false;
                UpdateRunning();
            }
            
            if (currentDistance < _stoppingDistance)
            {
                _isRunning = false;
                Animate(moveToPoint: false);
                _animationState = AnimationState.Idle;
                if (_moveTarget)
                {
                    _navMeshAgent.enabled = false;
                    _moveTarget = null;

                    OnReached?.Invoke();
                }
            }
        }

        internal void Animate(int animHash = 0, float moveSpeed = 0f, bool moveToPoint = true)
        {
            if (animHash == 0)
            {
                animHash = _base;
                if(_animator.gameObject.activeSelf) _animator.Play(_base);
                _animator.applyRootMotion = false;
            }
            else
            {
                _animator.applyRootMotion = true;
            }

            if (moveToPoint && CharacterPoint)
            {
                ForceMoveTo(CharacterPoint.transform);
            }

            _animator.SetFloat(_speed, moveSpeed);
            var animatorTransform = _animator.transform;
            animatorTransform.localPosition = Vector3.zero;
            animatorTransform.localRotation = Quaternion.identity;
            if(_animator.gameObject.activeSelf) _animator.CrossFade(animHash, 0.1f);
        }

        public void GoTo(Transform pointTransform, Action onReach = null, int priority = 2)
        {
            _moveTarget = pointTransform;
            GoTo(_moveTarget.position, priority);
            OnReached = onReach;
        }

        public bool GoTo(Vector3 point, int priority = 2)
        {
            _navMeshAgent.enabled = true;
            _animationState = AnimationState.Moving;
            var isGoing = _navMeshAgent.SetDestination(point);
            var currentDistance = Vector3.Distance(_navMeshAgent.transform.position, _navMeshAgent.destination);
            SetStopDistance(0f);
            Run = priority;
            if (!_isRunning && currentDistance > 4f && priority > 0)
            {
                _isRunning = true;
            }
            UpdateRunning();
            OnReached = null;
            return isGoing;
        }

        private void UpdateRunning()
        {
            if (Run > 0 || _isRunning)
            {
                _navMeshAgent.speed = MoveSpeed * 2f;
                Animate(0, 2f, false);
            }
            else
            {
                _navMeshAgent.speed = MoveSpeed;
                Animate(0, 1f, false);
            }
        }

        public void CacheRoom(RoomView room)
        {
            _cachedTargetRoom = room;
        }

        public bool TryExitQueue()
        {
            var point = _pointsFactory.GetRoomPoint(this, _cachedTargetRoom, false);
            if (point)
            {
                InvokeSystem.CancelInvoke(WaitingInQueue);
                InvokeSystem.CancelInvoke(DrawEmotion, true);
                CharacterPoint.IsAvailable = true;
                if (point.Type != Type.Queue)
                {
                    CurrentEmotion = UIFactory.EmotionType.Neutral;
                    EmotionChanged?.Invoke();
                    NextBehaviourStep();
                    return true;
                }
                
                GoTo(point.transform, priority: 1);
                point.IsAvailable = false;
            }

            return false;
        }

        public void WaitingInQueue()
        {
            OnReached -= WaitingInQueue;

            InvokeSystem.CancelInvoke(DrawEmotion);
            CurrentEmotion = UIFactory.EmotionType.Angry;
            EmotionChanged?.Invoke();
            InvokeSystem.StartInvoke(DrawEmotion, 7f);
            InvokeSystem.StartInvoke(WaitingInQueue, 12f);
        }

        public void WaitingFor(UIFactory.EmotionType emotion)
        {
            InvokeSystem.CancelInvoke(DrawEmotion);
            CurrentEmotion = emotion;
            InvokeSystem.StartInvoke(DrawEmotion, 7f);
            InvokeSystem.StartInvoke(WaitingUpdate, 12f);
            EmotionChanged?.Invoke();
        }

        private void WaitingUpdate()
        {
            WaitingFor(CurrentEmotion);
        }

        public void EndWaiting()
        {
            InvokeSystem.CancelInvoke(WaitingUpdate, true);
            CurrentEmotion = UIFactory.EmotionType.Neutral;
            EmotionChanged?.Invoke();
        }

        public void GetPointLogic()
        {
            if (CharacterPoint == null) return;
            Init(CharacterPoint.GetPointLogic());
        }

        public void AddBehaviour(EntityBehaviourBase behaviour)
        {
            _behaviourList.Add(behaviour);
            _container.Inject(behaviour);
        }

        public void InsertBehaviour(EntityBehaviourBase behaviour)
        {
            _behaviourList.Insert(0, behaviour);
            _container.Inject(behaviour);
        }

        public void RemoveBehaviour(EntityBehaviourBase behaviour)
        {
            _behaviourList.Remove(behaviour);
            behaviour.Destroy();
            NextBehaviourStep();
        }

        private void ClearBehaviour()
        {
            foreach (var behaviour in _behaviourList)
            {
                behaviour.Destroy();
            }
            _behaviourList.Clear();
        }

        public void NextBehaviourStep()
        {
            _pointsFactory.FreePoint(CharacterPoint);
            if (_behaviourList.Count == 0)
            {
                Init();
                return;
            }

            _behaviourList[0].RunNextStep();
        }

        public void DrawBubble(string key, bool isText = false, float time = -1f)
        {
            RemoveBubble();
            _bubble = _uiFactory.SpawnBubble();
            if (isText)
            {
                _bubble.ShowText(key);
            }
            else
            {
                _bubble.Show(key);
            }
            _bubble.transform.position = transform.position;
            if (time > 0)
            {
                InvokeSystem.StartInvoke(RemoveBubble, time);
            }
        }

        public void DrawEmotion(UIFactory.EmotionType emotionType)
        {
            var lastEmotion = CurrentEmotion;
            CurrentEmotion = emotionType;
            DrawEmotion();
            if (emotionType == UIFactory.EmotionType.TouchReaction)
            {
                CurrentEmotion = lastEmotion;
            }
            EmotionChanged?.Invoke();
        }
        
        public void DrawEmotion()
        {
            RemoveBubble();
            _bubble = _uiFactory.SpawnEmotion(CurrentEmotion, _cachedTargetRoom? _cachedTargetRoom.Type : RoomType.None);
            _bubble.transform.position = transform.position;
            InvokeSystem.StartInvoke(RemoveBubble, _resources.CharacterEmotionLength);
        }

        public void RemoveBubble()
        {
            OnReached -= DrawEmotion;
            InvokeSystem.CancelInvoke(DrawEmotion, true);
            InvokeSystem.CancelInvoke(RemoveBubble, true);
            if (!_bubble) return;
            _uiFactory.RemoveBubble(_bubble);
            _bubble = null;
        }
        
        public void CarryCoffinLeft()
        {
            _animator.transform.localPosition = Vector3.zero;
            _animator.transform.localRotation = Quaternion.identity;
            _animator.applyRootMotion = false;
            _animator.CrossFade(_carryCoffinLeft, 0.1f);
        }

        public void CarryCoffinRight()
        {
            _animator.transform.localPosition = Vector3.zero;
            _animator.transform.localRotation = Quaternion.identity;
            _animator.applyRootMotion = false;
            _animator.CrossFade(_carryCoffinRight, 0.1f);
        }

        public CharacterItemView GetParentFor(string key, bool stack = true)
        {
            if (stack)
            {
                foreach (var itemPoint in _takenItems)
                {
                    if (itemPoint.Key == key) return itemPoint;
                }
            }
            
            foreach (var itemPoint in _characterItems)
            {
                if (itemPoint.Key == key) return itemPoint;
            }

            return null;
        }

        public void TakeItem(CharacterItemView item, string key = "")
        {
            var itemParent = GetParentFor(key.Length > 0 ? key : item.Key);
            if(itemParent == null) return;
            
            var items = item.GetComponentsInChildren<CharacterItemView>();
            foreach (var newItem in items)
            {
                _takenItems.Add(newItem);
            }
            item.SetParent(itemParent.transform);
            item.transform.DOLocalMove(Vector3.zero, 0.5f);
            item.transform.DOLocalRotate(Vector3.zero, 0.5f);
            item.transform.DOScale(Vector3.one, 0.5f);
        }

        public void PutItem(CharacterItemView item, Transform parent = null)
        {
            if(_takenItems.Count == 0) return;
            var items = item.GetComponentsInChildren<CharacterItemView>();
            foreach (var newItem in items)
            {
                _takenItems.Remove(newItem);
            }

            if (parent == null)
            {
                item.SetParent(null);
                return;
            }
            item.SetParent(parent.transform);
            item.transform.DOLocalMove(Vector3.zero, 0.5f);
            item.transform.DOLocalRotate(Vector3.zero, 0.5f);
            item.transform.DOScale(Vector3.one, 0.5f);
        }

        public void PutItem(int id = 0, Transform parent = null)
        {
            if(_takenItems.Count == 0) return;
            var item = _takenItems[id];
            PutItem(item, parent);
        }

        public void ShowMoneyFx(Vector3 position, float scale = 1f)
        {
            if(!_moneyFx) return;
            _moneyFx.Deactivate();
            _moneyFx.transform.position = position;
            _moneyFx.transform.SetParent(null);
            _moneyFx.transform.localScale = Vector3.one * scale;
            _moneyFx.Activate();
        }

        public void End()
        {
            _pointsFactory.RemoveFromQueue(this);
            ClearBehaviour();
            RemoveBubble();
        }
    }
}
