using _Game.Scripts.Tools;
using UnityEngine;

namespace _Game.Scripts.View.Character
{
    public class CharacterItemView : BaseView
    {
        [SerializeField] private string _key;
        private VisualView _visual;

        public string Key => _key;
        
        public void Show(params string[] keys)
        {
            this.Deactivate();
            foreach (var key in keys)
            {
                if (key == _key) this.Activate();
            }
        }

        public void ShowSkin(int id)
        {
            if (!_visual)
            {
                _visual = GetComponentInChildren<VisualView>(true);
                _visual.Init();
            }
            _visual.Show(id);
        }
    }
}
