using _Game.Scripts.Enums;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.View.MapPoints;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View.Corpse
{
    public class CryogenItemView : CremationItemView
    {
        private static readonly int Drop = Animator.StringToHash("CryogenDrop");
        private static readonly int Base = Animator.StringToHash("base");

        [Inject] private MapPointsFactory _points;

        public override void Init()
        {
            base.Init();
            var workPoint = _points.GetAllPoints(Type.RoomCycle, RoomType.Cryogenic)[0] as BaseRoomPointView;
            if(!workPoint) return;
            workPoint.Init();
            workPoint.Cycle.CompletedEvent += DropCorpse;
        }

        private void DropCorpse()
        {
            var corpseView = GetComponentInChildren<CorpseView>(true);
            if(corpseView) corpseView.ShowSkin(1);
            PlayAnimation(Drop);
        }

        public override void Close()
        {
            PlayAnimation(Base);
            base.Close();
        }
    }
}
