using _Game.Scripts.View.Character;
using Zenject;

namespace _Game.Scripts.View.Corpse
{
    public class CorpseView : CharacterItemView
    {
        public class Pool : MonoMemoryPool<CorpseView>
        {
            protected override void Reinitialize(CorpseView corpse)
            {
                corpse.Reset();
            }
        }
    }
}
