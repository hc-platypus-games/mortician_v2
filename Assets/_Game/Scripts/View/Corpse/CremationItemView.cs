using _Game.Scripts.Tools;
using UnityEngine;

namespace _Game.Scripts.View.Corpse
{
    public class CremationItemView : CorpseStorageView
    {
        [SerializeField] private GameObject _fxInCycle;
        [SerializeField] private Transform _overrideProgressbarPosition;

        public Transform ProgressbarPosition => _overrideProgressbarPosition;

        public override void Init()
        {
            base.Init();
            _fxInCycle?.Deactivate();
        }

        public void StartCremation()
        {
            _fxInCycle?.Activate();
        }

        public void EndCremation()
        {
            _fxInCycle?.Deactivate();
        }
    }
}
