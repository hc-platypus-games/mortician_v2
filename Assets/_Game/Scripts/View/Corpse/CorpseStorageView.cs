using System;
using _Game.Scripts.View.MapPoints.PointsWithLogic;
using UnityEngine;

namespace _Game.Scripts.View.Corpse
{
    public class CorpseStorageView : BaseRoomPointView
    {
        public Action OnClose;
        
        private Animator _animator;
        private int _users;
        private static readonly int Opened = Animator.StringToHash("opened");

        public override void Init()
        {
            base.Init();
            _animator = GetComponentInParent<Animator>();
        }

        public void Open()
        {
            _users++;
            _animator?.SetInteger(Opened, _users);
        }

        public virtual void Close()
        {
            _users--;
            if (_users < 0) _users = 0;
            _animator?.SetInteger(Opened, _users);
            OnClose?.Invoke();
        }

        public void PlayAnimation(int hash)
        {
            _animator?.Play(hash);
        }
    }
}
