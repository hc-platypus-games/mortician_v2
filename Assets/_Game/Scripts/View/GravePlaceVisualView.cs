using _Game.Scripts.Tools;
using _Game.Scripts.View.MapPoints;
using UnityEngine;

namespace _Game.Scripts.View
{
    public class GravePlaceVisualView : VisualView
    {
        public enum CemeteryItemType
        {
            Idle,
            Grave,
            Coffin,
            Waiting
        }

        private GravePlacePointView _grave;

        [SerializeField] private CemeteryItemType _type;

        public CemeteryItemType Type => _type;

        public override void Show(int id, bool selected = false)
        {
            _grave ??= GetComponentInParent<GravePlacePointView>(true);
            if (selected)
            {
                base.Show(id, selected);
            }
            else
            {
                switch (_grave.CurrentState)
                {
                    case GravePlacePointView.State.Wait:
                        SetVisual(id, _type is CemeteryItemType.Waiting or CemeteryItemType.Idle);
                        break;
                    case GravePlacePointView.State.ReadyToWork:
                        SetVisual(id, _type == CemeteryItemType.Coffin);
                        break;
                    case GravePlacePointView.State.WaitCorpsePortering:
                        SetVisual(id, false);
                        break;
                    case GravePlacePointView.State.Reset:
                        SetVisual(id, _type == CemeteryItemType.Grave);
                        break;
                }
            }
        }

        private void SetVisual(int level, bool value)
        {
            if (Type == CemeteryItemType.Idle)
            {
                base.Show(level);
            }
            else
            {
                if (value)
                {
                    base.Show(level);
                }
                else
                {
                    this.Deactivate();
                }

                if (_grave.CurrentState == GravePlacePointView.State.WaitCorpsePortering &&
                    _type == CemeteryItemType.Waiting)
                {
                    
                }
            }
        }
    }
}
