using System;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.View
{
    public class VisualView : MonoBehaviour
    {
        [SerializeField] private VisualConfig[] _configs;
        [SerializeField] private bool _dontDrawSelected;
        [SerializeField] private bool _dontAnimateUpgrade;

        [Inject] private GameResources _resources;
        [Inject] private UIFactory _uiFactory;
            
        private MeshFilter _mesh;
        private SkinnedMeshRenderer _skinnedMesh;
        private MeshRenderer _meshRender;
        private static readonly int Opacity = Shader.PropertyToID("_Opacity");
        private int _cachedLevel;

        private int Clamped(int value) => _configs.Length > value ? value : _configs.Length - 1;
        
        public void Init()
        {
            _mesh = GetComponent<MeshFilter>();
            _skinnedMesh = GetComponent<SkinnedMeshRenderer>();
            _meshRender = GetComponent<MeshRenderer>();
        }
        
        public virtual void Show(int id = 0)
        {
            if (_configs.Length == 0)
            {
                this.Deactivate();
                return;
            }
            
            if(_mesh) _mesh.sharedMesh = _configs.Length > id ? _configs[id].Mesh : _configs[^1].Mesh;
            if(_meshRender) _meshRender.materials = _configs.Length > id ? _configs[id].Materials : _configs[^1].Materials;
            if (_skinnedMesh)
            {
                _skinnedMesh.sharedMesh = _configs.Length > id ? _configs[id].Mesh : _configs[^1].Mesh;
                _skinnedMesh.materials = _configs.Length > id ? _configs[id].Materials : _configs[^1].Materials;
            }
            if(_mesh == null) return; 
            if(_mesh.sharedMesh && !isActiveAndEnabled)this.Activate();
        }
        
        public virtual void Show(int id, bool selected = false)
        {
            if (_configs.Length == 0)
            {
                this.Deactivate();
                return;
            }

            var startId = Clamped(id);

            if (selected && _configs[Clamped(id)].Mesh == null) id++;

            id = Clamped(id);
            
            if(_mesh) _mesh.sharedMesh = _configs[id].Mesh;
            if(_meshRender) _meshRender.materials = _configs[id].Materials;
            if (_skinnedMesh)
            {
                _skinnedMesh.sharedMesh = _configs[id].Mesh;
                _skinnedMesh.materials = _configs[id].Materials;
            }
            
            if (selected && !_dontDrawSelected)
            {
                var materials = _meshRender.materials;
                for (int i = 0; i < _meshRender.materials.Length; i++)
                {
                    var material = new Material(_configs[startId].Mesh == null ? _resources.SelectedItemMaterial
                        : _resources.SelectedBoughtItemMaterial);
                    materials[i] = material;
                }

                _meshRender.materials = materials;
            }
            
            if(!_mesh) return;
            if(_mesh.sharedMesh && !isActiveAndEnabled)this.Activate();
            if(!_mesh.sharedMesh && isActiveAndEnabled)this.Deactivate();
        }

        public void SmoothAppear(float duration)
        {
            if (_meshRender)
            {
                _meshRender.material.SetFloat(Opacity, 0f);
                _meshRender.material.DOFloat(1f, Opacity, duration);
            }

            if (_skinnedMesh)
            {
                _skinnedMesh.material.SetFloat(Opacity, 0f);
                _skinnedMesh.material.DOFloat(1f, Opacity, duration);
            }
        }

        public void SmoothHide(float duration)
        {
            if (_meshRender)
            {
                if (duration <= 0f)
                {
                    _meshRender.material.SetFloat(Opacity, 0f);
                    return;
                }
                _meshRender.material.DOFloat(0f, Opacity, duration);
            }

            if (_skinnedMesh)
            {                
                if (duration <= 0f)
                {
                    _skinnedMesh.material.SetFloat(Opacity, 0f);
                    return;
                }
                _skinnedMesh.material.DOFloat(0f, Opacity, duration);
            }
        }

        public void AnimateUpgrade(int level)
        {
            if(_dontAnimateUpgrade) return;
            _cachedLevel = level;
            if(gameObject.activeSelf == false)
            {
                InvokeSystem.StartInvoke(ShowInAnimating, 0.2f);
                return;
            }
            InvokeSystem.StartInvoke(ShowInAnimating, 0.2f);
            var tween = DOTween.Sequence();
            tween.Append(transform.DOScaleY(0f, 0.2f));
            tween.Append(transform.DOScaleY(1f, 1f).SetEase(Ease.OutElastic)); 
        }

        private void ShowInAnimating()
        {
            Show(_cachedLevel);
            if (gameObject.activeSelf == false) return;
            _uiFactory.SpawnPoofFx(transform.position);
            transform.DOScaleY(1f, 1f).SetEase(Ease.OutElastic);
        }
    }
    
    [Serializable]
    public class VisualConfig
    {
        public Mesh Mesh;
        public Material[] Materials;
    }
}