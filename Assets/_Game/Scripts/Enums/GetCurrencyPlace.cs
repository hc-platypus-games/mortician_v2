namespace _Game.Scripts.Enums
{
    public enum GetCurrencyPlace
    {
        None,
        Income,
        Graveyard,
        Store,
        Offline,
        AdOffer,
        Cheats,
        Gardener,
        Quest,
        VideoBoostWindow,
        ClickableEvent
    }
}