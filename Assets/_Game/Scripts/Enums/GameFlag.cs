using System;

namespace _Game.Scripts.Enums
{
    [Serializable] 
    public enum GameFlag
    {
        None,
        TutorialFinished,
        AllAdsRemoved,
        InAppWasBought,
        CharactersOpened,
        CorpseOrdersOpened,
        FirstHardReceived,
        TutorialRunning,
        RateGameShowed,
        RateGameCompleted,
        ManagerIsBought,
        MarketerIsBought
    }
}