namespace _Game.Scripts.Enums
{
    public enum GameEvents
    {
        //События рекламы
        RewardStart,
        RewardShowed,
        RewardClicked,
        RewardFinish,
        RewardCanceled,
        RewardErrorLoaded,
        RewardErrorDisplay,
        InterRequest,
        InterShowed,
        InterErrorLoaded,
        InterErrorDisplay,
        
        BannerShowed,
        BannerHidden,
        
        //События покупок
        FirstInAppPurchased,
        InAppPurchased,

        //События офферов
        // ShowMainWindowOffer,
        // ClickMainWindowOffer,
        // PurchMainWindowOffer,

        //События аналитики
        Install,
        Launch,
        TutorialStep,
        TutorialFinish,
        LevelStart,
        LevelFinish,
        CurrencyGet,
        CurrencySpend,

        //События игрового процесса
        CameraFocused,
        OpenCharacter,
        UpgradeCard,
        UpgradeCharacter,
        TaskReceived,
        TaskFinished,
        WatchAdVideo,
        PhoneCalled,
        CallEventEnded,
        CorpseDelivered,
        CorpseEmbalmed,
        CorpseDeliveredToGrave,
        CorpseBuried,
        BoxOpened,
        ElectricianEventEnded,

        //Типы тасков
        BuyRoomItem,
        UpgradeRoomItem,
        ReachItemLevel,
        SetFlags,
        UpgradeRoom,
        BuyRoom,
        OpenRoom,
        BuyGravePlaces,
        TaskCompleted
    }
}