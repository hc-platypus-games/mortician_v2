namespace _Game.Scripts.Enums
{
    public enum RoomType
    {
        None,
        Hall,
        Garage,
        Embalming,
        FoodService,
        ToiletService,
        RestService,
        Farewell,
        Cemetery,
        Cremation,
        Cryogenic,
        FuneralVisitorParking,
        CorpsePorter,
        Events,
        Cleaner,
        Research,
        Director,
    }
}