namespace _Game.Scripts.Enums
{
    public enum GameParamType
    {
        None,
        
        // Currency
        Soft,
        Hard,
        Tokens,
        Researchers,
        Workers,
        Card,
        
        AdTickets,
        
        // Ad params
        VideoBoostCounter,
        MainWindowOfferShowDuration,
        MainWindowOfferPlayDuration,
        
        // Game logic params
        Level,
        Price,
        MorticianHirePrice,
        MorticianTrainPrice,
        CycleDelay,
        MorticianCycle,
        Income,
        Seats,
        Capacity,
        CardIncomeBonus,
        CardCycleDelayBonus,
        BuildingTimer,
        IncomeFriction,
        PriceFriction,

        //
        OfflineTime,
        OfflineValue,
        MaxOfflineTime,
        DailyQuestsRewardReceived,
        DailyQuestsRewardCounter,
        CorpseOrderTimer,
        PriceMorticianHireFriction,
        PriceMorticianTrainFriction,
        BaseFriction,
        CorpsesProgress,
        MaxWorkers,
        MaxResearchers,
        PurchaseCount 
    }
}