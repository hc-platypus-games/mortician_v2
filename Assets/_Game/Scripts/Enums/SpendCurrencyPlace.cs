﻿namespace _Game.Scripts.Enums
{
    public enum SpendCurrencyPlace
    {
        None,
        HardReturnToGameWindow,
        RoomInfoWindow,
        BuyRoomWindow,
        BuyBox,
        BuySkipTime,
        BuyCemetery,
        CemeteryWindow,
        UpgradeCharacter,
        MapPointWindow,
        UpgradeRoom
    }
}