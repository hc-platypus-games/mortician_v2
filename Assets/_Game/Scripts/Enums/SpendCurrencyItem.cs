namespace _Game.Scripts.Enums
{
    public enum SpendCurrencyItem
    {
        None,
        Room,
        RoomItem,
        Character,
        Cemetery,
        Box,
        SkipTime,
        HireGardener,
        HireMortician,
        TrainGardener,
        TrainMortician,
        Soft,
        BuyPointWindow,
        Hard
    }
}