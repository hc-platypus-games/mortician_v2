namespace _Game.Scripts.Enums
{
    public enum AdPlacement
    {
        None,
        VideoBoost,
        DailyQuests,
        RoomIncome,
        RoomSpeed,
        ItemsUpgradeDiscount,
        FirstHard,
        SpecialEvent,
        ReturnToWindow,
        FastCash,
        FreeBox,
        FreeHard
    }
}