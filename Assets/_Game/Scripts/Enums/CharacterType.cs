using System;

namespace _Game.Scripts.Enums
{
        
    [Flags]
    public enum CharacterType
    {
        Default = 1 << 1,
        Visitor = 1 << 2,
        Gardener = 1 << 3
    }
}
