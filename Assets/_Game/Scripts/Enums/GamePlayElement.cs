﻿namespace _Game.Scripts.Enums
{
    public enum GamePlayElement
    {
        None,
        Currency, //1
        CardsButton, //2
        VideoBoostButton, //3
        Soft, //4
        Hard, //5
        Tokens, //6
        TasksButton, //7
        StoreButton, //8
        SettingsButton, //9
        LocationsButton, //10
        CorpseOrdersButton, //11
        AdTickets, //12
        StatisticButton, //13
        MapButtons, //14
        CurrentEmotionButton, //15
        SpeedUpButton, //16
    }
}
