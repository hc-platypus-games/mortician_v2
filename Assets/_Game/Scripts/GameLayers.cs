﻿namespace _Game.Scripts
{
    public static class GameLayers
    {
        public const int
            PLAYER_LAYER = 11,
            DOORS_LAYER = 12,
            NAVMESH_OBSTACLES = 11;

        public const int
            PLAYER_MASK = 1 << PLAYER_LAYER,
            DOORS_MASK = 1 << DOORS_LAYER,
            OBSTACLES_MASK = 1 << NAVMESH_OBSTACLES;

    }
}