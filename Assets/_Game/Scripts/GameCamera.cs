using System;
using _Game.Scripts.Enums;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Game.Scripts
{
    public class GameCamera : MonoBehaviour, ITickableSystem
    {
        public Action CameraFocused;

        [SerializeField] private Vector3 _offsetPostition;
        
        [Inject] private AppEventProvider _eventProvider;

        private LevelSystem _levels;
        private InputSystem _input;

        private static Camera _unityCam;

        private const float ZOOM_STEP = 0.05f;
        private const float TARGET_FOLLOW_SPEED = 0.1f;
        private float _minCamSize;
        private float _maxCamSize;

        private Vector3 _boundsMin;
        private Vector3 _boundsMax;

        private Tween _tweenFocus;
        private Tween _tweenFocusScale;
        private Transform _target;
        private Vector3 _camPositionLerp;
        private float _lastCamSize;

        [Inject]
        public void Construct(LevelSystem levels, InputSystem inputSystem)
        {
            _unityCam = GetComponent<Camera>();
            _levels = levels;
            _levels.OnLoadedLevel += OnLevelLoaded;
            _input = inputSystem;
        }

        private void OnLevelLoaded()
        {
            _minCamSize = _levels.CurrentLevel.MinCamSize;
            _maxCamSize = _levels.CurrentLevel.MaxCamSize;
            _boundsMin = _levels.CurrentLevel.BoundsMin;
            _boundsMax = _levels.CurrentLevel.BoundsMax;

            transform.position = _levels.CurrentLevel.DefaultPos + _offsetPostition;

            _unityCam.orthographicSize = _minCamSize;
        }

        public void Tick(float deltaTime)
        {
            if (_target != null)
            {
                Move(_target?.transform);
            }
        }
        
        public bool Move(Vector3 delta)
        {
            var localPosition = transform.position - _offsetPostition;
            var deltaPos = new Vector3(localPosition.x + delta.x,
                localPosition.y,
                localPosition.z + delta.z);

            var newPosition = GetBounded(deltaPos);
            transform.localPosition = newPosition + _offsetPostition;
            return (deltaPos - newPosition).magnitude > 0.01f;
        }
		
        public bool Move(Transform target)
        {
            var position = target.position;
            var newPosition = GetBounded(position);
            _camPositionLerp = Vector3.Lerp(_camPositionLerp, newPosition, TARGET_FOLLOW_SPEED);
            transform.position = _camPositionLerp + _offsetPostition;
            return (position - newPosition).magnitude > 0.01f;
        }

        public bool MoveTo(Vector3 position)
        {
            var newPosition = GetBounded(position);
            transform.position = newPosition + _offsetPostition;
            return (position - newPosition).magnitude > 0.01f;
        }

        public void Zoom(bool inc, float zoomStep = 0)
        {
            var zoom = zoomStep > 0 ? zoomStep : ZOOM_STEP;
            var size = Mathf.Clamp(_unityCam.orthographicSize + (inc ? zoom : -zoom),
                _minCamSize,
                _maxCamSize);

            _unityCam.orthographicSize = size;
        }

        public void Zoom(float delta)
        {
            var size = Mathf.Clamp(_unityCam.orthographicSize + delta * ZOOM_STEP,
                _minCamSize,
                _maxCamSize);

            _unityCam.orthographicSize = size;
        }


        private Vector3 GetBounded(Vector3 position)
        {
            if (position.x < _boundsMin.x) position.x = _boundsMin.x;
            if (position.x > _boundsMax.x) position.x = _boundsMax.x;

            if (position.z < _boundsMin.z) position.z = _boundsMin.z;
            if (position.z > _boundsMax.z) position.z = _boundsMax.z;

            return position;
        }

        public Ray GetRay(Vector3 pos = default)
        {
            return _unityCam.ScreenPointToRay(pos == default ? Input.mousePosition : pos);
        }

        public void SetScale(float value = -1f, float duration = 1f)
        {
            _lastCamSize = Mathf.Clamp(_lastCamSize, _minCamSize, _maxCamSize);
            if (value <= 0) value = _lastCamSize;
            var delta = _unityCam.orthographicSize - value;
            if (delta < 0) delta *= -1;
            UpdateLastCamSize();
            _tweenFocusScale?.Kill();
            _tweenFocusScale = _unityCam.DOOrthoSize(value, duration * delta);
        }

        public void SetLastScale()
        {
            SetScale(-1f);
        }

        public void SetMinScale()
        {
            SetScale(_minCamSize);
        }

        internal void UpdateLastCamSize()
        {
            _lastCamSize = _unityCam.orthographicSize;
            _lastCamSize = Mathf.Clamp(_lastCamSize, _minCamSize, _maxCamSize);
        }

        public void Focus(Vector3 pos, float duration = 1f, bool isAction = false)
        {
            pos += _offsetPostition;
            _tweenFocus?.Kill();
            _target = null;
            _input.StopScroll();

            if (isAction)
            {
                _tweenFocus = transform.DOMove(pos, duration)
                    .SetEase(Ease.Linear)
                    .OnComplete(OnCameraFocused);
            }
            else
            {
                _tweenFocus = transform.DOMove(pos, duration).SetEase(Ease.Linear);
            }
        }

        public void Focus(Vector3 pos, float scale, float duration = 1f, bool isAction = false)
        {
            UpdateLastCamSize();
            _tweenFocusScale?.Kill();
            _tweenFocusScale = _unityCam.DOOrthoSize(scale, duration)
                .SetEase(Ease.Linear);
            Focus(pos, duration, isAction);
        }

        private void OnCameraFocused()
        {
            CameraFocused?.Invoke();
            _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.CameraFocused);
        }

        public void FollowObject(Transform target, float duration = 0.5f)
        {
            _tweenFocus?.Kill();
            _input.StopScroll();
            _tweenFocus = transform.DOMove(target.transform.position + _offsetPostition, duration)
                .SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    _target = target;
                    _camPositionLerp = transform.position - _offsetPostition;
                });
        }

        public void RemoveTarget()
        {
            _tweenFocus?.Kill();
            _target = null;
        }
    }
}