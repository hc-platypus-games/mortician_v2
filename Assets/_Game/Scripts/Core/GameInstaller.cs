using System;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Ads;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Systems.PushNotifications;
using _Game.Scripts.Systems.Save;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Systems.Tutorial;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Cards;
using _Game.Scripts.Ui.Offers;
using _Game.Scripts.Ui.Room;
using _Game.Scripts.Ui.WorldSpace;
using _Game.Scripts.UI.WorldSpace;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.Corpse;
using _Game.Scripts.View.Fx;
using _Game.Scripts.View.Vehicle;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Core
{
    public class GameInstaller : MonoInstaller, IInitializable, IFixedTickable
    {
        [SerializeField] private SceneData _sceneData;

        [Inject] private Prefabs _prefabs;

        private bool _inited;
        private GameSystem _game;
        private LoadingSystem _loading;
        private PushNotificationSystem _pushNotifications;
        private InAppSystem _inApps;

        public override void InstallBindings()
        {
            BindScriptableObjects();
            BindSystems();
            BindFactories();
            BindPools();
            InitExecutionOrder();

            _game = Container.Resolve<GameSystem>();
            _loading = Container.Resolve<LoadingSystem>();
            _pushNotifications = Container.Resolve<PushNotificationSystem>();
            _inApps = Container.Resolve<InAppSystem>();
        }

        private void BindScriptableObjects()
        {
            Container.BindInstance(_sceneData).AsSingle().NonLazy();
        }
        
        private void BindSystems()
        {
            Container.Bind<InAppSystem>().AsSingle().NonLazy();
            Container.Bind<AppEventProvider>().AsSingle().NonLazy();
            Container.Bind<TestTools>().AsSingle().NonLazy();
            Container.Bind<LoadingSystem>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<GameSystem>().AsSingle().NonLazy();
            Container.Bind<LevelSystem>().AsSingle().NonLazy();
            Container.Bind<GameSettings>().AsSingle().NonLazy();
            Container.Bind<AdSystem>().AsSingle().NonLazy();
            Container.Bind<PushNotificationSystem>().AsSingle().NonLazy();
            Container.Bind<TutorialSystem>().AsSingle().NonLazy();
            
            Container.BindInterfacesAndSelfTo<GameInstaller>().FromInstance(this).AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<GameCamera>().FromInstance(_sceneData.Camera).AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<InputSystem>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<CheatsSystem>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SaveSystem>().AsSingle().NonLazy();

            AddSinglePrefab<ConnectionSystem>();
            AddSinglePrefab<TutorialArrow>();
            AddSinglePrefab<TaskCompletedUI>();

            //Bind game logic systems
            Container.Bind<MathSystem>().AsSingle().NonLazy();
            Container.Bind<CardsFactory>().AsSingle().NonLazy();
            Container.Bind<HardSystem>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<BoostSystem>().AsSingle().NonLazy();
            Container.Bind<TaskSystem>().AsSingle().NonLazy();
            
            Container.BindInterfacesAndSelfTo<WindowsSystem>().AsSingle().NonLazy();
        }

        private void BindFactories()
        {
            Container.BindInterfacesAndSelfTo<GameProgressFactory>().AsSingle().NonLazy();
            Container.Bind<RoomsFactory>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<OffersFactory>().AsSingle().NonLazy();
            
            Container.Bind<MapPointsFactory>().AsSingle().NonLazy();
            Container.Bind<LevelEventsFactory>().AsSingle().NonLazy();

            Container.Bind<CorpseFactory>().AsSingle().NonLazy();
            Container.Bind<GameParamFactory>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<CharactersFactory>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<VehicleFactory>().AsSingle().NonLazy();
            Container.Bind<UIFactory>().AsSingle().NonLazy();
            
            Container.BindFactory<CardUI, CardUI.Factory>()
                     .FromComponentInNewPrefab(_prefabs.LoadPrefab<CardUI>());
        }

        private void BindPools()
        {
            //Init game params
            Container.BindMemoryPool<GameParam, GameParam.Pool>().WithInitialSize(100);
            Container.BindMemoryPool<GameProgress, GameProgress.Pool>().WithInitialSize(100);
            
            //Init game objects
            Container.BindMemoryPool<CarView, CarView.Pool>()
                .WithInitialSize(10)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<CarView>(i => i.IsPoolPrefab))
                .UnderTransformGroup("Cars");
            
            Container.BindMemoryPool<CharacterView, CharacterView.Pool>()
                .WithInitialSize(30)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<CharacterView>())
                .UnderTransformGroup("Characters");
            
            Container.BindMemoryPool<CorpseView, CorpseView.Pool>()
                .WithInitialSize(20)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<CorpseView>())
                .UnderTransformGroup("Corpses");
            
            Container.BindMemoryPool<PoofFx, PoofFx.Pool>()
                .WithInitialSize(10)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<PoofFx>())
                .UnderTransformGroup("FX");
            
            //Init UI elements
            Container.BindMemoryPool<RoomInfoItem, RoomInfoItem.Pool>()
                .WithInitialSize(20)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<RoomInfoItem>())
                .UnderTransformGroup("RoomInfoItems");
            
            Container.BindMemoryPool<MessageUI, MessageUI.Pool>()
                .WithInitialSize(10)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<MessageUI>())
                .UnderTransformGroup("Messages");
            
            Container.BindMemoryPool<ResourceBubbleUI, ResourceBubbleUI.Pool>()
                .WithInitialSize(10)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<ResourceBubbleUI>())
                .UnderTransformGroup("ResourceBubbles");
            
            //Init UI World Space elements
            Container.BindMemoryPool<Bubble, Bubble.Pool>()
                .WithInitialSize(20)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<Bubble>())
                .UnderTransformGroup("Bubbles");
            
            Container.BindMemoryPool<ProgressBarView, ProgressBarView.Pool>()
                .WithInitialSize(20)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<ProgressBarView>())
                .UnderTransformGroup("Bubbles");
            
            Container.BindMemoryPool<TextFxView, TextFxView.Pool>()
                .WithInitialSize(20)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<TextFxView>())
                .UnderTransformGroup("Bubbles");
            
            Container.BindMemoryPool<MainWindowOffer, MainWindowOffer.Pool>()
                .WithInitialSize(5)
                .FromComponentInNewPrefab(_prefabs.LoadPrefab<MainWindowOffer>())
                .UnderTransformGroup("MainWindowOffers");
        }
        
        private void AddSinglePrefab<T>(bool copyPrefab = false) where T : MonoBehaviour
        {
            var prefab = copyPrefab 
                ? _prefabs.CopyPrefab<T>(transform) 
                : GetComponentInChildren<T>(true);
            
            if (prefab == null) return;
            Container.BindInterfacesAndSelfTo<T>().FromInstance(prefab).AsSingle().NonLazy();
        }
        
        private void InitExecutionOrder()
        {
        }

        public void Initialize()
        {
            _loading.Loading();
            _inited = true;
        }

        private void Update()
        {
            Tick();
        }

        private void Tick()
        {
            var deltaTime = Time.deltaTime;
            if (_game.GamePaused)
            {
                deltaTime = Time.unscaledDeltaTime;
                InvokeSystem.ForceTick(deltaTime);
                return;
            }
            
            DOTween.ManualUpdate(deltaTime, Time.unscaledDeltaTime);
            foreach (var element in Container.ResolveAll<ITickableSystem>())
            {
                element.Tick(deltaTime);
            }
            
            InvokeSystem.Tick(deltaTime);
        }

        public void FixedTick()
        {
            if (_game.GamePaused) return;
            
            var deltaTime = Time.fixedDeltaTime;
            foreach (var element in Container.ResolveAll<IFixedTickableSystem>())
            {
                element.Tick(deltaTime);
            }
        }
        
        private void OnApplicationPause(bool pause)
        {
            if (!_inited) return;
            if(_inApps.IsPurchasingNow) return;
            var adSystem = Container.Resolve<AdSystem>();
            if(adSystem.AdIsShowing) return;
            if (pause)
            {
                Debug.Log("Paused" + DateTime.Now);
                _game.PauseGame();
            }
            else
            {
                Debug.Log("Loading" + DateTime.Now);
                _loading.ResumeGame();
            }
            
            _pushNotifications.UpdateNotifications(pause);
        }
    }
}