using _Game.Scripts.Components.Base;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using Zenject;

namespace _Game.Scripts.Components.Loader
{
    public class EndLoadingComponent : BaseComponent
    {
        [Inject] private UIFactory _uiFactory;
        [Inject] private OffersFactory _offers;
        
        public override void Start()
        {
            _uiFactory.UpdateBadges();
            _offers.StartMainWindowOffers();

            InvokeSystem.StartExternalInvoke(End, 1f);
        }
    }
}