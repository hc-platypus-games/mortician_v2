﻿using _Game.Scripts.Components.Base;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Systems.Save;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Systems.Tutorial;
using Zenject;

namespace _Game.Scripts.Components.Loader
{
    /// <summary>
    /// Класс для инициализации систем перед загрузкой игры
    /// </summary>
    public class InitGameSystems : BaseComponent
    {
        [Inject] private InAppSystem _inApps;
        [Inject] private SaveSystem _save;
        [Inject] private TutorialSystem _tutorial;
        [Inject] private GameSystem _game;
        [Inject] private CardsFactory _cards;
        [Inject] private OffersFactory _offers;
        [Inject] private BoostSystem _boosts;
        [Inject] private TaskSystem _tasks;
        [Inject] private CorpseFactory _corpseFactory;
        [Inject] private LevelEventsFactory _levelEvents;
        
        public override void Start()
        {
            _save.Init();
            _inApps.Init();
            _game.Init();
            _cards.Init();
            _boosts.Init();
            _tasks.Init();
            _offers.Init();
            _tutorial.Init();
            _corpseFactory.Init();
            _levelEvents.Init();

            base.Start();
            End();
        }
    }
}