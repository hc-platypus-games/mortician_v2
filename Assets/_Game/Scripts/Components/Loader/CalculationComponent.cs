﻿using System;
using _Game.Scripts.Balance;
using _Game.Scripts.Components.Base;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Systems.Save;
using _Game.Scripts.Ui.Offers;
using _Game.Scripts.View.MapPoints;
using Zenject;

namespace _Game.Scripts.Components.Loader
{
    public class CalculationComponent : BaseComponent, IGameParam
    {
        [Inject] private SaveSystem _save;
        [Inject] private GameParamFactory _params;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private BoostSystem _boosts;
        [Inject] private GameSystem _game;
        [Inject] private OffersFactory _offers;
        [Inject] private GameProgressFactory _progresses;
        [Inject] private GameFlags _flags;

        private double _offlineTime;
        public float OfflineValue { get; private set; }
        public float CashValue { get; private set; }

        public override void Start()
        {
            if (!_save.IsSave && _game.GameIsLoading)
            {
                End();
                return;
            }
            var maxTime = _params.GetParamValue<GameSystem>(GameParamType.MaxOfflineTime);
            _offlineTime = (DateTime.Now - _save.SaveTime).TotalSeconds;
            if (_offlineTime > maxTime) _offlineTime = maxTime;
            if (_offlineTime < 0) return;

            CalculationOfflineIncome();
            CalculateOfflineOffers();
            CalculateOfflineProgresses();
            CalculateOfflineBoosts();
            End();
        }

        private void CalculationOfflineIncome()
        {
            var softParam = _params.GetParam<GameSystem>(GameParamType.Soft);
            var previousIncome = softParam.Value;
            OfflineValue = GetIncomeByTime((float)_offlineTime);
            if (OfflineValue > 0)
            {
                _game.AddCurrency(GameParamType.Soft, OfflineValue, GetCurrencyPlace.Offline);
            }
            var param = _params.CreateParam(this, GameParamType.OfflineValue, 0);
            param.SetValue(OfflineValue);
        }

        private void CalculateOfflineOffers()
        {
            foreach (var item in _offers.GetSavedOffers())
            {
                var offer = (StoreOffer)item;
                offer.CalculateOffline(_offlineTime);
            }
        }

        private void CalculateOfflineProgresses()
        {
            foreach (var item in _progresses.Progresses)
            {
                if(item.Owner is GravePlacePointView) continue;
                item.Tick((float)_offlineTime);
            }
        }

        private void CalculateOfflineBoosts()
        {
            foreach (var item in _boosts.Boosts)
            {
                item.Update((float)_offlineTime);
            }
        }

        public float GetIncomeByTime(float time)
        {
            var cycles = Math.Round(time);
            var boost = _boosts.GetAdBoost(AdPlacement.VideoBoost);
            var profitValue = _game.Income() / 30;
            if (boost != null && boost.IsActive)
            {
                var boostCycles = Math.Round(boost.LeftDuration);
                if (boostCycles > cycles) boostCycles = cycles;
                cycles += boostCycles;
                profitValue /= 2;
            }
            return (float) (cycles < 1 ? 0 : profitValue * cycles);
        }

        public void SetCashValue(float value)
        {
            CashValue = value;
        }
    }
}