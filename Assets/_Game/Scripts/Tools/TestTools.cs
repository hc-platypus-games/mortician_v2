using System.IO;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Save;
using _Game.Scripts.Systems.Tutorial;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Room;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine.UDP.Common;
using Zenject;

namespace _Game.Scripts.Tools
{
    public class TestTools
    {
#if UNITY_EDITOR
        private static DiContainer _container;
        private static bool _isPause;
        
        public TestTools(DiContainer container)
        {
            _container = container;
        }

        public static void RunMagicTest(string param = "")
        {
            string path = EditorUtility.OpenFilePanel("Load Save From File", "", "json");
            var file = File.Exists(path);
            if (!file)
            {
                return;
            }
            var data = File.ReadAllText(path);
            var data1 = (JObject)JsonConvert.DeserializeObject(data);
            
            var wrapper = MiniJson.JsonDecode(data);
            if (wrapper != null) 
            {
                //var store = (string) wrapper["Store"];
                //var transactionID = (string) wrapper["TransactionID"];
                var payload = (string) wrapper["Payload"];
            
                var details = MiniJson.JsonDecode(payload);
                var receiptJson = (string) details["json"];
                //var signature = (string) details["signature"];
            
                var orderId = MiniJson.JsonDecode(receiptJson);
                var a = orderId["orderId"];
            }
        }
#endif
    }
}