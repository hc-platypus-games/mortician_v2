using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Tools;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Zenject;
using Random = _Game.Scripts.Tools.Random;

namespace _Game.Scripts.ScriptableObjects
{
    /// <summary>
    /// Используется для хранения ресурсов
    /// </summary>
    [CreateAssetMenu(fileName = "GameResource", menuName = "_Game/GameResources", order = 1)]
    public class GameResources : ScriptableObjectInstaller
    {
        [SerializeField] private List<Sprite> _sprites;
        [SerializeField] private List<Sprite> _avatars;
        [SerializeField] private List<Sprite> _characterIcons;
        [SerializeField] private List<Sprite> _roomIcons;
        [SerializeField] private List<Sprite> _roomImages;
        [SerializeField] private List<Sprite> _mapEventPoints;
        
        [SerializeField] private Sprite _placeholder;
        [SerializeField] private Sprite _placeholderAvatar;
        [SerializeField] private Sprite _placeholderRoomImage;
        
        [SerializeField] private Material _selectedItemMaterial;
        [SerializeField] private Material _selectedBoughtItemMaterial;
        [SerializeField] private Material _grayscaleSpriteMaterial;
        
        [Header("Character Emotions")]
        public float CharacterEmotionLength = 5;
        public Sprite[] ToiletNeed;
        public Sprite[] KitchenNeed;
        public Sprite[] RestNeed;
        public Sprite[] AngryEmotions;
        public Sprite[] SadEmotions;
        public Sprite[] NeutralEmotions;
        
        [Header("Character Text Emotions")]
        public string[] QueueTextEmotions;
        public string[] TouchReactionTexts;
        public string[] CremationsTexts;
        public string[] CryogenicsTexts;
   
        public Material SelectedItemMaterial => _selectedItemMaterial;
   
        public Material SelectedBoughtItemMaterial => _selectedBoughtItemMaterial;
        public Material GrayscaleSpriteMaterial => _grayscaleSpriteMaterial;
        
        private static GameResources _instance;
        public static GameResources Instance
        {
            get
            {
                if (_instance == null) _instance = Resources.Load<GameResources>(nameof(GameResources));
                return _instance;
            }
        }
        
        public Sprite GetSprite<T>(T type) where T: Enum
        {
            var result = _sprites.FirstOrDefault(s => s.name.Equals(type.ToString()));
            return result == null ? _placeholder : result;
        }
        
        public Sprite GetSprite(string key)
        {
            var result = _sprites.FirstOrDefault(s => s.name == key);
            return result == null ? _placeholder : result;
        }
        
        public Sprite GetAvatarSprite(string key)
        {
            var result = _avatars.FirstOrDefault(s => s.name == key);
            return result == null ? _placeholderAvatar : result;
        }
        
        public Sprite GetRoomIconSprite(string key)
        {
            var result = _roomIcons.FirstOrDefault(s => s.name == key);
            return result == null ? _placeholder : result;
        }
        
        public Sprite GetRoomImagesSprite(string key)
        {
            var result = _roomImages.FirstOrDefault(s => s.name == key);
            return result == null ? _placeholderRoomImage : result;
        }
        
        public Sprite GetMapEventPointIconSprite(string key)
        {
            var result = _mapEventPoints.FirstOrDefault(s => s.name == key);
            return result == null ? _placeholder : result;
        }
        
        public Sprite GetCharacterIconSprite(int id)
        {
            return _characterIcons[id];
        }

        public int GetCharacterIconId()
        {
            return Random.Range(0, _characterIcons.Count);
        }
        
        [Button("Parse")]
        public void Parse()
        {
            _avatars = Resources.LoadAll<Sprite>("Avatars").ToList();
            _roomIcons = Resources.LoadAll<Sprite>("RoomItemIcons").ToList();
            _roomImages = Resources.LoadAll<Sprite>("RoomImages").ToList();
            _mapEventPoints = Resources.LoadAll<Sprite>("MapEventPointsSprites").ToList();
            _characterIcons = Resources.LoadAll<Sprite>("CharacterIconsSprites").ToList();
            
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }
    }
}