using System;
using System.Collections.Generic;
using _Game.Scripts.Balance.Attributes;
using _Game.Scripts.Enums;
using UnityEngine;

namespace _Game.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "BalanceConfig", menuName = "_Game/BalanceConfig", order = 0)]
    [TableSheetName("Constants")]
    public class BalanceConfig : ScriptableObject, IParsable
    {
        public int StartSoft;
        public int IncomeDelay;
        public int MaxOfflineTime;
        
        public int DailyQuestsComplete;
        public int ReturnWindowHardPrice;
        public int RewardCompleteTask;

        public float PriceHardPerMinute;
        
        [Header("CorpseOrders")]
        public int CorpseOrderDelay;
        public int MaxCorpseOrder;
        public int SoftRewardForCorpseOrder;
        public int TokensRewardForCorpseOrder;
        
        [Header("CardsSettings")]
        public int SoftMinWeight;
        public int SoftMaxWeight;
        public int SoftWeight;
        public int CardSoftReward;
        public int CardTokensUpgradePrice;

        [Header("AdSettings")] 
        public int VideoBoostHardValue;
        public int MainWindowOfferDuration;
        public int FirstHardValue;
        public int FastCashValue;
        public int DelayBetweenOffers;
        public int DelayBetweenOffersWithoutAds;

        [Header("OffersSettings")] 
        public int AgentIncomeInTime;
        public int AgentHardAmount;
        
        [Header("CarsSettings")]
        public int CarMoveSpeed;
        public int CarRotationSpeed;
        public int CarStoppingDistance;

        [Header("Tutorial")] public float TutorialCycleDelay;
        
        public List<RoomConfig> Rooms;
        public List<RoomItemConfig> Items;
        public List<ProgressionConfig> Progressions;
        public List<CardConfig> Cards;
        public List<BoostConfig> Boosts;
        public List<TaskConfig> Tasks;
        public List<TutorialConfig> Tutorial;
        public List<OfferConfig> Offers;
        public List<MapEventPointConfig> MapEventPoints;
        public List<MapConfig> MapConfigs;
        
        public void OnParsed()
        {
            foreach (var task in Tasks)
            {
                task.OnParsed();
            }
        }
    }
    
    [Serializable]
        public class RoomConfig
        {
            public RoomType RoomType;
            public int StartLevel;
            public float CycleDelay;
            public float CycleDelay2;
            public float CycleDelay3;
            public float PriceLevel1;
            public float PriceLevel2;
            public float PriceLevel3;
            public float TimerLevel1;
            public float TimerLevel2;
            public float TimerLevel3;
            public bool IncludeStatistic;
        }
	
        [Serializable]
        public class RoomItemConfig
        {
            public RoomType RoomType;
            public int ItemId;
            public int SortId;
            public bool BoughtForHard;
            public int RequiredLevel;
            public int StartLevel;
            public int Cap;
            public string Name;
            public int Capacity;
            public float Price;
            public int Income;
            public float CycleDelay;
            public int Seats;
            public float BuildingTimer;
            public int FullId => (int) RoomType * 1000 + ItemId;
        }

        [Serializable]
        public class ProgressionConfig
        {
            public GameParamType ParamType;
            public float Value;
            public int BaseValue;
        }

        [Serializable]
        public class CharacterConfig
        {
            public RoomType Room;
            public CharacterType Type;
            public CardConfig Card;
            public bool Savable;
        }
        
        [Serializable]
        public class CardConfig
        {
            public int Region;
            public int Id;
            public CardType CardType;
            public CardQuality Quality;
            public RoomType RoomType;
            public int RareBoxWeight;
            public int EpicBoxWeight;
            public int LegendaryBoxWeight;
            public string Name;
        }
	
        [Serializable]
        public enum CardType
        {
            CardType1,
            CardType2,
            CardType3,
            CardType4
        }

        [Serializable]
        public enum CardQuality
        {
            Common,
            Epic,
            Legendary
        }
        
        [Serializable]
        public class BoostConfig
        {
            public int Id;
            public AdPlacement Placement;
            public RoomType RoomType;
            public int Sort;
            public GameParamType ParamType;
            public float K;
            public float Duration;
            public float NextDurationDelta;
            public int MaxCount;
            public bool IncludePrevDuration;
            public bool Global;
        }
        
        [Serializable]
        public class TaskConfig : IParsable
        {
            public int ID;
            public int Region;
            public GameEvents TaskType;
            public string Var1;
            public string Var2;
            public string Var3;
            public string RewardText;
            public TaskClass Class;
            public TaskAction ActionStart;
            public TaskAction ActionCompleted;
            public TaskAction ActionFinish;
            [SkipTableValue] public Reward Reward;
            
            public void OnParsed()
            {
                if (string.IsNullOrEmpty(RewardText)) return;
			
                var parse = RewardText.Split('-');
                Enum.TryParse(parse[0], true, out GameParamType type);
                float.TryParse(parse[1], out var amount);

                var id = 0;
                if (parse.Length >= 3) int.TryParse(parse[2], out id);
                    
                if (type == GameParamType.None || amount == 0) return;
                    
                Reward = new Reward
                {
                    Type = type,
                    Amount = amount,
                    Id = id
                };
            }
        }
        
        [Serializable]
        public enum TaskClass
        {
            Task,
            DailyQuest
        }
        
        [Serializable]
        public enum TaskAction
        {
            None,
            RateGame,
            RunCardTutorial
        }
        
        [Serializable]
        public class Reward
        {
            public GameParamType Type;
            public int Id;
            public float Amount;
        }
        
        [Serializable]
        public class TutorialConfig
        {
            public int Region;
            public int StepId;
            public TutorialStepAction StepAction;
            public string StepActionParam1;
            public string AnalyticsStep;
        }

        [Serializable]
        public enum TutorialStepAction
        {
            None,
            ShowTips,
            ShowPhoneCall,
            FocusOnRoom,
            TutorialFocusOnRoom,
            SetCameraScale,
            RoomInfoOpen,
            RoomInfoPressButton,
            RoomInfoClose,
            FirstEventCall,
            FirstCorpseDelivery,
            FirstCorpseReadyToEmbalm,
            FirstCorpseEmbalm,
            PortCorpseToGrave,
            CemeteryOpen,
            CemeteryPressButton,
            CemeteryClose,
            FirstCorpseBury,
            FocusGamePlayElement,
            VideoBoostPressButton,
            VideoBoostCloseButton,
            TaskInfoPressButton,
            BuyRoomOpen,
            BuyRoomPressButton,
            HideGamePlayElement,
            ShowGamePlayElement,
            StorePressBox,
            BoxReceive,
            RoomInfoPressCharacter,
            CardInfoPressButton,
            TutorialFinish,
            GardenGrave,
            ElectricianEvent,
            RestorePurchases,
            RoomAvailableEvent
        }

        [Serializable]
        public class OfferConfig
        {
            public int Region;
            public int Id;
            public string ShopId;
            public string SKU;
            public string SkuGroup;
            public bool NonConsumable;
            public int Value;
            public int Value1;
            public int Price;
            public int Count;
            public int CycleDelay;
            public string Type;
            public string ShopIdFull;
        }
        
        [Serializable]
        public class MapEventPointConfig
        {
            public int Id;
            public string Name;
            public float Price;
            public int BuildingTime;
            public int CycleDelay;
            public int StartLevel;
            public int Cap;
        }

        [Serializable]
        public class MapConfig
        {
            public int Id;
            public int Corpse;
        }
}