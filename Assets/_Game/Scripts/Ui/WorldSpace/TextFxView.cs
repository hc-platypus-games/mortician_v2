using System;
using _Game.Scripts.Ui.Base;
using TMPro;
using Zenject;

namespace _Game.Scripts.Ui.WorldSpace
{
    public class TextFxView : BaseUIView
    {
        private TextMeshProUGUI _tmp;
        
        public class Pool : MonoMemoryPool<TextFxView>
        {
            protected override void Reinitialize(TextFxView bubble)
            {
                bubble.Reset();
            }
        }

        private void Reset()
        {
            _tmp = GetComponentInChildren<TextMeshProUGUI>(true);
        }

        public void Show(string value)
        {
            _tmp.text = value;
        }
    }
}
