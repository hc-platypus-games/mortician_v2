using _Game.Scripts.Tools;
using _Game.Scripts.View;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.UI.WorldSpace
{
    public class ProgressBarView : BaseView
    {
        [SerializeField] private Image _bar;
        [SerializeField] private Color _characterColor;
        [SerializeField] private Color _resetColor;
        
        public class Pool : MonoMemoryPool<ProgressBarView>
        {
            protected override void Reinitialize(ProgressBarView bubble)
            {
                bubble.Reset();
            }
        }

        public override void Init()
        {
            transform.localScale = Vector3.one;
            this.Activate();
        }

        public void UpdateUI(float value)
        {
            _bar.fillAmount = value;
        }

        public void SetType(int type)
        {
            _bar.color = type == 1 ? _resetColor : _characterColor;
        }
    }
}

