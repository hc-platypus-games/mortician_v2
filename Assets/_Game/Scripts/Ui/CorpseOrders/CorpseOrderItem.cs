﻿using System;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.CorpseOrders
{
    public class CorpseOrderItem : BaseUIView
    {
        public Action<CorpseOrderItem> OnCompleted;
        
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _status;
        [SerializeField] private TextMeshProUGUI _preparingDesc;
        [SerializeField] private TextMeshProUGUI _reward1;
        [SerializeField] private TextMeshProUGUI _reward2;
        [SerializeField] private TextMeshProUGUI _sliderText;
        [SerializeField] private GameObject _sliderObj;
        [SerializeField] private Image _slider;
        [SerializeField] private Image _characterImage;
        [SerializeField] private BaseButton _button;

        [Inject] private UIFactory _ui;

        public CorpseOrder Order { get; private set; }
        private GameProgress _progress;

        public void Init()
        {
            _button.SetCallback(OnPressedButton);
        }

        private void OnPressedButton()
        {
            OnCompleted?.Invoke(this);
            foreach (var reward in Order.Rewards)
            {
                _ui.SpawnResourceBubble(reward.Type, reward.Amount, _button.RectTransform.position);
            }
        }
        
        public void UpdateLocalization()
        {
            _button.SetText("GET".ToLocalized());
            _preparingDesc.SetText("BODY_STATUS".ToLocalized());
        }
        
        public void SetOrder(CorpseOrder order)
        {
            Order = order;
            if(Order?.Progress != null) Order.Progress.UpdatedEvent += OnUpdateProgress;
            this.Activate();
            
            Redraw();
        }

        public void RemoveOrder()
        {
            if(Order?.Progress == null) return;
            Order.Progress.UpdatedEvent -= OnUpdateProgress;
        }
        
        private void Redraw()
        {
            _title.text = Order.RoomType.ToString().ToUpper().ToLocalized();
            _status.text = $"{"STATUS".ToLocalized()}: {Order.State.ToString().ToUpper().ToLocalized()}";
            _reward1.text = $"{"ORDER_VALUE".ToLocalized()}: {Order.GetRewardText(0)}";
            _reward2.text = $"{"ORDER_VALUE".ToLocalized()}: {Order.GetRewardText(1)}";
            _characterImage.sprite = Order.Avatar;
            
            switch (Order.State)
            {
                case CorpseOrderState.Preparing:
                    _sliderObj.Deactivate();
                    _button.Deactivate();
                    _preparingDesc.Activate();
                    _reward1.Deactivate();
                    _reward2.Deactivate();
                    break;
                case CorpseOrderState.InProgress:
                    _sliderText.text = "";
                    _slider.fillAmount = 1f;
                    OnUpdateProgress();
                    _button.Deactivate();
                    _sliderObj.Activate();
                    _preparingDesc.Deactivate();
                    _reward1.Activate();
                    _reward2.Activate();
                    break;
                case CorpseOrderState.Completed:
                    _button.Activate();
                    _sliderObj.Deactivate();
                    _preparingDesc.Deactivate();
                    _reward1.Activate();
                    _reward2.Activate();
                    break;
                default:
                    _sliderText.text = "";
                    _slider.fillAmount = 1f;
                    break;
            }
        }

        private void OnUpdateProgress()
        {
            if(Order?.Progress == null) return;
            _sliderText.text = Order.Progress.LeftValue.ToTimeStr(3, true, true);
            _slider.fillAmount = Order.Progress.ProgressValue;
        }
    }
}