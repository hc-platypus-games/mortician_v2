﻿using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Ads;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.CorpseOrders
{
    public class CorpseOrdersWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _text;
        
        [SerializeField] private RectTransform _characterTextBg;
        [SerializeField] private TextMeshProUGUI _characterText;
        
        [SerializeField] private GameObject _orders;
        [SerializeField] private BaseButton _leftArrowButton;
        [SerializeField] private BaseButton _rightArrowButton;
        
        [Header("NewOrders")]
        [SerializeField] private TextMeshProUGUI _newOrders;
        [SerializeField] private TextMeshProUGUI _reward1;
        [SerializeField] private TextMeshProUGUI _reward2;
        [SerializeField] private BaseButton _rejectButton;
        [SerializeField] private BaseButton _acceptButton;
        [SerializeField] private AdWatchButton _x2Button;
        [SerializeField] private Image _characterImage;
        
        [SerializeField] private CorpseOrderItem[] _items;
        
        [Inject] private CorpseFactory _corpseFactory;
        [Inject] private GameFlags _flags;
        [Inject] private AdSystem _ad;

        private CorpseOrder _currentOrder;
        
        public override void Init()
        {
            _leftArrowButton.SetCallback(OnPressedLeftArrowButton);
            _rightArrowButton.SetCallback(OnPressedRightArrowButton);
            _rejectButton.SetCallback(OnPressedRejectButton);
            _acceptButton.SetCallback(OnPressedAcceptButton);
            _x2Button.SetCallback(OnPressedX2ValueButton);

            foreach (var item in _items)
            {
                item.Init();
                item.OnCompleted += OnCompletedOrder;
            }
            
            base.Init();
        }

        private void OnCompletedOrder(CorpseOrderItem item)
        {
            _corpseFactory.RemoveCorpseOrder(item.Order);
            item.RemoveOrder();
            item.Deactivate();
        }

        public override void UpdateLocalization()
        {
            _title.text = "CORPSE_ORDERS".ToLocalized();
            _newOrders.text = "NEW_ORDERS".ToLocalized();
            _rejectButton.SetText("REJECT".ToLocalized());
            _acceptButton.SetText("ACCEPT".ToLocalized());

            foreach (var item in _items)
            {
                item.UpdateLocalization();
            }
            
            base.UpdateLocalization();
        }

        public override void Open(params object[] list)
        {
            _corpseFactory.OrdersTimer.UpdatedEvent += RedrawText;
            _corpseFactory.OrdersTimer.CompletedEvent += RedrawNextNewOrder;
            
            RedrawNextNewOrder();
            RedrawActiveOrders();
            
            base.Open(list);
        }
        
        private void RedrawActiveOrders()
        {
            var orders = _corpseFactory.GetActiveCorpseOrders();
            for (int i = 0; i < _items.Count(); i++)
            {
                if (i >= orders.Count)
                {
                    _items[i].Deactivate();
                    continue;
                }
                
                _items[i].SetOrder(orders[i]);
            }
        }

        private void RedrawNextNewOrder()
        {
            var newOrders = _corpseFactory.GetNewCorpseOrders();
            switch (newOrders.Count)
            {
                case 0:
                    _orders.Deactivate();
                    return;
                case 1:
                    _leftArrowButton.Deactivate();
                    _rightArrowButton.Deactivate();
                    _orders.Activate();
                    break;
                default:
                    _leftArrowButton.SetInteractable(false);
                    _rightArrowButton.SetInteractable(true);
                    _leftArrowButton.Activate();
                    _rightArrowButton.Activate();
                    _orders.Activate();
                    break;
            }

            RedrawText();
            
            var order = newOrders.FirstOrDefault();
            if (order == _currentOrder) return;
            _currentOrder = order;
            RedrawCurrentOrder();
        }

        private void RedrawText()
        {
            if (_corpseFactory.OrdersTimer.IsActive)
            {
                _text.text = $"{"CORPSE_ORDERS_TEXT2".ToLocalized()}: " +
                             $"{_corpseFactory.OrdersTimer.LeftValue.ToTimeStr(3, true, true)}";
            }
            else
            {
                _text.text = "CORPSE_ORDERS_TEXT1".ToLocalized();
            }
        }

        private void UpdateTextSize()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(_characterTextBg);
            var width = LayoutUtility.GetPreferredWidth(_characterText.rectTransform) + 80f;
            _characterTextBg.sizeDelta = new Vector2( width, _characterTextBg.sizeDelta.y);
        }

        private void RedrawCurrentOrder()
        {
            _characterImage.sprite = _currentOrder.Avatar;
            _characterText.text = _currentOrder.Text.ToLocalized();
            
            InvokeSystem.StartInvoke(UpdateTextSize, 1);

            var rewardStr = _currentOrder.GetRewardText(0);
            if (!rewardStr.IsNullOrEmpty())
            {
                _reward1.text = $"{"ORDER_VALUE".ToLocalized()}:{rewardStr}";
                _reward1.Activate();
            }
            else
            {
                _reward1.Deactivate();
            }
            
            rewardStr = _currentOrder.GetRewardText(1);
            if (!rewardStr.IsNullOrEmpty())
            {
                _reward2.text = $"{"ORDER_VALUE".ToLocalized()}:{rewardStr}";
                _reward2.Activate();
            }
            else
            {
                _reward2.Deactivate();
            }
            
            if (_currentOrder.IsHardRewardOrder())
            {
                var tickets = _ad.GetTickets();
                if (tickets > 0)
                {
                    _x2Button.ShowTicketsButton(tickets, "X2_VALUE".ToLocalized());
                }
                else
                {
                    _x2Button.ShowDefaultButton("X2_VALUE".ToLocalized(), !_flags.Has(GameFlag.AllAdsRemoved));
                }

                _x2Button.Activate();
                _acceptButton.Deactivate();
            }
            else
            {
                _x2Button.Deactivate();
                _acceptButton.Activate();
            }
        }

        public override void Close()
        {
            _corpseFactory.OrdersTimer.UpdatedEvent -= RedrawText;
            _corpseFactory.OrdersTimer.CompletedEvent -= RedrawNextNewOrder;
            foreach (var item in _items)
            {
                item.RemoveOrder();
            }
            base.Close();
        }
        
        private void OnPressedLeftArrowButton()
        {
            var orders = _corpseFactory.GetNewCorpseOrders();
            var id = orders.IndexOf(_currentOrder) - 1;
            
            if (id < 0 || id > orders.Count-1) return;
            
            _currentOrder = orders[id];
            _leftArrowButton.SetInteractable(id > 0);
            _rightArrowButton.SetInteractable(true);
            
            RedrawCurrentOrder();
        }

        private void OnPressedRightArrowButton()
        {
            var orders = _corpseFactory.GetNewCorpseOrders();
            var id = orders.IndexOf(_currentOrder) + 1;
            
            if (id > orders.Count-1) return;
            
            _currentOrder = orders[id];
            _rightArrowButton.SetInteractable(id < orders.Count-1);
            _leftArrowButton.SetInteractable(true);
            
            RedrawCurrentOrder();
        }
        
        private void OnPressedRejectButton()
        {
            _corpseFactory.RemoveCorpseOrder(_currentOrder);
            RedrawNextNewOrder();
        }
        
        private void OnPressedAcceptButton()
        {
            _corpseFactory.StartOrder(_currentOrder);
            RedrawNextNewOrder();
            RedrawActiveOrders();
        }

        private void OnPressedX2ValueButton()
        {
            
        }
    }
}