﻿using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.DailyQuests
{
    public class DailyQuestWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _text1;
        [SerializeField] private TextMeshProUGUI _progressText;
        [SerializeField] private TextMeshProUGUI _timer;
        
        [SerializeField] private Image _progress;
        // [SerializeField] private Image _boxMark;
        // [SerializeField] private ParticleSystem _boxParticle;
        // [SerializeField] private Animation _boxAnimation;
        
        //[SerializeField] private BaseButton _getRewardButton;
        [SerializeField] private DailyQuestItem[] _items;

        [Inject] private GameParamFactory _params;
        [Inject] private GameProgressFactory _progresses;
        [Inject] private GameResources _resources;

        [Inject] private DiContainer _container;
        [Inject] private TaskSystem _tasks;
        [Inject] private GameSystem _game;
        
        private GameParam _rewardReceived;
        private GameProgress _rewardProgress;
        
        public override void Init()
        {
            //_getRewardButton.SetCallback(OnPressedGet);

            foreach (var item in _items)
            {
                item.Init();
                _container.BindInstance(item);
            }
            
            base.Init();
        }
        
        public override void UpdateLocalization()
        {
            _title.text = "WINDOW_DAILY_REWARDS_TITLE".ToLocalized();
            _text1.text = "WINDOW_DAILY_REWARDS_TEXT".ToLocalized();
            base.UpdateLocalization();
        }

        public override void Open(params object[] list)
        {
            _rewardReceived = _params.GetParam<GameSystem>(GameParamType.DailyQuestsRewardReceived);
            _rewardProgress = _progresses.GetProgress<GameSystem>(GameParamType.DailyQuestsRewardCounter);
            _rewardProgress.UpdatedEvent += UpdateRewardProgress;

            UpdateRewardProgress();
            FillQuests();
            base.Open(list);
        }
        
        private void OnPressedGet()
        {
            if (!_rewardProgress.IsCompleted || _rewardReceived.Value >= 1) return;
            _rewardReceived.SetValue(1);
            // _boxParticle.SetActive(_rewardProgress.IsCompleted && _rewardReceived.Value < 1);
            // _boxAnimation.enabled = _rewardProgress.IsCompleted && _rewardReceived.Value < 1;
            // _boxMark.SetActive(_rewardReceived.Value >= 1);
            
            // var offer = Models.Get<LoaderModel>().Balance.Offers.FirstOrDefault(o => o.Id == 2);
            // if (offer == null) return;
            // var items = Models.Get<CardsModel>().GetBoxItems(offer.Id, offer.MinValue, offer.Value);
            //GameUI.Get<BoxOpeningWindow>().Open(items, offer.Id, BoxOpeningWindow.BoxReceivePlace.DailyQuests);
        }
        
        public void FillQuests()
        {
            foreach (var item in _items)
            {
                item.Clear();
                item.Deactivate();
            }
            
            foreach (var task in _tasks.DailyQuests.Where(q => q.IsCompleted && !q.IsFinished))
            {
                var item = _items.FirstOrDefault(q => q.Quest == null);
                if (item == null) break;
                item.SetConfig(task);
                item.Activate();
            }
            
            foreach (var task in _tasks.DailyQuests.Where(q => !q.IsCompleted && !q.IsFinished))
            {
                var item = _items.FirstOrDefault(q => q.Quest == null);
                if (item == null) break;
                item.SetConfig(task);
                item.Activate();
            }
            
            foreach (var task in _tasks.DailyQuests.Where(q => q.IsFinished))
            {
                var item = _items.FirstOrDefault(q => q.Quest == null);
                if (item == null) break;
                item.SetConfig(task);
                item.Activate();
            }
            
            // _boxMark.SetActive(_rewardReceived.Value >= 1);
            // _boxParticle.SetActive(_rewardProgress.IsCompleted && _rewardReceived.Value < 1);
            // _boxAnimation.enabled = _rewardProgress.IsCompleted && _rewardReceived.Value < 1;
        }
        
        private void UpdateRewardProgress()
        {
            _progressText.text = $"{_rewardProgress.CurrentValue}/{_rewardProgress.TargetValue}";
            _progress.fillAmount = _rewardProgress.ProgressValue;
        }

        public override void Tick(float deltaTime)
        {
            var time = _game.ToNextDayTime;
            if (time > 0)
            {
                _timer.text = $"{"QUEST_UPDATE".ToLocalized()}: {time.ToTimeStr()}";   
            }
            else
            {
                if (_timer.isActiveAndEnabled) _timer.Deactivate();
            }
            base.Tick(deltaTime);
        }

        public override void Close()
        {
            _rewardProgress.UpdatedEvent -= UpdateRewardProgress;
            base.Close();
        }
    }
}