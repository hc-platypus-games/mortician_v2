﻿using System;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Ads;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.DailyQuests
{
    public class DailyQuestItem : BaseUIView
    {
        public Action<DailyQuestItem> OnReceived;
        
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private TextMeshProUGUI _rewardsText;
        [SerializeField] private TextMeshProUGUI _progressText;
        
        [SerializeField] private BaseButton _button;
        [SerializeField] private BaseButton _focusButton;
        
        [SerializeField] private Image _mark;
        [SerializeField] private Image _icon;
        [SerializeField] private Image _progress;

        [Inject] private GameResources _resources;
        [Inject] private UIFactory _uiFactory;
        [Inject] private AdSystem _ad;
        
        public GameTask Quest { get; private set; }

        public void Init()
        {
            _button.SetCallback(OnPressedButton);
            _focusButton.SetCallback(OnPressedFocus);
        }

        public void SetConfig(GameTask quest)
        {
            Quest = quest;
            
            _text.text = $"TASK{Quest.Type}".ToLocalized();

            var reward = Quest.Config.Reward;
            var text = $"<sprite name={reward.Type}>{reward.Amount}";
            _rewardsText.text = $"{"REWARDS".ToLocalized()}: {text}";

            _icon.sprite = _resources.GetSprite($"TASK{Quest.Type}".ToUpper());
            
            Redraw();
        }

        private void OnPressedFocus()
        {
        }
        
        private void OnPressedButton()
        {
            if (Quest.IsCompleted)
            {
                var reward = Quest.Config.Reward;
                var pos = _button.RectTransform.position;
                _uiFactory.SpawnResourceBubble(reward.Type, reward.Amount, pos);
               
                Quest.Finish();
                OnReceived?.Invoke(this);
                Redraw();
            }
            else
            {
                switch (Quest.Type)
                {
                    case GameEvents.WatchAdVideo:
                        _ad.OnCompleted += OnAdCompleted;
                        _ad.ShowReward(AdPlacement.DailyQuests);
                        break;
                }
            }
        }
        
        private void OnAdCompleted(bool success)
        {
            _ad.OnCompleted -= OnAdCompleted;
            if (!success) return;
            
            Quest.Increment(Quest.NeedCount);
            Redraw();
        }
        
        private void Redraw()
        {
            if (Quest.IsFinished)
            {
                _mark.Activate();
                _button.Deactivate();
                _progress.Deactivate();
                _focusButton.Deactivate();
                return;
            }

            _mark.Deactivate();
            _button.Activate();
            _progress.Activate();
            
            _progressText.text = $"{Quest.CurrentCount}/{Quest.NeedCount}";
            _progress.fillAmount = Quest.Progress;

            if (Quest.Type == GameEvents.WatchAdVideo)
            {
                //_button.SetText(_ad.GetAdButtonText());
                _button.SetInteractable(true);
            }
            else
            {
                _button.SetText("CLAIM".ToLocalized());
                _button.SetInteractable(Quest.IsCompleted);
            }

            switch (Quest.Type == GameEvents.WatchAdVideo)
            {
                default:
                    _focusButton.Deactivate();
                    break;
            }
        }
        
        public void Clear()
        {
            Quest = null;
        }
    }
}