using UnityEngine;

namespace _Game.Scripts.Ui
{
    public class CustomTutorialArrowAnchor : MonoBehaviour
    {
        public Vector2 Anchor;
        public Vector3 Scale = Vector3.one;
        public Quaternion Rotation;
    }
}
