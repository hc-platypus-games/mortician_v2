﻿using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.View.MapPoints.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Locations
{
    public class LocationPointItem : BaseUIView
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _timeText;
        [SerializeField] private TextMeshProUGUI _countText;
        [SerializeField] private TextMeshProUGUI _priceText;
        [SerializeField] private TextMeshProUGUI _buildingProgressText;

        [SerializeField] private Image _pointImage;
        [SerializeField] private Image _buildingProgress;
        [SerializeField] private Slider _slider;

        [SerializeField] private GameObject _newPoint;
        [SerializeField] private GameObject _pointInfo;
        [SerializeField] private GameObject _progressPoint;

        [SerializeField] private BaseButton _unlockButton;
        [SerializeField] private BaseButton _adSkipButton;
        [SerializeField] private BaseButton _hardSkipButton;

        [Inject] private GameResources _resources;
        [Inject] private GameProgressFactory _progresses;
        [Inject] private GameSystem _game;
        [Inject] private HardSystem _hard;
        [Inject] private UIFactory _uiFactory;
        [Inject] private GameParamFactory _params;

        private MapEventPointView _pointView;
        
        public void Init()
        {
            _unlockButton?.SetCallback(OnPressedUnlockButton);
            _adSkipButton?.SetCallback(OnPressedAdSkipButton);
            _hardSkipButton?.SetCallback(OnPressedHardSkipButton);
        }

        private void OnPressedAdSkipButton()
        {
            
        }

        private void OnPressedHardSkipButton()
        {
            if (_pointView.Timer != null && _pointView.Timer.IsActive)
            {
                var price = _hard.GetPriceForTime(_pointView.Timer.LeftValue);
                if (_hard.IsEnough(price))
                {
                    _pointView.Timer.Change(_pointView.Timer.LeftValue);
                    _hard.SpendHard(price,
                        SpendCurrencyPlace.BuySkipTime, SpendCurrencyItem.Room);
                }
                else
                {
                    _uiFactory.SpawnMessage("HARD_NOT_ENOUGH".ToLocalized());
                }
            }
        }

        private void OnPressedUnlockButton()
        {
            var price = _pointView.Config.Price;
            var workers = _params.GetParam<GameSystem>(GameParamType.Workers);
            if (workers.Value <= 0)
            {
                _uiFactory.SpawnMessage("NOT_ENOUGH_WORKERS".ToLocalized());
                return;
            }
            
            if (_game.IsEnoughCurrency(GameParamType.Soft, price))
            {
                _pointView.Unlock();
                Redraw();
                _game.SpendCurrency(GameParamType.Soft, price, SpendCurrencyPlace.MapPointWindow, SpendCurrencyItem.BuyPointWindow);
            }
            else
            {
                //TODO отправлять в магаз
            }
        }

        public void RedrawPoint(MapEventPointView pointView)
        {
            if (pointView.Config == null)
            {
                this.Deactivate();
                return;
            }
            
            _pointView = pointView;
            _pointView.CycleDelay.UpdatedEvent += OnUpdateCycle;
            _pointView.Capacity.UpdatedEvent += OnUpdateCapacity;
            _pointView.Timer.UpdatedEvent += OnUpdateBuildingProgress;

            _title.text = $"LOCATION_POINT_{_pointView.Id}".ToLocalized();

            _timeText.Activate();
            
            Redraw();
        }

        private void OnUpdateCycle()
        {
            _slider.value = _pointView.CycleDelay.ProgressValue;
            _timeText.text = $"{"TIME_TO_CORPSE".ToLocalized()}: {_pointView.CycleDelay.LeftValue.ToTimeStr(3, false, true)}";
        }
        
        private void OnUpdateCapacity()
        {
            _countText.text = $"{"CORPSES".ToLocalized()}: {_pointView.Capacity.CurrentValue}/{_pointView.Capacity.TargetValue}";

            if (!_pointView.Capacity.IsCompleted) return;
            
            _timeText.Deactivate();
            _slider.value = 0;
        }

        private void OnUpdateBuildingProgress()
        {
            _buildingProgress.fillAmount = _pointView.Timer.ProgressValue;
            _buildingProgressText.text = _pointView.Timer.LeftValue.ToTimeStr(2, false, true);
            var price = _hard.GetPriceForTime(_pointView.Timer.LeftValue);                
            if (_hard.IsEnough(price))
            {
                _hardSkipButton.SetInteractable(true);
            }
            else
            {
                _hardSkipButton.SetInteractable(false);
            }
            _hardSkipButton.SetText("SPEEDUP".ToLocalized());
            _hardSkipButton.SetText(1, $"<sprite name=Hard>{price}");
        }

        private void Redraw()
        {
            _pointImage.sprite = _resources.GetMapEventPointIconSprite($"location{_pointView.Id}");
            
            if (_pointView.IsBought)
            {
                _newPoint.Deactivate();
                _progressPoint.Deactivate();
                _pointInfo.Activate();

                OnUpdateCapacity();
                OnUpdateCycle();
            }
            else
            {
                if (_pointView.Timer.IsActive)
                {
                    _pointView.Timer.UpdatedEvent += OnUpdateBuildingProgress;
                    _pointView.Timer.CompletedEvent += Redraw;
                    _progressPoint.Activate();
                    OnUpdateBuildingProgress();
                }
                else
                {
                    _newPoint.Activate();
                    _pointInfo.Deactivate();
                    _progressPoint.Deactivate();
                
                    _priceText.text = $"{"UNLOCK".ToLocalized()}\n <sprite name=Soft>{_pointView.Config.Price.ToFormattedString()}";   
                }
            }
        }

        public void Clear()
        {
            if(!_pointView) return;
            _pointView.CycleDelay.UpdatedEvent -= OnUpdateCycle;
            _pointView.Capacity.UpdatedEvent -= OnUpdateCapacity;
            _pointView.Timer.UpdatedEvent -= OnUpdateBuildingProgress;
            _pointView.Timer.CompletedEvent -= Redraw;
        }
    }
}