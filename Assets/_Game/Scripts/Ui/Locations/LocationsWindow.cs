﻿using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui.Locations
{
    public class LocationsWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private TextMeshProUGUI _totalCorpses;

        [SerializeField] private LocationPointItem[] _items;
       
        [Inject] private WindowsSystem _windows;
        [Inject] private LevelEventsFactory _levelEvents;
        
        public override void Init()
        {
            foreach (var item in _items)
            {
                item.Init();
            }
            base.Init();
        }

        public override void UpdateLocalization()
        {
            _title.text = "CORPSE_POINTS".ToLocalized();
            _text.text = "CORPSE_POINTS_TEXT".ToLocalized();
            base.UpdateLocalization();
        }

        public override void Open(params object[] list)
        {
            var points = _levelEvents.GetCorpsePoints();
            for (var i = 0; i < _items.Length; i++)
            {
                if (i >= points.Count)
                {
                    _items[i].Deactivate();
                    continue;
                }

                var point = points[i];
                _items[i].RedrawPoint(point);
                _items[i].Activate();
                point.Capacity.UpdatedEvent += UpdateCorpses;
            }

            UpdateCorpses();

            base.Open(list);
        }

        private void UpdateCorpses()
        {
            _totalCorpses.text = $"{"TOTAL_CORPSES".ToLocalized()}: {_levelEvents.GetCorpsesCount()}";
        }

        public override void Close()
        {
            foreach (var item in _items)
            {
                item.Clear();
            }

            foreach (var point in _levelEvents.GetCorpsePoints())
            {
                point.Capacity.UpdatedEvent -= UpdateCorpses;
            }
            
            base.Close();
        }
    }
}