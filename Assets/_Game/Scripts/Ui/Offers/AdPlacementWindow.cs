﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.Systems.Ads;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Offers
{
    public class AdPlacementWindow : BaseWindow
    {
        public Action AdViewed;
        
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _rewardCount;
        [SerializeField] private TextMeshProUGUI _rewardText;
        [SerializeField] private TextMeshProUGUI _discountText;
        
        [SerializeField] private GameObject _discountInfo;
        [SerializeField] private GameObject _legacyInfo;

        [SerializeField] private AdWatchButton _adButton;
        
        [SerializeField] private Image _icon;
        [SerializeField] private List<AdPlacementConfig> _configs;
        [SerializeField] private BaseOffer _offer;
        
        [Inject] private AdSystem _ad;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private UIFactory _uiFactory;
        [Inject] private OffersFactory _offers;
        [Inject] private CalculationComponent _calculation;
        [Inject] private EndLoadingComponent _endLoadingComponent;
        [Inject] private GameFlags _flags;
        [Inject] private AppEventProvider _eventProvider;

        private AdPlacement _placement;
        private AdPlacementConfig _config;
        
        public override void Init()
        {
            _adButton.SetCallback(OnPressedWatch);
            _offer.Init(_balance.DefaultBalance.Offers.FirstOrDefault(o=>o.Id == 18));
            base.Init();
        }

        private void OnPressedWatch()
        {
            _ad.OnCompleted += OnAdCompleted;
            _ad.ShowReward(_placement);
        }
        
        private void OnAdCompleted(bool success)
        {
            _ad.OnCompleted -= OnAdCompleted;
            if (!success) return;
#if UNITY_EDITOR
            DrawRewardBubbles();
#else
            _endLoadingComponent.OnEnd += DrawRewardBubbles;
#endif
            
            AdViewed?.Invoke();
            
            Close();
        }

        private void DrawRewardBubbles()
        {
            _endLoadingComponent.OnEnd -= DrawRewardBubbles;
            switch (_placement)
            {
                case AdPlacement.FirstHard:
                    _uiFactory.SpawnResourceBubble(GameParamType.Hard, _balance.DefaultBalance.FirstHardValue, _adButton.RectTransform.position);
                    break;
                
                case AdPlacement.FastCash:
                    _uiFactory.SpawnResourceBubble(GameParamType.Soft, 10, _adButton.RectTransform.position);
                    break;
                
                case AdPlacement.ItemsUpgradeDiscount:
                    break;
            }
        }

        public override void UpdateLocalization()
        {
            _offer.UpdateLocalization();
            base.UpdateLocalization();
        }

        public override void Open(params object[] list)
        {
            if (list.Length == 0) return;
            
            _placement = (AdPlacement)list[0];

            _legacyInfo.Deactivate();
            _discountInfo.Deactivate();
            
            _config = _configs.FirstOrDefault(c => c.Placement == _placement);
            if (_config == null) return;
            
            _title.text = _config.Title.ToLocalized();
            _rewardText.fontSize = _config.TextSize;
            _rewardText.color = _config.TextColor;

            if (_config.Icon != null)
            {
                _icon.Activate();
                _icon.sprite = _config.Icon;
                _icon.rectTransform.localScale = _config.Scale;
            }

            UpdatePlacementText();
            
            var tickets = _ad.GetTickets();
            if (tickets > 0)
            {
                _adButton.ShowTicketsButton(tickets, "GET".ToLocalized());
            }
            else
            {
                _adButton.ShowDefaultButton("GET".ToLocalized(), !_flags.Has(GameFlag.AllAdsRemoved));
            }
            
            _offer.Redraw();
            _eventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.RewardShowed, _placement.ToString());
            base.Open(list);
        }

        private void UpdatePlacementText()
        {
            switch (_placement)
            {
                case AdPlacement.FirstHard:
                    _rewardText.text = $"{_config.Token.ToLocalized()}"
                        .Replace("%1", 
                            $"<sprite name=Hard>+{_balance.DefaultBalance.FirstHardValue}");
                    _rewardCount.text = _balance.DefaultBalance.FirstHardValue.ToString();

                    _legacyInfo.Activate();
                    break;
                
                case AdPlacement.FastCash:
                    var cash = _calculation.GetIncomeByTime(_balance.DefaultBalance.FastCashValue);
                    if (cash == 0) cash = 1000;
                    _calculation.SetCashValue(cash);
                    _rewardText.text = _config.Token.ToLocalized();
                    _rewardCount.text = cash.ToFormattedString();
                    
                    _legacyInfo.Activate();
                    break;
                
                case AdPlacement.ItemsUpgradeDiscount:
                    _discountText.text = _config.Token.ToLocalized();
                    _discountInfo.Activate();
                    break;
            }
        }

        public override void Close()
        {
            _placement = AdPlacement.None;
            base.Close();
        }
    }
}