﻿using System;
using _Game.Scripts.Enums;
using UnityEngine;

namespace _Game.Scripts.Ui.Offers
{
    [Serializable]
    public class AdPlacementConfig
    {
        public AdPlacement Placement;
        public Sprite Icon;
        public string Title;
        public string Token;
        public float TextSize;
        public Color TextColor;
        public Vector3 Scale;
        public ParticleSystem Fx;
    }
}