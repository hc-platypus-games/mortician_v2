﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Base.Animations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Offers
{
    public class MainWindowOffer : BaseOffer, IGameProgress
    {
        private enum OfferState
        {
            Pause,
            Showing,
            Opening
        }

        public Action<MainWindowOffer> OnOfferClicked, OnOfferHided;
        
        [SerializeField] private Image _slider;
        [SerializeField] private Image _icon;
        
        [SerializeField] private TextMeshProUGUI _state1Text;
        [SerializeField] private TextMeshProUGUI _state2Text;

        [SerializeField] private GameObject _state1Container;
        [SerializeField] private GameObject _state2Container;

        [SerializeField] private List<AdPlacementConfig> _offerConfigs;

        [Inject] private GameProgressFactory _progressFactory;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private WindowsSystem _windows;
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private BoostSystem _boosts;

        private RectTransform _rect;
        private PositionGuiAnim _anim;
        private AdPlacementWindow _window;

        private AdPlacementConfig _config;
        private OfferState _state;
        private GameProgress _showProgress;
        private Boost _boost;
        
        public AdPlacement Placement { get; private set; }
        
        [Inject]
        public void Construct()
        {
            _button.SetCallback(OnPressedButton);
            _state = OfferState.Pause;
            _anim = GetComponentInChildren<PositionGuiAnim>();
            _rect = transform.GetChild(0).GetComponent<RectTransform>();
        }

        public class Pool : MonoMemoryPool<Transform, AdPlacement, MainWindowOffer>
        {
            protected override void Reinitialize(Transform parent, AdPlacement placement, MainWindowOffer item)
            {
                item.SetConfig(parent, placement);
            }
        }

        private void SetConfig(Transform parent, AdPlacement placement)
        {
            if (_showProgress == null)
            {
                _showProgress = _progressFactory.CreateProgress(this,
                    GameParamType.MainWindowOfferShowDuration,
                    _balance.DefaultBalance.MainWindowOfferDuration,
                    false);
            
                _showProgress.UpdatedEvent += OnShowingUpdate;
                _showProgress.CompletedEvent += OnShowingComplete;
            }
            
            if (_showProgress.IsCompleted || _showProgress.IsPaused) _showProgress.Reset();

            transform.SetParent(parent);
            RectTransform.localScale = Vector3.one;
            _rect.anchoredPosition = _anim.DefaultPos;
            this.Activate();
            
            _window = (AdPlacementWindow)_windows.GetWindow<AdPlacementWindow>();
            
            Placement = placement;
            _config = _offerConfigs.FirstOrDefault(c => c.Placement == Placement);
            if (_config == null) return;
            
            _icon.sprite = _config.Icon;
            _icon.rectTransform.localScale = _config.Scale;
            
            _slider.fillAmount = 1;
            _state1Text.text = _config.Token.ToLocalized();

            _state = OfferState.Showing;
            
            if (_config.Fx != null) _config.Fx.Play();
            
            _state1Container.Activate();
            _state2Container.Deactivate();

            _rect.localScale = Vector3.one;

            if (_anim == null) Hide();
            _anim.PlayOpenAnimation(_rect);
        }

        private void OnShowingUpdate()
        {
            _slider.fillAmount = 1 - _showProgress.ProgressValue;
        }

        private void OnShowingComplete()
        {
            Hide();
        }
       
        private void OnPressedButton()
        {
            if (_state != OfferState.Showing) return;
            _state = OfferState.Opening;
            
            _window.AdViewed += OnAdViewed;
            _window.Closed += OnWindowClosed;
            _windows.OpenWindow<AdPlacementWindow>(Placement);

            _anim.PlayCloseAnimation(_rect);

            _showProgress.Change(-_showProgress.CurrentValue);
            _showProgress.Pause();
            OnOfferClicked?.Invoke(this);
            
            _eventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.RewardClicked, Placement.ToString());
        }

        private void OnAdViewed()
        {
            _window.AdViewed -= OnAdViewed;
            _window.Closed -= OnWindowClosed;

            switch (Placement)
            {
                case AdPlacement.ItemsUpgradeDiscount:
                    _boost = _boosts.GetAdBoost(Placement);
                    if (_boost == null)
                    {
                        return;
                    }

                    _boost.UpdateEvent += OnBoostUpdate;
                    _boost.FinishedEvent += OnBoostFinish;
                    _boost.Play();

                    _showProgress.Pause();
                    
                    _state1Container.Deactivate();
                    _state2Container.Activate();
                    
                    _anim.PlayOpenAnimation(_rect);
                    break;
                
                default:
                    Hide();
                    break;
            }
        }

        private void OnBoostUpdate()
        {
            _state2Text.text = _boost.LeftDuration.ToTimeStr(2, true, true);
        }

        private void OnBoostFinish()
        {
            _boost.UpdateEvent -= OnBoostUpdate;
            _boost.FinishedEvent -= OnBoostFinish;
            Hide();
        }

        private void OnWindowClosed(BaseWindow window)
        {
            _window.AdViewed -= OnAdViewed;
            _window.Closed -= OnWindowClosed;
            
            _anim.PlayCloseAnimation(_rect);
            Hide();
        }

        private void Hide()
        {
            _anim.PlayCloseAnimation(_rect, Despawn);
        }

        private void Despawn()
        {
            OnOfferHided?.Invoke(this);
        }
    }
}