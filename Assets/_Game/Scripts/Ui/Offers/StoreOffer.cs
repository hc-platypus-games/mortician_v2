﻿using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Ads;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Tutorial;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Cards;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Offers
{
    public class StoreOffer : BaseOffer
    {
        [SerializeField] private TextMeshProUGUI _value;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private TextMeshProUGUI _badgeAmount;
        [SerializeField] private Image _badge;
        [SerializeField] private bool _saved;
        [SerializeField] private AdWatchButton _adWatchButton;

        [Inject] private GameFlags _flags;
        [Inject] private TutorialSystem _tutorial;
        [Inject] private AdSystem _ad;
        [Inject] private InAppSystem _inApps;
        [Inject] private GameParamFactory _params;
        [Inject] private UIFactory _uiFactory;
        [Inject] private WindowsSystem _windows;
        [Inject] private GameSystem _game;
        [Inject] private CardsFactory _cards;
        [Inject] private CalculationComponent _calculation;

        private OfferConfig _config;

        public int Count { get; private set; }
        public double CycleDelay { get; private set; }

        public override void Init(OfferConfig config)
        {
            _button.SetCallback(OnPressedBuy);
         
            _config = config;
            Count = config.Count;
            base.Init(config);

            _game.OnResetLocalTimers += OnResetLocalTimers;
            if (config.Price == 0)
            {
                var tickets = _params.GetParam<GameSystem>(GameParamType.AdTickets);
                tickets.UpdatedEvent += Redraw;
            }
            
            SetSave(_saved);
        }

        public override void Tick(float deltaTime)
        {
            switch (_id)
            {
                case 1 when CycleDelay <= 0:
                    return;
                case 1:
                {
                    CycleDelay -= deltaTime;
                    UpdateBoxTimer();
                    if (CycleDelay <= 0) _uiFactory.UpdateBadges(BadgeNotificationType.Store);
                    break;
                }
                case 7:
                    UpdateHardTimer();
                    break;
            }
        }

        public override void Redraw()
        {
            if (_config == null)
            {
                this.Deactivate();
                return;
            }
            
            SetActive(true);
            
            _badge.Deactivate();
            _adWatchButton.SetBlind(false);
            
            switch (_config.Id)
            {
                case 1:
                case 2:
                case 3:
                    RedrawBox();
                    break;
                
                case 4:
                case 5:
                case 6:
                    RedrawTickets();
                    break;
                
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    RedrawHard();
                    break;
                
                case 13:
                case 14:
                case 15:
                    RedrawSkipTime();
                    break;
            }
            
            base.Redraw();
        }
        
        private void RedrawBox()
        {
            if (!_flags.Has(GameFlag.CharactersOpened))
            {
                SetActive(false);
                return;
            }
            
            _text.Deactivate();
            _value.text = $"BOX_{_config.Id}".ToLocalized();
            
            switch (_config.Id)
            {
                case 1:
                    if (_tutorial.CurrentTutorialId == 100)
                    {
                        _badge.Deactivate();
                        _adWatchButton.ShowDefaultButton("GET".ToLocalized());
                        _adWatchButton.SetBlind(true);
                    }
                    else
                    {
                        UpdateBadges();
                        UpdateBoxTimer();
                        if (!_adWatchButton.isActiveAndEnabled) _adWatchButton.Activate();
                    }
                    break;

                case 2:
                case 3:
                    _adWatchButton.ShowDefaultButton($"<sprite name=Hard> {_config.Price}");
                    break;
            }
        }
        
        private void RedrawTickets()
        {
            _text.Deactivate();
            _value.text = $"<sprite name=Tickets>{_config.Value}";

            if (_inApps.StoreControllerAvailable)
            {
                var productData = _inApps.GetProductData(_config.ShopId);
                _adWatchButton.ShowDefaultButton(productData.localizedPriceString);
            }
            else
            {
                _adWatchButton.ShowDefaultButton(_config.Price.ToString());
            }
        }
        
        private void RedrawHard()
        {
            _text.Deactivate();
            _value.text = $"<sprite name=Hard1>{_config.Value}";
            if (_config.Id == 7)
            {
                UpdateHardTimer();
                UpdateBadges();
            }
            else
            {
                if (_inApps.StoreControllerAvailable)
                {
                    var productData = _inApps.GetProductData(_config.ShopId);
                    _adWatchButton.ShowDefaultButton(productData.localizedPriceString);
                }
                else
                {
                    _adWatchButton.ShowDefaultButton(_config.Price.ToString());
                }
            }
        }

        private void RedrawSkipTime()
        {
            var value = _calculation.GetIncomeByTime(_config.Value);
            if (value == 0)
            {
                SetActive(false);
                return;
            }

            _text.Activate();
            _text.text = $"{_config.Value1} min";
            _value.text = $"<sprite name=Soft> {value.ToFormattedString()}";
            
            _adWatchButton.ShowDefaultButton($"<sprite name=Hard> {_config.Price}");
        }

        private void UpdateBadges()
        {
            var count = Count;
            if (count > 0)
            {
                _badge.Activate();
                _badgeAmount.text = count.ToString();
            }
            else
            {
                _badge.Deactivate();
            }
        }

        private void UpdateBoxTimer()
        {
            if (CycleDelay > 0)
            {
                _adWatchButton.ShowDefaultButton(((float)CycleDelay).ToTimeStr(3, true));
            }
            else
            {
                var tickets = _ad.GetTickets();
                if (tickets > 0)
                {
                    _adWatchButton.ShowTicketsButton(tickets, "BOX1".ToLocalized());
                }
                else
                {
                    _adWatchButton.ShowDefaultButton("BOX1".ToLocalized(), !_flags.Has(GameFlag.AllAdsRemoved));
                    _adWatchButton.SetBlind(true);
                }
            }
        }

        private void UpdateHardTimer()
        {
            if (Count == 0)
            {
                _button.SetInteractable(false);
                _adWatchButton.SetBlind(false);
                _adWatchButton.ShowDefaultButton(_game.ToNextDayTime.ToTimeStr(3, true));
            }
            else
            {
                _button.SetInteractable(true);
                var tickets = _ad.GetTickets();
                if (tickets > 0)
                {
                    _adWatchButton.ShowTicketsButton(tickets, "FREE".ToLocalized());
                }
                else
                {
                    _adWatchButton.ShowDefaultButton("FREE".ToLocalized(), !_flags.Has(GameFlag.AllAdsRemoved));
                    _adWatchButton.SetBlind(true);
                }
                if (!_adWatchButton.isActiveAndEnabled) _adWatchButton.Activate();
            }
        }

        private void OnResetLocalTimers()
        {
            switch (_config.Id)
            {
                case 1:
                    Count = _config.Count;
                    _uiFactory.UpdateBadges(BadgeNotificationType.Store);
                    break;
            }
        }
        
        private void OnPressedBuy()
        {
            switch (_config.Id)
            {
                case 1:
                    if (CycleDelay > 0 || Count <= 0) return;
                    
                    if (Count > 0) Count--;
                    
                    CycleDelay = _config.CycleDelay;
                    Purchase();
                    break;
                case 2:
                case 3:
                    Purchase();
                    break;
                
                case 4:
                case 5:
                case 6:
                    _inApps.Purchase(_config, InAppPlacement.Shop, OnPurchased);
                    break;
                
                case 7:
                    if (Count <= 0) return;
                    Count--;
                    _button.SetInteractable(false);
                    Purchase();
                    break;
                
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    _inApps.Purchase(_config, InAppPlacement.Shop, OnPurchased);
                    break;
                
                case 13:
                case 14:
                case 15:
                    Purchase();
                    break;
            }
        }

        private void Purchase()
        {
            if (_tutorial.CurrentTutorialId == 100)
            {
                OnPurchased(true);
                return;
            }
            
            if (_game.IsEnoughCurrency(GameParamType.Hard, _config.Price))
            {
                _inApps.ClaimRewards(_config);

                switch (_config.Id)
                {
                    case 1:
                        _ad.OnCompleted += OnPurchased;
                        _ad.ShowReward(AdPlacement.FreeBox);
                        break;
                    case 2:
                    case 3:
                        _game.SpendCurrency(GameParamType.Hard, _config.Price, SpendCurrencyPlace.BuyBox, SpendCurrencyItem.Box);
                        OnPurchased(true);
                        break;
                    
                    case 7:
                        _ad.OnCompleted += OnPurchased;
                        _ad.ShowReward(AdPlacement.FreeHard);
                        break;
                    
                    case 13:
                    case 14:
                    case 15:
                        _game.SpendCurrency(GameParamType.Hard, _config.Price, SpendCurrencyPlace.BuySkipTime, SpendCurrencyItem.SkipTime);
                        OnPurchased(true);
                        break;
                    
                    default:
                        _game.SpendCurrency(GameParamType.Hard, _config.Price, SpendCurrencyPlace.None, SpendCurrencyItem.None);
                        OnPurchased(true);
                        break;
                }
            }
            else
            {
                _uiFactory.SpawnMessage("HARD_NOT_ENOUGH".ToLocalized());
            }
        }

        private void OnPurchased(bool success)
        {
            if (!success) return;
            
            switch (_config.Id)
            {
                case 1:
                case 2:
                case 3:
                    if (_config.Id == 1) _ad.OnCompleted -= OnPurchased;
                    UpdateBadges();
                    _uiFactory.UpdateBadges(BadgeNotificationType.Store);
                    var items = _cards.GetBoxItems(_config.Id, _config.Value, _config.Value1);
                    if (items.Count > 0)
                    {
                        var window = (CaseOpeningWindow)_windows.OpenWindow<CaseOpeningWindow>();
                        window.SetRewardsList(items, _config.Id);

                        foreach (var item in items)
                        {
                            _game.ClaimReward(item.Type, item.Amount, item.RewardId, GetCurrencyPlace.Store);
                        }
                    }
                    break;
                
                case 4:
                case 5:
                case 6:
                    UpdateBadges();
                    _params.GetParam<GameSystem>(GameParamType.AdTickets).Change(_config.Value);
                    _uiFactory.SpawnResourceBubble(GameParamType.AdTickets, _config.Value, _button.RectTransform.position);
                    break;
                
                case 7:
                    _uiFactory.SpawnResourceBubble(GameParamType.Hard, _config.Value, _button.RectTransform.position);
                    _uiFactory.UpdateBadges(BadgeNotificationType.Store);
                    RedrawHard();
                    break;
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    _uiFactory.SpawnResourceBubble(GameParamType.Hard, _config.Value, _button.RectTransform.position);
                    RedrawHard();
                    break;
                
                case 13:
                case 14:
                case 15:
                    var value = _calculation.GetIncomeByTime(_config.Value);
                    _game.ClaimReward(GameParamType.Soft, value);
                    _uiFactory.SpawnResourceBubble(GameParamType.Soft, _config.Value, _button.RectTransform.position);
                    break;
            }
        }

        public void CalculateOffline(double time)
        {
            CycleDelay -= time;
        }

        public override void LoadFromData(int itemCount, double itemTimer)
        {
            Count = itemCount;
            CycleDelay = itemTimer;
        }

        public int IsAvailable()
        {
            return _id switch
            {
                1 => Count > 0 && CycleDelay <= 0 ? 1 : 0,
                7 => Count > 0 ? 1 : 0,
                _ => 0
            };
        }
    }
}