﻿using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Ui.Base;
using UnityEngine;

namespace _Game.Scripts.Ui.Offers
{
    public class BaseOffer : BaseUIView
    {
        [SerializeField] protected int _id = -1;
        [SerializeField] protected BaseButton _button;

        public bool Active { get; private set; }
        public bool Saved { get; private set; }
        public int Id => _id;

        public BaseButton Button => _button;
        
        public virtual void Init(OfferConfig config)
        {
        }
        
        public virtual void Redraw()
        {
        }

        public virtual void UpdateLocalization()
        {
        }

        public virtual void Tick(float deltaTime)
        {
        }

        protected void SetActive(bool flag)
        {
            Active = flag;
        }

        protected void SetSave(bool flag)
        {
            Saved = flag;
        }

        public virtual void LoadFromData(int itemCount, double itemTimer)
        {
            
        }
    }
}