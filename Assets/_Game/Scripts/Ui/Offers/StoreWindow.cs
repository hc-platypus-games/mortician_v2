﻿using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui.Offers
{
    public class StoreWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _ticketsText;
        [SerializeField] private StoreWindowManagerOffer _manager;
        [SerializeField] private StoreWindowMarketerOffer _marketer;
        [SerializeField] private StoreWindowAgentOffer _agent;
        
        [Inject] private GameParamFactory _params;
        [Inject] private GameBalanceConfigs _balance;
        
        private StoreWindowGroup[] _groups;
        
        public override void Init()
        {
            _groups = GetComponentsInChildren<StoreWindowGroup>(true);
            base.Init();
            var tickets = _params.GetParam<GameSystem>(GameParamType.AdTickets);
            if(tickets != null) tickets.UpdatedEvent += UpdateTickets;
            UpdateTickets();
            
            _manager.Init(_balance.DefaultBalance.Offers.FirstOrDefault(o=>o.Id == 16));
            _marketer.Init(_balance.DefaultBalance.Offers.FirstOrDefault(o=>o.Id == 17));
            _agent.Init(_balance.DefaultBalance.Offers.FirstOrDefault(o=>o.Id == 18));
        }

        public override void UpdateLocalization()
        {
            if (_title != null) _title.text = "STORE_WINDOW_TITLE".ToLocalized();
            
            foreach (var group in _groups)
            {
                group.UpdateLocalization();
            }
            
            _manager.UpdateLocalization();
            _marketer.UpdateLocalization();
            _agent.UpdateLocalization();
            base.UpdateLocalization();
        }

        private void UpdateTickets()
        {
            _ticketsText.text = $"<sprite name=Tickets>{_params.GetParamValue<GameSystem>(GameParamType.AdTickets).ToFormattedString()}";
        }

        public override void Open(params object[] list)
        {
            foreach (var group in _groups)
            {
                group.Redraw();
            }
            
            _manager.Redraw();
            _marketer.Redraw();
            _agent.Redraw();
            
            base.Open(list);
        }

        public override void Tick(float deltaTime)
        {
            foreach (var group in _groups)
            {
                group.Tick(deltaTime);
            }
            base.Tick(deltaTime);
        }

        public override BaseButton GetTutorialButton()
        {
            _groups[0].Redraw();
            return _groups[0].Offers[0].Button;
        }

        public override void Close()
        {
            base.Close();
        }
    }
}