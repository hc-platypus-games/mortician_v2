﻿using System.Linq;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;

namespace _Game.Scripts.Ui.Offers
{
    public class StoreWindowGroup : BaseUIView
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private int _region;
        [SerializeField] private string _token;
        [SerializeField] private BaseOffer[] _offers;

        public BaseOffer[] Offers => _offers;

        public void UpdateLocalization()
        {
            if (_title != null) _title.text = _token.ToLocalized();
        }

        public void Redraw()
        {
            foreach (var offer in _offers)
            {
                offer.Redraw();
            }

            this.SetActive(_offers.Count(o => o.Active) > 0);
        }

        public void Tick(float deltaTime)
        {
            foreach (var offer in _offers.Where(o => o.Saved))
            {
                offer.Tick(deltaTime);
            }
        }
    }
}