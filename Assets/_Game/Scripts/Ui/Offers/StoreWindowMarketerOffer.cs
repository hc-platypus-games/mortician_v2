using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Tutorial;
using _Game.Scripts.Tools;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui.Offers
{
    public class StoreWindowMarketerOffer : BaseOffer
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _text1;
        [SerializeField] private TextMeshProUGUI _text2;
        [SerializeField] private TextMeshProUGUI _price;
        
        [Inject] private InAppSystem _inApps;
        [Inject] private GameFlags _flags;
        [Inject] private TutorialSystem _tutorial;
        
        private OfferConfig _config;
        
        public override void Init(OfferConfig config)
        {
            _config = config;
            _button.SetCallback(OnPressButton);
            base.Init(config);
        }

        private void OnPressButton()
        {
            _inApps.Purchase(_config, InAppPlacement.Shop, OnPurchased);
        }

        public override void Redraw()
        {
            if (_tutorial.CurrentTutorialId > 0)
            {
                this.Deactivate();
                return;
            }
            
            if (_config == null)
            {
                this.Deactivate();
                return;
            }
            
            SetActive(true);
            
            if (_flags.Has(GameFlag.MarketerIsBought))
            {
                this.Deactivate();
                return;
            }
            
            if (_inApps.StoreControllerAvailable)
            {
                var productData = _inApps.GetProductData(_config.ShopId);
                _price.SetText(productData.localizedPriceString);
            }
            else
            {
                _price.SetText(_config.Price.ToString());
            }
            
            if (!isActiveAndEnabled) this.Activate();
            
            base.Redraw();
        }

        public override void UpdateLocalization()
        {
            _title.text = "STORE_WINDOW_MARKETER_TITLE".ToLocalized();
            _text1.text = "STORE_WINDOW_MARKETER_TEXT1".ToLocalized();
            _text2.text = "STORE_WINDOW_MARKETER_TEXT2".ToLocalized();
            base.UpdateLocalization();
        }
        
        private void OnPurchased(bool success)
        {
            this.Deactivate();
        }
    }
}