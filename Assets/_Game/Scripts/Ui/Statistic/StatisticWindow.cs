using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui.Statistic
{
    public class StatisticWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private TextMeshProUGUI _incomeText;
        [SerializeField] private TextMeshProUGUI _corpseIncomeText;
        [SerializeField] private TextMeshProUGUI _offlineLimitText;
        [SerializeField] private TextMeshProUGUI _incomeTitle;
        [SerializeField] private TextMeshProUGUI _corpseIncomeTitle;
        [SerializeField] private TextMeshProUGUI _offlineLimitTitle;
        
        [SerializeField] private StatisticWindowItem[] _items;
        [SerializeField] private BaseButton _closeFrameButton;
        
        [Inject] private GameSystem _game;
        [Inject] private GameParamFactory _params;
        [Inject] private RoomsFactory _rooms;
        [Inject] private GameResources _resources;

        public override void Init()
        {
            _closeFrameButton.SetCallback(Close);
            base.Init();
        }

        public override void Open(params object[] list)
        {
            _incomeText.text = $"+{(_game.Income()*2).ToFormattedString()}/{"MIN".ToLocalized()}";
            _offlineLimitText.text = _params.GetParamValue<GameSystem>(GameParamType.MaxOfflineTime).ToTimeStr(2, true, true);
            
            var corpse = _rooms.GetRoomItem(RoomType.Cemetery, 1);
            var value = corpse.GetParamLevelValue(GameParamType.Income).ToFormattedString();
            _corpseIncomeText.text = $"+{value}/{"MIN".ToLocalized()}";

            var rooms = _rooms.GetRooms().FindAll(r => r.Config.IncludeStatistic);
            for (int i = 0; i < _items.Length; i++)
            {
                if (i >= rooms.Count())
                {
                    _items[i].Deactivate();
                    continue;
                }

                var type = rooms[i].Type;
                var sprite = _resources.GetRoomImagesSprite(type.ToString());
                _items[i].Redraw(type.ToString(), sprite, rooms[i].Level, _resources.GrayscaleSpriteMaterial, 
                    _rooms.GetRoomItemValue(type, GameParamType.Income, rooms[i].Level), 
                    _rooms.GetRoomItemValue(type, GameParamType.Income, 26), 
                    _rooms.GetRoomItemValue(type, GameParamType.Income, 51));
            }

            base.Open(list);
        }

        public override void UpdateLocalization()
        {
            _title.text = "STATISTIC_WINDOW_TITLE".ToLocalized();
            _text.text = "STATISTIC_WINDOW_TEXT".ToLocalized();
            _incomeTitle.text = $"{"TOTAL_INCOME".ToLocalized()}:";
            _corpseIncomeTitle.text = $"{"CORPSE_INCOME".ToLocalized()}:";
            _offlineLimitTitle.text = $"{"OFFLINE_TIME".ToLocalized()}:";
                
            foreach (var item in _items)
            {
                item.UpdateLocalization();
            }
            base.UpdateLocalization();
        }
    }
}