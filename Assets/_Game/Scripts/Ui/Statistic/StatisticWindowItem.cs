using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Ui.Statistic
{
    public class StatisticWindowItem : BaseUIView
    {
        [SerializeField] private Image _roomImage1;
        [SerializeField] private Image _roomImage2;
        [SerializeField] private Image _roomImage3;

        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _level1Text;
        [SerializeField] private TextMeshProUGUI _level2Text;
        [SerializeField] private TextMeshProUGUI _level3Text;
        [SerializeField] private TextMeshProUGUI _level1Value;
        [SerializeField] private TextMeshProUGUI _level2Value;
        [SerializeField] private TextMeshProUGUI _level3Value;
        
        public void Redraw(string name, Sprite sprite, int level, Material grayMaterial, float value1, float value2, float value3)
        {
            _roomImage1.sprite = sprite;
            _roomImage1.material = level == 0 ? grayMaterial : null;
            _roomImage2.sprite = sprite;
            _roomImage3.sprite = sprite;

            _title.text = name.ToUpper().ToLocalized();
            _level1Value.text = $"<sprite name=Soft>{(value1 * 2).ToFormattedString()}/{"MIN".ToLocalized()}";
            _level2Value.text = $"<sprite name=Soft>{(value2 * 2).ToFormattedString()}/{"MIN".ToLocalized()}";
            _level3Value.text = $"<sprite name=Soft>{(value3 * 2).ToFormattedString()}/{"MIN".ToLocalized()}";
        }

        public void UpdateLocalization()
        {
            _level1Text.text = $"{"LEVEL".ToLocalized()} 1";
            _level2Text.text = $"{"LEVEL".ToLocalized()} 2";
            _level3Text.text = $"{"LEVEL".ToLocalized()} 3";
        }
    }
}