﻿using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Ads;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Offers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class VideoBoostWindow : BaseWindow, IGameParam
    {
        [SerializeField] private AdWatchButton _watchButton;
        [SerializeField] private BaseButton _getRewardButton;
        
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _text1;
        [SerializeField] private TextMeshProUGUI _text2;
        [SerializeField] private TextMeshProUGUI _progressText;
        [SerializeField] private TextMeshProUGUI _progressText1;
        [SerializeField] private TextMeshProUGUI _hardRewardText;
        
        [SerializeField] private Image _progress;
        [SerializeField] private Image[] _marks;

        [SerializeField] private BaseOffer _offer;

        [Inject] private GameParamFactory _params;
        [Inject] private AdSystem _ad;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private BoostSystem _boosts;
        [Inject] private GameSystem _game;
        [Inject] private UIFactory _ui;
        [Inject] private GameFlags _flags;
        [Inject] private UIFactory _uiFactory;
        
        private GameParam _counter;
        private GameParam _maxTime;
        private Boost _boost;
        
        public override void Init()
        {
            _watchButton.SetCallback(OnPressedWatch);
            _getRewardButton.SetCallback(OnPressedGetRewards);
            _offer.Init(_balance.DefaultBalance.Offers.FirstOrDefault(o=>o.Id == 18));
            base.Init();
        }

        public override void UpdateLocalization()
        {
            _title.text = "VIDEO_BOOST_TITLE".ToLocalized();
            _offer.UpdateLocalization();
            base.UpdateLocalization();
        }

        public override void Open(params object[] list)
        {
            _counter = _params.GetParam<GameSystem>(GameParamType.VideoBoostCounter);
            _maxTime = _params.GetParam<GameSystem>(GameParamType.MaxOfflineTime);
            _boost = _boosts.GetAdBoost(AdPlacement.VideoBoost);
            var tickets = _params.GetParam<GameSystem>(GameParamType.AdTickets);
            if(tickets != null) tickets.UpdatedEvent += Redraw;
            _boost.UpdateEvent += RedrawProgress;
            
            var time = _boost.LeftDuration;
            _progress.fillAmount = time;
            _progressText.text = time > 0 ? _boost.LeftDuration.ToTimeStr() : "INCOME_X2".ToLocalized();
            
            _progressText1.text = $"{"ACCUMULATIVE_MAX".ToLocalized()}"
                .Replace("%1", _maxTime.Value.ToTimeStr(1));
            
            _offer.Redraw();
            
            Redraw();
            RedrawMarks();

            Debug.Log("AD window Opening");
            
            base.Open(list);
        }

        private void RedrawProgress()
        {
            var time = _boost.LeftDuration;
            if (time <= 0) return;
            
            _progress.fillAmount = _boost.Progress;
            _progressText.text = time > 0 ? _boost.LeftDuration.ToTimeStr(3, true, true) : "INCOME_X2".ToLocalized();
        }

        private void OnPressedWatch()
        {
            Debug.Log("Pressed watch");
            _ad.OnCompleted += OnAdCompleted;
            _ad.ShowReward(AdPlacement.VideoBoost);
        }
        
        private void OnAdCompleted(bool success)
        {
            _ad.OnCompleted -= OnAdCompleted;
            if (!success) return;
            
            if (_counter.Value < _marks.Length) _counter.Change(1);
            Redraw();
            RedrawMarks();
            
            _uiFactory.UpdateBadges(BadgeNotificationType.VideoBoost);
        }
        
        private void Redraw()
        {
            var isCapBoost = _boost.IsBoostCap();
            _watchButton.SetInteractable(!isCapBoost);

            var tickets = _ad.GetTickets();
            if (tickets > 0)
            {
                _watchButton.ShowTicketsButton(tickets, "INCOME_X2".ToLocalized());
            }
            else
            {
                _watchButton.ShowDefaultButton("INCOME_X2".ToLocalized(), !_flags.Has(GameFlag.AllAdsRemoved));
            }
            
            _text1.text = "VIDEO_BOOST_TEXT1".ToLocalized()
                .Replace("%1", _boost.GetNextUpgradeDuration().ToTimeStr(1, false, true));

        }
        
        private void RedrawMarks()
        {
            for (int i = 0; i < _marks.Length; i++)
            {
                _marks[i].SetActive(_counter.Value - 1 >= i);
            }
            
            _getRewardButton.SetInteractable(_counter.Value >= _marks.Length);
            
            _text2.text = "VIDEO_BOOST_TEXT2".ToLocalized()
                .Replace("%1", $"{_marks.Length - _counter.Value}");
            _hardRewardText.text = _balance.DefaultBalance.VideoBoostHardValue.ToString();
        }
        
        private void OnPressedGetRewards()
        {
            _game.AddCurrency(GameParamType.Hard, _balance.DefaultBalance.VideoBoostHardValue, GetCurrencyPlace.VideoBoostWindow);
            _ui.SpawnResourceBubble(GameParamType.Hard, _balance.DefaultBalance.VideoBoostHardValue, _getRewardButton.RectTransform.position);
            _counter.SetValue(0, false);
            RedrawMarks();
        }

        public override BaseButton GetTutorialButton()
        {
            return _watchButton;
        }

        public override void Close()
        {
            _boost.UpdateEvent -= RedrawProgress;
            var tickets = _params.GetParam<GameSystem>(GameParamType.AdTickets);
            if(tickets != null) tickets.UpdatedEvent -= Redraw;
            base.Close();
        }
    }
}