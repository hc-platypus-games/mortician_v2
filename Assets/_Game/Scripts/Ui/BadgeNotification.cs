﻿using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Tools;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui
{
    public enum BadgeNotificationType
    {
        None,
        Store,
        Cards,
        Task,
        DailyQuests,
        VideoBoost,
        AngryCharacters,
        CorpsePoint,
        CorpseOrders
    }
    
    public class BadgeNotification : MonoBehaviour
    {
        [SerializeField] private BadgeNotificationType _type;
        [SerializeField] private Image _complete;
        [SerializeField] private Image _active;
        [SerializeField] private TextMeshProUGUI _text;

        [Inject] private TaskSystem _tasks;
        [Inject] private CharactersFactory _characters;
        [Inject] private GameFlags _flags;
        [Inject] private OffersFactory _offers;
        [Inject] private BoostSystem _boosts;
        [Inject] private CardsFactory _cards;
        [Inject] private CorpseFactory _corpses;
        [Inject] private LevelEventsFactory _levelPoints;
        
        public BadgeNotificationType Type => _type;

        public void Redraw()
        {
            var count = GetCount();
            if (count == 0)
            {
                Hide();
                return;
            }

            this.Activate();

            switch (_type)
            {
                case BadgeNotificationType.VideoBoost:
                    _text.text = "!";
                    break;
                
                default:
                    _text.text = count.ToString();
                    break;
            }
        }

        private void Hide()
        {
            this.Deactivate();
        }
        
        private int GetCount()
        {
            switch (_type)
            {
                case BadgeNotificationType.Task:
                    return GetPossibleTasks();
                
                case BadgeNotificationType.AngryCharacters:
                    return _characters.GetAngryCharactersCount();
                
                case BadgeNotificationType.Store:
                    return GetPossibleOffers();
                
                case BadgeNotificationType.VideoBoost:
                    return GetPossibleVideoBoost();

                case BadgeNotificationType.Cards:
                    return GetPossibleCards();
                
                case BadgeNotificationType.CorpsePoint:
                    return GetPossibleCorpsePoints();
                
                case BadgeNotificationType.CorpseOrders:
                    return GetPossibleCorpseOrders();
                
                default:
                    return 0;
            }
        }

        private int GetPossibleTasks()
        {
            return _tasks.Tasks.Count(t => t.IsCompleted && !t.IsFinished);
        }
        
        private int GetPossibleOffers()
        {
            if (!_flags.Has(GameFlag.TutorialFinished)) return 0;
            var freeBox = _flags.Has(GameFlag.CharactersOpened) ? _offers.GetOfferById(1).IsAvailable() : 0;
            var hardOffer = _offers.GetOfferById(7).IsAvailable();

            return freeBox + hardOffer;
        }
        
        private int GetPossibleVideoBoost()
        {
            return _boosts.GetAdBoost(AdPlacement.VideoBoost).Counter == 0 ? 1 : 0;
        }
        
        private int GetPossibleCards()
        {
            return !_flags.Has(GameFlag.CharactersOpened) ? 0 : _cards.CanUpgradeCards();
        }
        
        private int GetPossibleCorpsePoints()
        {
            return _levelPoints.GetAvailablePoints();
        }
        
        private int GetPossibleCorpseOrders()
        {
            return _corpses.GetAvailableOrders();
        }
    }
}