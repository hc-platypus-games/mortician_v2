﻿using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui.Room
{
    public class CemeteryWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _roomInfo;

        [Header("TopButtons")] 
        [SerializeField] private BaseButton _workersButton;
        [SerializeField] private BaseButton _itemsButton;
        [SerializeField] private Color _activeButtonColor;
        [SerializeField] private Color _inactiveButtonColor;
        
        [Header("Morticians")]
        [SerializeField] private TextMeshProUGUI _morticianTitle;
        [SerializeField] private TextMeshProUGUI _morticianTrainTitleText;
        [SerializeField] private TextMeshProUGUI _morticianIncomeText;
        [SerializeField] private TextMeshProUGUI _morticianCycleText;
        [SerializeField] private TextMeshProUGUI _morticianHireTitleText;
        [SerializeField] private TextMeshProUGUI _morticianHireQuantityText;
        [SerializeField] private BaseButton _morticianTrainButton;
        [SerializeField] private BaseButton _morticianHireButton;
        
        [Header("Gardeners")]
        [SerializeField] private TextMeshProUGUI _gardenersTitle;
        [SerializeField] private TextMeshProUGUI _gardenersTrainTitleText;
        [SerializeField] private TextMeshProUGUI _gardenersIncomeText;
        [SerializeField] private TextMeshProUGUI _gardenersCycleText;
        [SerializeField] private TextMeshProUGUI _gardenersHireTitleText;
        [SerializeField] private TextMeshProUGUI _gardenersHireQuantityText;
        [SerializeField] private BaseButton _gardenersTrainButton;
        [SerializeField] private BaseButton _gardenersHireButton;

        [SerializeField] private GameObject _workersContainer;
        [SerializeField] private GameObject _itemsContainer;
        
        [SerializeField] private CemeteryWindowItem[] _cemeteryWindowItems;
        
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private RoomsFactory _roomFactory;
        [Inject] private CharactersFactory _charactersFactory;
        [Inject] private GameParamFactory _params;
        [Inject] private GameProgressFactory _progresses;
        [Inject] private GameSystem _game;
        [Inject] private LevelSystem _levelSystem;
        [Inject] private AppEventProvider _eventProvider;

        public BaseButton MorticianHireButton => _morticianHireButton;

        private float _hireMorticianPrice;
        private float _trainMorticianPrice;
        
        private float _hireGardenersPrice;
        private float _trainGardenersPrice;
        
        public override void Init()
        {
            _workersButton.SetCallback(OnPressedWorkersButton);
            _itemsButton.SetCallback(OnPressedItemsButton);
            
            _morticianTrainButton.SetCallback(OnPressedMorticianTrainButton);
            _morticianHireButton.SetCallback(OnPressedMorticianHireButton);
            _gardenersTrainButton.SetCallback(OnPressedGardenersTrainButton);
            _gardenersHireButton.SetCallback(OnPressedGardenersHireButton);

            var items = _balance.DefaultBalance.Items.FindAll(i => i.RoomType == RoomType.Cemetery && i.ItemId > 2);
            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];
                if (i < _cemeteryWindowItems.Length)
                {
                    _cemeteryWindowItems[i].Init(i, item, _roomFactory, _progresses, _game);
                }
            }
            
            base.Init();
        }

        private void OnPressedWorkersButton()
        {
            if (_workersContainer.activeInHierarchy) return;
            ActivateWorkerButton();
        }

        private void ActivateWorkerButton()
        {
            _workersContainer.Activate();
            _itemsContainer.Deactivate();

            _workersButton.transform.localScale = new Vector3(1.35f, 1.35f, 1.35f);
            _itemsButton.transform.localScale = new Vector3(1f, 1f, 1f);
            
            _workersButton.SetColors(_activeButtonColor, _activeButtonColor);
            _itemsButton.SetColors(_inactiveButtonColor, _inactiveButtonColor);
        }

        private void OnPressedItemsButton()
        {
            if (_itemsContainer.activeInHierarchy) return;
            ActivateItemsButton();
            
            foreach (var item in _cemeteryWindowItems)
            {
                if (item.isActiveAndEnabled) item.Redraw();
            }
        }

        private void ActivateItemsButton()
        {
            _workersContainer.Deactivate();
            _itemsContainer.Activate();
            
            _workersButton.transform.localScale = new Vector3(1f, 1f, 1f);
            _itemsButton.transform.localScale = new Vector3(1.35f, 1.35f, 1.35f);
            
            _workersButton.SetColors(_inactiveButtonColor, _inactiveButtonColor);
            _itemsButton.SetColors(_activeButtonColor, _activeButtonColor);
        }

        public override void UpdateLocalization()
        {
            _title.text = "CEMETERY".ToLocalized();
            _morticianTitle.text = "MORTICIANS".ToLocalized();
            _gardenersTitle.text = "GARDENERS".ToLocalized();
            _morticianHireTitleText.text = "HIRE".ToLocalized();
            _gardenersHireTitleText.text = "HIRE".ToLocalized();
            _morticianTrainButton.SetText("TRAIN".ToLocalized());
            _morticianHireButton.SetText("HIRE".ToLocalized());
            _gardenersTrainButton.SetText("TRAIN".ToLocalized());
            _gardenersHireButton.SetText("HIRE".ToLocalized());

            foreach (var item in _cemeteryWindowItems)
            {
                item.UpdateLocalization();
            }
            
            base.UpdateLocalization();
        }

        public override void Open(params object[] list)
        {
            var soft = _params.GetParam<GameSystem>(GameParamType.Soft);
            soft.UpdatedEvent += RedrawButtons;

            UpdateCharacterPrices();
            ActivateWorkerButton();
            RedrawButtons();
            base.Open(list);
        }

        private void UpdateCharacterPrices()
        {
            var quantity = _charactersFactory.GetCharactersCount(RoomType.Cemetery, CharacterType.Default);
            var quantityMax = _roomFactory.GetParamCurrentValue(RoomType.Cemetery, GameParamType.Seats);
            
            var workerItem = _roomFactory.GetRoomItem(RoomType.Cemetery, 1);
            if (workerItem != null)
            {
                _morticianTrainTitleText.text = $"{"TRAIN".ToLocalized()}: {"LEVEL".ToLocalized()} {workerItem.Level}";
                if (workerItem.IsCapLevel())
                {
                    _morticianTrainButton.SetInteractable(false);
                    _morticianTrainButton.SetText(1, "MAX".ToLocalized());
                }
                else
                {
                    _trainMorticianPrice = workerItem.GetParamLevelValue(GameParamType.MorticianTrainPrice);
                    _morticianTrainButton.SetInteractable(_game.IsEnoughCurrency(GameParamType.Soft, _trainMorticianPrice) && quantity > 0);
                    _morticianTrainButton.SetText(1, $"<sprite name=Soft>{_trainMorticianPrice.ToFormattedString()}");
                }
                _hireMorticianPrice = workerItem.GetParamLevelValue(GameParamType.MorticianHirePrice, false, quantity);

                var income = workerItem.Level == 0
                    ? workerItem.GetParamLevelValue(GameParamType.Income)
                    : workerItem.GetParamLevelValue(GameParamType.Income, true);
                income -= workerItem.GetParamValue(GameParamType.Income);
                
                _morticianIncomeText.text = $"{"INCOME".ToLocalized()}: " +
                                            $"<sprite name=Soft>{workerItem.GetParamLevelValue(GameParamType.Income).ToFormattedString()}" +
                                            $"+{income.ToFormattedString()}";

                var cd = workerItem.GetParamLevelValue(GameParamType.MorticianCycle, true)
                         - workerItem.GetParamLevelValue(GameParamType.MorticianCycle);
                var cdText = cd == 0 ? "" : $"{cd.ToFormattedString()}{"SS".ToLocalized()}";
                _morticianCycleText.text = $"{"CYCLE".ToLocalized()}: <sprite name=Soft>{workerItem.GetParamLevelValue(GameParamType.MorticianCycle)}" +
                                           $"{"SS".ToLocalized()}{cdText}";
                
                _morticianHireQuantityText.text = $"{"QUANTITY".ToLocalized()}: {quantity}/{quantityMax}";
                _morticianHireButton.SetInteractable(quantity < quantityMax && _game.IsEnoughCurrency(GameParamType.Soft, _hireMorticianPrice));
                _morticianHireButton.SetText(1, $"<sprite name=Soft>{_hireMorticianPrice.ToFormattedString()}");
            }
            
            quantity = _charactersFactory.GetCharactersCount(RoomType.Cemetery, CharacterType.Gardener);
            workerItem = _roomFactory.GetRoomItem(RoomType.Cemetery, 2);
            if (workerItem != null)
            {
                _gardenersTrainTitleText.text = $"{"TRAIN".ToLocalized()}: {"LEVEL".ToLocalized()} {workerItem.Level}";
                
                if (workerItem.IsCapLevel())
                {
                    _gardenersTrainButton.SetInteractable(false);
                    _gardenersTrainButton.SetText(1, "MAX".ToLocalized());
                }
                else
                {
                    _trainGardenersPrice = workerItem.GetParamLevelValue(GameParamType.MorticianTrainPrice);
                    _gardenersTrainButton.SetInteractable(_game.IsEnoughCurrency(GameParamType.Soft, _trainGardenersPrice) && quantity > 0);
                    _gardenersTrainButton.SetText(1, $"<sprite name=Soft>{_trainGardenersPrice.ToFormattedString()}");
                }
                _hireGardenersPrice = workerItem.GetParamLevelValue(GameParamType.MorticianHirePrice, false, quantity);
                
                var income = workerItem.Level == 0 
                    ? workerItem.GetParamLevelValue(GameParamType.Income)
                    : workerItem.GetParamLevelValue(GameParamType.Income, true);
                income -= workerItem.GetParamValue(GameParamType.Income);
                var incomeText = income == 0 ? "" : $"+{income.ToFormattedString()}";
                _gardenersIncomeText.text = $"{"INCOME".ToLocalized()}: <sprite name=Soft>{workerItem.GetParamValue(GameParamType.Income)}{incomeText}";
                
                var cd = workerItem.GetParamLevelValue(GameParamType.MorticianCycle, true)
                         - workerItem.GetParamLevelValue(GameParamType.MorticianCycle);
                var cdText = cd == 0 ? "" : $"{cd.ToFormattedString()}{"SS".ToLocalized()}";
                _gardenersCycleText.text = $"{"CYCLE".ToLocalized()}: <sprite name=Soft>{workerItem.GetParamValue(GameParamType.CycleDelay)}" +
                                           $"{"SS".ToLocalized()}{cdText}";
                _gardenersHireQuantityText.text = $"{"QUANTITY".ToLocalized()}: {quantity}/{quantityMax}";
                
                _gardenersHireButton.SetInteractable(quantity < quantityMax && _game.IsEnoughCurrency(GameParamType.Soft, _hireGardenersPrice));
                _gardenersHireButton.SetText(1, $"<sprite name=Soft>{_hireGardenersPrice}");
            }
        }

        private void RedrawButtons()
        {
            _roomInfo.text =
                $"<sprite name=Soft>{_roomFactory.GetParamCurrentValue(RoomType.Cemetery, GameParamType.Income)}" +
                $" <sprite name=Seats>{_charactersFactory.GetCharactersCount(RoomType.Cemetery)}";
            if (_workersContainer.activeInHierarchy)
            {
                UpdateCharacterPrices();
            }
            else
            {
                foreach (var item in _cemeteryWindowItems)
                {
                    item.Redraw();
                }
            }
        }

        public override void Close()
        {
            var soft = _params.GetParam<GameSystem>(GameParamType.Soft);
            soft.UpdatedEvent -= RedrawButtons;
            
            foreach (var item in _cemeteryWindowItems)
            {
                item.Clear();
            }
            base.Close();
        }
        
        private void OnPressedMorticianTrainButton()
        {
            var workerItem = _roomFactory.GetRoomItem(RoomType.Cemetery, 1);
            workerItem.Upgrade(true);
            _game.SpendCurrency(GameParamType.Soft, _trainMorticianPrice, SpendCurrencyPlace.CemeteryWindow, SpendCurrencyItem.TrainMortician);
            RedrawButtons();
            
            _eventProvider.TriggerEvent(AppEventType.Analytics,
                GameEvents.UpgradeCharacter,
                _levelSystem.CurrentLevel.Id,
                _levelSystem.CurrentLevel.LevelName,
                workerItem.Level, 
                "Cemetery Morticians Train");
        }

        private void OnPressedMorticianHireButton()
        {
            var quantity = _charactersFactory.GetCharactersCount(RoomType.Cemetery, CharacterType.Default);
            var quantityMax = _roomFactory.GetParamCurrentValue(RoomType.Cemetery, GameParamType.Seats);
            if (quantity >= quantityMax) return;
            
            _charactersFactory.SpawnWorker(RoomType.Cemetery);
            _game.SpendCurrency(GameParamType.Soft, _hireMorticianPrice, SpendCurrencyPlace.CemeteryWindow, SpendCurrencyItem.HireMortician);
            RedrawButtons();
            
            _eventProvider.TriggerEvent(AppEventType.Analytics,
                GameEvents.UpgradeCharacter,
                _levelSystem.CurrentLevel.Id,
                _levelSystem.CurrentLevel.LevelName,
                quantity, 
                "Cemetery Workers Count");
        }

        private void OnPressedGardenersTrainButton()
        {
            var workerItem = _roomFactory.GetRoomItem(RoomType.Cemetery, 2);
            workerItem.Upgrade(true);
            _game.SpendCurrency(GameParamType.Soft, _trainGardenersPrice, SpendCurrencyPlace.CemeteryWindow, SpendCurrencyItem.TrainGardener);
            RedrawButtons();
            
            _eventProvider.TriggerEvent(AppEventType.Analytics,
                GameEvents.UpgradeCharacter,
                _levelSystem.CurrentLevel.Id,
                _levelSystem.CurrentLevel.LevelName,
                workerItem.Level, 
                "Cemetery Gardeners Train");
        }

        private void OnPressedGardenersHireButton()
        {
            var quantity = _charactersFactory.GetCharactersCount(RoomType.Cemetery, CharacterType.Gardener);
            var quantityMax = _roomFactory.GetParamCurrentValue(RoomType.Cemetery, GameParamType.Seats);
            if (quantity >= quantityMax) return;
            
            _charactersFactory.SpawnWorker(RoomType.Cemetery, CharacterType.Gardener);
            _game.SpendCurrency(GameParamType.Soft, _hireGardenersPrice, SpendCurrencyPlace.CemeteryWindow, SpendCurrencyItem.HireGardener);
            RedrawButtons();
            
            _eventProvider.TriggerEvent(AppEventType.Analytics,
                GameEvents.UpgradeCharacter,
                _levelSystem.CurrentLevel.Id,
                _levelSystem.CurrentLevel.LevelName,
                quantity, 
                "Cemetery Workers Count");
        }
    }
}