using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Buttons;
using _Game.Scripts.View.Room;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Room
{
    public class CemeteryWindowItem : BaseUIView
    {
        [SerializeField] private TextMeshProUGUI _itemName;
        [SerializeField] private TextMeshProUGUI _resetTime;
        [SerializeField] private TextMeshProUGUI _maxGraves;
        [SerializeField] private TextMeshProUGUI _completedText;
        [SerializeField] private Image _completedImage;
        [SerializeField] private ProgressButton _button;

        [Inject] private HardSystem _hard;

        private int _id;
        private RoomItemConfig _config;
        private RoomsFactory _factory;
        private GameProgressFactory _progresses;
        private GameSystem _game;

        private RoomItemView _item;
        private GameProgress _buildingProgress;
        
        public void Init(int id, RoomItemConfig config, RoomsFactory factory, GameProgressFactory progresses, GameSystem game)
        {
            _id = id;
            _config = config;
            _factory = factory;
            _progresses = progresses;
            _game = game;
            
            _item = _factory.GetRoomItem(RoomType.Cemetery, _config.ItemId);
            
            _button.SetCallback(OnPressedButton);
        }

        private void OnPressedButton()
        {
            if (_item == null) return;
            _buildingProgress = _progresses.GetProgress(_item, GameParamType.BuildingTimer);
            if (_buildingProgress != null)
            {
                var price = _hard.GetPriceForTime(_buildingProgress.LeftValue);
                if (_hard.IsEnough(price))
                {
                    _buildingProgress.Change(_buildingProgress.LeftValue);
                    _hard.SpendHard(price,
                        SpendCurrencyPlace.BuySkipTime, SpendCurrencyItem.Cemetery);
                }
                return;
            }
            _buildingProgress = _item.StartBuilding();
            if(_buildingProgress == null) return;
            _buildingProgress.UpdatedEvent += OnUpdateTimer;
            _buildingProgress.CompletedEvent += OnCompletedTimer;
            
            RedrawButton();
        }

        private void OnCompletedTimer()
        {
            _button.Deactivate();
        }

        public void Redraw()
        {
            _item ??= _factory.GetRoomItem(RoomType.Cemetery, _config.ItemId);
            if (_item == null) return;

            if (_item.IsBought)
            {
                if (!_completedText.isActiveAndEnabled) _completedText.Activate();
                if (!_completedImage.isActiveAndEnabled) _completedImage.Activate();
                if (_button.isActiveAndEnabled) _button.Deactivate();
            }
            else
            {
                if (!_completedText.isActiveAndEnabled) _completedText.Deactivate();
                if (!_completedImage.isActiveAndEnabled) _completedImage.Deactivate();
                if (!_button.isActiveAndEnabled) _button.Activate();
                _buildingProgress = _progresses.GetProgress(_item, GameParamType.BuildingTimer);
                RedrawButton();
            }
            
            _resetTime.text = $"{"RESET_TIME".ToLocalized()}: {_config.CycleDelay.ToTimeStr(2, true, true)}";
            _maxGraves.text = $"{"GRAVES".ToLocalized()}: {_config.Capacity}";
        }

        private void RedrawButton()
        {
            if (_buildingProgress != null)
            {
                _buildingProgress.UpdatedEvent += OnUpdateTimer;
                _buildingProgress.CompletedEvent += OnCompletedTimer;
                var hardPrice = _hard.GetPriceForTime(_buildingProgress.LeftValue);
                _button.SetText("SPEEDUP".ToLocalized());
                _button.SetText(1, $"<sprite name=Hard>{hardPrice}");
                _button.SetText(2, _buildingProgress.LeftValue.ToTimeStr(3, true, true));
                _button.SetHardColor();
                _button.SetInteractable(_game.IsEnoughCurrency(GameParamType.Hard, hardPrice));
            }
            else
            { 
                _button.SetText("BUY".ToLocalized());
                _button.SetText(1, $"<sprite name=Soft>{_config.Price.ToFormattedString()}");
                _button.SetText(2, _config.BuildingTimer.ToTimeStr(3, false, true));
                _button.RestoreColors();
                _button.SetInteractable(_game.IsEnoughCurrency(GameParamType.Soft, _config.Price));
            }
        }

        private void OnUpdateTimer()
        {
            if (_buildingProgress != null)
            {
                var price = _hard.GetPriceForTime(_buildingProgress.LeftValue);
                if (_hard.IsEnough(price))
                {
                    _button.SetInteractable(true);
                    _button.SetHardColor();
                }
                else
                {
                    _button.SetInteractable(false);
                }
                _button.SetText(2, _buildingProgress.LeftValue.ToTimeStr(2, true, true));
                _button.SetText(1, $"<sprite name=Hard>{_hard.GetPriceForTime(_buildingProgress.LeftValue)}");
            }
        }

        public void UpdateLocalization()
        {
            if (_item != null)
            {
                _button.SetText("BUY".ToLocalized());
                _button.SetText(1, $"<sprite name=Soft>{100}");
            }

            var nameItem = $"{_config.Name.ToUpper()}_ITEM".ToLocalized();
            _itemName.text = $"{nameItem} #{_id + 1}";
            _completedText.text = "BUILT".ToLocalized();
        }

        public void Clear()
        {
            if (_buildingProgress != null)
            {
                _buildingProgress.UpdatedEvent -= OnUpdateTimer;
                _buildingProgress.CompletedEvent -= OnCompletedTimer;
                _buildingProgress = null;   
            }
        }
    }
}