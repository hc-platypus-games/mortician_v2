﻿using System;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.View.Room;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Room
{
    public class RoomInfoItem : BaseUIView
    {
        public Action<RoomInfoItem> OnPressed;
        
        [SerializeField] private Color _currentButtonColor1;
        [SerializeField] private Color _currentButtonColor2;
        [SerializeField] private Color _currentButtonIconColor;
        
        [SerializeField] private Color _boughtButtonColor1;
        [SerializeField] private Color _boughtButtonColor2;
        [SerializeField] private Color _boughtButtonIconColor;
        
        [SerializeField] private Color _unBoughtButtonColor1;
        [SerializeField] private Color _unBoughtButtonColor2;
        [SerializeField] private Color _unBoughtButtonIconColor;
        [SerializeField] private Color _lockedButtonIconColor;
        
        [SerializeField] private Color _hardButtonColor1;
        [SerializeField] private Color _hardButtonColor2;
        [SerializeField] private Color _hardButtonIconColor;
        
        [SerializeField] private Image _levelBack;
        [SerializeField] private Image _plus;
        [SerializeField] private Image _lock;
        
        [SerializeField] private TextMeshProUGUI _level;
        
        [SerializeField] private BaseButton _button;

        [Inject] private GameResources _resources;
        [Inject] private TutorialArrow _arrow;
        
        public BaseButton Button => _button;
        public RoomItemView RoomItem { get; private set; }
        
        public bool CanUpgrade { get; private set; }

        [Inject]
        private void Construct()
        {
            _button.Callback += () => OnPressed?.Invoke(this);
            _button.Callback += _arrow.Hide;
        }
        
        public class Pool : MonoMemoryPool<Transform, RoomItemView, RoomInfoItem>
        {
            protected override void Reinitialize(Transform parent, RoomItemView roomItem, RoomInfoItem item)
            {
                item.SetConfig(parent, roomItem);
            }
        }

        private void SetConfig(Transform parent, RoomItemView roomItem)
        {
            transform.SetParent(parent);
            RoomItem = roomItem;
            RectTransform.localScale = Vector3.one;
            if (roomItem == null)
            {
                _button.SetSprite(_resources.GetRoomIconSprite("EMPTY_ROOM_ITEM"));
                return;
            }
            if (RoomItem.Config != null) _button.SetSprite(_resources.GetRoomIconSprite(RoomItem.Config.Name));
        }

        public void Redraw(bool selected)
        {
            CanUpgrade = false;
            if (selected)
            {
                _button.SetColors(_currentButtonIconColor, _currentButtonColor1, _currentButtonColor2);
            }
            else if (RoomItem != null && RoomItem.Config.BoughtForHard)
            {
                _button.SetColors(_hardButtonIconColor, _hardButtonColor1, _hardButtonColor2);
            }
            else if (RoomItem != null && RoomItem.Level == 0)
            {
                _button.SetColors(_unBoughtButtonIconColor, _unBoughtButtonColor1, _unBoughtButtonColor2);
            }
            else
            {
                _button.SetColors(_boughtButtonIconColor, _boughtButtonColor1, _boughtButtonColor2);
            }
            
            if (RoomItem == null)
            {
                _button.SetColors(_lockedButtonIconColor);
                _lock.Activate();
                _plus.Deactivate();
                _levelBack.Deactivate();
                return;
            }
            
            if (RoomItem.Level == 0)
            {
                _levelBack.Deactivate();
                CanUpgrade = RoomItem.CanBuy();
                _plus.SetActive(CanUpgrade);
            }
            else
            {
                _levelBack.Activate();
                _level.text = $"{"level".ToLocalized()} {RoomItem.Level}";
                _levelBack.SetActive(!selected);
                _level.color = selected ? _currentButtonIconColor : Color.white;
                _plus.Deactivate();
            }
        
            RoomItem.ShowsItem(selected);
            if (RoomItem.IsEnoughLevel())
            {
                _lock.Deactivate();
            }
            else
            {
                _button.SetColors(_lockedButtonIconColor);
                _lock.Activate();
                _plus.Deactivate();
            }
        }
    }
}