﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Buttons;
using _Game.Scripts.Ui.Cards;
using _Game.Scripts.Ui.Statistic;
using _Game.Scripts.View.Fx;
using _Game.Scripts.View.Room;
using TMPro;
using UnityEngine;
using Zenject;
using Image = UnityEngine.UI.Image;

namespace _Game.Scripts.Ui.Room
{
    public class RoomInfoWindow : BaseWindow
    {
        [Header("Room")]
        [SerializeField] private TextMeshProUGUI _roomTitleText;
        [SerializeField] private TextMeshProUGUI _roomLevelText;
        [SerializeField] private TextMeshProUGUI _roomExpText;
        [SerializeField] private Image _roomExp;
        
        [Header("Character")]
        [SerializeField] private Image _avatar;
        [SerializeField] private Image _emptyAvatar;
        [SerializeField] private Image _lights;
        [SerializeField] private Image _unassignedBack;

        [Header("Buttons")]
        [SerializeField] private RepeatableButton _upgradeButton;
        [SerializeField] private BaseButton _closeFrameButton;
        [SerializeField] private BaseButton _avatarButton;

        [Header("Item")]
        [SerializeField] private Image _itemIcon;
        [SerializeField] private TextMeshProUGUI _itemName;
        [SerializeField] private TextMeshProUGUI _itemDesc;
        [SerializeField] private TextMeshProUGUI _itemLevel;
        [SerializeField] private TextMeshProUGUI _incomeTitle;
        [SerializeField] private TextMeshProUGUI _seatsTitle;
        [SerializeField] private TextMeshProUGUI _cycleTitle;
        [SerializeField] private TextMeshProUGUI _incomeValue;
        [SerializeField] private TextMeshProUGUI _seatsValue;
        [SerializeField] private TextMeshProUGUI _cycleValue;
        [SerializeField] private TextMeshProUGUI _unassignedText;
        
        [SerializeField] private RectTransform _itemsContainer;

        [Header("Room")] 
        [SerializeField] private StatisticWindowItem _roomStatistic;
        [SerializeField] private ProgressButton _roomUpgradeButton;

        [Inject] private GameSystem _game;
        [Inject] private HardSystem _hard;
        [Inject] private RoomsFactory _rooms;
        [Inject] private UIFactory _uiFactory;
        [Inject] private GameResources _resources;
        [Inject] private GameCamera _camera;
        [Inject] private GameParamFactory _params;
        [Inject] private GameProgressFactory _progresses;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private CardsFactory _cards;
        [Inject] private WindowsSystem _windows;
        [Inject] private TaskSystem _tasks;
        [Inject] private TutorialArrow _arrow;
        [Inject] private MapPointsFactory _points;

        private readonly List<RoomInfoItem> _items = new();
        
        private RoomView _room;
        private RoomItemView _item;
        private RoomInfoItem _currentItem;
        private CardUI _card;
        private GameProgress _buildingProgress;

        private RoomUpgradeBlind[] _roomUpgradeBlinds;
        private float _itemSpacing;

        private string _incomeBoost;
        private string _cycleBoost;

        public BaseButton UpgradeButton => _upgradeButton;

        public override void Init()
        {
            _closeFrameButton.SetCallback(Close);
            _avatarButton.SetCallback(OnPressedAvatar);
            _upgradeButton.SetCallback(OnPressedUpgrade);
            _roomUpgradeButton.SetCallback(OnPressedUpgradeRoom);
            _roomStatistic.UpdateLocalization();
            _roomUpgradeBlinds = GetComponentsInChildren<RoomUpgradeBlind>(true);
            if (_roomUpgradeBlinds.Length > 0)
            {
                foreach (var item in _roomUpgradeBlinds)
                {
                    item.Init();
                }
            }
            base.Init();
        }

        public override void UpdateLocalization()
        {
            _incomeTitle.text = "INCOME".ToLocalized();
            _seatsTitle.text = "SEATS".ToLocalized();
            _cycleTitle.text = "CYCLE_DELAY".ToLocalized();
            _unassignedText.text = "UNASSIGNED".ToLocalized();
            base.UpdateLocalization();
        }

        private void PlayFxUpgrade()
        {
            if (_roomUpgradeBlinds.Length > 0)
            {
                foreach (var item in _roomUpgradeBlinds)
                {
                    item.Play();
                }
            }
            
            if(_incomeBoost is {Length: > 0}) _uiFactory.SpawnTextFx(_incomeBoost, Vector3.zero, _incomeValue.rectTransform);
            if(_cycleBoost is {Length: > 0}) _uiFactory.SpawnTextFx(_cycleBoost, Vector3.zero, _cycleValue.rectTransform);
        }

        private void OnPressedUpgrade()
        {
            if(_currentItem == null) return;
            _arrow.Hide();
            _currentItem.RoomItem.Upgrade();
            PlayFxUpgrade();
            RedrawRoomInfo();
            RedrawItemInfo();
            RedrawItems();
        }

        private void OnPressedUpgradeRoom()
        {
            _arrow.Hide();            
            if (_buildingProgress is {IsActive: true})
            {
                var price = _hard.GetPriceForTime(_buildingProgress.LeftValue);
                if (_hard.IsEnough(price))
                {
                    _buildingProgress.Change(_buildingProgress.LeftValue);
                    _hard.SpendHard(price,
                        SpendCurrencyPlace.BuySkipTime, SpendCurrencyItem.Room);
                }
                else
                {
                    _uiFactory.SpawnMessage("HARD_NOT_ENOUGH".ToLocalized());
                }
                return;
            }     
            
            if (_buildingProgress is {IsCompleted: true})
            {
                if (_room != null) _room.Upgrade();
                Close();
                var point = _points.GetFocusPoint(_room.Type);
                _camera.Focus(point.position, 0.5f, false);
                return;
            }
            
            if (_room != null)
            {
                _room.StartUpgrade();
                RedrawUpgradeTab();
            }
        }

        public override void Open(params object[] list)
        {
            if (list.Length == 0)
            {
                Close();
                return;
            }
            
            _room = (RoomView)list[0];
            if (list.Length > 1) _item = (RoomItemView) list[1];
            else _item = null;
            RedrawItemsTab();
            base.Open();
        }

        private void RedrawItemsTab()
        {
            var soft = _params.GetParam<GameSystem>(GameParamType.Soft);
            soft.UpdatedEvent += RedrawBuyButton;
            
            UpdateItems();

            if (_items.Count == 0)
            {
                _camera.UpdateLastCamSize();
                Close();
                return;
            }
            
            RedrawRoomInfo();
            RedrawCharInfo();
            if (_tasks.FollowingTask != null && _tasks.FollowingTask.GetRoomId() == (int)_room.Type)
            {
                switch (_tasks.FollowingTask.Type)
                {
                    case GameEvents.BuyRoom:
                    case GameEvents.UpgradeRoom:
                        _arrow.Hide();
                        break;
                    
                    case GameEvents.BuyRoomItem:
                    case GameEvents.UpgradeRoomItem:
                    case GameEvents.ReachItemLevel:
                        var itemId = _tasks.FollowingTask.GetItemId();
                        var selectedItem = _items.FirstOrDefault(i => i.RoomItem.Config.ItemId == itemId);
                        if (selectedItem) SetCurrentItem(selectedItem);
                        _arrow.Show(_upgradeButton.RectTransform);
                        break;
                }
            }
            else
            {
                _arrow.Hide();
                var selectedItem = _items[0];
                if (_item != null)
                {
                    selectedItem = _items.FirstOrDefault(i => i.RoomItem.Id == _item.Id);
                }
                SetCurrentItem(selectedItem);
            }
            
            _tasks.ClearFollowingTask();
        }

        private void RedrawUpgradeTab()
        {
            var type = _room.Type;
            var sprite = _resources.GetRoomImagesSprite(type.ToString());
            _roomStatistic.Redraw(type.ToString(), sprite, _room.Level, _resources.GrayscaleSpriteMaterial, 
                _rooms.GetRoomItemValue(type, GameParamType.Income, _room.Level), 
                _rooms.GetRoomItemValue(type, GameParamType.Income, 26), 
                _rooms.GetRoomItemValue(type, GameParamType.Income, 51));
            
            _roomUpgradeButton.SetInteractable(_room.CanUpgrade() && _game.IsEnoughCurrency(GameParamType.Soft, _room.UpgradePrice()));
            
            _buildingProgress = _progresses.GetProgress(_room, GameParamType.BuildingTimer);

            if (_room.Level == 3)
            {
                _roomUpgradeButton.SetText("MAX".ToLocalized());
                _roomUpgradeButton.SetText(1,"LEVEL".ToLocalized());
                _roomUpgradeButton.SetText(2,"");
                return;
            }
            
            var timeToBuild = _room.TimeToUpgrade().ToTimeStr(3, true, true);
            if (_buildingProgress is {IsActive: true})
            {
                _buildingProgress.UpdatedEvent += OnUpdateTimer;
                _buildingProgress.CompletedEvent += RedrawOnComplete;
                _roomUpgradeButton.SetText(1, $"<sprite name=Hard>{_hard.GetPriceForTime(_buildingProgress.LeftValue)}");
                
                var timeLeft = _buildingProgress.LeftValue.ToTimeStr(3, true, true);
                _roomUpgradeButton.SetText(2, timeLeft);
                _roomUpgradeButton.SetHardColor();
            }
            else if (_buildingProgress is {IsCompleted: true})
            {
                RedrawOnComplete();
            } else
            { 
                _roomUpgradeButton.SetText(2, timeToBuild);
                if (_room.CanUpgrade())
                {
                    _roomUpgradeButton.SetText("UPGRADE".ToLocalized());
                    _roomUpgradeButton.SetText(1, _room.UpgradePrice() == 0 ? "FREE".ToLocalized() : $"<sprite name=Soft>{_room.UpgradePrice().ToFormattedString()}");
                }
                else
                {
                    _roomUpgradeButton.SetText("CANT_UPGRADE_ROOM".ToLocalized());
                    var capExp = _rooms.GetCapRoomExp(_room);
                    var currentExp = _rooms.GetRoomExp(_room);
                    var exp = (float)currentExp / capExp;
                    _roomUpgradeButton.SetText(1,  $"{Mathf.Round(100 * exp).ToString(CultureInfo.InvariantCulture)}%");
                }
            }
        }

        private void OnUpdateTimer()
        {
            if (_buildingProgress != null)
            {
                var price = _hard.GetPriceForTime(_buildingProgress.LeftValue);
                if (_hard.IsEnough(price))
                {
                    _roomUpgradeButton.SetInteractable(true);
                    _roomUpgradeButton.SetHardColor();
                }
                else
                {
                    _roomUpgradeButton.SetInteractable(false);
                }
                _roomUpgradeButton.SetText(2, _buildingProgress.LeftValue.ToTimeStr(3, true, true));
                _roomUpgradeButton.SetText(1, $"<sprite name=Hard>{price}");
            }
        }

        private void RedrawOnComplete()
        {
            _roomUpgradeButton.SetInteractable(true);
            _roomUpgradeButton.SetText(2, "COMPLETE".ToLocalized());
            _roomUpgradeButton.SetText(1, $"LEVEL".ToLocalized());
        }

        private void RedrawRoomInfo()
        {
            _roomTitleText.text = _room.Type.ToString().ToUpper().ToLocalized();
            _roomLevelText.text = $"{"LVL".ToLocalized()}: {_room.Level}";
            
            var capExp = _rooms.GetCapRoomExp(_room);
            var currentExp = _rooms.GetRoomExp(_room);
            var exp = (float)currentExp / capExp;
            _roomExpText.text = $"{Mathf.Round(100 * exp).ToString(CultureInfo.InvariantCulture)}%";
            _roomExp.fillAmount = exp;
            
            RedrawUpgradeTab();
        }

        private void RedrawCharInfo()
        {
            _card = _cards.GetCardByRoomType(_room.Type);

            if (_card == null || _card.Level == 0)
            {
                _avatar.Deactivate();
                _emptyAvatar.Activate();
                _lights.Deactivate();
                _unassignedBack.Deactivate();
            }
            else
            {
                _avatar.Activate();
                _emptyAvatar.Deactivate();
                _lights.Activate();
                _unassignedBack.SetActive(!_card.Assigned);
                _avatar.sprite = _resources.GetAvatarSprite(_card == null ? "" : _card.Config.Name);
                _avatar.material = _card.Assigned ? null : _resources.GrayscaleSpriteMaterial;
            }
        }

        private void UpdateItems()
        {
            var allItems = _balance.DefaultBalance.Items.Count(r => r.RoomType == _room.Type);
            var roomItems = _rooms.GetRoomItems(_room.Type)
                                                             .OrderBy(i => i.Config.SortId)
                                                             .ToList();
            foreach (var element in roomItems)
            {
                var item = _uiFactory.RoomItemSpawn(_itemsContainer, element);
                item.OnPressed += SetCurrentItem;
                _items.Add(item);
            }

            for (int i = 0; i < allItems - roomItems.Count(); i++)
            {
                var item = _uiFactory.RoomItemSpawn(_itemsContainer, null);
                item.OnPressed += SetCurrentItem;
                _items.Add(item);
            }
        }

        private void RedrawItems()
        {
            foreach (var element in _items)
            {
                element.Redraw(element == _currentItem);
            }
        }

        private void SetCurrentItem(RoomInfoItem item)
        {
            _currentItem = item;
            
            var roomItem = _currentItem.RoomItem;
            if (roomItem == null)
            {
                _itemIcon.sprite = _resources.GetRoomIconSprite("EMPTY_ROOM_ITEM");
                _itemName.text = "EMPTY_ROOM_ITEM_ITEM".ToLocalized();
                _itemDesc.text = "EMPTY_ROOM_ITEM_DESC".ToLocalized();
                RedrawItemInfo();
                RedrawItems();
                return;
            }
            
            roomItem.ShowsItem(true);
            
            if (roomItem.Config == null) return;
            
            _itemIcon.sprite = _resources.GetRoomIconSprite(roomItem.Config.Name.ToUpper());
            _itemName.text = $"{roomItem.Config.Name}_ITEM".ToLocalized();
            _itemDesc.text = $"{roomItem.Config.Name}_DESC".ToLocalized();
            
            RedrawItemInfo();
            RedrawItems();
            
            _camera.Focus(roomItem.transform.position + SceneData.ItemFocusOffset, _room.CamFocusScale, 0.5f);
        }
        
        private void RedrawItemInfo()
        {
            if(!_currentItem) return;
            var item = _currentItem.RoomItem;

            if (item == null)
            {
                _itemLevel.text = "";
                _incomeValue.text = "";
                _cycleValue.text = "";
                _seatsValue.text = "";
                RedrawBuyButton();
                return;
            }
            
            _itemLevel.text = $"{"ITEM_LEVEL".ToLocalized()}: {item.Level}";
            
            var incomeValue = item.Level == 0 ? item.GetParamLevelValue(GameParamType.Income, true) :
                item.GetParamLevelValue(GameParamType.Income, true) - item.GetParamLevelValue(GameParamType.Income);
            _incomeBoost = $"+{incomeValue}";
            var currentIncome = item.Level == 0 ? 0: item.GetParamLevelValue(GameParamType.Income);
            _incomeValue.text = $"<sprite name=Soft>{currentIncome.ToFormattedString()}" + _incomeBoost;
            
            var cdValue = _rooms.GetParamCurrentValue(item.RoomType, GameParamType.CycleDelay);
            if (cdValue == 0)
            {
                _cycleTitle.Deactivate();
                _cycleValue.Deactivate();
            }
            else
            {
                _cycleTitle.Activate();
                _cycleValue.Activate();
                var cd = item.Level == 0 
                ? item.GetParamValue(GameParamType.CycleDelay)
                : item.GetParamLevelValue(GameParamType.CycleDelay, true) - item.GetParamValue(GameParamType.CycleDelay);
                
                _cycleBoost = cd == 0 ? "" : $"-{cd.ToFormattedString()}{"SS".ToLocalized()}";
                _cycleValue.text = $"<sprite name=CycleDelay>{_rooms.GetParamCurrentValue(item.RoomType, GameParamType.CycleDelay).ToFormattedString()}" +
                                   $"{"SS".ToLocalized()}{_cycleBoost}";
            }
            
            var seatsText = item.Level > 0 || item.Config.Seats == 0 ? "" : $"+{item.Config.Seats}";
            _seatsValue.text = $"<sprite name=Seats>{_rooms.GetParamCurrentValue(item.RoomType, GameParamType.Seats)}" +
                                $"{seatsText}";
            
            RedrawBuyButton();
        }

        private void RedrawBuyButton()
        {
            if(_currentItem == null) return;
            var item = _currentItem.RoomItem;
            if (item == null)
            {
                _upgradeButton.SetInteractable(false);
                _upgradeButton.SetText("COMING".ToLocalized());
                _upgradeButton.SetText(1, "SOON".ToLocalized());
                return;
            }
            if (item.Config.BoughtForHard)
            {
                _upgradeButton.SetText(item.Level == 0 ? "BUY".ToLocalized() : "MAX".ToLocalized());
                _upgradeButton.SetText(1, $"<sprite name=Hard>{item.Price}");
                _upgradeButton.SetInteractable(item.Level == 0);
            }
            else
            {
                if (item.IsEnoughLevel())
                {
                    var text = item.Level == 0 
                        ? "BUY".ToLocalized() : item.IsCapLevel() 
                            ? "MAX".ToLocalized() : "UPGRADE".ToLocalized();
                    _upgradeButton.SetText(text);
                    
                    var priceText = item.Price > 0 ? item.Price.ToFormattedString() : "FREE".ToLower();
                    _upgradeButton.SetText(1, $"<sprite name=Soft>{priceText}");
                    _upgradeButton.SetInteractable(!item.IsCapLevel() && item.CanBuy());
                }
                else
                {
                    _upgradeButton.SetText("REQUIRED".ToLocalized());
                    _upgradeButton.SetText(1, $"{"LVL".ToLocalized()}:{item.Config.RequiredLevel}");
                    _upgradeButton.SetInteractable(false);
                }
            }
        }

        private void OnPressedAvatar()
        {
            if(!_card) return;
            if (_card.Level == 0) return;
            var window = (CardInfoWindow)_windows.GetWindow<CardInfoWindow>();
            window.Assigned += OnCardAssigned;
            window.Open(_card, true);
        }

        private void OnCardAssigned(CardUI card)
        {
            var window = (CardInfoWindow)_windows.GetWindow<CardInfoWindow>();
            window.Assigned -= OnCardAssigned;
            RedrawCharInfo();
        }

        public void PressButton(int id)
        {
            var item = _items.FirstOrDefault(i => i.RoomItem.Id == id);
            if (item == null) return;
            item.Button.Callback?.Invoke();
        }

        public override BaseButton GetTutorialButton()
        {
            return _avatarButton;
        }

        public override void Close()
        {
            var soft = _params.GetParam<GameSystem>(GameParamType.Soft);
            soft.UpdatedEvent -= RedrawItems;
            _upgradeButton.Reset();
            if (_currentItem && _currentItem.RoomItem) _currentItem.RoomItem.ShowsItem();
            _currentItem = null;
            foreach (var item in _items)
            {
                item.OnPressed -= SetCurrentItem;
                _uiFactory.RemoveItem(item);
            }
            
            if (_buildingProgress != null)
            {
                _buildingProgress.UpdatedEvent -= OnUpdateTimer;
                _buildingProgress.CompletedEvent -= RedrawOnComplete;
                _buildingProgress = null;   
            }
            
            _camera.SetLastScale();
            _items.Clear();
            base.Close();
        }
    }
}
