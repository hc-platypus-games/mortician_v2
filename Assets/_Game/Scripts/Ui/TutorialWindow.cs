﻿using System;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace _Game.Scripts.Ui
{
    public class TutorialWindow : BaseWindow
    {
        public Action OnCompleted;
        
        [SerializeField] private TextMeshProUGUI _tipText;
        [SerializeField] private string _color;
        [SerializeField] private BaseButton _button;

        private Tween _textTween;

        private const float TIP_DURATION = 0.03f;
        private int _tipChangingFrame;

        public override void Init()
        {
            _button.SetCallback(ForceTip);
            base.Init();
        }

        public void Open(string text)
        {
            ShowTips(text);
            if (!isActiveAndEnabled) this.Activate();
        }

        public void ShowTips(string text)
        {
            _tipText.text = text.Replace("%1", $"<color=#{_color}>")
                                .Replace("%2", "</color>");

            PlayTipAnim();
        }

        private void PlayTipAnim()
        {
            _textTween?.Kill();
            _textTween = null;

            _tipChangingFrame = Time.frameCount;
            _tipText.maxVisibleCharacters = 0;
            _textTween = PlayTextAnim(_tipText, TIP_DURATION, RestoreTargetFrameRaycast);
        }
        
        private static Tween PlayTextAnim(TMP_Text text, float charDuration = 0.03f, Action onComplete = null)
        {
            text.ForceMeshUpdate();
            Debug.Log(text.GetTextInfo(text.text));
            var characterCount = text.GetTextInfo(text.text).characterCount;
            var duration = characterCount * charDuration;
            text.maxVisibleCharacters = 0;

            return DOTween.To(() => text.maxVisibleCharacters,
                    value => text.maxVisibleCharacters = value,
                    characterCount,
                    duration)
                .SetUpdate(true)
                .SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    onComplete?.Invoke();
                    text.maxVisibleCharacters = int.MaxValue;
                });
        }
        
        private void RestoreTargetFrameRaycast()
        {
            _textTween = null;
        }

        private void ForceTip()
        {
            if (_tipChangingFrame == Time.frameCount) return;

            if (_textTween == null)
            {
                OnCompleted?.Invoke();
            }
            else
            {
                _textTween?.Complete(false);
                _textTween = null;
			
                RestoreTargetFrameRaycast();   
            }
        }

        public override void Close()
        {
            this.Deactivate();
        }
    }
}