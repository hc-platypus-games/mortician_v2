﻿using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Tasks
{
    public class TaskListWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _taskProgressText;
        [SerializeField] private TextMeshProUGUI _taskProgressSliderText;
        [SerializeField] private Image _taskProgress;
        [SerializeField] private BaseButton _claimRewards;
        [SerializeField] private TaskListItem[] _items;
        
        [Inject] private GameSystem _game;
        [Inject] private TaskSystem _tasks;
        [Inject] private UIFactory _uiFactory;
        [Inject] private GameResources _resources;
        [Inject] private RoomsFactory _rooms;
        
        public override void Init()
        {
            _claimRewards.SetCallback(OnPressedClaim);
            
            foreach (var item in _items)
            {
                item.Init(_game, _uiFactory, _tasks, _resources, _rooms);
            }
            base.Init();
        }

        private void OnPressedClaim()
        {
            _game.AddCurrency(GameParamType.Hard, _tasks.TaskHardReward, GetCurrencyPlace.Quest);
            _uiFactory.SpawnResourceBubble(GameParamType.Hard, _tasks.TaskHardReward);
            _tasks.ClearTasks();
            RedrawTasks();
            UpdateTaskProgress();
        }

        public override BaseButton GetTutorialButton()
        {
            return _items[0].Button;
        }

        public override void UpdateLocalization()
        {
            _title.text = "TASK_LIST_WINDOW_TITLE".ToLocalized();
            base.UpdateLocalization();
        }

        public override void Open(params object[] list)
        {
            RedrawTasks();
            UpdateTaskProgress();
            
            base.Open(list);
        }

        private void RedrawTasks()
        {
            for (int i = 0; i < _items.Length; i++)
            {
                if (i >= _tasks.Tasks.Count)
                {
                    _items[i].Deactivate();
                }
                else
                {
                    _items[i].Redraw(_tasks.Tasks[i]);
                    _items[i].ButtonPressed += UpdateTaskProgress;
                    if (!_items[i].isActiveAndEnabled) _items[i].Activate();
                }
            }
        }

        private void UpdateTaskProgress()
        {
            var current = _tasks.Tasks.Count(t => t.IsFinished);
            var need = _tasks.Tasks.Count;
            _claimRewards.SetInteractable(current == need);
            
            _taskProgress.fillAmount = (float)current / need;
            _taskProgressSliderText.text =  $"{((float)current).ToFormattedString()}/{((float)need).ToFormattedString()}";
            
            _taskProgressText.text = "TASK_LIST_WINDOW_TEXT"
                .ToLocalized()
                .Replace("%1", (need - current).ToString())
                .Replace("%2", $"{_tasks.TaskHardReward}<sprite name=Hard>");
        }
    }
}