using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.GamePlayElements;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Tools;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Tasks
{
    public class TaskHudButton : MainWindowButton
    {
        [SerializeField] private TextMeshProUGUI _taskText;
        [SerializeField] private TextMeshProUGUI _progressText;
        [SerializeField] private Image _progressImage;

        [Inject] private GameResources _resources;
        [Inject] private TaskSystem _tasks;

        private float _lastProgress;

        public override void Init(WindowsSystem windows)
        {
            base.Init(windows);
            _tasks.UpdateEvent += UpdateUI;
        }

        private void UpdateUI()
        {
            ReturnUI();
            var task = _tasks.GetTaskForUI();
            _progressImage.fillAmount = task.Progress;
            _progressText.text = task.IsCompleted 
                ? "COMPLETED".ToLocalized() 
                : $"{task.CurrentCount.ToFormattedString()}/{task.NeedCount.ToFormattedString()}";
            
            Blind(task.IsCompleted);
            _taskText.text = task.GetTaskText();
            _lastProgress = task.CurrentCount;
        }

        private void ReturnUI()
        {
            RectTransform.rotation = Quaternion.identity;
        }
    }
}
