﻿using System;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Ui.Tasks
{
    public class TaskListItem : BaseUIView
    {
        public Action ButtonPressed;
        
        [SerializeField] private Image _itemIcon;
        [SerializeField] private Image _roomIcon;
        [SerializeField] private Image _progress;
        
        [SerializeField] private TextMeshProUGUI _taskText;
        [SerializeField] private TextMeshProUGUI _rewardText;
        [SerializeField] private TextMeshProUGUI _progressText;

        [SerializeField] private Color _backColor;
        [SerializeField] private Color _frontColor;
        
        [SerializeField] private BaseButton _claimButton;

        private GameSystem _game;
        private TaskSystem _taskSystem;
        private UIFactory _uiFactory;
        private GameTask _task;
        private GameResources _resources;
        private RoomsFactory _rooms;

        public BaseButton Button => _claimButton;

        public void Init(GameSystem game, UIFactory uiFactory, TaskSystem taskSystem, GameResources resources, RoomsFactory rooms)
        {
            _game = game;
            _uiFactory = uiFactory;
            _taskSystem = taskSystem;
            _resources = resources;
            _rooms = rooms;
            
            _claimButton.SetCallback(OnPressedButton);
        }

        private void OnPressedButton()
        {
            if (_task.IsFinished) return;

            if (_task.IsCompleted == false)
            {
                _taskSystem.FollowTask(_task);
                return;
            }
            
            var reward = _task.Config.Reward;
            _task.Finish();
            _game.AddCurrency(reward.Type, reward.Amount, GetCurrencyPlace.Quest);
            _uiFactory.SpawnResourceBubble(reward.Type, reward.Amount);
            
            Redraw(_task);
            ButtonPressed?.Invoke();
        }

        public void Redraw(GameTask task)
        {
            _task = task;
            
            _claimButton.SetBlind(_task.IsCompleted);
            
            if (_task.IsCompleted)
            {
                _claimButton.SetText("CLAIM".ToLocalized());
                _claimButton.SetInteractable(!_task.IsFinished);
            }
            else if (_task.IsFinished)
            {
                _claimButton.SetText("COMPLETED".ToLocalized());
                _claimButton.SetInteractable(!_task.IsFinished);
            }
            else
            {
                _claimButton.SetText("FOLLOW".ToLocalized());
                _claimButton.SetInteractable(true);
                SetFollowColors();
            }
            
            _taskText.text = _task.GetTaskText();
            _progress.fillAmount = _task.Progress;
            _progressText.text = $"{_task.CurrentCount.ToFormattedString()}/{_task.NeedCount.ToFormattedString()}";

            var type = (RoomType)_task.GetRoomId();
            switch (_task.Type)
            {
                case GameEvents.BuyRoom:
                case GameEvents.UpgradeRoom:
                    _roomIcon.sprite = _resources.GetRoomImagesSprite(type.ToString().ToUpper());
                    _roomIcon.Activate();
                    _itemIcon.Deactivate();
                    break;
                
                case GameEvents.BuyRoomItem:
                case GameEvents.UpgradeRoomItem:
                case GameEvents.ReachItemLevel:
                    var item = _rooms.GetRoomItem(type, _task.GetItemId());
                    _itemIcon.sprite = _resources.GetRoomIconSprite(item.Config.Name);
                    _itemIcon.Activate();
                    _roomIcon.Deactivate();
                    break;
            }

            if (_task.IsFinished)
            {
                _rewardText.text = "COMPLETED".ToLocalized();
                return;
            }

            _rewardText.text = _task.GetRewardIcon();
        }

        private void SetFollowColors()
        {
            _claimButton.SetColors(default, _backColor, _frontColor);
        }
    }
}