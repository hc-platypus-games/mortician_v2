using _Game.Scripts.Core;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class TutorialArrow : BaseUIView
    {
        [Inject] private SceneData _sceneData;
        
        private RectTransform _defaultParent;
        private Transform _worldScapeParent;

        private Vector3 _worldSpaceScale = new(0.3f, 0.3f, 0.3f);
        private Vector3 _worldScapeOffset = new(1f, 1f, 1f);
        
        public void Init()
        {
            _defaultParent = transform.parent.GetComponent<RectTransform>();
            _worldScapeParent = _sceneData.WorldSpaceCanvas;
            Hide();
        }
        
        public void Show(RectTransform element)
        {
            InvokeSystem.CancelInvoke(Hide, true);
            RectTransform.SetParent(_defaultParent);
            RectTransform.localScale = Vector3.one;
            RectTransform.localRotation = Quaternion.identity;
            
            RectTransform.SetParent(element, false);

            var anchoredPosition = new Vector2();
            var scale = element.localScale;
            var rotate = element.rotation;
			
            if (element.TryGetComponent(out CustomTutorialArrowAnchor customTutorialArrowAnchor))
            {
                anchoredPosition = customTutorialArrowAnchor.Anchor;
                scale = customTutorialArrowAnchor.Scale;
                rotate = customTutorialArrowAnchor.Rotation;
            }
		
            RectTransform.anchoredPosition = anchoredPosition;
            RectTransform.localScale = scale;
            RectTransform.rotation = rotate;
            this.Activate();
        }

        public void Show(Vector3 worldPosition)
        {
            InvokeSystem.CancelInvoke(Hide, true);
            RectTransform.SetParent(_worldScapeParent);
            RectTransform.localScale = _worldSpaceScale;
            RectTransform.localRotation = Quaternion.Euler(30f, -135f, 0f);
            RectTransform.position = worldPosition + _worldScapeOffset;
            this.Activate();
        }

        public void ShowCenterOfScreen()
        {
            Show(_defaultParent);
        }

        public void Hide()
        {
            this.Deactivate();
        }
    }
}
