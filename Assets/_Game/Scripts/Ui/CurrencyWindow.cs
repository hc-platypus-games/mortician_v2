using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Buttons;
using _Game.Scripts.Ui.Offers;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class CurrencyWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _softText;
        [SerializeField] private TextMeshProUGUI _hardText;
        [SerializeField] private TextMeshProUGUI _tokensText;
        [SerializeField] private TextMeshProUGUI _researchersText;
        [SerializeField] private TextMeshProUGUI _workersText;
        
        [Inject] private GameParamFactory _params;
        [Inject] private WindowsSystem _windows;
        
        private GameParam _soft;
        private GameParam _hard;
        private GameParam _tokens;
        private GameParam _researchers;
        private GameParam _workers;

        private CurrencyButton[] _currencyButtons;

        public override void Init()
        {
            _currencyButtons = GetComponentsInChildren<CurrencyButton>();
            foreach (var button in _currencyButtons)
            {
                button.Callback = () => _windows.OpenWindow<StoreWindow>();
            }
            base.Init();
        }

        public override void Open(params object[] list)
        {
            _soft = _params.GetParam<GameSystem>(GameParamType.Soft);
            _soft.UpdatedEvent += OnUpdateSoft;
                
            _hard = _params.GetParam<GameSystem>(GameParamType.Hard);
            _hard.UpdatedEvent += OnUpdateHard;
        
            _tokens = _params.GetParam<GameSystem>(GameParamType.Tokens);
            _tokens.UpdatedEvent += OnUpdateTokens;
            
            _researchers = _params.GetParam<GameSystem>(GameParamType.Researchers);
            _researchers.UpdatedEvent += OnUpdateResearchers;
            
            _workers = _params.GetParam<GameSystem>(GameParamType.Workers);
            _workers.UpdatedEvent += OnUpdateWorkers;

            UpdateCurrency();
            
            base.Open(list);
        }

        private void UpdateCurrency()
        {
            OnUpdateSoft();
            OnUpdateHard();
            OnUpdateTokens();
            OnUpdateResearchers();
            OnUpdateWorkers();
        }

        private void OnUpdateSoft()
        {
            _softText.text = _soft.Value.ToFormattedString();
        }

        private void OnUpdateHard()
        {
            _hardText.text = _hard.Value.ToFormattedString();
        }
        
        private void OnUpdateTokens()
        {
            _tokensText.text = _tokens.Value.ToFormattedString();
        }
        
        private void OnUpdateResearchers()
        {
            _researchersText.text = $"{_researchers.Value}/{1}";
        }
        
        private void OnUpdateWorkers()
        {
            _workersText.text = $"{_workers.Value}/{1}";
        }

        public override void Close()
        {
            _soft.UpdatedEvent -= OnUpdateSoft;
            _hard.UpdatedEvent -= OnUpdateHard;
            _tokens.UpdatedEvent -= OnUpdateTokens;
            _researchers.UpdatedEvent -= OnUpdateResearchers;
            _workers.UpdatedEvent -= OnUpdateWorkers;
            
            base.Close();
        }
    }
}