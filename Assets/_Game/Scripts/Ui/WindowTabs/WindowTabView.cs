using _Game.Scripts.Ui.Base;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Ui.WindowTabs
{
    public class WindowTabView : BaseButton
    {
        [SerializeField] private Color _activeColor;
        [SerializeField] private Color _defaultColor;
        [SerializeField] private GameObject _container;
        
        private BaseWindow _window;

        public void Init()
        {
            Callback += OnPressed;
            _window = GetComponentInParent<BaseWindow>(true);
            _image = GetComponent<Image>();
        }

        private void OnPressed()
        {
            _window.SelectTab(this);
        }

        public void Select(bool value)
        {
            _image.color = value ? _activeColor : _defaultColor;
            _container.SetActive(value);
        }
    }
}
