using System;
using System.Collections.Generic;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Offers;
using Coffee.UIExtensions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Cards
{
    public class CaseOpeningWindow : BaseWindow
    {
        private enum OpeningState
        {
            None,
            Started,
            Idle,
            Animating,
            End
        }

        [SerializeField] private GameObject _windowBase;
        [SerializeField] private TextMeshProUGUI _rewardCounterText;
        [SerializeField] private TextMeshProUGUI _rewardText;
        [SerializeField] private UIParticle _explosionParticle;
        [SerializeField] private Animator _cardAnimator;
        [SerializeField] private Image _cardImage;
        [SerializeField] private GameObject _cardInfo;
        [SerializeField] private TextMeshProUGUI _cardInfoTitleText;
        [SerializeField] private TextMeshProUGUI _cardInfoDescText;
        
        [SerializeField] private GameObject[] _particles;
        [SerializeField] private BaseButton _tapButton;

        [SerializeField] private Color _commonColor;
        [SerializeField] private Color _epicColor;
        [SerializeField] private Color _legendaryColor;
        
        [Inject] private WindowsSystem _windows;
        [Inject] private CardsFactory _cards;
        [Inject] private GameResources _resources;
        [Inject] private AppEventProvider _eventProvider;
        
        private OpeningState _openingState;
        private List<CardsWindow.RewardConfig> _rewards;
        private int _currentReward;

        private int _animationClip = Animator.StringToHash("CaseOpen");

        public override void Init()
        {
            base.Init();
            _tapButton.SetCallback(GoNext);
        }

        public void SetRewardsList(List<CardsWindow.RewardConfig> rewards, int boxId)
        {
            _currentReward = 0;
            _rewards = rewards;
 
            _openingState = OpeningState.None;
            GoNext();
        }

        private void GoNext()
        {
            InvokeSystem.CancelInvoke(GoNext);
            
            if (_openingState == OpeningState.End)
            {
                Close();
                //_windows.GetWindow<StoreWindow>().Close();
                _eventProvider.TriggerEvent(AppEventType.Tutorial, GameEvents.BoxOpened);
                return;
            }

            switch (_openingState)
            {
                case OpeningState.None:
                    DrawReward(_rewards[0]);
                    for (var i = 0; i < _particles.Length; i++)
                    {
                        _particles[i].Deactivate();
                    }
                    _cardInfo.Deactivate();
                    _cardAnimator.Play(_animationClip,0, 0f);
                    _cardAnimator.speed = 0f;
                    _tapButton.SetText("TAP_TO_CONTINUE".ToLocalized());
                    InvokeSystem.StartInvoke(GoNext, 1f);
                    _openingState = OpeningState.Started;
                    _windowBase.Deactivate();
                    break;
                case OpeningState.Idle:
                    _cardAnimator.Play(_animationClip,0, 1f);
                    _cardAnimator.speed = 0f;
                    _tapButton.SetText("TAP_TO_CONTINUE".ToLocalized());
                    _openingState = _currentReward >= _rewards.Count ? OpeningState.End : OpeningState.Animating;
                    break;
                case OpeningState.Started:
                case OpeningState.Animating:
                    _windowBase.Activate();
                    var reward = _rewards[_currentReward];
                    _cardAnimator.Play(_animationClip,0, 0f);
                    DrawReward(reward);
                    _explosionParticle.Play();
                    _cardAnimator.speed = 1f;
                    _tapButton.SetText("");
                    _openingState = _currentReward >= _rewards.Count ? OpeningState.End : OpeningState.Idle;
                    InvokeSystem.StartInvoke(GoNext, 2f);
                    _currentReward++;
                    break;
            }
        }

        private void DrawReward(CardsWindow.RewardConfig reward)
        {
            _rewardCounterText.text = $"{_currentReward + 1}/{_rewards.Count}";
            switch (reward.Type)
            {
                case GameParamType.Soft:
                    _cardImage.sprite = _resources.GetSprite("cash-1");
                    _rewardText.text = $"<sprite name=Soft>{reward.Amount.ToFormattedRoundedString()}";
                    _cardInfo.Deactivate();
                    for (var i = 0; i < _particles.Length; i++)
                    {
                        _particles[i].Deactivate();
                    }
                    _cardAnimator.Play(_animationClip,0, 1f);
                    break;
                
                case GameParamType.Tokens:
                    _cardImage.sprite = _resources.GetSprite("Tokens");
                    _rewardText.text = $"<sprite name=Tokens>{reward.Amount}";
                    _cardInfo.Deactivate();
                    for (var i = 0; i < _particles.Length; i++)
                    {
                        _particles[i].Deactivate();
                    }
                    _cardAnimator.Play(_animationClip,0, 1f);
                    break;
                
                case GameParamType.Card:
                    var card = _cards.GetCard(reward.RewardId);
                    _cardImage.sprite = _resources.GetAvatarSprite(card.Config.Name.ToUpper());
                    _rewardText.text = card.Config.Name.ToLocalized();
                    _cardInfo.Activate();
                    _cardInfoTitleText.text = card.Config.Quality.ToString().ToUpper().ToLocalized();

                    switch (card.Config.Quality)
                    {
                        case CardQuality.Common:
                            _cardInfoTitleText.color = _commonColor;
                            break;
                        case CardQuality.Epic:
                            _cardInfoTitleText.color = _epicColor;
                            break;
                        case CardQuality.Legendary:
                            _cardInfoTitleText.color = _legendaryColor;
                            break;
                    }
                    
                    _cardInfoDescText.text = card.GetCardDescText();
                    for (var i = 0; i < _particles.Length; i++)
                    {
                        _particles[i].SetActive(i == (int)card.Config.Quality);
                    }
                    break;
            }
        }
    }
}
