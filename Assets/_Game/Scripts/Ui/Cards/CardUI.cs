﻿using System;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Cards
{
    public class CardUI : BaseUIView
    {
        public Action<CardUI> UpdateEvent;

        [SerializeField] private BaseButton _button;
        
        [SerializeField] private Image _slider;
        [SerializeField] private Image _avatar;
        [SerializeField] private Image _emptyAvatar;
        [SerializeField] private Image _back;
        [SerializeField] private Image _levelBack;
        [SerializeField] private Image _badge;
        [SerializeField] private Image _unlockButton;
		
        [SerializeField] private Color _commonColor;
        [SerializeField] private Color _epicColor;
        [SerializeField] private Color _legendaryColor;

        [SerializeField] private TextMeshProUGUI _nameText;
        [SerializeField] private TextMeshProUGUI _levelText;
        [SerializeField] private TextMeshProUGUI _progressText;
        [SerializeField] private TextMeshProUGUI _bonusText;
        [SerializeField] private string _boostTextColor;
        
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private WindowsSystem _windows;
        [Inject] private GameSystem _game;
        [Inject] private LevelSystem _levelSystem;
        [Inject] private GameResources _resources;
        [Inject] private CharactersFactory _charactersFactory;
        [Inject] private BoostSystem _boosts;
        [Inject] private UIFactory _uiFactory;
        
        public CardConfig Config { get; private set; }

        public bool IsExists => Level > 0;
        public int Id { get; private set; }
        public int Amount { get; private set; }
        public int Level { get; private set; }
        public bool Assigned { get; private set; }

        public class Factory : PlaceholderFactory<CardUI>
        {
        }

        public void Init(CardConfig config)
        {
            Config = config;
            Id = config.Id;
            
            _button.SetCallback(OnPressedAvatar);
            _nameText.text = Config.Name.ToUpper().ToLocalized();
        }

        private void OnPressedAvatar()
        {
            if (Level == 0) return;
            _windows.OpenWindow<CardInfoWindow>(this, false);
        }

        private string GetCardBonusText()
        {
            switch (GetCardBoostParam())
            {
                case GameParamType.Income:
                    return $"x{GetCardBonusValue()}<sprite name=Soft>";
                
                case GameParamType.CycleDelay:
                    return $"-{GetCardBonusValue()}<sprite name=CycleDelay>";
                
                default:
                    return "";
            }
        }

        public GameParamType GetCardBoostParam()
        {
            return _boosts.GetCardBoostParam(Config.RoomType);
        }

        public string GetCardDescText()
        {
            switch (GetCardBoostParam())
            {
                case GameParamType.Income:
                    return $"{"INCREASE_INCOME_BY".ToLocalized()}: " +
                           $"<color=#{_boostTextColor}>" +
                           $"X{GetCardBonusValue()}<sprite name=Soft>";
                
                case GameParamType.CycleDelay:
                    return $"{"SPEED_UP_ON".ToLocalized()}: " +
                           $"<color=#{_boostTextColor}>" +
                           $"X{GetCardBonusValue()}<sprite name=CycleDelay>";
                
                default:
                    return "";
            }
        }
        
        public float GetCardBonusValue(bool nextLevel = false)
        {
            var value = _boosts.GetCardBoostValue(Config.RoomType);
            var level = nextLevel ? Level + 1 : Level;
            return value * level;
        }

        public void IncreaseAmount(int amount)
        {
            if (Level == 0)
            {
                Level++;
            }
            Amount += amount;
            
            _eventProvider.TriggerEvent(AppEventType.Analytics,
                GameEvents.OpenCharacter,
                _levelSystem.CurrentLevel.Id,
                _levelSystem.CurrentLevel.LevelName,
                Config.Id,
                $"{Config.Name}",
                amount,
                Amount, 
                "case",
                _levelSystem.CurrentLevel.Id == 1 ? "main" : "LTE");
            
            _uiFactory.UpdateBadges(BadgeNotificationType.Cards);
        }

        public void AssignCharacter()
        {
            Assigned = true;
            
            _charactersFactory.SpawnCharacter(Config);
            _boosts.RunCardBoost(Config.RoomType);
                
            UpdateEvent?.Invoke(this);
        }

        public void Upgrade(bool forHard = false)
        {
            Amount -= GetAmountUpgrade();
            if (Amount < 0) Amount = 0;
            
            Level++;

            UpdateEvent?.Invoke(this);

            _badge.SetActive(CouldBeUpgraded());
            _eventProvider.TriggerEvent(AppEventType.Analytics, 
                GameEvents.UpgradeCard, 
                Config.Quality, 
                forHard, 
                Config.Name, 
                Level);
            
            _game.SpendCurrency(GameParamType.Tokens, GetPriceUpgrade(Level - 1), SpendCurrencyPlace.UpgradeCharacter, SpendCurrencyItem.Character);
        }
    
        public void Redraw()
        {
            RectTransform.localScale = Vector3.one;

            if (Level == 0)
            {
                _badge.Deactivate();
                _avatar.Deactivate();
                _emptyAvatar.Activate();
                _nameText.transform.parent.Deactivate();
                _levelText.transform.parent.Deactivate();
                _bonusText.transform.parent.Deactivate();
                _unlockButton.Activate();
                return;
            }
            
            _badge.SetActive(CouldBeUpgraded());
            
            _nameText.transform.parent.Activate();
            _levelText.transform.parent.Activate();
            _bonusText.transform.parent.Activate();
            
            _levelText.text = $"{"LVL".ToLocalized()}:{Level}";
            var needAmount = GetAmountUpgrade();
            _progressText.text = $"{Amount}/{needAmount}";
            _slider.fillAmount = (float)Amount / needAmount;
            _bonusText.text = GetCardBonusText();
            
            _avatar.Activate();
            _emptyAvatar.Deactivate();
            _avatar.sprite = _resources.GetAvatarSprite(Config.Name.ToUpper());
            _avatar.material = null;
            
            _unlockButton.Deactivate();
                
            switch (Config.Quality)
            {
                case CardQuality.Common:
                    _back.color = _commonColor;
                    _levelBack.color = _commonColor;
                    break;
                case CardQuality.Epic:
                    _back.color = _epicColor;
                    _levelBack.color = _epicColor;
                    break;
                case CardQuality.Legendary:
                    _back.color = _legendaryColor;
                    _levelBack.color = _legendaryColor;
                    break;
            }
        }
        
        public bool CouldBeUpgraded()
        {
            var needAmount = GetAmountUpgrade();
            return Amount >= needAmount 
                   && needAmount > 0 
                   && _game.IsEnoughCurrency(GameParamType.Tokens, GetPriceUpgrade());
        }

        public void SetCardParams(int amount, int level, bool assigned)
        {
            Amount = amount;
            Level = level;
            Assigned = assigned;
            if (Assigned)
            {
                _charactersFactory.SpawnCharacter(Config);
            }
        }

        public int GetPriceUpgrade(int level = default)
        {
            if (level == default) level = Level;
            switch (Level)
            {
                case 0:
                    return 0;
                
                case 1:
                    return _balance.DefaultBalance.CardTokensUpgradePrice;
                
                default:
                    return _balance.DefaultBalance.CardTokensUpgradePrice * level;
            }
        }
        
        public int GetAmountUpgrade()
        {
            switch (Level)
            {
                case 0:
                    return 1;
                
                case 1:
                    return 2;
                
                case 2:
                    return 5;
                
                default:
                    return 5 * (Level - 1);
            }
        }
    }
}