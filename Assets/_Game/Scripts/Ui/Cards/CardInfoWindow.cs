using System;
using System.Globalization;
using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using Coffee.UIExtensions;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui.Cards
{
    public class CardInfoWindow : BaseWindow
    {
        public Action<CardUI> Assigned;
        
        [SerializeField] private TextMeshProUGUI _nameText;
        [SerializeField] private TextMeshProUGUI _quality;
        [SerializeField] private TextMeshProUGUI _bonusText;
        [SerializeField] private TextMeshProUGUI _levelText;
        [SerializeField] private TextMeshProUGUI _progress;
        [SerializeField] private TextMeshProUGUI _roomName;
        
        [SerializeField] private BaseButton _upgradeButton;
        [SerializeField] private BaseButton _assignButton;
        [SerializeField] private BaseButton _leftArrow;
        [SerializeField] private BaseButton _rightArrow;
        
        [SerializeField] private Image _slider;
        [SerializeField] private Image _roomImage;
        [SerializeField] private Image _avatar;
        [SerializeField] private Image _levelBack;
        
        [SerializeField] private Color _commonColor;
        [SerializeField] private Color _epicColor;
        [SerializeField] private Color _legendaryColor;
        [SerializeField] private string _boostTextColor;
        [SerializeField] private UIParticle _levelUpParticle;
        
        [Inject] private GameResources _resources;
        [Inject] private GameSystem _game;

        private CardUI _card;
        
        public override void Init()
        {
            _upgradeButton.SetCallback(OnPressedUpgradeButton);
            _assignButton.SetCallback(OnPressedAssignButton);
            base.Init();
        }

        private void OnPressedUpgradeButton()
        {
            _card.Upgrade();
            _levelUpParticle.Play();
            _bonusText.rectTransform.DOShakeScale(0.5f);
            _avatar.rectTransform.DOShakeScale(0.5f);
            Redraw();
        }

        private void OnPressedAssignButton()
        {
            _card.AssignCharacter();
            Assigned?.Invoke(_card);
            Close();
        }

        public override void Open(params object[] list)
        {
            if (list.Length < 2) return;
            _card = (CardUI) list[0];
            var assignMode = (bool) list[1];

            _upgradeButton.SetActive(!assignMode || _card.Assigned);
            _assignButton.SetActive(assignMode && !_card.Assigned);
            
            _nameText.text = _card.Config.Name.ToUpper().ToLocalized();
            _avatar.sprite = _resources.GetAvatarSprite(_card.Config.Name.ToUpper());
            _roomImage.sprite = _resources.GetRoomImagesSprite(_card.Config.RoomType.ToString());
            _quality.text = _card.Config.Quality.ToString().ToUpper().ToLocalized();
            _roomName.text = _card.Config.RoomType.ToString().ToUpper().ToLocalized();
            
            switch (_card.Config.Quality)
            {
                case CardQuality.Common:
                    _levelBack.color = _commonColor;
                    break;
                case CardQuality.Epic:
                    _levelBack.color = _epicColor;
                    break;
                case CardQuality.Legendary:
                    _levelBack.color = _legendaryColor;
                    break;
            }

            Redraw();
            
            base.Open(list);
        }

        private void Redraw()
        {
            _levelText.text = $"{"LVL".ToLocalized()}:{_card.Level}";
            _bonusText.text = GetCardBonusText();
            
            var needCard = _card.GetAmountUpgrade();
            _slider.fillAmount = (float)_card.Amount / needCard;
            _progress.text = $"{_card.Amount}/{needCard}";
            
            _upgradeButton.SetText(1, $"{_card.GetPriceUpgrade().ToString(CultureInfo.InvariantCulture)}<sprite name=Tokens>");
            _upgradeButton.SetInteractable(_card.CouldBeUpgraded());
        }

        public override BaseButton GetTutorialButton()
        {
            return _assignButton;
        }

        public override void UpdateLocalization()
        {
            _upgradeButton.SetText("LEVEL_UP".ToLocalized());
            _assignButton.SetText("ASSIGN".ToLocalized());
            _assignButton.SetText(1, "CHARACTER".ToLocalized());
            base.UpdateLocalization();
        }
        
        private string GetCardBonusText()
        {
            switch (_card.GetCardBoostParam())
            {
                case GameParamType.Income:
                    return $"{"INCOME".ToLocalized()}: x{_card.GetCardBonusValue()}<sprite name=Soft>-" +
                           $"<color=#{_boostTextColor}> x{_card.GetCardBonusValue(true)}<sprite name=Soft></color>";
                
                case GameParamType.CycleDelay:
                    return $"{"CYCLE_DELAY".ToLocalized()}: {_card.GetCardBonusValue().ToTimeStr()}<sprite name=CycleDelay>-" +
                           $"<color=#{_boostTextColor}> x{_card.GetCardBonusValue(true)}<sprite name=CycleDelay></color>";
                
                default:
                    return "";
            }
        }
    }
}