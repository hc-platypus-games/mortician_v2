using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.GamePlayElements;
using _Game.Scripts.Tools;
using _Game.Scripts.View.Character;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class CurrentEmotionButton : MainWindowButton
    {
        [Inject] private CharactersFactory _charactersFactory;
        [Inject] private GameFlags _flags;
        [Inject] private GameCamera _camera;
        [Inject] private UIFactory _uiFactory;
        
        private bool _hasWaitingCharacters;
        private Animation _animation;

        public CurrentEmotionButton()
        {
            SetType(GamePlayElement.CurrentEmotionButton);
        }

        public override void Init(WindowsSystem windows)
        {
            base.Init(windows);
            Button.Callback += OnPressed;
            CharacterViewBase.EmotionChanged += UpdateEmotion;
            gameObject.Deactivate();
            _animation = GetComponent<Animation>();
        }

        private void UpdateEmotion()
        {
            var angryCharacters = _charactersFactory.GetAngryCharactersCount();
            _hasWaitingCharacters = angryCharacters > 0;
            if (_hasWaitingCharacters)
            {
                if(!_flags.Has(GameFlag.TutorialFinished) || _flags.Has(GameFlag.TutorialRunning)) return;
                this.Activate();
                _animation.Play();
                _uiFactory.UpdateBadges(BadgeNotificationType.AngryCharacters);
            }
            else
            {
                this.Deactivate();
            }
        }

        private void OnPressed()
        {
            _animation.Stop();
            _animation.Play();
            UpdateEmotion();
            if(!_hasWaitingCharacters) return;
            var angryCharacter = _charactersFactory.GetAngryCharacter();
            if(angryCharacter == null) return;
            _camera.Focus(angryCharacter.transform.position);
            _camera.SetMinScale();
            if(!_flags.Has(GameFlag.TutorialFinished) || _flags.Has(GameFlag.TutorialRunning)) this.Deactivate();
        }
    }
}
