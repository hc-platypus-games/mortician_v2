using _Game.Scripts.Ui.Base;
using UnityEngine;

namespace _Game.Scripts.Ui
{
    public class MainWindow : BaseWindow
    {
        [SerializeField] private Transform _adOffersContainer;

        public Transform AdOffersContainer => _adOffersContainer;
        
        public override void Init()
        {
            base.Init();
        }
    }
}