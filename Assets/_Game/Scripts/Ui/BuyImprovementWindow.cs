﻿using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class BuyImprovementWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _desc;
        [SerializeField] private TextMeshProUGUI _completeText;
        [SerializeField] private Image _icon;

        [SerializeField] private BaseButton _improveButton;
        [SerializeField] private BaseButton _finishButton;
        [SerializeField] private GameObject _complete;
        
        public override void Init()
        {
            _improveButton.SetCallback(OnPressedImproveButton);
            _finishButton.SetCallback(OnPressedFinish);
            
            base.Init();
        }

        public override void Open(params object[] list)
        {
            if (list.Length == 0) return;
            
            _title.text = list[0].ToString().ToLocalized();
            _desc.text = list[1].ToString().ToLocalized();
            _icon.sprite = (Sprite)list[2];

            var completed = (bool)list[3];
            if (completed)
            {
                _improveButton.Deactivate();
                _finishButton.Deactivate();
                _complete.Activate();
            }
            else
            {
                _complete.Deactivate();
                
                var gameProgress = (GameProgress)list[4];
                if (gameProgress.IsActive)
                {
                    _improveButton.Deactivate();
                    _finishButton.Activate();
                    
                    _finishButton.SetText(1, $"<sprite name=Hard>{(float)list[5]}");
                    _finishButton.SetText(2, gameProgress.LeftValue.ToTimeStr(3,false,true));
                }
                else
                {
                    _improveButton.Activate();
                    _finishButton.Deactivate();
                    
                    _improveButton.SetText(1, $"<sprite name=Soft>{(float)list[5]}");
                    _improveButton.SetText(2, gameProgress.LeftValue.ToTimeStr(3,false,true));
                }
            }

            base.Open(list);
        }

        public override void UpdateLocalization()
        {
            _completeText.text = "COMPLETED".ToLocalized();
            _improveButton.SetText("IMPROVE".ToLocalized());
            _finishButton.SetText("FINISH".ToLocalized());
            base.UpdateLocalization();
        }
        
        
        private void OnPressedImproveButton()
        {
            
        }

        private void OnPressedFinish()
        {
            
        }

    }
}