using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class RateGameWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private BaseButton _rateButton;

        [Inject] private ProjectSettings _settings;
        [Inject] private GameFlags _flags;
        
        public override void Init()
        {
            _rateButton.SetCallback(OnPressedButton);
            base.Init();
        }

        private void OnPressedButton()
        {
#if UNITY_ANDROID
            Application.OpenURL(_settings.GooglePlayLink);
#elif UNITY_IPHONE || UNITY_IOS
            Application.OpenURL(_settings.AppStoreLink);
#endif
            
            _flags.Set(GameFlag.RateGameCompleted);
            Close();
        }

        public override void Open(params object[] list)
        {
            _flags.Set(GameFlag.RateGameShowed);
            base.Open(list);
        }

        public override void UpdateLocalization()
        {
            _rateButton.SetText("RATE_GAME".ToLocalized());
            _title.text = "RATE_GAME_TITLE".ToLocalized();
            _text.text = "RATE_GAME_TEXT".ToLocalized();
            base.UpdateLocalization();
        }
    }
}