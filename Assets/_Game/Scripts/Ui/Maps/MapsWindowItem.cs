﻿using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;

namespace _Game.Scripts.Ui.Maps
{
    public class MapsWindowItem : BaseUIView
    {
        [SerializeField] private int _id;
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _desc;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private TextMeshProUGUI _value;
        [SerializeField] private TextMeshProUGUI _youHereText;
        [SerializeField] private GameObject _youHereContainer;

        public int Id => _id;
        
        public void Init()
        {
        }

        public void Redraw(bool currentMap, int corpses)
        {
            _value.text = corpses.ToString();
            
            if (currentMap)
            {
                _text.text = "BURIED_CORPSES".ToLocalized();
                _youHereContainer.Activate();
            }
            else
            {
                _text.text = "NEED_TO_BURY_BODIES".ToLocalized();
                _youHereContainer.Deactivate();
            }
        }
        
        public void UpdateLocalization()
        {
            _title.text = $"MAP{_id}".ToLocalized();
            _desc.text = $"MAP{_id}_DESC".ToLocalized();
            _youHereText.text = "YOU_HERE".ToLocalized();
        }
    }
}