﻿using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui.Maps
{
    public class MapsWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;

        [Inject] private GameSystem _game;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private GameParamFactory _params;
        
        private MapsWindowItem[] _maps;
        private GameParam _corpses;

        public override void Init()
        {
            _corpses = _params.GetParam<GameSystem>(GameParamType.CorpsesProgress);
            _maps = GetComponentsInChildren<MapsWindowItem>();
            
            foreach (var map in _maps)
            {
                map.Init();
            }
            base.Init();
        }

        public override void UpdateLocalization()
        {
            _title.text = "MAPS".ToLocalized();
            foreach (var map in _maps)
            {
                map.UpdateLocalization();
            }
            
            base.UpdateLocalization();
        }

        public override void Open(params object[] list)
        {
            foreach (var map in _maps)
            {
                var configValue = _balance.DefaultBalance.MapConfigs.FirstOrDefault(m => m.Id == map.Id)?.Corpse ?? 0;
                var corpses = _game.MapId == map.Id ? _corpses.Value : configValue;
                map.Redraw(_game.MapId == map.Id, (int) corpses);
            }
            base.Open(list);
        }

        public override void Close()
        {
            base.Close();
        }
    }
}