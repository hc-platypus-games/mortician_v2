using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class CheatsWindow : BaseWindow
    {
        [SerializeField] private BaseButton _addSoft;
        [SerializeField] private BaseButton _addHard;
        [SerializeField] private BaseButton _addTokens;
        [SerializeField] private BaseButton _upgradeMax;
        [SerializeField] private BaseButton _hideHUD;
        [SerializeField] private BaseButton _showHUD;
        [SerializeField] private BaseButton _removePurchases;

        [Inject] private CheatsSystem _cheats;
        [Inject] private WindowsSystem _windows;
        
        public override void Init()
        {
            _addSoft.SetCallback(_cheats.AddSoft);
            _addHard.SetCallback(_cheats.AddHard);
            _addTokens.SetCallback(_cheats.AddTokens);
            //_upgradeMax.SetCallback(Cheats.MaximumUpgradeAllItems);
            _removePurchases.SetCallback(_cheats.RemoveAllPurchases);
            _hideHUD.SetCallback(HideUI);
            _showHUD.SetCallback(ShowUI);
            
            base.Init();
        }

        private void HideUI()
        {
            _windows.GetWindow<MainWindow>().Deactivate();
        }

        private void ShowUI()
        {
            _windows.GetWindow<MainWindow>().Activate();
        }
    }
}