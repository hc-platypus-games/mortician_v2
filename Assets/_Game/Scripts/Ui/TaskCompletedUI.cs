using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class TaskCompletedUI : BaseUIView
    {
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private Image _itemIcon;
        [SerializeField] private Image _roomIcon;

        [Inject] private TaskSystem _tasks;
        [Inject] private GameResources _resources;
        [Inject] private RoomsFactory _rooms;
        [Inject] private GameSystem _game;
        
        private Animation _anim;
        
        [Inject]
        public void Construct()
        {
            _anim = GetComponentInChildren<Animation>();
            this.Activate();
        }

        public void Init()
        {
            _tasks.TaskCompleted += Show;
        }

        private void Show(GameTask task)
        {
            if (_game.GamePaused) return;
            
            _anim.Play();
            _text.text = "TASK_COMPLETED".ToLocalized();
            var type = (RoomType)task.GetRoomId();
            switch (task.Type)
            {
                case GameEvents.BuyRoom:
                case GameEvents.UpgradeRoom:
                    _roomIcon.sprite = _resources.GetRoomImagesSprite(type.ToString().ToUpper());
                    _roomIcon.Activate();
                    _itemIcon.Deactivate();
                    break;
                
                case GameEvents.BuyRoomItem:
                case GameEvents.UpgradeRoomItem:
                case GameEvents.ReachItemLevel:
                    var item = _rooms.GetRoomItem(type, task.GetItemId());
                    _itemIcon.sprite = _resources.GetRoomIconSprite(item.Config.Name);
                    _itemIcon.Activate();
                    _roomIcon.Deactivate();
                    break;
            }
        }
    }
}