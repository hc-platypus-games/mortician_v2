using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Buttons;
using _Game.Scripts.Ui.Room;
using _Game.Scripts.View.Room;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class NewBuildingWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _titleText;
        [SerializeField] private TextMeshProUGUI _descriptionText;
        [SerializeField] private TextMeshProUGUI _buildingTimeText;
        [SerializeField] private TextMeshProUGUI _buildingTimerLeftText;
        [SerializeField] private TextMeshProUGUI _soonText;
        [SerializeField] private Image _image;
        [SerializeField] private ProgressButton _button;

        [Inject] private GameResources _resources;
        [Inject] private GameProgressFactory _progresses;
        [Inject] private GameSystem _game;
        [Inject] private WindowsSystem _windows;
        [Inject] private HardSystem _hard;
        [Inject] private UIFactory _uiFactory;
        [Inject] private GameParamFactory _params;
        
        private RoomView _room;
        
        private GameProgress _buildingProgress;

        public override void Init()
        {
            base.Init();
            _button.SetCallback(OnPressedButton);
        }

        public override void Open(params object[] list)
        {
            if (list.Length == 0)
            {
                Close();
                return;
            }
            
            _room = (RoomView)list[0];
            if(_room != null) RedrawRoomInfo();

            var soft = _params.GetParam<GameSystem>(GameParamType.Soft);
            soft.UpdatedEvent += RedrawBuyButton;
            
            _titleText.text = _room.Type.ToString().ToUpper().ToLocalized();
            _descriptionText.text = $"{_room.Type.ToString().ToUpper()}_DESC".ToLocalized();
            _buildingTimeText.text = "BUILDING_TIME".ToLocalized();
            _image.sprite = _resources.GetRoomImagesSprite(_room.Type.ToString());
            
            base.Open(list);
        }

        public override void UpdateLocalization()
        {
            _soonText.text = "ROOM_WILL_BE_AVAILABLE_SOON".ToLocalized();
            base.UpdateLocalization();
        }

        private void RedrawRoomInfo()
        {
            _buildingProgress = _progresses.GetProgress(_room, GameParamType.BuildingTimer);
            
            var timeToBuild = _room.Config.TimerLevel1.ToTimeStr(3, false, true);
            _buildingTimerLeftText.text = timeToBuild;
            if (_buildingProgress != null)
            {
                _buildingProgress.UpdatedEvent += OnUpdateTimer;
                _buildingProgress.CompletedEvent += OnCompleteTimer;
                _button.SetText(1, $"<sprite name=Hard>{_hard.GetPriceForTime(_buildingProgress.LeftValue)}");
                
                var timeLeft = _buildingProgress.LeftValue.ToTimeStr(3, true, true);
                _button.SetText("SPEEDUP".ToLocalized());
                _button.SetText(2, timeLeft);
                _button.SetHardColor();
            }
            else
            { 
                _button.SetText("buy".ToLocalized());
                _button.SetText(1, _room.Config.PriceLevel1 == 0 ? "FREE".ToLocalized() : $"<sprite name=Soft>{_room.Config.PriceLevel1.ToFormattedString()}");
                _button.SetText(2, timeToBuild);
            }
            RedrawBuyButton();
        }

        private void RedrawBuyButton()
        {
            if (_room.Config.PriceLevel1 > 0)
            {
                if (!_button.isActiveAndEnabled) _button.Activate();
                _button.SetInteractable(_game.IsEnoughCurrency(GameParamType.Soft, _room.Config.PriceLevel1));
                _soonText.Deactivate();
            }
            else
            {
                _button.Deactivate();
                _soonText.Activate();   
            }
        }

        private void OnPressedButton()
        {
            if (_buildingProgress != null)
            {
                var price = _hard.GetPriceForTime(_buildingProgress.LeftValue);
                if (_hard.IsEnough(price))
                {
                    _buildingProgress.Change(_buildingProgress.LeftValue);
                    _hard.SpendHard(price, SpendCurrencyPlace.BuySkipTime, SpendCurrencyItem.Room);
                }
                else
                {
                    _uiFactory.SpawnMessage("HARD_NOT_ENOUGH".ToLocalized());
                }
                return;
            }
            
            if (_room != null)
            {
                _room.StartUpgrade();
                RedrawRoomInfo();
            }
        }
        
        private void OnUpdateTimer()
        {
            if (_buildingProgress != null)
            {
                var price = _hard.GetPriceForTime(_buildingProgress.LeftValue);
                if (_hard.IsEnough(price))
                {
                    _button.SetInteractable(true);
                    _button.SetHardColor();
                }
                else
                {
                    _button.SetInteractable(false);
                }
                _button.SetText(2, _buildingProgress.LeftValue.ToTimeStr(3, true, true));
                _button.SetText(1, $"<sprite name=Hard>{price}");
            }
        }

        private void OnCompleteTimer()
        {
            if (_room != null)
            {
                Close();
            }
        }

        public override BaseButton GetTutorialButton()
        {
            return _button;
        }

        public override void Close()
        {
            Clear();
            base.Close();
        }

        private void Clear()
        {
            var soft = _params.GetParam<GameSystem>(GameParamType.Soft);
            soft.UpdatedEvent -= RedrawBuyButton;
            
            _room = null;
            if (_buildingProgress != null)
            {
                _buildingProgress.UpdatedEvent -= OnUpdateTimer;
                _buildingProgress.CompletedEvent -= OnCompleteTimer;
                _buildingProgress = null;   
            }
        }
    }
}
