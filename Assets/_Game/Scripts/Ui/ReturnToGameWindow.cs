﻿using System;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Ads;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Save;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Offers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Game.Scripts.Ui
{
    public class ReturnToGameWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _text1;
        [SerializeField] private TextMeshProUGUI _text2;
        [SerializeField] private TextMeshProUGUI _text3;
        [SerializeField] private TextMeshProUGUI _maxIdleTimeText;
        [SerializeField] private TextMeshProUGUI _incomeText;
        [SerializeField] private TextMeshProUGUI _maxTimeText;
        [SerializeField] private TextMeshProUGUI _sliderText;
        
        [SerializeField] private Image _slider;
        
        [SerializeField] private AdWatchButton _adButton;
        [SerializeField] private BaseButton _hardButton;
        [SerializeField] private BaseButton _continueButton;

        [SerializeField] private BaseOffer _offer;

        [Inject] private GameFlags _flags;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private AdSystem _ad;
        [Inject] private SaveSystem _save;
        [Inject] private GameSystem _game;
        [Inject] private GameParamFactory _params;
        [Inject] private ProjectSettings _settings;
        [Inject] private UIFactory _uiFactory;
        [Inject] private CalculationComponent _calculation;

        private int _hardPrice;

        public override void Init()
        {
            _continueButton.SetCallback(OnPressedClose);
            _hardButton.SetCallback(OnPressedHard);
            _adButton.SetCallback(OnPressedAd);
            _offer.Init(_balance.DefaultBalance.Offers.FirstOrDefault(o=>o.Id == 16));

            _hardPrice = _balance.DefaultBalance.ReturnWindowHardPrice;
            
            base.Init();
        }

        private void OnPressedClose()
        {
            _uiFactory.SpawnResourceBubble(GameParamType.Soft, _calculation.OfflineValue, _continueButton.RectTransform.position);
            Close();
        }

        public override void UpdateLocalization()
        {
            _title.text = "RETURN_TO_GAME_WELCOME_BACK".ToLocalized();
            _text1.text = "RETURN_TO_GAME_TEXT1".ToLocalized();
            _text2.text = "RETURN_TO_GAME_TEXT2".ToLocalized();
            _text3.text = "RETURN_TO_GAME_OFFLINE_TIME".ToLocalized();
            
            _continueButton.SetText("CONTINUE".ToLocalized());
            _offer.UpdateLocalization();
            
            base.UpdateLocalization();
        }

        public override void Open(params object[] list)
        {
            if (!_flags.Has(GameFlag.TutorialFinished) || _flags.Has(GameFlag.TutorialRunning))
            {
                Close();
                return;
            }
            if (!_save.IsSave)
            {
                Close();
                return;
            }
            
            var offlineTime = (float)(DateTime.Now - _save.SaveTime).TotalSeconds;
            var maxTime = _params.GetParamValue<GameSystem>(GameParamType.MaxOfflineTime);
              
            if (offlineTime == 0 || offlineTime <= _settings.ReturnWindowOfflineTime)
            {
                Close();
                return;
            }

            var value = _params.GetParamValue<CalculationComponent>(GameParamType.OfflineValue);
            if (value == 0)
            {
                Close();
                return;
            }
            
            base.Open(list);
            
            _slider.fillAmount = offlineTime / maxTime;
            if (offlineTime > maxTime) offlineTime = maxTime;
            _incomeText.text = $"{value.ToFormattedString()}";
            
            var time = $" {maxTime.ToTimeStr(1, true, true)}";
            _maxTimeText.text = time;
            _maxIdleTimeText.text = "MAX_IDLE_TIME".ToLocalized().Replace("%1", time);
            
            _sliderText.text = offlineTime.ToTimeStr(2, true, true);

            var tickets = _ad.GetTickets();
            if (tickets > 0)
            {
                _adButton.ShowTicketsButton(tickets, "INCOME_X2".ToLocalized());
            }
            else
            {
                _adButton.ShowDefaultButton("INCOME_X2".ToLocalized(), !_flags.Has(GameFlag.AllAdsRemoved));
            }
            
            _hardButton.SetText(0, _hardPrice.ToString());
            _hardButton.SetText(1, "INCOME_X3".ToLocalized());
            
            if (!_game.IsEnoughCurrency(GameParamType.Hard, _hardPrice))
            {
                _hardButton.SetInteractableColor();
            }
            
            _offer.Redraw();
        }
        
        private void OnPressedHard()
        {
            if (!_game.IsEnoughCurrency(GameParamType.Hard, _hardPrice))
            {
                _uiFactory.SpawnMessage("HARD_NOT_ENOUGH".ToLocalized());
                return;
            }

            var value = _params.GetParamValue<CalculationComponent>(GameParamType.OfflineValue) * 2;
            _game.AddCurrency(GameParamType.Soft, value, GetCurrencyPlace.Offline);
            _game.SpendCurrency(GameParamType.Hard, _hardPrice, SpendCurrencyPlace.HardReturnToGameWindow, SpendCurrencyItem.Soft);
            
            Close();
        }
        
        private void OnPressedAd()
        {
            _ad.OnCompleted += OnAdCompleted;
            _ad.ShowReward(AdPlacement.ReturnToWindow);
            var tickets = _ad.GetTickets();
            if (tickets > 0)
            {
                _adButton.ShowTicketsButton(tickets, "INCOME_X2".ToLocalized());
            }
            else
            {
                _adButton.ShowDefaultButton("INCOME_X2".ToLocalized(), !_flags.Has(GameFlag.AllAdsRemoved));
            }
        }
        
        private void OnAdCompleted(bool success)
        {
            _ad.OnCompleted -= OnAdCompleted;
            if (!success) return;
            
            _uiFactory.SpawnResourceBubble(GameParamType.Soft, _calculation.OfflineValue, _adButton.RectTransform.position);

            Close();
        }
    }
}