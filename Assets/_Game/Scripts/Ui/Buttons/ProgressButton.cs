using _Game.Scripts.Ui.Base;
using UnityEngine;

namespace _Game.Scripts.Ui.Buttons
{
    public class ProgressButton : BaseButton
    {
        [SerializeField] private Color _hardFrontColor;
        [SerializeField] private Color _hardBackColor;

        public void SetHardColor()
        {
            SetColorFront(_hardFrontColor);
            SetColorBack(_hardBackColor);
        }
    }
}