using _Game.Scripts.Enums;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Ui.Base;
using UnityEngine.EventSystems;
using Zenject;

namespace _Game.Scripts.Ui.Buttons
{
    public class CurrencyButton : BaseButton
    {
        [Inject] private GameFlags _flags;

        protected override void OnClick()
        {
            if(_flags.Has(GameFlag.TutorialFinished) == false) return;
            base.OnClick();
        }
    }
}