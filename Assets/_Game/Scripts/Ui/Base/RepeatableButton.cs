using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace _Game.Scripts.Ui.Base
{
    public class RepeatableButton : BaseButton, IGameProgress
    {
        [Header("Repeatable Settings")]
        [SerializeField] private float _repeatInterval = 0.2f;
        
        [Inject] private GameProgressFactory _progressFactory;
        
        private GameProgress _repeatProgress;
        private bool _inited;
        
        protected override void OnEnable()
        {
            if (!_inited) CreateRepeatableProgress();
            base.OnEnable();
        }

        private void CreateRepeatableProgress()
        {
            if (_progressFactory == null) return;
            _repeatProgress = _progressFactory.CreateProgress(this, GameParamType.CycleDelay, _repeatInterval);
            _repeatProgress.CompletedEvent += SimulateClick;
            _repeatProgress.Pause();

            _inited = true;
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            _repeatProgress.Play();
            base.OnPointerDown(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            Reset();
            base.OnPointerUp(eventData);
        }

        public void Reset()
        {
            _repeatProgress.Reset(false);
            _repeatProgress.Pause();
        }
    }
}