using _Game.Scripts.Tools;
using UnityEngine;

namespace _Game.Scripts.Ui.Base
{
    public class AdWatchButton : BaseButton
    {
        [SerializeField] private GameObject _defaultContainer;
        [SerializeField] private GameObject _ticketsContainer;

        public void ShowDefaultButton(string text, bool showIcon = false)
        {
            SetText(0, text);
            if (showIcon)
            {
                ShowImage();
            }
            else
            {
                HideImage();
            }
            
            _defaultContainer.Activate();
            _ticketsContainer.Deactivate();
        }

        public void ShowTicketsButton(int tickets, string text)
        {
            SetText(1, $"1/{tickets}");
            SetText(2, text);
            ShowImage();
            _defaultContainer.Deactivate();
            _ticketsContainer.Activate();
        }
    }
}