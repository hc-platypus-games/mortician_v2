﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Cards;
using _Game.Scripts.Ui.Offers;
using _Game.Scripts.Ui.Room;
using _Game.Scripts.Ui.Tasks;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints.Events;
using _Game.Scripts.View.Scenarios;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Systems.Tutorial
{
    public class TutorialSystem
    {
        private enum TutorialNextStepCondition
        {
            None,
            Tap,
            PressedButton,
            FinishedTask,
            Action,
            UIOpened,
            UIClosed,
            GoToNextStep
        }
        
        private int _currentTutorialId;
        private int _currentStepIndex;

        [Inject] private DiContainer _container;
        [Inject] private ProjectSettings _settings;
        [Inject] private InputSystem _input;
        [Inject] private WindowsSystem _windows;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private GameFlags _flags;
        [Inject] private GameCamera _camera;
        [Inject] private RoomsFactory _rooms;
        [Inject] private MapPointsFactory _points;
        [Inject] private CharactersFactory _characters;
        [Inject] private LevelEventsFactory _levelEvents;
        [Inject] private VehicleFactory _vehicles;
        [Inject] private TutorialArrow _arrow;
        [Inject] private LevelSystem _levels;
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private EndLoadingComponent _endLoadingComponent;

        private readonly List<Func<(TutorialNextStepCondition, object)>> _steps = new();

        private TutorialNextStepCondition _nextStepCondition;
        private object _nextStepConditionParam;
        
        private TutorialWindow _tutorial;

        public int CurrentTutorialId => _currentTutorialId;
        
        public void Init()
        {
            _endLoadingComponent.OnEnd += OnLevelStarted;
            
            _tutorial = (TutorialWindow)_windows.GetWindow<TutorialWindow>();
            _arrow.Init();
            if (_settings.SkipTutorial)
            {
                _flags.Set(GameFlag.TutorialFinished);
                _flags.Set(GameFlag.CharactersOpened);
            }
        }

        private void OnLevelStarted()
        {
            if(_flags.Has(GameFlag.TutorialFinished)) return;
            if(_flags.Has(GameFlag.TutorialRunning)) return;
            _eventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.Install);
            StartTutorial(1, 1);
        }

        public void StartTutorial(int tutorialId, int stepId)
        {
            if(_flags.Has(GameFlag.TutorialFinished) && tutorialId == 1) return;
            if(_flags.Has(GameFlag.CharactersOpened) && tutorialId == 100) return;
            
            _flags.Set(GameFlag.TutorialRunning);
            _steps.Clear();
            BaseButton.AnyButtonClickedEvent += OnButtonPressed;
            _windows.WindowOpenedEvent += OnWindowOpened;
            _windows.WindowClosedEvent += OnWindowClosed;
            StartTutorialSteps(tutorialId, stepId);
        }

        private void StartTutorialSteps(int tutorialId, int id)
        {
            _currentTutorialId = tutorialId;
            
            var steps = _balance.DefaultBalance.Tutorial.FindAll(s => s.Region == tutorialId && s.StepId >= id);
            foreach (var step in steps)
            {
                SetTutorialAction(step);
            }
            
            StartStep(0);
        }

        private void SetTutorialAction(TutorialConfig config)
        {
            int.TryParse(config.StepActionParam1, out var value);
            float.TryParse(config.StepActionParam1, out var floatValue);
            switch (config.StepAction)
            {
                case TutorialStepAction.None:
                case TutorialStepAction.ShowTips:
                    _steps.Add(() =>
                    {
                        _windows.CloseAllWindows();
                        _input.BlockTouch();
                        _input.BlockScroll();
                        _camera.RemoveTarget();
                        _tutorial.Open(config.StepActionParam1.ToLocalized());
                        _tutorial.OnCompleted += OnCompleteTips;
                        _windows.SetGamePlayElementActive(GamePlayElement.SpeedUpButton, false);
                        return (TutorialNextStepCondition.Tap, default);	
                    });
                    break;
                
                case TutorialStepAction.HideGamePlayElement:
                    _steps.Add(() =>
                    {
                        _windows.SetGamePlayElementActive((GamePlayElement) value, false);
                        return (TutorialNextStepCondition.GoToNextStep, default);
                    });
                    break;
                
                case TutorialStepAction.ShowGamePlayElement:
                    _steps.Add(() =>
                    {
                        _windows.SetGamePlayElementActive((GamePlayElement) value, true);
                        return (TutorialNextStepCondition.GoToNextStep, default);
                    });
                    break;
                
                case TutorialStepAction.FocusOnRoom:
                    _steps.Add(() =>
                    {
                        var point = _points.GetFocusPoint((RoomType) value);
                        if(point == null) point = _rooms.GetRoom((RoomType)value).transform;
                        _camera.Focus(point.position, isAction: true);
                        return (TutorialNextStepCondition.Action, GameEvents.CameraFocused);
                    });
                    break;
                
                case TutorialStepAction.TutorialFocusOnRoom:
                    _steps.Add(() =>
                    {
                        var point = _points.GetFocusPoint((RoomType) value, true);
                        if(point == null) point = _rooms.GetRoom((RoomType)value).transform;
                        _camera.Focus(point.position, isAction: true);
                        return (TutorialNextStepCondition.Action, GameEvents.CameraFocused);
                    });
                    break;
                
                case TutorialStepAction.SetCameraScale:
                    _steps.Add(() =>
                    {
                        _camera.SetScale(floatValue);
                        return (TutorialNextStepCondition.GoToNextStep, default);
                    });
                    break;
                
                case TutorialStepAction.RoomInfoOpen:
                    _steps.Add(() =>
                    {
                        var room = _rooms.GetRoom((RoomType)value);
                        _input.SetAllowedToTouch(room);
                        _arrow.ShowCenterOfScreen();
                        return (TutorialNextStepCondition.UIOpened, typeof(RoomInfoWindow));
                    });
                    break;

                case TutorialStepAction.RoomInfoPressButton:
                    _steps.Add(() =>
                    {
                        var window = _windows.GetWindow<RoomInfoWindow>() as RoomInfoWindow;
                        if (!window) return (TutorialNextStepCondition.GoToNextStep, default);
                        window.PressButton(value);
                        var upgradeButton = window.UpgradeButton;
                        _arrow.Show(upgradeButton.RectTransform);
                        BaseButton.SetAllowedButton(upgradeButton);
                        return (TutorialNextStepCondition.PressedButton, default);
                    });
                    break;

                case TutorialStepAction.RoomInfoClose:
                    _steps.Add(() =>
                    {
                        _windows.CloseWindow<RoomInfoWindow>();
                        return (TutorialNextStepCondition.GoToNextStep, default);
                    });
                    break;
                
                case TutorialStepAction.ShowPhoneCall:
                    _steps.Add(() =>
                    {
                        var point = _points.GetFocusPoint((RoomType) value, true);
                        if(point == null) point = _rooms.GetRoom((RoomType)value).transform;
                        _camera.Focus(point.position, 1.2f, 1f);
                        var phone = _rooms.GetRoom((RoomType) value).GetComponentInChildren<TutorialPhoneView>();
                        if(!phone) return (TutorialNextStepCondition.GoToNextStep, default);
                        phone.StartAnimation();
                        return (TutorialNextStepCondition.Action, GameEvents.PhoneCalled);
                    });
                    break;
                
                case TutorialStepAction.FirstEventCall:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var character = _characters.GetCharacter(RoomType.Hall);
                        if(character == null) return (TutorialNextStepCondition.GoToNextStep, default);
                        _levelEvents.AddTutorialEvent();
                        _camera.FollowObject(character.transform);
                        _windows.SetGamePlayElementActive(GamePlayElement.SpeedUpButton, true);
                        return (TutorialNextStepCondition.Action, GameEvents.CallEventEnded);
                    });
                    break;
                
                case TutorialStepAction.FirstCorpseDelivery:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var character = _characters.GetCharacter(RoomType.Garage);
                        var car = _vehicles.FindCar(RoomType.Garage, 1);
                        if(character == null || car == null) return (TutorialNextStepCondition.GoToNextStep, default);
                        _camera.FollowObject(car.transform);
                        character.ForceMoveTo(character.CharacterPoint.transform.position);
                        _windows.SetGamePlayElementActive(GamePlayElement.SpeedUpButton, true);
                        return (TutorialNextStepCondition.Action, GameEvents.CorpseDelivered);
                    });
                    break;
                
                case TutorialStepAction.FirstCorpseReadyToEmbalm:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var character = _characters.GetCharacter(RoomType.Garage);
                        if(character == null) return (TutorialNextStepCondition.GoToNextStep, default);
                        _camera.FollowObject(character.transform);
                        _windows.SetGamePlayElementActive(GamePlayElement.SpeedUpButton, true);
                        return (TutorialNextStepCondition.Action, GameEvents.CorpseDelivered);
                    });
                    break;
                
                case TutorialStepAction.FirstCorpseEmbalm:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var character = _characters.GetCharacter(RoomType.Embalming);
                        if(character == null) return (TutorialNextStepCondition.GoToNextStep, default);
                        character.ForceMoveTo(character.CharacterPoint.transform.position);
                        _camera.FollowObject(character.transform);
                        _windows.SetGamePlayElementActive(GamePlayElement.SpeedUpButton, true);
                        return (TutorialNextStepCondition.Action, GameEvents.CorpseEmbalmed);
                    });
                    break;
                
                case TutorialStepAction.CemeteryOpen:
                    _steps.Add(() =>
                    {
                        var room = _rooms.GetRoom(RoomType.Cemetery);
                        _input.SetAllowedToTouch(room);
                        _arrow.ShowCenterOfScreen();
                        return (TutorialNextStepCondition.UIOpened, typeof(CemeteryWindow));
                    });
                    break;

                case TutorialStepAction.CemeteryPressButton:
                    _steps.Add(() =>
                    {
                        var window = _windows.GetWindow<CemeteryWindow>() as CemeteryWindow;
                        if (!window) return (TutorialNextStepCondition.GoToNextStep, default);
                        // TODO выделение итема если надо
                        var upgradeButton = window.MorticianHireButton;
                        _arrow.Show(upgradeButton.RectTransform);
                        BaseButton.SetAllowedButton(upgradeButton);
                        return (TutorialNextStepCondition.PressedButton, default);
                    });
                    break;

                case TutorialStepAction.CemeteryClose:
                    _steps.Add(() =>
                    {
                        _windows.CloseWindow<CemeteryWindow>();
                        return (TutorialNextStepCondition.GoToNextStep, default);
                    });
                    break;
                
                case TutorialStepAction.PortCorpseToGrave:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var character = _characters.GetCharacter(RoomType.CorpsePorter);
                        if(character == null) return (TutorialNextStepCondition.GoToNextStep, default);
                        _camera.FollowObject(character.transform);
                        _windows.SetGamePlayElementActive(GamePlayElement.SpeedUpButton, true);
                        return (TutorialNextStepCondition.Action, GameEvents.CorpseDeliveredToGrave);
                    });
                    break;
                
                case TutorialStepAction.FirstCorpseBury:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var character = _characters.GetCharacter(RoomType.Cemetery);
                        if(character == null) return (TutorialNextStepCondition.GoToNextStep, default);
                        character.ForceMoveTo(character.CharacterPoint.transform.position);
                        var grave = _points.GetTutorialGrave();
                        _camera.FollowObject(grave.transform);
                        _windows.SetGamePlayElementActive(GamePlayElement.SpeedUpButton, true);
                        return (TutorialNextStepCondition.Action, GameEvents.CorpseBuried);
                    });
                    break;
                
                case TutorialStepAction.FocusGamePlayElement:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var item = _windows.GetGamePlayElement((GamePlayElement)value).Button;
                        _arrow.Show(item.RectTransform);
                        BaseButton.SetAllowedButton(item);
                        return (TutorialNextStepCondition.PressedButton, item);
                    });
                    break;
                
                case TutorialStepAction.VideoBoostPressButton:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var item = _windows.GetWindow<VideoBoostWindow>().GetTutorialButton();
                        _arrow.Show(item.RectTransform);
                        BaseButton.SetAllowedButton(item);
                        return (TutorialNextStepCondition.PressedButton, item);
                    });
                    break;
                
                case TutorialStepAction.VideoBoostCloseButton:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var item = _windows.GetWindow<VideoBoostWindow>().CloseButton;
                        _arrow.Show(item.RectTransform);
                        BaseButton.SetAllowedButton(item);
                        return (TutorialNextStepCondition.PressedButton, default);
                    });
                    break;
                
                case TutorialStepAction.TaskInfoPressButton:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var window = _windows.GetWindow<TaskListWindow>() as TaskListWindow;
                        if (!window) return (TutorialNextStepCondition.GoToNextStep, default);
                        var button = window.GetTutorialButton();
                        _arrow.Show(button.RectTransform);
                        return (TutorialNextStepCondition.GoToNextStep, default);
                    });
                    break;
                
                case TutorialStepAction.BuyRoomOpen:
                    _steps.Add(() =>
                    {
                        var room = _rooms.GetRoom((RoomType)value);
                        _input.SetAllowedToTouch(room);
                        _arrow.ShowCenterOfScreen();
                        return (TutorialNextStepCondition.UIOpened, typeof(NewBuildingWindow));
                    });
                    break;

                case TutorialStepAction.BuyRoomPressButton:
                    _steps.Add(() =>
                    {
                        var window = _windows.GetWindow<NewBuildingWindow>() as NewBuildingWindow;
                        if (!window) return (TutorialNextStepCondition.GoToNextStep, default);
                        var item = window.GetTutorialButton();
                        _arrow.Show(item.RectTransform);
                        BaseButton.SetAllowedButton(item);
                        return (TutorialNextStepCondition.PressedButton, default);
                    });
                    break;

                case TutorialStepAction.StorePressBox:
                    _steps.Add(() =>
                    {
                        var window = _windows.GetWindow<StoreWindow>() as StoreWindow;
                        if (!window) return (TutorialNextStepCondition.GoToNextStep, default);
                        _flags.Set(GameFlag.CharactersOpened);
                        var item = window.GetTutorialButton();
                        _arrow.Show(item.RectTransform);
                        BaseButton.SetAllowedButton(item);
                        return (TutorialNextStepCondition.PressedButton, default);
                    });
                    break;

                case TutorialStepAction.BoxReceive:
                    _steps.Add(() =>
                    {
                        return (TutorialNextStepCondition.Action, GameEvents.BoxOpened);
                    });
                    break;

                case TutorialStepAction.RoomInfoPressCharacter:
                    _steps.Add(() =>
                    {
                        var window = _windows.GetWindow<RoomInfoWindow>() as RoomInfoWindow;
                        if (!window) return (TutorialNextStepCondition.GoToNextStep, default);
                        var item = window.GetTutorialButton();
                        _arrow.Show(item.RectTransform);
                        BaseButton.SetAllowedButton(item);
                        return (TutorialNextStepCondition.PressedButton, default);
                    });
                    break;

                case TutorialStepAction.CardInfoPressButton:
                    _steps.Add(() =>
                    {
                        var window = _windows.GetWindow<CardInfoWindow>() as CardInfoWindow;
                        if (!window) return (TutorialNextStepCondition.GoToNextStep, default);
                        var item = window.GetTutorialButton();
                        _arrow.Show(item.RectTransform);
                        BaseButton.SetAllowedButton(item);
                        return (TutorialNextStepCondition.PressedButton, default);
                    });
                    break;
                
                case TutorialStepAction.TutorialFinish:
                    _steps.Add(() =>
                    {
                        _eventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.TutorialFinish);
                        return (TutorialNextStepCondition.GoToNextStep, default);
                    });
                    break;
                
                case TutorialStepAction.GardenGrave:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        var character = _characters.GetCharacter(CharacterType.Gardener);
                        if(character == null) return (TutorialNextStepCondition.GoToNextStep, default);
                        character.ForceMoveTo(character.CharacterPoint.transform.position);
                        var grave = _points.GetTutorialGrave();
                        _camera.FollowObject(grave.transform);
                        _windows.SetGamePlayElementActive(GamePlayElement.SpeedUpButton, true);
                        return (TutorialNextStepCondition.Action, GameEvents.CorpseBuried);
                    });
                    break;
                
                case TutorialStepAction.ElectricianEvent:
                    _steps.Add(() =>
                    {
                        _input.BlockTouch();
                        _windows.CloseAllWindows();
                        var point = _points.GetAllPoints(View.MapPoints.Type.MapEvent)[1] as MapEventPointView;
                        point.EndCycle();
                        var character = point.GetComponentInChildren<CharacterView>(true);
                        if(character == null) return (TutorialNextStepCondition.GoToNextStep, default);
                        character.SpeedUp();
                        _camera.MoveTo(character.transform.position);
                        _camera.FollowObject(character.transform);
                        _windows.SetGamePlayElementActive(GamePlayElement.SpeedUpButton, true);
                        return (TutorialNextStepCondition.Action, GameEvents.ElectricianEventEnded);
                    });
                    break;
                
                case TutorialStepAction.RestorePurchases:
                    _steps.Add(() =>
                    {
                        _container.Resolve<InAppSystem>()?.RestorePurchases();
                        return (TutorialNextStepCondition.GoToNextStep, default);
                    });
                    break;
                
                case TutorialStepAction.RoomAvailableEvent:
                    _steps.Add(() =>
                    {
                        var room = _rooms.GetRoom((RoomType)value);
                        if(!room)return (TutorialNextStepCondition.GoToNextStep, default);
                        
                        _eventProvider.TriggerEvent(AppEventType.Analytics,
                            GameEvents.OpenRoom,
                            _levels.CurrentLevel.Id,
                            _levels.CurrentLevel.LevelName,
                            (int)room.Config.RoomType, 
                            $"room {room.Type}");
                        return (TutorialNextStepCondition.GoToNextStep, default);
                    });
                    break;
            }
        }

        public void RestoreGameSpeed()
        {
            if (Time.timeScale > 1) Time.timeScale = 1f;
        }
        
        private void OnCompleteTips()
        {
            _tutorial.OnCompleted -= OnCompleteTips;
            _tutorial.Close();
            OnStepFinished();
        }

        private void StartStep(int stepIndex)
        {
            if (_steps.Count <= stepIndex)
            {
                FinishTutorial();
                return;
            }

            _currentStepIndex = stepIndex;
            (_nextStepCondition, _nextStepConditionParam) = _steps[stepIndex].Invoke();
            
            var config = _balance.DefaultBalance.Tutorial
                .FirstOrDefault(s => s.Region == _currentTutorialId && s.StepId == _currentStepIndex);
            if (config != null)
            {
                if (!config.AnalyticsStep.IsNullOrEmpty())
                {
                    _eventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.TutorialStep, config.Region, config.StepId, config.AnalyticsStep);   
                }
            }
            Debug.Log($"tutorial step {stepIndex}");
            
            if (_nextStepCondition != TutorialNextStepCondition.GoToNextStep) return;
            StartStep(_currentStepIndex + 1);
        }
        
        private void OnStepFinished()
        {
            _input.BlockScroll();
            _arrow.Hide();
            _camera.RemoveTarget();
            
            RestoreGameSpeed();

            StartStep(_currentStepIndex + 1);
        }
        
        public void OnAction(GameEvents eventType, object[] parameters)
        {
            if (_nextStepCondition == TutorialNextStepCondition.Action &&
                _nextStepConditionParam is GameEvents conditionEvent &&
                conditionEvent == eventType)
            {
                StartStep(_currentStepIndex + 1);
            }
        }
        
        private void OnWindowOpened(BaseWindow window)
        {
            if (_nextStepCondition == TutorialNextStepCondition.UIOpened &&
                _nextStepConditionParam is Type type &&
                type == window.GetType())
            {
                OnStepFinished();
            }
        }

        private void OnWindowClosed(BaseWindow window)
        {
            if (_nextStepCondition == TutorialNextStepCondition.UIClosed &&
                _nextStepConditionParam is Type type &&
                type == window.GetType())
            {
                OnStepFinished();
            }
        }

        private void OnButtonPressed(BaseButton button)
        {
            if (_nextStepConditionParam == null) _nextStepConditionParam = button;
            if (_nextStepCondition == TutorialNextStepCondition.PressedButton && ReferenceEquals(_nextStepConditionParam, button))
            {
                OnStepFinished();
            }
        }

        private void FinishTutorial()
        {
            if (_tutorial.IsOpened) _tutorial.Close();
            _flags.Remove(GameFlag.TutorialRunning);
            _currentTutorialId = -1;
            _currentStepIndex = -1;
            _input.UnblockInput();
            _camera.RemoveTarget();
            _flags.Set(GameFlag.TutorialFinished);
            BaseButton.AnyButtonClickedEvent -= OnButtonPressed;
        }
    }
}