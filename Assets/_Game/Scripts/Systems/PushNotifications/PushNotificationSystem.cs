﻿using System.Collections.Generic;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Tools;
using Unity.Notifications.Android;
using Zenject;

namespace _Game.Scripts.Systems.PushNotifications
{
    public class PushNotificationSystem
    {
        [Inject] private GameSettings _settings;
        [Inject] private GameParamFactory _params;
        [Inject] private BoostSystem _boosts;
        [Inject] private OffersFactory _offers;
        
        private const int _offlineId = 10000;
        private const int _freeBoxId = 10010;
        private const int _freeHardId = 10020;
        private const int _videoBoostId = 10030;
        
        private readonly IPushNotification _notifications;
        
        private readonly List<int> _identifiers = new()
        {
            _offlineId,
            _freeBoxId,
            _freeHardId,
            _videoBoostId
        };

        public PushNotificationSystem()
        {
#if UNITY_ANDROID
            _notifications = new PushNotificationAndroid();
#elif UNITY_IPHONE || UNITY_IOS
            _notifications = new PushNotificationIos();
#endif
            
            _notifications.Init();
        }
        
        public void UpdateNotifications(bool active)
        {
            if (_settings.DisablePushNotifications || active) return;

            foreach (var identifier in _identifiers)
            {
                var time = GetTime(identifier);
                if (time <= 60) return;
#if UNITY_ANDROID
                var notificationStatus = AndroidNotificationCenter.CheckScheduledNotificationStatus(identifier);
                switch (notificationStatus)
                {
                    case NotificationStatus.Scheduled:
                        
                        _notifications.SendNotification(identifier, 
                            $"{identifier}_PUSH_TITLE".ToLocalized(), 
                            $"{identifier}_PUSH_TEXT".ToLocalized(),
                            time);
                        break;
                    
                    case NotificationStatus.Delivered:
                        AndroidNotificationCenter.CancelNotification(identifier);
                        break;
                    
                    case NotificationStatus.Unknown:
                        _notifications.SendNotification(identifier, 
                            $"{identifier}_PUSH_TITLE".ToLocalized(), 
                            $"{identifier}_PUSH_TEXT".ToLocalized(),
                            time);
                        break;
                }
#elif UNITY_IPHONE || UNITY_IOS
                _notifications.SendNotification(identifier, 
                    $"{identifier}_PUSH_TITLE".ToLocalized(), 
                    $"{identifier}_PUSH_TEXT".ToLocalized(),
                    time);
#endif
            }
        }

        private int GetTime(int identifier)
        {
            switch (identifier)
            {
                case _offlineId:
                    return (int)_params.GetParamValue<GameSystem>(GameParamType.MaxOfflineTime);
                
                case _freeBoxId:
                    return (int)_offers.GetOfferById(1).CycleDelay;
                
                case _freeHardId:
                    return (int)_offers.GetOfferById(7).CycleDelay;
                
                case _videoBoostId:
                    return (int)_boosts.GetAdBoost(AdPlacement.VideoBoost).LeftDuration;
                
                default:
                    return -1;
            }
        }
    }
}