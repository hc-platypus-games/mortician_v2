using System;
using _Game.Scripts.Enums;
using _Game.Scripts.Interfaces;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Systems
{
    public enum GameProgressState
    {
        None,
        Active,
        Completed,
        Paused
    }

    [Serializable]
    public class GameProgress
    {
        [SerializeField] private GameProgressState _state;
        [SerializeField] private GameParamType _type;
        [SerializeField] private float _target;
        [SerializeField] private float _current;
        private bool _looped;
        private bool _updatable;
        private float _delta;
       
        public event Action UpdatedEvent, CompletedEvent;

        public IGameProgress Owner { get; private set; }
        public GameParamType Type => _type;

        public bool IsActive => _state == GameProgressState.Active;
        public bool IsCompleted => _state == GameProgressState.Completed;
        public bool IsPaused => _state == GameProgressState.Paused;
        
        public float ProgressValue => _current != 0 ? _current / _target : 0;
        public float CurrentValue => _current;
        public float TargetValue => _target;
        public float LeftValue => _target - _current;

        public GameProgress()
        {
            _state = GameProgressState.Active;
        }
        
        private void Init(IGameProgress owner, GameParamType type, float target, bool looped, bool updatable)
        {
            Owner = owner;
            _type = type;
            _target = target;
            _looped = looped;
            _updatable = updatable;
        }

        public GameProgress Play()
        {
            if (_state == GameProgressState.Paused || _state == GameProgressState.Completed)
            {
                SetState(GameProgressState.Active);
            }

            return this;
        }

        public GameProgress Pause()
        {
            if (_state == GameProgressState.Active)
            {
                SetState(GameProgressState.Paused);
            }

            return this;
        }
        
        public void Tick(float deltaTime)
        {
            if (_state != GameProgressState.Active || !_updatable) return;
            Change(deltaTime);
        }

        public void Change(float delta, bool checkProgress = true)
        {
            _current += delta;
            UpdatedEvent?.Invoke();
            if (checkProgress) CheckProgress();
        }

        public void SetTarget(float target)
        {
            _target = target;
            CheckProgress();
        }

        private void CheckProgress()
        {
            if (_state != GameProgressState.Active) return;
            if (_current < _target)
            {
                return;
            }

            if (_looped)
            {
                _current = 0;
            }
            else
            {
                _current = _target;
                SetState(GameProgressState.Completed);
            }
            
            CompletedEvent?.Invoke();
        }

        public GameProgress Reset(bool checkProgress = true)
        {
            _state = GameProgressState.Active;
            _current = 0;
            if (!checkProgress) return this;
            CheckProgress();
            UpdatedEvent?.Invoke();
            return this;
        }
        
        private void SetState(GameProgressState state)
        {
            if (_state == state) return;
            _state = state;
        }

        public override string ToString()
        {
            return $"{_current} / {_target}";
        }

        public class Pool : MemoryPool<IGameProgress, GameParamType, float, bool, bool, GameProgress>
        {
            protected override void Reinitialize(IGameProgress owner, GameParamType type, float target, bool looped, bool updatable, GameProgress progress)
            {
                progress.Init(owner, type, target, looped, updatable);
            }
        }
    }
}