using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Systems.Base;
using Zenject;

namespace _Game.Scripts.Systems
{
    public class HardSystem
    {
        [Inject] private GameSystem _game;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private AppEventProvider _eventProvider;

        public int GetPriceForTime(float time)
        {
            var price = _balance.DefaultBalance.PriceHardPerMinute * time / 60;
            return (int)price;
        }

        public bool IsEnough(float value)
        {
            return _game.IsEnoughCurrency(GameParamType.Hard, value);
        }

        public void SpendHard(float value, SpendCurrencyPlace place, SpendCurrencyItem item)
        {
            if (!_game.IsEnoughCurrency(GameParamType.Hard, value)) return;
            
            _game.SpendCurrency(GameParamType.Hard, value, place, item);
        }

        public void AddHard(float value, GetCurrencyPlace place)
        {
            _game.AddCurrency(GameParamType.Hard, value, place);
        }
    }
}
