using System.Collections.Generic;
using _Game.Scripts.Enums;
using Sirenix.Serialization;
using UnityEngine;

namespace _Game.Scripts.Systems.Base
{
    public class GameFlags
    {
        public delegate void GameFlagEvent(GameFlag flag);

        public GameFlagEvent OnFlagSet;
        public List<GameFlag> Flags { get; } = new();

        public void Set(GameFlag flag)
        {
            if (Has(flag)) return;
            Flags.Add(flag);
            OnFlagSet?.Invoke(flag);
        }

        public bool Has(GameFlag flag)
        {
            return Flags.Contains(flag);
        }

        public void Remove(GameFlag flag)
        {
            Flags.Remove(flag);
        }
    }
}