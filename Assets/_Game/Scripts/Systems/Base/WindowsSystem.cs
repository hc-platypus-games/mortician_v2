using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Core;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems.GamePlayElements;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Base;
using Zenject;

namespace _Game.Scripts.Systems.Base
{
    public class WindowsSystem : ITickableSystem
    {
        public event Action<BaseWindow> WindowOpenedEvent, WindowClosedEvent;

        private readonly SceneData _sceneData;
        private readonly GameFlags _flags;

        private readonly List<BaseWindow> _all = new();
        private readonly List<BaseWindow> _windowsStack = new();
        private readonly List<BaseGamePlayElement> _gamePlayElements = new();

        private BaseWindow _openedWindow;
        private bool _allowedOpenWindows = true;

        public WindowsSystem(SceneData sceneData, GameFlags flags)
        {
            _sceneData = sceneData;
            _flags = flags;
            AddWindows();
        }

        private void AddWindows()
        {
            var windows = _sceneData.UI.GetComponentsInChildren<BaseWindow>(true);
            foreach (var window in windows)
            {
                _all.Add(window);
            }
        }

        public void InitWindows()
        {
            _flags.OnFlagSet += OnFlagSet;
            foreach (var window in _all)
            {
                window.Init();
            }
        }

        public void InitGamePlayElements()
        {
            var elements = _sceneData.UI.GetComponentsInChildren<BaseGamePlayElement>(true);
            foreach (var element in elements)
            {
                element.Init(this);
                _gamePlayElements.Add(element);
            }

            SetGamePlayElementActive(GamePlayElement.None, true);
        }

        public BaseWindow OpenWindow<T>(params object[] list)
        {
            if (!_allowedOpenWindows) return null;
            _openedWindow = _all.FirstOrDefault(w => w.GetType() == typeof(T));
            if (_openedWindow == null) return null;
            if (_openedWindow.IsOpened) return null;
            _all.FirstOrDefault(w => w.GetType() == typeof(MainWindow))?.Close();
            if (_openedWindow.AddToOpenedStack)
            {
                if (_windowsStack.Count != 0)
                {
                    if(!_windowsStack.Contains(_openedWindow)) _windowsStack.Add(_openedWindow);
                    if (_windowsStack[0].IsClosed) OpenFromStack();
                    return _openedWindow;
                }
                _windowsStack.Add(_openedWindow);
                _openedWindow.Closed += OnCloseButtonPressed;
                _openedWindow.Open(list);
                WindowOpenedEvent?.Invoke(_openedWindow);
            }
            else
            {
                if(!_openedWindow.ToOpenOverStack) CloseAllWindows();
                _openedWindow.Closed += OnCloseButtonPressed;
                _openedWindow.Open(list);
                WindowOpenedEvent?.Invoke(_openedWindow);
            }

            return _openedWindow;
        }

        private void OnCloseButtonPressed(BaseWindow window)
        {
            window.Closed -= OnCloseButtonPressed;
            
            _openedWindow = null;
            if (_windowsStack.Contains(window)) _windowsStack.Remove(window);
            if (_windowsStack.Count > 0)
            {
                _openedWindow = _windowsStack[0];
                if (_openedWindow.IsClosed) OpenNextWindow();
            }
            else
            {
                _all.FirstOrDefault(w => w.GetType() == typeof(MainWindow))?.Open();
            }
        }

        private void OpenNextWindow()
        {
            if (_windowsStack.Count == 0) return;
            _allowedOpenWindows = false;
            InvokeSystem.StartInvoke(OpenFromStack, 0.8f);
        }

        private void OpenFromStack()
        {
            _allowedOpenWindows = true;
            InvokeSystem.CancelInvoke(OpenFromStack, true);
            if(_windowsStack.Count == 0) return;
            _openedWindow = _windowsStack[0];
            _openedWindow.Closed += OnCloseButtonPressed;
            _openedWindow.Open();
        }

        public void CloseAllWindows()
        {
            if (_openedWindow != null) _openedWindow.Closed -= OnCloseButtonPressed;
            _openedWindow = null;
            
            for (var i = 0; i < _windowsStack.Count; i++)
            {
                var window = _windowsStack[i];
                if (CloseWindow(window))
                {
                    _windowsStack.Remove(window);
                    i--;
                }
            }
            _all.FirstOrDefault(w => w.GetType() == typeof(MainWindow)).Open();
        }

        public void CloseWindow<T>()
        {
            var window = _all.FirstOrDefault(w => w.GetType() == typeof(T));
            CloseWindow(window);
            _openedWindow = _all.FirstOrDefault(w => w.GetType() == typeof(T));
        }

        public bool CloseWindow(BaseWindow window)
        {
            if (window == null) return false;
            if (window.IsClosed) return true;
            window.Close();
            
            _openedWindow = null;
            
            WindowClosedEvent?.Invoke(window);
            
            if (_windowsStack.Contains(window)) _windowsStack.Remove(window);
            OpenNextWindow();
            return true;
        }

        public BaseWindow GetWindow<T>()
        {
            return _all.FirstOrDefault(w => w.GetType() == typeof(T));
        }

        public void UpdateLocalization()
        {
            foreach (var window in _all)
            {
                window.UpdateLocalization();
            }
        }

        public void Tick(float deltaTime)
        {
            if (_openedWindow != null) _openedWindow.Tick(deltaTime);
        }

        public BaseGamePlayElement GetGamePlayElement(GamePlayElement element)
        {
            return _gamePlayElements.Find(e => e.Type == element);
        }

        public void SetGamePlayElementActive(GamePlayElement type, bool value)
        {
            if (type != GamePlayElement.None)
            {
                var element = GetGamePlayElement(type);
                element.SetActive(value);
                return;
            }

            foreach (var element in _gamePlayElements)
            {
                bool hasFlag;
                switch (element.Type)
                {
                    case GamePlayElement.Currency:
                    case GamePlayElement.Soft:
                    case GamePlayElement.Hard:
                    case GamePlayElement.Tokens:
                        break;
                    case GamePlayElement.CardsButton:
                        hasFlag = _flags.Has(GameFlag.CharactersOpened);
                        element.SetActive(value && hasFlag);
                        break;
                    case GamePlayElement.CorpseOrdersButton:
                        hasFlag = _flags.Has(GameFlag.CorpseOrdersOpened);
                        element.SetActive(value && hasFlag);
                        break;
                    case GamePlayElement.SpeedUpButton:
                    case GamePlayElement.CurrentEmotionButton:
                        element.SetActive(false);
                        break;
                    default:
                        element.SetActive(value);
                        break;
                }
            }
        }

        private void OnFlagSet(GameFlag flag)
        {
            switch (flag)
            {
                case GameFlag.CorpseOrdersOpened:
                    SetGamePlayElementActive(GamePlayElement.CorpseOrdersButton, true);
                    break;
                case GameFlag.CharactersOpened:
                    SetGamePlayElementActive(GamePlayElement.CardsButton, true);
                    break;
            }
        }

        public virtual void MoveToOverlay(params GamePlayElement[] elements)
        {
            foreach (var element in elements)
            {
                var gamePlayElement = _gamePlayElements.Find(e => e.Type == element);
                if (gamePlayElement == null) continue;
                
                if (gamePlayElement.transform.parent == _sceneData.WindowsOverlay) continue;
                gamePlayElement.transform.SetParent(_sceneData.WindowsOverlay, true);
                gamePlayElement.Activate();
            }
        }
        
        public virtual void ReturnFromOverlay(params GamePlayElement[] elements)
        {
            foreach (var element in elements)
            {
                var gamePlayElement = _gamePlayElements.Find(e => e.Type == element);
                if (gamePlayElement != null)
                {
                    gamePlayElement.RestorePosition();
                }
            }
        }

        public bool IsWindowOpened()
        {
            if (_openedWindow && _openedWindow.IsClosed) _openedWindow = null;
            return _openedWindow != null || _windowsStack.Count > 0;
        }
    }
}