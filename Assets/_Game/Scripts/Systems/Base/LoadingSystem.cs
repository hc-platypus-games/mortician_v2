using System;
using System.Collections.Generic;
using _Game.Scripts.Components.Base;
using _Game.Scripts.Components.Loader;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using Zenject;

namespace _Game.Scripts.Systems.Base
{
    public class LoadingSystem
    {
        public enum LoaderState
        {
            GameLoading,
            LevelLoading,
            ResumeGame
        }
        
        public Action OnLoadedGame;
        public Action OnResumeGame;
        public Action<float> OnUpdateProgress;
        
        [Inject] private WindowsSystem _windows;
        
        private readonly DiContainer _container;
        private LoaderState _state;

        private LoadingWindow _window;
        
        private readonly List<BaseComponent> _steps = new();
        private BaseComponent _currentStep;
        private float _loadingProgress;

        public LoadingSystem(DiContainer container)
        {
            _container = container;
            _container.Bind<ATTrackingStatusComponent>().AsSingle().NonLazy();
            _container.Bind<ConnectionCheckComponent>().AsSingle().NonLazy();
            _container.Bind<PrivacyPolicyComponent>().AsSingle().NonLazy();
            _container.Bind<AnalyticsComponent>().AsSingle().NonLazy();
            _container.Bind<GameBalanceInitializeComponent>().AsSingle().NonLazy();
            _container.Bind<InitGameUI>().AsSingle().NonLazy();
            _container.Bind<InitGameSystems>().AsSingle().NonLazy();
            _container.Bind<LevelLoaderComponent>().AsSingle().NonLazy();
            _container.Bind<SaveLoaderComponent>().AsSingle().NonLazy();
            _container.Bind<CalculationComponent>().AsSingle().NonLazy();
            _container.Bind<EndLoadingComponent>().AsSingle().NonLazy();
        }
        
        public void Loading()
        {
            _window = (LoadingWindow)_windows.GetWindow<LoadingWindow>();
            StartLoader(LoaderState.GameLoading);
        }
        
        public void ResumeGame()
        {
            StartLoader(LoaderState.ResumeGame);
        }

        private void StartLoader(LoaderState state)
        {
            _state = state;
            _steps.Clear();
            
            switch (_state)
            {
                case LoaderState.GameLoading:
                    /// <summary>
                    /// Порядок событий при запуске игры. Закоментировать ненужное
                    /// </summary>
                    _steps.Add(_container.Resolve<ATTrackingStatusComponent>());
                    //_steps.Add(_container.Resolve<PrivacyPolicyComponent>());
                    _steps.Add(_container.Resolve<AnalyticsComponent>());
                    _steps.Add(_container.Resolve<GameBalanceInitializeComponent>());
                    _steps.Add(_container.Resolve<InitGameSystems>());
                    _steps.Add(_container.Resolve<InitGameUI>());
                    _steps.Add(_container.Resolve<ConnectionCheckComponent>());
                    _steps.Add(_container.Resolve<LevelLoaderComponent>());
                    _steps.Add(_container.Resolve<SaveLoaderComponent>());
                    _steps.Add(_container.Resolve<CalculationComponent>());
                    _steps.Add(_container.Resolve<EndLoadingComponent>());
                    break;
                
                case LoaderState.LevelLoading:
                    _steps.Add(_container.Resolve<LevelLoaderComponent>());
                    break;
                
                case LoaderState.ResumeGame:
                    _steps.Add(_container.Resolve<CalculationComponent>());
                    _steps.Add(_container.Resolve<EndLoadingComponent>());
                    break;
            }

            _window.Activate();
            
            GoToNextStep();
        }

        private void GoToNextStep()
        {
            _currentStep = _steps[0];
            _currentStep.OnEnd += OnEndStep;
            _currentStep.Start();
        }

        private void OnEndStep()
        {
            UpdateProgress();
            
            _currentStep.OnEnd -= OnEndStep;
            _steps.Remove(_currentStep);
            _currentStep = null;

            if (_steps.Count == 0)
            {
                _window.Deactivate();
                OnLoadedGame?.Invoke();
                OnResumeGame?.Invoke();
                OpenWindows();
                return;
            }
            
            InvokeSystem.StartExternalInvoke(GoToNextStep, 0.05f);
        }

        private void OpenWindows()
        {
            _windows.OpenWindow<ReturnToGameWindow>();
        }

        private void UpdateProgress()
        {
            _loadingProgress = (float)(_steps.IndexOf(_currentStep)+1) / _steps.Count;
            OnUpdateProgress?.Invoke(_loadingProgress);
        }
    }
}