using System;
using _Game.Scripts.Core;
using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.View;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Systems.Base
{
    /// <summary>
    /// Класс для загрузки/уничтожения уровней
    /// </summary>
    public class LevelSystem
    {
        public Action OnLoadedLevel,
            OnStartedLevel,
            OnDestroyLevel;
            
        [Inject] private ProjectSettings _projectSettings;
        [Inject] private SceneData _root;
        [Inject] private Prefabs _prefabs;
        [Inject] private AppEventProvider _appEventProvider;
        [Inject] private DiContainer _container;
        
        public LevelView CurrentLevel { get; private set; }

        public void LoadLevel(int id)
        {
            if (CurrentLevel != null) DestroyLevel();

            if (_projectSettings.DevBuild)
            {
                CurrentLevel = _root.GetComponentInChildren<LevelView>(true);
                if (CurrentLevel == null)
                {
                    CurrentLevel = _prefabs.LoadPrefab<LevelView>(config => config.Id == id);
                    if (CurrentLevel == null)
                    {
                        Debug.LogWarning($"(0001) Level with index {id} not found");
                        return;
                    }
                    CurrentLevel = _container.InstantiatePrefab(CurrentLevel, _root.transform).GetComponent<LevelView>();
                }
                else
                {
                    _container.BindInterfacesAndSelfTo<LevelView>().FromInstance(CurrentLevel).AsSingle().NonLazy();
                }
            }
            else
            {
                CurrentLevel = _prefabs.LoadPrefab<LevelView>(config => config.Id == id);
                if (CurrentLevel == null)
                {
                    Debug.LogWarning($"(0001) Level with index {id} not found");
                    return;
                }
                
                CurrentLevel = _container.InstantiatePrefab(CurrentLevel, _root.transform).GetComponent<LevelView>();
            }

            CurrentLevel.Activate();
            
            _appEventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.LevelStart, CurrentLevel.Id, CurrentLevel.LevelName);
            
            OnLoadedLevel?.Invoke();
            
            OnStartedLevel?.Invoke();
        }

        public void DestroyLevel()
        {
            OnDestroyLevel?.Invoke();
            InvokeSystem.Clear();
            _container.Unbind<LevelView>();
            CurrentLevel.gameObject.Destroy();
            CurrentLevel = null;
        }
    }
}