﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.MapPoints;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Save.SaveStructures;
using _Game.Scripts.Systems.Tutorial;
using _Game.Scripts.Ui;
using Zenject;

namespace _Game.Scripts.Systems.Tasks
{
    public class TaskSystem
    {
        public Action UpdateEvent;
        public Action<GameTask> TaskCompleted;
        
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private LevelSystem _levelSystem;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private RoomsFactory _rooms;
        [Inject] private TutorialSystem _tutorial;
        [Inject] private WindowsSystem _windows;
        [Inject] private GameCamera _camera;
        [Inject] private MapPointsFactory _points;
        [Inject] private TutorialArrow _arrow;
        [Inject] private UIFactory _ui;

        private int _lastTaskId;
        private readonly List<GameTask> _tasks = new();
        private readonly List<GameTask> _dailyQuests = new();

        private const int MAX_TASKS = 3;

        public int TaskHardReward { get; private set; }

        public int LastTaskId => _lastTaskId;
        public List<GameTask> Tasks => _tasks;
        public List<GameTask> DailyQuests => _dailyQuests;

        public GameTask FollowingTask {get; private set; }

        public void Init()
        {
            _lastTaskId = 0;
            TaskHardReward = _balance.DefaultBalance.RewardCompleteTask;
            _levelSystem.OnLoadedLevel += OnLoadedLevel;
        }

        private void OnLoadedLevel()
        {
            RunTasks();
        }

        public void LoadFromSave(int lastTaskId, params TaskData[] taskDatas)
        {
            _lastTaskId = lastTaskId;
            ClearTasks();
            foreach (var task in _tasks)
            {
                var data = taskDatas.FirstOrDefault(i => i.Id == task.ID);
                if(data.Id == 0) continue;
                task.LoadFromData(data.CurrentCount);
                if(data.Finished) task.Finish();
            }
            
            UpdateEvent?.Invoke();
        }

        private void RunTasks()
        {
            if (_tasks.Count >= MAX_TASKS) return;
            
            var region = _levelSystem.CurrentLevel.Id;
            for (int i = 0; i < MAX_TASKS; i++)
            {
                var config = _balance.DefaultBalance.Tasks.FirstOrDefault(t => t.Region == region && t.ID > _lastTaskId);
                if (config == null) break;
                
                var task = new GameTask(config, _rooms);
                task.OnCompleted += OnCompleteTask;
                task.OnFinished += OnFinishedTask;
            
                RunTaskAction(task.Config.ActionStart);

                _lastTaskId++;
                _tasks.Add(task);

                CheckIfComplete(task);
                
                _eventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.TaskReceived, task.ID, task.Type.ToString(), _levelSystem.CurrentLevel.Id, _levelSystem.CurrentLevel.LevelName);
            }
            UpdateEvent?.Invoke();
            _ui.UpdateBadges(BadgeNotificationType.Task);
        }

        private void CheckIfComplete(GameTask task)
        {
            var room = _rooms.GetRoom((RoomType)task.GetRoomId());
            var item = _rooms.GetRoomItem((RoomType)task.GetRoomId(), task.GetItemId());
            switch (task.Type)
            {
                case GameEvents.BuyRoom:
                    if(!room) return;
                    if (room.Level > 0) task.Increment();
                    break;
                case GameEvents.UpgradeRoom:
                    if(!room) return;
                    if (room.Level >= 3) task.Increment();
                    break;
                case GameEvents.BuyRoomItem:
                    if(!item) return;
                    if (item.Level > 0) task.Increment();
                    break;
                case GameEvents.UpgradeRoomItem:
                    if(!item) return;
                    if (item.Level >= item.Config.Cap) task.Increment();
                    break;
                case GameEvents.ReachItemLevel:
                    if(!item) return;
                    if (item.Level >= item.Config.Cap) task.Increment(task.NeedCount - task.CurrentCount);
                    else task.Increment(item.Level);
                    break;
            }
        }

        private void OnCompleteTask(GameTask task)
        {
            switch (task.Config.Class)
            {
                case TaskClass.Task:
                    RunTaskAction(task.Config.ActionCompleted);
                    UpdateEvent?.Invoke();
                    TaskCompleted?.Invoke(task);
                    _ui.UpdateBadges(BadgeNotificationType.Task);
                    
                    _eventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.TaskCompleted, 
                        task.ID, task.Type.ToString(), _levelSystem.CurrentLevel.Id, _levelSystem.CurrentLevel.LevelName);
                    break;
                
                case TaskClass.DailyQuest:
                    break;
            }
        }

        private void OnFinishedTask(GameTask task)
        {
            switch (task.Config.Class)
            {
                case TaskClass.Task:
                    RunTaskAction(task.Config.ActionFinish);
                    
                    task.OnCompleted -= OnCompleteTask;
                    task.OnFinished -= OnFinishedTask;

                    _eventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.TaskFinished, 
                        task.ID, task.Type.ToString(), _levelSystem.CurrentLevel.Id, _levelSystem.CurrentLevel.LevelName);
                    UpdateEvent?.Invoke();
                    _ui.UpdateBadges(BadgeNotificationType.Task);
                    break;
                
                case TaskClass.DailyQuest:
                    break;
            }
        }

        public void OnTaskAction(GameEvents eventType, object[] parameters)
        {
            var tasks = _tasks.FindAll(t => t.Type == eventType);
            foreach (var task in tasks)
            {
                task.Process(parameters);
            }
            
            UpdateEvent?.Invoke();
        }

        private void RunTaskAction(TaskAction taskAction)
        {
            switch (taskAction)
            {
                case TaskAction.RunCardTutorial:
                    _tutorial.StartTutorial(100, 1);
                    break;
                
                case TaskAction.RateGame:
                    _windows.OpenWindow<RateGameWindow>();
                    break;
            }
        }

        public void ClearTasks()
        {
            _tasks.Clear();
            RunTasks();
        }

        public GameTask GetTaskForUI()
        {
            GameTask targetTask = null;
            foreach (var task in _tasks.Where(t => !t.IsFinished))
            {
                if (targetTask == null)
                {
                    targetTask = task;
                    continue;
                }
                if (targetTask.Progress < task.Progress)
                {
                    targetTask = task;
                }    
            }

            return targetTask ??= _tasks.FirstOrDefault();
        }

        public void FollowTask(GameTask task)
        {
            FollowingTask = task;
            _windows.CloseAllWindows();
            var point = _points.GetFocusPoint((RoomType)task.GetRoomId());
            switch (task.Type)
            {
                case GameEvents.UpgradeCharacter:
                    break;
                case GameEvents.UpgradeRoomItem:
                case GameEvents.ReachItemLevel:
                    point = _rooms.GetRoomItem((RoomType) task.GetRoomId(), task.GetItemId())?.transform;
                    if(!point) break;
                    var position = point.position;
                    _arrow.Show(position);
                    _camera.Focus(position);
                    break;
                case GameEvents.BuyRoomItem:
                case GameEvents.UpgradeRoom:
                case GameEvents.BuyRoom:
                    _camera.Focus(point.position);
                    if(!point) break;
                    _arrow.Show(point.position);
                    break;
                case GameEvents.BuyGravePlaces:
                    break;
            }

            InvokeSystem.StartInvoke(_arrow.Hide, 10f);
        }

        public void ClearFollowingTask()
        {
            FollowingTask = null;
        }
    }
}