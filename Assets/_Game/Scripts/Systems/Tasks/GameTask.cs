﻿using System;
using System.Globalization;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Tools;
using Zenject;

namespace _Game.Scripts.Systems.Tasks
{
    public enum TaskState
    {
        None,
        InProgress,
        Completed,
        Finished,
    }
    
    public class GameTask
    {
        public Action<GameTask> OnCompleted;
        public Action<GameTask> OnFinished;

        private RoomsFactory _rooms;

        private TaskState _state;

        public TaskConfig Config { get; private set; }
        private readonly int _param;
        private readonly int _param1;
        private readonly int _param2;
        
        public int ID { get; private set; }
        public int Region { get; private set; }
        public float NeedCount { get; private set; }
        public float CurrentCount { get; private set; }
        public GameEvents Type { get; private set; }
        public float Progress => CurrentCount != 0 ? CurrentCount / NeedCount : 0;

        public bool IsCompleted => _state == TaskState.Completed;
        public bool IsFinished => _state == TaskState.Finished;
        
        public GameTask(TaskConfig config, RoomsFactory rooms)
        {
            _rooms = rooms;
            Config = config;
            ID = config.ID;
            Region = config.Region;
            Type = config.TaskType;
            _state = TaskState.InProgress;
            
            int.TryParse(Config.Var1, out _param);
            int.TryParse(Config.Var2, out _param1);
            int.TryParse(Config.Var3, out _param2);

            switch (Type)
            {
                case GameEvents.BuyRoom:
                case GameEvents.UpgradeRoom:
                case GameEvents.BuyRoomItem:
                case GameEvents.UpgradeRoomItem:
                    NeedCount = 1;
                    break;
                
                case GameEvents.ReachItemLevel:
                    NeedCount = _param2;
                    break;
            }
        }

        public void Process(object[] parameters)
        {
            switch (Type)
            {
                case GameEvents.BuyRoom:
                case GameEvents.UpgradeRoom:
                    if (parameters[0] is int room && room != _param) return;
                    Increment();
                    break;
                case GameEvents.BuyRoomItem:
                case GameEvents.UpgradeRoomItem:
                    if (parameters[0] is int roomId && roomId != _param || 
                        parameters[1] is int itemId && itemId != _param1) return;
                    Increment();
                    break;
                case GameEvents.ReachItemLevel:
                    if (parameters[0] is int roomId2 && roomId2 != _param || 
                        parameters[1] is int itemId2 && itemId2 != _param1 ||
                        parameters[2] is float == false) return;
                    Increment((float)parameters[2] - CurrentCount);
                    break;
            }
        }

        public void LoadFromData(float value)
        {
            CurrentCount = value;
            CurrentCount = CurrentCount.Clamp(0, NeedCount);
            if (CurrentCount < NeedCount) return;
            
            Complete();
        }
        
        public void Increment(float count = 1)
        {
            if (_state != TaskState.InProgress) return;
            var val = CurrentCount + count;
            CurrentCount = (float) val.Clamp(0, NeedCount);
            if (CurrentCount < NeedCount) return;
            
            Complete();
        }

        private void Complete()
        {
            _state = TaskState.Completed;
            OnCompleted?.Invoke(this);
        }
        
        public void Finish()
        {
            _state = TaskState.Finished;
            OnFinished?.Invoke(this);
        }

        public int GetRoomId()
        {
            int.TryParse(Config.Var1, out int value);
            return value;
        }

        public int GetItemId()
        {
            int.TryParse(Config.Var2, out int value);
            return value;
        }
        
        public int GetItemLevel()
        {
            int.TryParse(Config.Var3, out int value);
            return value;
        }

        public string GetTaskText()
        {
            var taskText = "";
            switch (Type)
            {
                case GameEvents.BuyRoomItem:
                case GameEvents.UpgradeRoomItem:
                case GameEvents.ReachItemLevel:
                    var item = _rooms.GetRoomItem((RoomType)GetRoomId(), GetItemId());
                    taskText = Type.ToString().ToUpper().ToLocalized()
                        .Replace("%1", $"<color=#51C445>{$"{item.Config.Name}_ITEM".ToLocalized()}#{item.Config.ItemId}</color>")
                        .Replace("%2", $"{item.RoomType.ToString().ToUpper().ToLocalized()}")
                        .Replace("%3", $"{GetItemLevel()}");
                    break;
                
                case GameEvents.BuyRoom:
                case GameEvents.UpgradeRoom:
                    var room = _rooms.GetRoom((RoomType)GetRoomId());
                    taskText = Type.ToString().ToUpper().ToLocalized()
                        .Replace("%1", $"{room.Type.ToString().ToUpper().ToLocalized()}");
                    break;
                
                default:
                    taskText = Type.ToString().ToLocalized();
                    break;
            }

            return taskText;
        }

        public string GetRewardIcon()
        {
            var reward = Config.Reward;
            switch (reward.Type)
            {
                case GameParamType.Soft:
                    return $"<sprite name=Soft>+{reward.Amount.ToFormattedString()}";

                case GameParamType.Hard:
                    return $"<sprite name=Hard>+{reward.Amount.ToFormattedString()}";
            }

            return "";
        }
        
        public string GetRewardImageStr()
        {
            var reward = Config.Reward;
            switch (reward.Type)
            {
                case GameParamType.Soft:
                    return "cash-1";

                case GameParamType.Hard:
                    return "diamonds-1";
            }

            return "";
        }

        public float GetRewardCount()
        {
            return Config.Reward.Amount;
        }
    }
}