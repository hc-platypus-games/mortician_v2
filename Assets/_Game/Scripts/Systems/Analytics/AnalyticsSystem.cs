using System;
using System.Collections.Generic;
using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;

namespace _Game.Scripts.Systems.Analytics
{
    /// <summary>
    /// Класс для интеграции с SDK аналитик
    /// </summary>
    public class AnalyticsSystem
    {
        public Action OnConnected;

        private readonly ProjectSettings _settings;
        private readonly List<IAnalyticsWrapper> _analyticsWrappers;
        
        public AnalyticsSystem(ProjectSettings settings)
        {
            _settings = settings;
            _analyticsWrappers = new List<IAnalyticsWrapper>
            {
                new GameAnalyticsWrapper(),
                new AppsflyerWrapper()
            };
        }

        public void Connect()
        {
            foreach (var wrapper in _analyticsWrappers)
            {
                wrapper.Init(_settings, OnConnected);
            }
        }

        public void SendEvent(GameEvents gameEvent, params object[] list)
        {
            foreach (var wrapper in _analyticsWrappers)
            {
                wrapper.SendEvent(gameEvent, list);
            }
        }
    }
}