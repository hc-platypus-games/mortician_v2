using System;
using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;
using GameAnalyticsSDK;

namespace _Game.Scripts.Systems.Analytics
{
    public class GameAnalyticsWrapper : IAnalyticsWrapper
    {
        public IAnalyticsWrapper Init(ProjectSettings settings, Action OnInitialized = null)
        {
            GameAnalytics.Initialize();
            OnInitialized?.Invoke();
            return this;
        }

        public void SendEvent(GameEvents eventType, params object[] parameters)
        {
            switch (eventType)
            {
                case GameEvents.RewardShowed:
                    GameAnalytics.NewDesignEvent($"rewarded:showed:{parameters[0].ToString().ToLower()}");
                    break;
                
                case GameEvents.RewardClicked:
                    GameAnalytics.NewDesignEvent($"rewarded:clicked:{parameters[0].ToString().ToLower()}");
                    break;
                
                case GameEvents.RewardStart:
                    GameAnalytics.NewDesignEvent($"rewarded:start:{parameters[0].ToString().ToLower()}");
                    break;
                
                case GameEvents.RewardFinish:
                    GameAnalytics.NewDesignEvent($"rewarded:finish:{parameters[0].ToString().ToLower()}");
                    break;
                
                case GameEvents.TaskReceived:
                    GameAnalytics.NewDesignEvent($"task:received:{parameters[0].ToString().ToLower()}");
                    break;
                
                case GameEvents.TaskFinished:
                    GameAnalytics.NewDesignEvent($"task:finished:{parameters[0].ToString().ToLower()}");
                    break;
                
                case GameEvents.TutorialStep:
                    var region = (int)parameters[0];
                    GameAnalytics.NewDesignEvent($"tutorial_step:{region}:{parameters[1].ToString().ToLower()}");
                    break;
            }
        }
    }
}