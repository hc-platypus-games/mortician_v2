using System;
using System.Collections.Generic;
using _Game.Scripts.Enums;
using _Game.Scripts.ScriptableObjects;
using AppsFlyerSDK;
using UnityEngine;

namespace _Game.Scripts.Systems.Analytics
{
    public class AppsflyerWrapper : IAnalyticsWrapper
    {
        private ProjectSettings _projectSettings;
        
        //cache
        private string _levelId;
        private string _levelName;
        
        public IAnalyticsWrapper Init(ProjectSettings settings, Action OnInitialized)
        {
            _projectSettings = settings;
            AppsFlyer.initSDK(_projectSettings.AppsflyerKey, "appID");
            AppsFlyer.startSDK();

            Debug.Log("AF inited");
            
            return this;
        }

        public void CacheLevelValues(int levelId, string levelName)
        {
            _levelId = levelId.ToString();
            _levelName = levelName.ToLower();
        }

        public void SendEvent(GameEvents eventType, params object[] parameters)
        {
            switch (eventType)
            {
                case GameEvents.InAppPurchased:
                    SendCustomEvent("af_purchase",
                        ("currency", parameters[0].ToString().ToLower()),
                        ("revenue", parameters[1].ToString().ToLower()),
                        ("product_id", parameters[2].ToString().ToLower()),
                        ("sku", parameters[3].ToString().ToLower()),
                        ("sku_group", parameters[4].ToString().ToLower()),
                        ("transaction_id", parameters[5].ToString().ToLower()),
                        ("transaction_number", parameters[6].ToString().ToLower()),
                        ("purchase_count", parameters[7].ToString().ToLower()),
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.FirstInAppPurchased:
                    SendCustomEvent("unique_pu",
                            ("sku", parameters[0].ToString().ToLower()),
                        ("sku_group", parameters[1].ToString().ToLower()), 
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;

                case GameEvents.RewardShowed:
                    SendCustomEvent("rv_show", 
                        ("placement", parameters[0].ToString().ToLower()),
                                 ("placement_type", "main"), 
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.RewardClicked:
                    SendCustomEvent("rv_click", 
                        ("placement", parameters[0].ToString().ToLower()),
                                 ("placement_type", "main"), 
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.RewardStart:
                    SendCustomEvent("rv_start", 
                        ("placement", parameters[0].ToString().ToLower()),
                                 ("placement_type", "main"), 
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.RewardFinish:
                    SendCustomEvent("rv_finish", 
                        ("placement", parameters[0].ToString().ToLower()),
                                 ("placement_type", "main"), 
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;

                case GameEvents.RewardErrorLoaded:
                    SendCustomEvent("rv_error", 
                        ("placement", parameters[0].ToString().ToLower()),
                        ("placement_type", "main"),
                        ("error_type", "load"),
                        ("error_code", parameters[1].ToString().ToLower()), 
                        ("network", parameters[2].ToString().ToLower()), 
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.RewardErrorDisplay:
                    SendCustomEvent("rv_error", 
                        ("placement", parameters[0].ToString().ToLower()),
                        ("placement_type", "main"),
                        ("error_type", "display"),
                        ("error_code", parameters[1].ToString().ToLower()),
                        ("network", parameters[2].ToString().ToLower()), 
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.LevelStart:
                    CacheLevelValues((int) parameters[0], parameters[1].ToString());
                    SendCustomEvent("level", ("level_number", parameters[0].ToString().ToLower()),
                        ("level_name", parameters[1].ToString().ToLower()));
                    break;
                
                case GameEvents.OpenRoom:
                    SendCustomEvent("level_step", ("level_number", ((int)parameters[0]).ToString().ToLower()),
                        ("level_name", parameters[1].ToString().ToLower()),
                        ("level_sub", ((int)parameters[2]).ToString().ToLower()),
                        ("level_sub_name", parameters[3].ToString().ToLower()));
                    break;
                
                case GameEvents.OpenCharacter:
                    // SendCustomEvent("level_step", ("level_number", parameters[0].ToString().ToLower()),
                    //     ("level_name", parameters[1].ToString().ToLower()),
                    //     ("level_sub", parameters[2].ToString().ToLower()),
                    //     ("level_sub_name", parameters[3].ToString().ToLower()),
                    //     ("level_number", _levelId),
                    //     ("level_name", _levelName));
                    SendCustomEvent("card_get", ("character_name", parameters[3].ToString().ToLower()),
                        ("value", parameters[4].ToString().ToLower()),
                        ("total", parameters[5].ToString().ToLower()),
                        ("placement", parameters[6].ToString().ToLower()),
                        ("placement_type", parameters[7].ToString().ToLower()),
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.UpgradeCard:
                    SendCustomEvent("character_upgrade", ("character_name", parameters[2].ToString().ToLower()),
                        ("character_level", parameters[3].ToString().ToLower()), 
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.LevelFinish:
                    break;
                
                case GameEvents.TaskReceived:
                    SendCustomEvent("quest", 
                        ("status", "start"),
                        ("quest_number", parameters[0].ToString().ToLower()),
                        ("quest_name", parameters[1].ToString().ToLower()),
                        ("quest_type", "main"),
                        ("level_number", parameters[2].ToString().ToLower()),
                        ("level_name", parameters[3].ToString().ToLower()),
                        ("placement_type", "main"));
                    break;
                
                case GameEvents.TaskCompleted:
                    SendCustomEvent("quest", 
                        ("status", "complete"),
                        ("quest_number", parameters[0].ToString().ToLower()),
                        ("quest_name", parameters[1].ToString().ToLower()),
                        ("quest_type", "main"),
                        ("level_number", parameters[2].ToString().ToLower()),
                        ("level_name", parameters[3].ToString().ToLower()),
                        ("placement_type", "main"));
                    break;
                
                case GameEvents.TaskFinished:
                    SendCustomEvent("quest", 
                        ("status", "finish"),
                        ("quest_number", parameters[0].ToString().ToLower()),
                        ("quest_name", parameters[1].ToString().ToLower()),
                        ("quest_type", "main"),
                        ("level_number", parameters[2].ToString().ToLower()),
                        ("level_name", parameters[3].ToString().ToLower()),
                        ("placement_type", "main"));
                    break;
                
                case GameEvents.TutorialStep:
                    var region = (int)parameters[0];
                    if (region == 1)
                    {
                        SendCustomEvent("tutorial", ("step", parameters[1].ToString().ToLower()),
                            ("step_name", parameters[2].ToString().ToLower()),
                            ("level_number", _levelId),
                            ("level_name", _levelName));
                        return;
                    }

                    var tutorialName = region switch
                    {
                        100 => "tutorial_characters",
                        101 => "tutorial_gardener",
                        _ => "soft_tutorial"
                    };
                    SendCustomEvent("soft_tutorial", ("tutorial_name", tutorialName),
                        ("step", parameters[1].ToString().ToLower()),
                        ("step_name", parameters[2].ToString().ToLower()),
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.TutorialFinish:
                    SendCustomEvent("tutorial_finish",
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
                
                case GameEvents.CurrencyGet:
                    SendCustomEvent("currencyget", ("currency_type", parameters[0].ToString().ToLower()),
                        ("currency_name", parameters[1].ToString().ToLower()),
                        ("placement", parameters[2].ToString().ToLower()),
                        ("value", parameters[3].ToString().ToLower()),
                        ("total", parameters[4].ToString().ToLower()),
                        ("placement_type", "main"));
                    break;
                
                case GameEvents.CurrencySpend:
                    SendCustomEvent("currencyspend", ("currency_type", parameters[0].ToString().ToLower()),
                        ("currency_name", parameters[1].ToString().ToLower()),
                        ("placement", parameters[2].ToString().ToLower()),
                        ("placement_type", "main"),
                        ("item", parameters[3].ToString().ToLower()),
                        ("value", parameters[4].ToString().ToLower()),
                        ("total", parameters[5].ToString().ToLower()));
                    break;
                
                case GameEvents.Install:
                    SendCustomEvent("install");
                    break;
                
                case GameEvents.Launch:
                    SendCustomEvent("launch", ("hard_currency", parameters[0].ToString().ToLower()),
                        ("soft_currency", parameters[1].ToString().ToLower()),
                        ("level_number", _levelId),
                        ("level_name", _levelName));
                    break;
            }
        }
        
        static void SendCustomEvent(string eventName, params (string, object)[] args)
        {
            Dictionary<string, string> eventParams = new Dictionary<string, string>();

            foreach (var pair in args)
            {
                eventParams.Add(pair.Item1, pair.Item2.ToString());
            }

            AppsFlyer.sendEvent(eventName, eventParams);
        }
    }
}