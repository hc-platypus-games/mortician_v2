﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Systems.Save;
using AppQuantum.IAP.Runtime;
using AppsFlyerSDK;
using UnityEngine;
using UnityEngine.Purchasing;
using Zenject;
using MiniJson = UnityEngine.UDP.Common.MiniJson;
using Product = UnityEngine.Purchasing.Product;

namespace _Game.Scripts.Systems
{
    public enum InAppPlacement
    {
        Shop,
        MainWindow
    }

    public class InAppSystem : IStoreListener
    {
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private GameFlags _flags;
        [Inject] private AppEventProvider _eventProvider;
        [Inject] private GameSystem _game;
        [Inject] private OffersFactory _offers;
        [Inject] private BoostSystem _boosts;
        [Inject] private GameParamFactory _params;
        [Inject] private CalculationComponent _calculation;
        [Inject] private SaveSystem _save;
        
        public bool StoreControllerAvailable => _storeController != null && _extensionProvider != null;
        
        private IStoreController _storeController;
        private IExtensionProvider _extensionProvider;
        
        private string _prefix;
        private Action<bool> _onPurchased;
        private OfferConfig _currentOffer;
        private InAppPlacement _placement;
        
        public bool IsPurchasingNow { get; private set; }

        public void Init()
        {
            _prefix = $"{Application.identifier}.";
            
            var configuration = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
             var offers = _balance.DefaultBalance.Offers
                 .FindAll(o => !string.IsNullOrEmpty(o.ShopId))
                 .ToList();
            
            AddProductsToBuilder(configuration, offers);
            IAPServerValidation.Initialize();
            UnityPurchasing.Initialize(this, configuration);
        }

        public void RestorePurchases()
        {
            if (StoreControllerAvailable && !_flags.Has(GameFlag.TutorialFinished))
            {
#if UNITY_ANDROID
                foreach (var config in _balance.DefaultBalance.Offers.Where(o => o.NonConsumable))
                {
                    ClaimRewards(config);
                }
#endif
            }
        }

        private string GetOfferId(string token)
        {
            return _prefix + token;
        }
        
        private void AddProductsToBuilder(ConfigurationBuilder builder, List<OfferConfig> tokens)
        {
            foreach (var token in tokens)
            {
                AddProductToConfiguration(builder, GetOfferId(token.ShopId), token.NonConsumable);
            }
        }

        private void AddProductToConfiguration(ConfigurationBuilder builder, string token, bool nonConsumable)
        {
            builder.AddProduct(token, nonConsumable ? ProductType.NonConsumable : ProductType.Consumable);
        }
        
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _storeController = controller;
            _extensionProvider = extensions;
            
            _extensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(result => { });
        }
        
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log($"(0005) InApp initialization failed: {error}");
        }
        
        public void Purchase(OfferConfig offer, InAppPlacement placement, Action<bool> onPurchased = null)
        {
            if (_currentOffer != null) return;
            
            IsPurchasingNow = true;
            
            _currentOffer = offer;
            _placement = placement;
            _onPurchased = onPurchased;

            _storeController.InitiatePurchase(GetOfferId(_currentOffer.ShopId));
        }
        
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            var product = e.purchasedProduct;
            IsPurchasingNow = false;

            if (!IsShopItemPurchased(product.definition.id))
            {
                Debug.Log($"ProcessPurchase: FAIL. Unrecognized product: '{e.purchasedProduct.definition}'");
            }
            else
            {
                ClaimRewards();
                
                var server = new IAPServerValidation();
                server.Validate(e.purchasedProduct, AppsFlyer.getAppsFlyerId(), Application.version, false, validation =>
                {
                    if (validation == ResultPurchaseValidation.VerifiedTransaction)
                    {
                        var config = _balance.DefaultBalance.Offers.FirstOrDefault(o => o.ShopIdFull == product.definition.id);
                        var sku = config?.SKU ?? "";
                        var skuGroup = config?.SkuGroup ?? "";
                        var orderId = "";
                        
                        if (!_flags.Has(GameFlag.InAppWasBought))
                        {
                            _flags.Set(GameFlag.InAppWasBought);
                            _eventProvider.TriggerEvent(AppEventType.Analytics,
                                GameEvents.FirstInAppPurchased,
                                sku,
                                skuGroup);
                        }

                        var param = _params.GetParam<GameSystem>(GameParamType.PurchaseCount);
                        param.Change(1);

                        var receipt = e.purchasedProduct.receipt;
                        var wrapper = MiniJson.JsonDecode(receipt);
                        if (wrapper != null) 
                        {
                            var payload = (string)wrapper["Payload"];
                            var details = MiniJson.JsonDecode(payload);
                            var receiptJson = (string)details["json"];
                            var dict = MiniJson.JsonDecode(receiptJson);
                            orderId = (string)dict["orderId"];
                        }
                        
                        _eventProvider.TriggerEvent(AppEventType.Analytics, 
                            GameEvents.InAppPurchased,
                            product.metadata.isoCurrencyCode,
                            product.metadata.localizedPrice.ToString(CultureInfo.InvariantCulture),
                            product.definition.id,
                            sku,
                            skuGroup,
                            product.transactionID,
                            orderId,
                            param.Value
                            );

                        _save.Save();
                        Debug.Log($"ProcessPurchase: SUCCESS. Product: '{e.purchasedProduct.definition}' --- {orderId}");
                        Debug.Log("receipt" + product.receipt);
                    }
                    //Обработка случая когда сервис валидации недоступен
                    //Либо имеются проблемы с интернет соединением
                    else if (validation == ResultPurchaseValidation.InternalError ||
                             validation == ResultPurchaseValidation.NetworkError)
                    {

                    }
                    //Обработка случая когда сервис валидации отклонил покупку игрока
                    else if (validation == ResultPurchaseValidation.UnverifiedTransaction)
                    {
                        OnPurchaseFailed(e.purchasedProduct, PurchaseFailureReason.PaymentDeclined);
                        ConfirmPendingPurchase(e.purchasedProduct.definition.id);
                    }
                });
            }
            
            _currentOffer = null;
            
            return PurchaseProcessingResult.Pending;
        }

        public ProductMetadata GetProductData(string token)
        {
            return _storeController.products.WithID(GetOfferId(token))?.metadata;
        }
        
        private bool IsShopItemPurchased(string token)
        {
            OnSuccessPurchase(token);
            return true;
        }

        private void OnSuccessPurchase(string token)
        {
            IsPurchasingNow = false;
            ConfirmPendingPurchase(token);
            
            _onPurchased?.Invoke(true);
            _onPurchased = null;
        }
        
        private void ConfirmPendingPurchase(string token)
        {
            var product = _storeController.products.WithID(token);
            _storeController.ConfirmPendingPurchase(product);
        }
        
        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            _currentOffer = null;
        }

        public void ClaimRewards(OfferConfig offer)
        {
            _currentOffer = offer;
            ClaimRewards();
        }

        private void ClaimRewards()
        {
            switch (_currentOffer.Id)
            {
                case 4:
                case 5:
                case 6:
                    _game.AddCurrency(GameParamType.AdTickets, _currentOffer.Value, GetCurrencyPlace.Store);
                    break;
                
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    _game.AddCurrency(GameParamType.Hard, _currentOffer.Value, GetCurrencyPlace.Store);
                    break;
                
                case 16:
                    _offers.SpawnDirectorCharacter();
                    _params.GetParam<GameSystem>(GameParamType.MaxOfflineTime).Change(36000);
                    _flags.Set(GameFlag.ManagerIsBought);
                    break;
                
                case 17:
                    _boosts.RunIdBoost(16);
                    _flags.Set(GameFlag.MarketerIsBought);
                    break;
                
                case 18:
                    _flags.Set(GameFlag.AllAdsRemoved);
                    _game.AddCurrency(GameParamType.Hard, _balance.DefaultBalance.AgentHardAmount, GetCurrencyPlace.Store);

                    var value = _calculation.GetIncomeByTime(_balance.DefaultBalance.AgentIncomeInTime);
                    if (value == 0) value = 10000;
                    _game.AddCurrency(GameParamType.Soft, value, GetCurrencyPlace.Store);
                    break;
            }

            _currentOffer = null;
        }
    }
}