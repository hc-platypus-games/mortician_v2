﻿using System;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.View.Room;

namespace _Game.Scripts.Systems.Boosts
{
    public class Boost
    {
        public enum BoostState
        {
            None,
            Active,
            Paused
        }
        
        public event Action UpdateEvent;
        public event Action FinishedEvent;

        public BoostConfig Config { get; private set; }

        public BoostState State { get; private set; }
        public float LeftDuration { get; private set; }
        public int Counter { get; private set; }
        public IGameParam Owner { get; private set; }

        public bool IsActive => State == BoostState.Active;
        public bool IsFinished => State == BoostState.Paused;
        
        public float Progress => LeftDuration / GetNextUpgradeDuration();

        private bool _updateable;

        public Boost(BoostConfig config)
        {
            Config = config;
            State = BoostState.Paused;
            _updateable = config.Duration > 0;
        }
        
        public void SetOwner(IGameParam owner)
        {
            Owner = owner;
        }
        
        public void Play()
        {
            State = BoostState.Active;
            if (Config.Duration > 0)
            {
                Counter++;
                if (Config.IncludePrevDuration)
                {
                    LeftDuration += GetNextUpgradeDuration();
                }
                else
                {
                    LeftDuration = Config.Duration;
                }
            }
            UpdateEvent?.Invoke();
        }

        public void Stop()
        {
            State = BoostState.Paused;
            LeftDuration = 0;
            Counter = 0;
            UpdateEvent?.Invoke();
        }

        public void Update(float deltaTime)
        {
            if (State == BoostState.Paused || !_updateable) return;
			
            LeftDuration -= deltaTime;
            UpdateEvent?.Invoke();
			
            if (LeftDuration > 0f) return;
            LeftDuration = 0;
            State = BoostState.Paused;
            FinishedEvent?.Invoke();
        }

        public float GetNextUpgradeDuration()
        {
            var duration = Config.Duration;
            for (int i = 0; i < Counter-1; i++)
            {
                duration += Config.NextDurationDelta;
            }

            return duration;
        }
        
        public float GetMaxBoostTime()
        {
            var duration = Config.Duration;
            for (int i = 0; i < Config.MaxCount - 1; i++)
            {
                duration += Config.NextDurationDelta;
            }

            return duration;
        }

        public bool IsBoostCap()
        {
            return Counter >= Config.MaxCount;
        }

        public void SetParams(BoostState state, float leftDuration, int counter)
        {
            State = state;
            LeftDuration = leftDuration;
            Counter = counter;
        }
    }
}