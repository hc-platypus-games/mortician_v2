﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Ui;
using Zenject;

namespace _Game.Scripts.Systems.Boosts
{
    public class BoostSystem : ITickableSystem
    {
        public Action<Boost> BoostStateChangedEvent;

        [Inject] private GameBalanceConfigs _balance;
        [Inject] private RoomsFactory _rooms;
        [Inject] private UIFactory _uiFactory;

        public List<Boost> Boosts { get; private set; } = new();

        public BoostSystem(LevelSystem levels)
        {
            levels.OnLoadedLevel += UpdateBoostOwners;
        }

        private void UpdateBoostOwners()
        {
            foreach (var boost in Boosts)
            {
                if (boost.Config.RoomType != RoomType.None)
                {
                    boost.SetOwner(_rooms.GetRoom(boost.Config.RoomType));
                }
            }
        }

        public void Init()
        {
            foreach (var newBoost in _balance.DefaultBalance.Boosts.Select(boost => new Boost(boost)))
            {
                newBoost.FinishedEvent += OnFinishBoost;
                Boosts.Add(newBoost);
            }
        }

        private void OnFinishBoost()
        {
            _uiFactory.UpdateBadges(BadgeNotificationType.VideoBoost);
        }

        public void Tick(float deltaTime)
        {
            foreach (var boost in Boosts)
            {
                boost.Update(deltaTime);
                if (boost.IsFinished) boost.Stop();
            }
        }
        
        public Boost GetBoostById(int id)
        {
            return Boosts.FirstOrDefault(c => c.Config.Id == id);
        }
        
        public float GetBoostValue(GameParamType type, IGameParam owner = null)
        {
            var k = Boosts
                .Where(c => c.Config.ParamType == type && c.Owner == owner && c.State == Boost.BoostState.Active)
                .Sum(b => b.Config.K);
            return k == 0 ? 1 : k;
        }

        public Boost GetAdBoost(AdPlacement placement)
        {
            return Boosts.FirstOrDefault(c => c.Config.Placement == placement);
        }

        public void RunAdBoost(AdPlacement placement)
        {
            var boost = GetAdBoost(placement);
            if (boost == null) return;
            boost.Play();
            BoostStateChangedEvent?.Invoke(boost);
        }

        public void RunIdBoost(int id)
        {
            var boost = Boosts.FirstOrDefault(c => c.Config.Id == id);
            if (boost == null) return;
            boost.Play();
            BoostStateChangedEvent?.Invoke(boost);
        }
        
        public void RunCardBoost(RoomType roomType)
        {
            var boost = Boosts.FirstOrDefault(c => c.Config.RoomType == roomType);
            if (boost == null) return;
            boost.Play();
            BoostStateChangedEvent?.Invoke(boost);
        }
        
        public GameParamType GetCardBoostParam(RoomType roomType)
        {
            var boost = Boosts.FirstOrDefault(c => c.Config.RoomType == roomType);
            return boost == null ? GameParamType.None : boost.Config.ParamType;
        }
        
        public float GetCardBoostValue(RoomType roomType)
        {
            var boost = Boosts.FirstOrDefault(c => c.Config.RoomType == roomType);
            return boost == null ? 0 : boost.Config.K;
        }
    }
}