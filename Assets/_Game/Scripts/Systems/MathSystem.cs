using System;
using System.Linq;
using _Game.Scripts.Balance;
using _Game.Scripts.Enums;
using Zenject;

namespace _Game.Scripts.Systems
{
    public class MathSystem
    {
        [Inject] private GameBalanceConfigs _balance;
        
        public float GetValue(GameParamType type, int level, float value = 0, bool nextLevel = false)
        {
            level = nextLevel ? level + 1 : level;
            
            var param = _balance.DefaultBalance.Progressions.FirstOrDefault(p => p.ParamType == type);
            var baseFriction = _balance.DefaultBalance.Progressions.FirstOrDefault(p => p.ParamType == GameParamType.BaseFriction);

            switch (type)
            {
                case GameParamType.Seats:
                    return 100;
                
                case GameParamType.Income:
                    if (param == null) return 0;
                    //if (level == 0) level = 1;
                    var incomeFriction = _balance.DefaultBalance.Progressions.FirstOrDefault(p => p.ParamType == GameParamType.IncomeFriction);
                    return(float) Math.Round(level == 1 ? value : value * Math.Pow(param.Value, (incomeFriction?.Value ?? 1) * level));
                
                case GameParamType.CycleDelay:
                    if (param == null) return 0;
                    if (level == 0) level = 1;
                    var cdFriction = _balance.DefaultBalance.Progressions.FirstOrDefault(p => p.ParamType == GameParamType.CycleDelay);
                    return(float) Math.Round(level == 1 ? value * 100 : value * Math.Pow(param.Value, (cdFriction?.Value ?? 1) * level) * 100) / 100;
               
                case GameParamType.MorticianCycle:
                    if (param == null) return 0;
                    if (level == 0) level = 1;
                    var mortFriction = _balance.DefaultBalance.Progressions.FirstOrDefault(p => p.ParamType == GameParamType.MorticianCycle);
                    return(float) Math.Round(level == 1 ? value * 100 : value * Math.Pow(param.Value, (mortFriction?.Value ?? 1) * level) * 100) / 100;
                
                case GameParamType.MorticianHirePrice:
                    var morticianHirePriceFriction = _balance.DefaultBalance.Progressions.FirstOrDefault(p => p.ParamType == GameParamType.PriceMorticianHireFriction);
                    var hireValue = morticianHirePriceFriction?.BaseValue ?? 1;
                    return level == 0 
                        ? hireValue
                        : GetRoundedValue(Math.Round(hireValue * Math.Pow(morticianHirePriceFriction?.Value ?? 1, (baseFriction?.Value ?? 1) * level)));
                
                case GameParamType.MorticianTrainPrice:
                    var morticianTrainPriceFriction = _balance.DefaultBalance.Progressions.FirstOrDefault(p => p.ParamType == GameParamType.PriceMorticianTrainFriction);
                    var trainValue = morticianTrainPriceFriction?.BaseValue ?? 1;
                    return level == 0 
                        ? trainValue 
                        : GetRoundedValue(Math.Round(trainValue * Math.Pow(morticianTrainPriceFriction?.Value ?? 1, (baseFriction?.Value ?? 1) * level)));
                
                case GameParamType.Price:
                    var priceFriction = _balance.DefaultBalance.Progressions.FirstOrDefault(p => p.ParamType == GameParamType.PriceFriction);
                    var newValue = Math.Round(value * Math.Pow(priceFriction?.Value ?? 1, (baseFriction?.Value ?? 1) * level));
                    return level == 0 ? value : GetRoundedValue(newValue);
                
                default:
                    return 0;
            }
        }

        private float GetRoundedValue(double value)
        {
            var length = ((int) value).ToString().Length;
            var exp = (int)Math.Pow(10, length - 2);
            if (exp < 1) exp = 1;
            return (float)Math.Round(value / exp) * exp;
        }
    }
}
