using System;
using _Game.Scripts.ScriptableObjects;

namespace _Game.Scripts.Systems.Ads
{
    public class MaxWrapper : AdWrapper
    {
        private ProjectSettings _projectSettings;
        private AppEventProvider _eventProvider;
        private int _retryAttempt;
            
        public override void Init(ProjectSettings settings)
        {
            _projectSettings = settings;
            
            MaxSdkCallbacks.OnSdkInitializedEvent += sdkConfiguration =>
            {
                InitSDK();
            };
            
            MaxSdk.SetSdkKey(_projectSettings.MaxAndroidKey);
            MaxSdk.InitializeSdk();
        }

        private void InitSDK()
        {
            MaxSdkCallbacks.Rewarded.OnAdLoadedEvent += OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent += OnRewardedAdLoadFailedEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayedEvent += OnRewardedAdDisplayedEvent;
            MaxSdkCallbacks.Rewarded.OnAdClickedEvent += OnRewardedAdClickedEvent;
            MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent += OnRewardedAdRevenuePaidEvent;
            MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += OnRewardedAdHiddenEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent += OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;
            
            LoadRewardedAd();
        }

        private void LoadRewardedAd()
        {
            MaxSdk.LoadRewardedAd(_projectSettings.MaxAndroidRewardsId);
        }
        
        public override void ShowReward()
        {
            MaxSdk.ShowRewardedAd(_projectSettings.MaxAndroidRewardsId);
        }

        public override void ShowInter()
        {
        }

        public override void ShowBanner()
        {
        }
        
        public override void HideBanner()
        {
        }

        public override bool RewardAvailable()
        {
            return MaxSdk.IsRewardedAdReady(_projectSettings.MaxAndroidRewardsId);
        }

        public override bool InterAvailable()
        {
            return true;
        }

        public override void ShowDebugger()
        {
            MaxSdk.ShowMediationDebugger();
        }
        
        private void OnRewardedAdLoadedEvent(string arg1, MaxSdkBase.AdInfo arg2)
        {
            // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'
            // Reset retry attempt
            _retryAttempt = 0;
        }
        
        private void OnRewardedAdLoadFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
        {
            // Rewarded ad failed to load 
            // AppLovin recommends that you retry with exponentially higher delays, up to a maximum delay (in this case 64 seconds).
            _retryAttempt++;
            double retryDelay = Math.Pow(2, Math.Min(6, _retryAttempt));
            InvokeSystem.StartInvoke(LoadRewardedAd, (float) retryDelay);

            if (errorInfo.WaterfallInfo.NetworkResponses.Count > 0)
            {
                OnRewardLoadFailed?.Invoke(errorInfo.Code.ToString(), errorInfo.WaterfallInfo.NetworkResponses[0].MediatedNetwork.Name);   
            }
            else
            {
                OnRewardLoadFailed?.Invoke(errorInfo.Code.ToString(), errorInfo.WaterfallInfo.Name);
            }
        }
        
        private void OnRewardedAdDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo) {}
       
        private void OnRewardedAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
        {
            OnRewardEnded?.Invoke(AdSystem.AdResult.ErrorDisplay, (int)errorInfo.Code, adInfo.NetworkName);
            //OnRewardEnded = null;
            
            // Rewarded ad failed to display. We recommend loading the next ad
            LoadRewardedAd();
        }
        
        private void OnRewardedAdClickedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo) {}
        
        private void OnRewardedAdHiddenEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Rewarded ad is hidden. Pre-load the next ad
            LoadRewardedAd();
        }
        
        private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward, MaxSdkBase.AdInfo adInfo)
        {
            OnRewardEnded?.Invoke(AdSystem.AdResult.Watched, 0, "");
            //OnRewardEnded = null;
        }
        
        private void OnRewardedAdRevenuePaidEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Ad revenue paid. Use this callback to track user revenue.
        }
    }
}