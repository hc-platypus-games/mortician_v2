using System;
using _Game.Scripts.Balance;
using _Game.Scripts.Components.Loader;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Systems.Ads
{
    public class AdSystem
    {
        public enum AdResult
        {
            Watched,
            Clicked,
            Canceled,
            ErrorLoaded,
            ErrorDisplay
        }

        public Action<bool> OnCompleted;
        
        [Inject] private GameFlags _flags;
        [Inject] private AppEventProvider _appEventProvider;
        [Inject] private GameSystem _game;
        [Inject] private OffersFactory _offers;
        [Inject] private BoostSystem _boosts;
        [Inject] private GameParamFactory _params;
        [Inject] private GameBalanceConfigs _balance;
        [Inject] private CalculationComponent _calculation;

        private readonly AdWrapper _currentWrapper;
        private AdPlacement _currentPlacement;
        
        public bool AdIsShowing { get; private set; }
        
        public AdSystem(ProjectSettings settings)
        {
            _currentWrapper = new MaxWrapper();
            _currentWrapper.Init(settings);
            _currentWrapper.OnRewardEnded += OnRewardEnded;
            _currentWrapper.OnInterEnded += OnInterEnded;
            _currentWrapper.OnRewardLoadFailed += OnRewardLoadFailed;
        }
        
        public void ShowDebugger()
        {
            _currentWrapper.ShowDebugger();
        }

        public void ShowBanner()
        {
            _currentWrapper.ShowBanner();
        }

        public void HideBanner()
        {
            _currentWrapper.HideBanner();
        }
        
        public void ShowInter(AdPlacement placement)
        {
            _currentPlacement = placement;

#if UNITY_EDITOR
            return;
#endif
            AdIsShowing = true;
            if (_flags.Has(GameFlag.AllAdsRemoved) || !_flags.Has(GameFlag.TutorialFinished))
            {
                return;
            }

            if (!_currentWrapper.RewardAvailable())
            {
                return;
            }
            
            _currentWrapper.ShowInter();
        }
        
        public void ShowReward(AdPlacement placement)
        {
            _currentPlacement = placement;
            AdIsShowing = true;
            
            var tickets = _params.GetParam<GameSystem>(GameParamType.AdTickets);
            if (tickets.Value > 0)
            {
                ClaimReward();
                OnCompleted?.Invoke(true);
                tickets.Change(-1);
                return;
            }

#if UNITY_EDITOR
            ClaimReward();
            OnCompleted?.Invoke(true);
            return;
#endif
            
            if (_flags.Has(GameFlag.AllAdsRemoved) || !_flags.Has(GameFlag.TutorialFinished))
            {
                ClaimReward();
                OnCompleted?.Invoke(true);
                return;
            }

            if (!_currentWrapper.RewardAvailable())
            {
                return;
            }

            Debug.Log("Start watch");
            
            _currentWrapper.ShowReward();
            _appEventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.RewardStart, _currentPlacement);
        }

        private void OnRewardLoadFailed(string errorCode, string network)
        {
            _appEventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.RewardErrorLoaded, _currentPlacement, errorCode, network);
        }

        private void OnRewardEnded(AdResult result, int code, string network)
        {
            Debug.Log("OnRewardEnded");
            switch (result)
            {
                case AdResult.ErrorDisplay:
                    _appEventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.RewardErrorDisplay, _currentPlacement, code, network);
                    OnCompleted?.Invoke(false);
                    break;
                
                case AdResult.Clicked:
                case AdResult.Watched:
                    
                    Debug.Log("OnRewardWatched" + _currentPlacement);
                    _appEventProvider.TriggerEvent(AppEventType.Analytics, GameEvents.RewardFinish, _currentPlacement);
                    ClaimReward();
                    OnCompleted?.Invoke(true);
                    break;
                
                default:
                    OnCompleted?.Invoke(false);
                    break;
            }
        }

        private void ClaimReward()
        {
            AdIsShowing = false;
            switch (_currentPlacement)
            {
                case AdPlacement.VideoBoost:
                    _boosts.RunAdBoost(_currentPlacement);
                    break;
                
                case AdPlacement.FirstHard:
                    _game.AddCurrency(GameParamType.Hard, _balance.DefaultBalance.FirstHardValue, GetCurrencyPlace.Store);
                    _offers.RemoveHardMainOffer();
                    break;
                
                case AdPlacement.FastCash:
                    _game.AddCurrency(GameParamType.Soft, _calculation.CashValue, GetCurrencyPlace.AdOffer);
                    break;
                
                case AdPlacement.ReturnToWindow:
                    _game.AddCurrency(GameParamType.Soft, _calculation.OfflineValue, GetCurrencyPlace.AdOffer);
                    break;
                
                case AdPlacement.ItemsUpgradeDiscount:
                    _boosts.RunAdBoost(_currentPlacement);
                    break;
            }
        }

        private void OnInterEnded(AdResult result, int code)
        {
        }

        public int GetTickets()
        {
            return (int)_params.GetParamValue<GameSystem>(GameParamType.AdTickets);
        }
    }
}