using _Game.Scripts.Enums;
using _Game.Scripts.Interfaces;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Base;
using _Game.Scripts.Ui.Room;
using _Game.Scripts.Ui.Statistic;
using _Game.Scripts.View;
using _Game.Scripts.View.Character;
using _Game.Scripts.View.MapPoints.Clickable;
using _Game.Scripts.View.Room;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Systems
{
    public class InputSystem : ITickableSystem
    {
        [Inject] private GameCamera _camera;
        [Inject] private WindowsSystem _windows;

        private BaseView _allowedToTouch;
        
        private Vector3? _prevTouchPos;
        private Vector3? _targetTouchPos;

        private float _k;
        private const float DELTA_TOUCH = 0.05f;
        private const float MAX_SCROLL_K = 0.75f;
        private const float ZOOM_SENSETIVITY = 0.12f;

        private bool _zoom;
        private bool _touching;
        private bool _dragging;
        private bool _scroll;
        
        private bool _ignoreDrag;
        private bool _ignoreTaps;
        private bool _ignoreSceneInteraction => _ignoreDrag && _ignoreTaps;
        
        private const int SECOND_MODEL_SELECTION = 1 << 6,
                          GROUND = 1 << 7,
                          FIRST_MODEL_SELECTION = 1 << 10;

        private MainWindow _mainWindow;
        
        public InputSystem()
        {
            _k = MAX_SCROLL_K;
        }

        public void Init()
        {
            
        }

        public void Tick(float deltaTime)
        {
            if (_windows.IsWindowOpened() || _ignoreSceneInteraction) return;
            
            var touchCount = Input.touchCount;

            if (touchCount == 2)
            {
                CheckZoom();
            }
            else
            {
                if (touchCount == 0 && _zoom)
                {
                    _k = 0;
                    _zoom = false;
                }
                CheckDrag(deltaTime);
            }

#if UNITY_EDITOR
            CheckZoom();
#endif
        }
        
        private void CheckZoom()
        {
            if(_ignoreDrag) return;
            _zoom = true;
#if UNITY_EDITOR
            var scroll = Input.mouseScrollDelta.y;
            if (!scroll.EqualTo(0f))
            {
                _camera.Zoom(scroll < 0);
            }
            _zoom = false;
            return;
#endif
            var touch1 = Input.touches[0];
            var touch2 = Input.touches[1];

            var touch1Delta = touch1.position - touch1.deltaPosition;
            var touch2Delta = touch2.position - touch2.deltaPosition;

            var prevTouchDeltaMag = (touch1Delta - touch2Delta).magnitude;
            var touchDeltaMag = (touch1.position - touch2.position).magnitude;

            var delta = prevTouchDeltaMag - touchDeltaMag;
			
            _camera.Zoom(delta * ZOOM_SENSETIVITY);
        }
        
        private void CheckDrag(float deltaTime)
        {
            if(_zoom) return;
            if (Input.GetMouseButtonDown(0))
            {
                _prevTouchPos = GetWorldTouchPos();
                _targetTouchPos = _prevTouchPos;
                _touching = true;
                _k = MAX_SCROLL_K;
                return;
            }

            if (!_touching && !_dragging) return;
            if (Input.GetMouseButtonUp(0) && !_dragging)
            {
                _touching = false;
                _targetTouchPos = GetWorldTouchPos();

                if (!_prevTouchPos.HasValue || !_targetTouchPos.HasValue) return;
                var deltaPos = _targetTouchPos.Value - _prevTouchPos.Value;

                if (deltaPos.magnitude < DELTA_TOUCH)
                {
                    CheckMouseDown();
                }
            }

            if(_ignoreDrag) return;
            var pressed = Input.GetMouseButton(0);
            if (pressed)
            {
                _targetTouchPos = GetWorldTouchPos();

                if (!_prevTouchPos.HasValue || !_targetTouchPos.HasValue) return;
                var deltaPos = _targetTouchPos.Value - _prevTouchPos.Value;
                if (deltaPos.magnitude < 0.001f) return;
                if(deltaPos.magnitude > DELTA_TOUCH) _dragging = true;
                _camera.Move(-deltaPos);
            }
            else
            {
                if (!_prevTouchPos.HasValue || !_targetTouchPos.HasValue) return;
                var deltaPos = (_targetTouchPos.Value - _prevTouchPos.Value) * _k;
                if (deltaPos.magnitude < 0.01f)
                {
                    ClearAllFlags();
                    return;
                }

                _scroll = true;
                _dragging = true;
            
                if (_camera.Move(-deltaPos))
                {
                    _scroll = false;
                    return;
                }

                _k -= deltaTime;
                if (_k <= DELTA_TOUCH)
                {
                    StopScroll();
                    _dragging = false;
                }
            }
        }

        private void ClearAllFlags()
        {
            _dragging = false;
            _touching = false;
            _zoom = false;
            _scroll = false;
        }

        private Vector3? GetWorldTouchPos()
        {
            var screenTouchPos = Input.mousePosition;
            var ray = _camera.GetRay(screenTouchPos);

            return Physics.Raycast(ray, out var hit, float.MaxValue, GROUND)
                ? hit.point
                : null;
        }
        
        private void CheckMouseDown()
        {
            if(_ignoreTaps) return;
            if(BaseButton.IsOver) return;
            
            _touching = false;

            var ray = _camera.GetRay();
            
            if (_scroll)
            {
                _scroll = false;
                return;
            }
            
            if (Physics.Raycast(ray, out var first_hit, float.MaxValue, FIRST_MODEL_SELECTION))
            {
                var view = first_hit.transform.GetComponent<SelectionCollider>();
                if (view != null)
                {
                    if(OnViewTouched(view)) return;
                }
            }

            if (!Physics.Raycast(ray, out var hit, float.MaxValue, SECOND_MODEL_SELECTION)) return;
            var selectionCollider = hit.transform.GetComponent<SelectionCollider>();
            if (selectionCollider != null) OnViewTouched(selectionCollider);
        }

        private bool OnViewTouched(SelectionCollider collider)
        {
            if (collider.View == null) return false;
            if (_allowedToTouch != null && collider.View != _allowedToTouch) return false;
            
            switch (collider.Type)
            {
                case SelectionColliderType.Room:
                    var room = (RoomView) collider.View;
                    switch (room.Type)
                    {
                        case RoomType.Cemetery:
                            _windows.OpenWindow<CemeteryWindow>();
                            break;
                        
                        case RoomType.Director:
                            _windows.OpenWindow<StatisticWindow>();
                            break;
                        
                        case RoomType.Cryogenic:
                        case RoomType.Cremation:
                            if (room.Level == 0)
                            {
                                _windows.OpenWindow<NewBuildingWindow>(collider.View);
                            }
                            break;
                        
                        case RoomType.CorpsePorter:
                            if (room.Level == 0)
                            {
                                _windows.OpenWindow<NewBuildingWindow>(collider.View);  
                            }
                            break;
                        
                        default:
                            if (room.Level == 0)
                            {
                                _windows.OpenWindow<NewBuildingWindow>(collider.View);
                                break;
                            }
                            _windows.OpenWindow<RoomInfoWindow>(collider.View);
                            break;
                    }

                    break;
                
                case SelectionColliderType.Item:
                    var item = (RoomItemView) collider.View;
                    if(item.Level == 0) return false;
                    if(item.Room.Level == 0) return false;
                    _windows.OpenWindow<RoomInfoWindow>(item.Room, item);
                    break;
                
                case SelectionColliderType.Character:
                    var character = (CharacterView) collider.View;
                    return character.Touch();
                case SelectionColliderType.ClickableEvent:
                    var view = (ClickableEventView) collider.View;
                    return view.Touch();
            }

            _allowedToTouch = null;
            return true;
        }
        
        public void StopScroll()
        {
            _k = 0;
        }

        public void BlockScroll()
        {
            ClearAllFlags();
            _ignoreDrag = true;
        }

        public void BlockTouch()
        {
            ClearAllFlags();
            _ignoreTaps = true;
        }

        public void UnblockInput()
        {
            _ignoreDrag = false;
            _ignoreTaps = false;
            StopScroll();
        }

        public void SetAllowedToTouch(BaseView view)
        {
            ClearAllFlags();
            _allowedToTouch = view;
            _ignoreTaps = false;
        }
    }
}