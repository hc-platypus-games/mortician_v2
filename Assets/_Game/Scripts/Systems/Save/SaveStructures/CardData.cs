using System;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    [Serializable]
    public struct CardData
    {
        public int Id;
        public int Amount;
        public int Level;
        public bool Assigned;
    }
}