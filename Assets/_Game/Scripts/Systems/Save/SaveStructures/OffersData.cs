using System;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    [Serializable]
    public struct OffersData
    {
        public int Id;
        public int Count;
        public double Timer;
    }
}