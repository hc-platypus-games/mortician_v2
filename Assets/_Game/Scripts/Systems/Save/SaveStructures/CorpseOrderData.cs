using System;
using System.Collections.Generic;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.ScriptableObjects;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    [Serializable]
    public struct CorpseOrderData
    {
        public RoomType RoomType;
        public List<Reward> Rewards;
        public string Text;
        public int Sprite;
        public CorpseOrderState State;
    }
}