using System;
using _Game.Scripts.Enums;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    [Serializable]
    public struct CharacterData
    {
        public RoomType RoomType;
        public CharacterType Type;
    }
}
