using System;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    [Serializable]
    public struct TaskData
    {
        public int Id;
        public float CurrentCount;
        public bool Finished;
    }
}
