using System;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    public enum CorpseState
    {
        Delivered,
        Embalmed,
        WaitInGrave,
    }
    
    [Serializable]
    public struct CorpseData
    {
        public int SKinId;
        public bool IsGold;
        public CorpseState State;
    }
}
