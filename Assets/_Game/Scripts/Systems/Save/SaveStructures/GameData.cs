﻿using System;
using System.Collections.Generic;
using _Game.Scripts.Enums;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    [Serializable]
    public class GameData
    {
        public int MapId;
        public string DailyRestartTime;
        public string TimeStr;
        public GameSettingsData GameSettingsData;
        public GameSystemData GameSystemData;
        public List<RoomData> Rooms;
        public List<RoomItemData> RoomItems;
        public List<CharacterData> Characters;
        public List<CardData> Cards;
        public List<GameFlag> Flags;
        public int LastTaskId;
        public List<TaskData> Tasks;
        public List<BoostData> Boosts;
        public List<MapEventData> MapEvents;
        public List<CorpseData> Corpses;
        public List<CorpseOrderData> CorpseOrders;
        public List<OffersData> Offers;
        public List<GameProgress> CemeteryProgresses;
    }
}