using System;
using _Game.Scripts.Systems.Boosts;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    [Serializable]
    public struct BoostData
    {
        public int Id;
        public Boost.BoostState State;
        public float LeftDuration;
        public int Counter;
    }
}