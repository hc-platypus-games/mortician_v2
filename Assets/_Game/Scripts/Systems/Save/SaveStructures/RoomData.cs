using System;
using _Game.Scripts.Enums;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    [Serializable]
    public struct RoomData
    {
        public RoomType Type;
        public int Level;
        public int BuildingTimerValue;
    }
}