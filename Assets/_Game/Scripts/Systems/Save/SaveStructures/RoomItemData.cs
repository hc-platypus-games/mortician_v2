using System;
using _Game.Scripts.Enums;

namespace _Game.Scripts.Systems.Save.SaveStructures
{
    [Serializable]
    public struct RoomItemData
    {
        public RoomType Type;
        public int Id;
        public int Level;
        public int BuildingTimerValue;
    }
}