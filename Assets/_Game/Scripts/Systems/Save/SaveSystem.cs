using System;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.Interfaces;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Systems.Tasks;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Systems.Save
{
    /// <summary>
    /// Данный класс для загрузки и сохранения данных
    /// </summary>
    public class SaveSystem : ITickableSystem
    {
        [Inject] private ProjectSettings _projectSettings;
        [Inject] private GameSettings _gameSettings;
        [Inject] private GameParamFactory _params;
        [Inject] private GameSystem _game;
        [Inject] private RoomsFactory _rooms;
        [Inject] private CardsFactory _cards;
        [Inject] private CharactersFactory _characters;
        [Inject] private GameFlags _flags;
        [Inject] private TaskSystem _tasks;
        [Inject] private BoostSystem _boosts;
        [Inject] private LevelEventsFactory _levelEvents;
        [Inject] private CorpseFactory _corpses;
        [Inject] private OffersFactory _offers;
        
        private const string KEY = "snapshot";
        private Snapshot _snapshot;

        private const float SAVE_TIME = 3f;
        private float _saveTimer;

        public bool IsSave { get; private set; }
        public DateTime SaveTime { get; private set; }

        public void Init()
        {
            _snapshot = new Snapshot();
        }
        
        public void Save()
        {
            SaveTime = DateTime.Now;
            SaveData();
            var json = JsonUtility.ToJson(_snapshot);
            PlayerPrefs.SetString(KEY, json);
            PlayerPrefs.Save();
        }
        
        private void SaveData()
        {
            _snapshot.InitAfterSave(_game.MapId);
            _snapshot.SaveFlagsData(_flags);
            _snapshot.SaveGameSettings(_gameSettings);
            _snapshot.SaveCurrentData(_game);
            _snapshot.SaveGameSystemData(_params);
            _snapshot.SaveRoomsData(_rooms);
            _snapshot.SaveRoomItemsData(_rooms);
            _snapshot.SaveCharactersData(_characters);
            _snapshot.SaveCardsData(_cards);
            _snapshot.SaveTaskData(_tasks);
            _snapshot.SaveBoostData(_boosts);
            _snapshot.SaveMapEventData(_levelEvents);
            _snapshot.SaveCorpsesData(_corpses);
            _snapshot.SaveCorpseOrdersData(_corpses);
            _snapshot.SaveOffersData(_offers);
        }
        
        public void Load()
        {
            if (!PlayerPrefs.HasKey(KEY)) return;
            
            if (_projectSettings.ClearSaves)
            { 
                DeleteSave();
                return;
            }
            
            try
            {
                var json = PlayerPrefs.GetString(KEY);
                _snapshot = JsonUtility.FromJson<Snapshot>(json);
                LoadData();
            }
            catch (Exception e)
            {
                Debug.LogWarning($"(0004) Error. Load preview save. {e}");
                DeleteSave();
            }
        }
        
        private void LoadData()
        {
            _snapshot.InitBeforeLoad();
            
            if (_snapshot.WasSavedInMainTutor)
            {
                DeleteSave();
                return;
            }
            
            _snapshot.LoadFlagsData(_flags);
            _snapshot.LoadGameSettingsData(_gameSettings);
            _snapshot.LoadGameDataSystem(_params, _game);
            _snapshot.LoadRoomsData(_rooms);
            _snapshot.LoadRoomItemsData(_rooms);
            _snapshot.LoadCharactersData(_characters);
            _snapshot.LoadCardsData(_cards);
            _snapshot.LoadTasksData(_tasks);
            _snapshot.LoadBoostsData(_boosts);
            _snapshot.LoadMapEventsData(_levelEvents);
            _snapshot.LoadCorpsesData(_corpses);
            _snapshot.LoadCorpseOrdersData(_corpses);
            _snapshot.LoadOffersData(_offers);
            
            IsSave = true;
            SaveTime = _snapshot.SaveTime;
        }

        private void DeleteSave()
        {
            SaveTime = DateTime.Now;
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }

        public void Tick(float deltaTime)
        {
            _saveTimer += deltaTime;
            if (_saveTimer >= SAVE_TIME)
            {
                Save();
                _saveTimer = 0;
            }
        }
    }
}