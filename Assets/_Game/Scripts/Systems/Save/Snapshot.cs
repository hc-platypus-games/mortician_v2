using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using _Game.Scripts.Enums;
using _Game.Scripts.Factories;
using _Game.Scripts.Factories.CorpseFactory;
using _Game.Scripts.Factories.Offers;
using _Game.Scripts.ScriptableObjects;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Systems.Boosts;
using _Game.Scripts.Systems.Save.SaveStructures;
using _Game.Scripts.Systems.Tasks;
using _Game.Scripts.Ui.Offers;
using UnityEngine;

namespace _Game.Scripts.Systems.Save
{
    public class Snapshot
    {
        [SerializeField] private List<GameData> _gameData = new();

        private GameData _currentData;
        public DateTime SaveTime { get; private set; }

        public bool WasSavedInMainTutor => _currentData.Flags.Contains(GameFlag.TutorialRunning) &&
                                           !_currentData.Flags.Contains(GameFlag.TutorialFinished);

        public void InitAfterSave(int mapId)
        {
            _currentData = _gameData.FirstOrDefault(d => d.MapId == mapId);

            if (_currentData == null)
            {
                _currentData = new GameData
                {
                    GameSystemData = new GameSystemData(),
                    Rooms = new List<RoomData>(),
                    RoomItems = new List<RoomItemData>(),
                    Characters = new List<CharacterData>(),
                    Cards = new List<CardData>(),
                    Flags = new List<GameFlag>(),
                    Tasks = new List<TaskData>(),
                    Boosts = new List<BoostData>(),
                    MapEvents = new List<MapEventData>(),
                    Corpses = new List<CorpseData>(),
                    CorpseOrders = new List<CorpseOrderData>(),
                    Offers = new List<OffersData>(),
                    CemeteryProgresses = new List<GameProgress>()
                };

                _currentData.GameSystemData.Params = new List<GameParamsData>();
                
                _gameData.Add(_currentData);
            }
            else
            {
                _currentData.GameSystemData.Params.Clear();
                _currentData.Rooms.Clear();
                _currentData.RoomItems.Clear();
                _currentData.Characters.Clear();
                _currentData.Cards.Clear();
                _currentData.Flags.Clear();
                _currentData.Tasks.Clear();
                _currentData.Boosts.Clear();
                _currentData.MapEvents.Clear();
                _currentData.Corpses.Clear();
                _currentData.CorpseOrders.Clear();
                _currentData.Offers.Clear();
                _currentData.CemeteryProgresses.Clear();
            }
        }

        public void InitBeforeLoad()
        {
            _currentData = _gameData.FirstOrDefault();
            SaveTime = DateTime.Parse(_currentData?.TimeStr, CultureInfo.InvariantCulture);
        }

        public void SaveCurrentData(GameSystem game)
        {
            _currentData.MapId = game.MapId;
            _currentData.DailyRestartTime = game.DailyRestartTime.ToString(CultureInfo.InvariantCulture);
            _currentData.TimeStr = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        public void SaveGameSettings(GameSettings gameSettings)
        {
            _currentData.GameSettingsData.IsMuteMusic = gameSettings.IsMuteMusic;
            _currentData.GameSettingsData.IsMuteSound = gameSettings.IsMuteSound;
            _currentData.GameSettingsData.IsDisablePushNotifications = gameSettings.IsDisablePushNotifications;
        }
        
        public void SaveGameSystemData(GameParamFactory paramFactory)
        {
            var dataParams = paramFactory.GetSavedParams<GameSystem>();
            foreach (var param in dataParams)
            {
                _currentData.GameSystemData.Params.Add(new GameParamsData {Type = param.Type, Value = param.Value});
            }
        }

        public void SaveRoomsData(RoomsFactory rooms)
        {
            foreach (var room in rooms.GetRooms())
            {
                _currentData.Rooms.Add(new RoomData
                {
                    Type = room.Type,
                    Level = room.Level,
                    BuildingTimerValue = room.GetBuildingProgressValue()
                });
            }
        }
        
        public void SaveRoomItemsData(RoomsFactory rooms)
        {
            foreach (var item in rooms.GetRoomItems())
            {
                _currentData.RoomItems.Add(new RoomItemData
                {
                    Type = item.RoomType,
                    Id = item.Id,
                    Level = item.Level,
                    BuildingTimerValue = item.GetBuildingProgressValue()
                });
            }
        }
        
        public void SaveCharactersData(CharactersFactory characters)
        {
            foreach (var item in characters.Characters)
            {
                if(!item.Config.Savable) continue;
                _currentData.Characters.Add(new CharacterData
                {
                    RoomType = item.Config.Room,
                    Type = item.Config.Type
                });
            }

            if (characters.Data.Count == 0) return;
            
            foreach (var item in characters.Data)
            {
                _currentData.Characters.Add(item);
            }
        }
        
        public void SaveFlagsData(GameFlags flags)
        {
            foreach (var newFlag in flags.Flags)
            {
                _currentData.Flags.Add(newFlag);
            }
        }
        
        public void SaveTaskData(TaskSystem taskSystem)
        {
            _currentData.LastTaskId = taskSystem.LastTaskId - taskSystem.Tasks.Count;
            foreach (var item in taskSystem.Tasks)
            {
                _currentData.Tasks.Add(new TaskData
                {
                    Id = item.ID,
                    CurrentCount = item.CurrentCount,
                    Finished = item.IsFinished
                });
            }
        }

        public void SaveCorpseOrders(CorpseFactory corpse)
        {
            
        }

        public void SaveCardsData(CardsFactory cards)
        {
            foreach (var card in cards.All)
            {
                _currentData.Cards.Add(new CardData
                {
                    Id = card.Id,
                    Amount = card.Amount,
                    Level = card.Level,
                    Assigned = card.Assigned
                });
            }
        }

        public void SaveBoostData(BoostSystem boosts)
        {
            foreach (var boost in boosts.Boosts)
            {
                _currentData.Boosts.Add(new BoostData
                {
                    Id = boost.Config.Id,
                    LeftDuration = boost.LeftDuration,
                    Counter = boost.Counter,
                    State = boost.State
                });
            }
        }

        public void SaveMapEventData(LevelEventsFactory events)
        {
            foreach (var item in events.EventsQueue)
            {
                _currentData.MapEvents.Add(new MapEventData
                {
                    Id = item.Config.Id,
                    Called = false,
                    Bought = item.IsBought
                });
            }
            
            foreach (var item in events.EventsCalledQueue)
            {
                _currentData.MapEvents.Add(new MapEventData
                {
                    Id = item.Config.Id,
                    Called = true,
                    Bought = item.IsBought
                });
            }

            foreach (var item in events.MapEvents)
            {
                if (_currentData.MapEvents.Count(i => i.Id == item.Id) > 0) continue;
                if(!item.Timer.IsActive) return;
                _currentData.MapEvents.Add(new MapEventData
                {
                    Id = item.Config.Id,
                    Called = false,
                    Bought = item.IsBought,
                    BuildingTime = (int)item.Timer.CurrentValue
                });
            }
        }

        public void SaveCorpsesData(CorpseFactory corpses)
        {
            foreach (var item in corpses.DeliveredCorpses)
            {
                _currentData.Corpses.Add(new CorpseData
                {
                    SKinId = item.SkinId,
                    State = CorpseState.Delivered,
                    IsGold = item.IsGold
                });
            }
            foreach (var item in corpses.EmbalmedCorpses)
            {
                _currentData.Corpses.Add(new CorpseData
                {
                    SKinId = item.SkinId,
                    State = CorpseState.Embalmed,
                    IsGold = item.IsGold
                });
            }
            foreach (var item in corpses.CemeteryCorpses)
            {
                _currentData.Corpses.Add(new CorpseData
                {
                    SKinId = item.SkinId,
                    State = CorpseState.WaitInGrave,
                    IsGold = item.IsGold
                });
            }

            foreach (var progress in corpses.CemeteryProgresses)
            {
                _currentData.CemeteryProgresses.Add(progress);
            }
        }

        public void SaveCorpseOrdersData(CorpseFactory corpses)
        {
            foreach (var item in corpses.SavableOrders)
            {
                var rewards = new List<Reward>();
                foreach (var reward in item.Rewards)
                {
                    rewards.Add(new Reward()
                    {
                        Type = reward.Type,
                        Amount = reward.Amount,
                        Id = reward.Id
                    });
                }
                
                _currentData.CorpseOrders.Add(new CorpseOrderData
                {
                    RoomType = item.RoomType,
                    State = item.State == CorpseOrderState.Preparing || item.State == CorpseOrderState.InProgress ? CorpseOrderState.Preparing : item.State,
                    Rewards = rewards,
                    Text = item.Text,
                    Sprite = item.AvatarId
                });
            }
        }
        
        public void SaveOffersData(OffersFactory offers)
        {
            var items = offers.GetSavedOffers();
            foreach (var item in items)
            {
                var offer = (StoreOffer)item;
                _currentData.Offers.Add(new OffersData
                {
                    Id = offer.Id,
                    Count = offer.Count,
                    Timer = offer.CycleDelay
                });
            }
        }
        
        public void LoadGameSettingsData(GameSettings gameSettings)
        {
            gameSettings.MuteMusic = _currentData.GameSettingsData.IsMuteMusic;
            gameSettings.MuteSound = _currentData.GameSettingsData.IsMuteSound;
            gameSettings.DisablePushNotifications = _currentData.GameSettingsData.IsDisablePushNotifications;
        }
        
        public void LoadGameDataSystem(GameParamFactory paramFactory, GameSystem game)
        {
            if (_currentData.DailyRestartTime == null) _currentData.DailyRestartTime = _currentData.TimeStr;
            var date = DateTime.Parse(_currentData.DailyRestartTime, CultureInfo.InvariantCulture);
            game.SetGameParams(date);
            foreach (var sourceParam in _currentData.GameSystemData.Params)
            {
                var param = paramFactory.GetParam<GameSystem>(sourceParam.Type);
                param?.SetValue(sourceParam.Value);
            }
        }

        public void LoadRoomsData(RoomsFactory rooms)
        {
            foreach (var data in _currentData.Rooms)
            {
                var room = rooms.GetRoom(data.Type);
                if (room != null) room.SetRoomParams(data.Level, data.BuildingTimerValue);
            }
        }
        
        public void LoadRoomItemsData(RoomsFactory rooms)
        {
            foreach (var data in _currentData.RoomItems)
            {
                var room = rooms.GetRoomItem(data.Type, data.Id);
                if (room != null) room.SetRoomParams(data.Level, data.BuildingTimerValue);
            }
        }
        
        public void LoadCharactersData(CharactersFactory characters)
        {
            characters.LoadFromSave(_currentData.Characters);
        }
        
        public void LoadFlagsData(GameFlags flags)
        {
            foreach (var data in _currentData.Flags)
            {
                flags.Set(data);
            }
        }
        
        public void LoadCardsData(CardsFactory cards)
        {
            foreach (var data in _currentData.Cards)
            {
                var card = cards.GetCard(data.Id);
                if (card != null) card.SetCardParams(data.Amount, data.Level, data.Assigned);
            }
        }
        
        public void LoadTasksData(TaskSystem tasks)
        {
            tasks.LoadFromSave(_currentData.LastTaskId, _currentData.Tasks.ToArray());
        }
        
        public void LoadBoostsData(BoostSystem boosts)
        {
            foreach (var data in _currentData.Boosts)
            {
                var boost = boosts.GetBoostById(data.Id);
                boost?.SetParams(data.State, data.LeftDuration, data.Counter);
            }
        }

        public void LoadMapEventsData(LevelEventsFactory levelEvents)
        {
            foreach (var item in _currentData.MapEvents)
            {
                levelEvents.LoadFromData(item);
            }
        }

        public void LoadCorpsesData(CorpseFactory corpses)
        {
            corpses.LoadFromData(_currentData.CemeteryProgresses);
            
            foreach (var item in _currentData.Corpses)
            {
                corpses.LoadFromData(item);
            }
        }

        public void LoadCorpseOrdersData(CorpseFactory corpses)
        {
            foreach (var item in _currentData.CorpseOrders)
            {
                corpses.LoadFromData(item);
            }
        }
        
        public void LoadOffersData(OffersFactory offers)
        {
            foreach (var item in _currentData.Offers)
            {
                var offer = offers.GetOfferById(item.Id);
                if (offer != null)
                {
                    offer.LoadFromData(item.Count, item.Timer);
                }
            }
        }
    }
}