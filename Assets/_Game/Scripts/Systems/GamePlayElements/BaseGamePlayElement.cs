﻿using _Game.Scripts.Enums;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Base;
using UnityEngine;

namespace _Game.Scripts.Systems.GamePlayElements
{
    public class BaseGamePlayElement : BaseUIView
    {
        [SerializeField] private GamePlayElement _type;
        [SerializeField] private BaseButton _button;
        
        protected WindowsSystem Windows; 
        
        private BlindEffect _blindEffect;
        private Transform _parent;
        private RectTransform _rect;
        private Vector2 _defaultPos;
        
        public BaseButton Button => _button;
        public GamePlayElement Type => _type;

        public virtual void Init(WindowsSystem windows)
        {
            if (_button != null) _button.SetCallback(OnPressedButton);

            Windows = windows;
            
            _parent = transform.parent;
            _rect = GetComponent<RectTransform>();
            _defaultPos = _rect.anchoredPosition;
            _blindEffect = GetComponentInChildren<BlindEffect>(true)?.Init(false);
        }

        protected void SetType(GamePlayElement type)
        {
            _type = type;
        }

        public void Blind(bool activate)
        {
            if(!_blindEffect) return;
            _blindEffect.SetActive(activate);
        }

        public virtual void RestorePosition()
        {
            transform.SetParent(_parent);
            _rect.anchoredPosition = _defaultPos;
        }

        protected virtual void OnPressedButton()
        {
            
        }

        public virtual void SetActive(bool value)
        {
            if(gameObject.activeSelf != value) gameObject.SetActive(value);
        }
    }
}
