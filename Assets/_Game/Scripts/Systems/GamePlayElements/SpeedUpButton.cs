using System;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.Systems.GamePlayElements
{
    public class SpeedUpButton : MainWindowButton
    {
        [SerializeField] private RectTransform _animatorContainer;
        [SerializeField] private RectTransform _animatorBack;
        private Tween _positionTween;
        private Tween _backTween;
        
        public override void Init(WindowsSystem windows)
        {
            base.Init(windows);
            
            SetActive(false);
            Button.OnDown += OnDown;
            Button.OnUp += OnUp;
            Button.OnExit += OnUp;

            _animatorBack.localScale = Vector3.zero;
        }

        private void OnDown()
        {
            CheatsSystem.CheckTimeScale(5);
            _positionTween?.Kill();
            _backTween?.Kill();
            _positionTween = _animatorContainer.DOAnchorPosY(210.5f, 0.5f);
            _backTween = _animatorBack.DOScale(1f, 1f);
        }

        private void OnUp()
        {
            CheatsSystem.CheckTimeScale(1);
            _positionTween?.Kill();
            _backTween?.Kill();
            _positionTween = _animatorContainer.DOAnchorPosY(10.5f, 0.2f);
            _backTween = _animatorBack.DOScale(0f, 0.2f);
        }

        public override void SetActive(bool value)
        {
            base.SetActive(value);
            OnUp();
        }
    }
}
