﻿using _Game.Scripts.Enums;
using _Game.Scripts.Systems.Base;
using _Game.Scripts.Tools;
using _Game.Scripts.Ui;
using _Game.Scripts.Ui.Cards;
using _Game.Scripts.Ui.CorpseOrders;
using _Game.Scripts.Ui.Locations;
using _Game.Scripts.Ui.Maps;
using _Game.Scripts.Ui.Offers;
using _Game.Scripts.Ui.Statistic;
using _Game.Scripts.Ui.Tasks;
using Zenject;

namespace _Game.Scripts.Systems.GamePlayElements
{
    public class MainWindowButton : BaseGamePlayElement
    {

        protected override void OnPressedButton()
        {
            switch (Type)
            {
                case GamePlayElement.StoreButton:
                    Windows.OpenWindow<StoreWindow>();
                    break;
                
                case GamePlayElement.CardsButton:
                    Windows.OpenWindow<CardsWindow>();
                    break;
                
                case GamePlayElement.TasksButton:
                    Windows.OpenWindow<TaskListWindow>();
                    break;
                
                case GamePlayElement.SettingsButton:
                    Windows.OpenWindow<GameSettingsWindow>();
                    break;
                
                case GamePlayElement.LocationsButton:
                    Windows.OpenWindow<LocationsWindow>();
                    break;
                
                case GamePlayElement.VideoBoostButton:
                    Windows.OpenWindow<VideoBoostWindow>();
                    break;
                
                case GamePlayElement.CorpseOrdersButton:
                    Windows.OpenWindow<CorpseOrdersWindow>();
                    break;
                
                case GamePlayElement.StatisticButton:
                    Windows.OpenWindow<StatisticWindow>();
                    break;
                
                case GamePlayElement.MapButtons:
                    Windows.OpenWindow<MapsWindow>();
                    break;
            }
        }
    }
}