using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace _Game.Scripts.Pool
{
    public abstract class MultiPrefabPool<TParam1, TValue> where TValue : Component, IReinitializable  //TParam1 must be enum
    {
        Dictionary<TParam1, CustomMonoMemeryPool<TValue>> _pools = new Dictionary<TParam1, CustomMonoMemeryPool<TValue>>();
        DiContainer _container;
        MemoryPoolSettings _settings;
        IFactory<TParam1, TValue> _factory;
        bool registrated = false;
        public MultiPrefabPool(DiContainer container, MemoryPoolSettings settings, IFactory<TParam1, TValue> factory)
        {
            _container = container;
            _settings = settings;
            _factory = factory;

        }
        public TValue Spawn(TParam1 param)
        {
            if (!registrated) //I wanted to register all pools in constructor and dont check anything in Spawn, but circular dependency arise so I thought that checking bool is okey
            {
                registrated = true;
                RegisterAllPools();
            }

            return _pools[param].Spawn();
        }
        public void DeSpawn(TParam1 param, TValue gameObject)
        {
            _pools[param].Despawn(gameObject);
        }

        void RegisterAllPools() 
        {
            var enumValues = Enum.GetValues(typeof(TParam1));
            for (int i = 0; i < enumValues.Length; i++)
            {
                var nEnum = (TParam1)enumValues.GetValue(i);
                var nPool = CreatePool(nEnum );
                _pools.Add(nEnum, nPool);
            }
        }
        CustomMonoMemeryPool<TValue> CreatePool(TParam1 param)
        {
            return _container.Instantiate<CustomMonoMemeryPool<TValue>>(new object[] { _settings, new FactoryProxy<TValue>(param, _factory) });
        }
        class FactoryProxy<TValue> : IFactory<TValue> //Factory which proxy all creation to my old factory
        {
            TParam1 _param;
            IFactory<TParam1, TValue> _factory;
            public FactoryProxy(TParam1 param, IFactory<TParam1, TValue> factory)
            {
                _param = param;
                _factory = factory;
            }
            public TValue Create()
            {
                return _factory.Create(_param);
            }
        }
        class CustomMonoMemeryPool<TValue> : MonoMemoryPool<TValue> where TValue : Component, IReinitializable 
        {
            protected override void Reinitialize(TValue item)
            {
                item.Reinitialize();
            }
        }
    }

    public interface IReinitializable
    {
        void Reinitialize();
    }
}