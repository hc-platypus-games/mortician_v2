using System.Collections;
using AppQuantum.IAP.Runtime;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace AppQuantum.IAP.Tests
{
    [TestFixture]
    public class GoogleValidationTests
    {
        private const string localhost = "http://localhost:3000/";
        private const string remote = "https://iap.appquantum.com/";
        private const string ServerAddress = localhost;

        //convert strings form google via http://easyonlineconverter.com/converters/dot-net-string-escape.html
        private static readonly RequestModel[] ValidRequestModels =
        {
            new RequestModel(
                "00000000-0000-0000-0000-000000000000",
                "00000000-0000-0000-0000-000000000000",
                "",
                "com.appquantum.iap.test_success",
                "com.appquantum.idlefarmer",
                "{\"Store\":\"GooglePlay\",\"TransactionID\":\"GPA.3323-4503-4616-00048\",\"Payload\":\"{\\\"json\\\":\\\"{\\\\\\\"orderId\\\\\\\":\\\\\\\"GPA.3323-4503-4616-00048\\\\\\\",\\\\\\\"packageName\\\\\\\":\\\\\\\"com.appquantum.idlefarmer\\\\\\\",\\\\\\\"productId\\\\\\\":\\\\\\\"com.appquantum.idlefarmer.gold_1\\\\\\\",\\\\\\\"purchaseTime\\\\\\\":1591636055126,\\\\\\\"purchaseState\\\\\\\":0,\\\\\\\"developerPayload\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"developerPayload\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"is_free_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"has_introductory_price_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"is_updated\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"accountId\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}\\\\\\\",\\\\\\\"purchaseToken\\\\\\\":\\\\\\\"demcicojjookeaniabgoohda.AO-J1OzvSyKVEhrPV-sZcoNcrvRpPbhDqLmQI4RO_yEU-6pxNZy8q1MPw9Iv5V-HxpZurxIiuD6niG1Xe-rYwaOGjzd1zk16o-FHqlCNtoOEzglb3FUrFEcTTy5Bq4W1sE1ZEMGCXDFqAEgCCiSFqHgXDvDVr0k_ag\\\\\\\"}\\\",\\\"signature\\\":\\\"UzkkE9Sd\\\\/zl4l5S8uuzCnPacTreI1E1AU6qFfZEZS5wBijSyT8XycFECZCWQn4kp0uRIUkCmFLgAbChzXUBHRjllYbqBnD5uHKtpxNNBJ9ARR6\\\\/SYdBz1BiyL9lgSujYX6nJ1dRan+5HRH81FiYqjQ\\\\/0xYUJ+fsp3me917F+TpStUUKockltSszR0Vk7My2B4VFRj+ynSF2fle7bKG9y7vwb2YtdxrmIOXdIdtmDpzCkDWR5ecWEFo8XUH1eVYwsrdnLg5ZPtEr/a/A6UuUwA/5Mm1vLkNu5An4B34DgxmXVOCNrObXUxqJ8Uq1LlVn1Aldd8JFWgqExg8RQ5JE5UA==\\\"}\"}",
                "0.0.1",
                false
            ),
        };

        private static readonly RequestModel[] InvalidRequestModels =
        {
            //Invalid idfv
            new RequestModel(
                "00000000-0000-0000-0000-000000000000",
                "",
                "",
                "com.appquantum.iap.test_failed",
                "com.appquantum.idlefarmer",
                "{\"Store\":\"GooglePlay\",\"TransactionID\":\"GPA.3323-ну н4503-4616-00048\",\"Payload\":\"{\\\"json\\\":\\\"{\\\\\\\"orderId\\\\\\\":\\\\\\\"GPA.3323-4503-4616-00048\\\\\\\",\\\\\\\"packageName\\\\\\\":\\\\\\\"com.appquantum.idlefarmer\\\\\\\",\\\\\\\"productId\\\\\\\":\\\\\\\"com.appquantum.idlefarmer.gold_1\\\\\\\",\\\\\\\"purchaseTime\\\\\\\":1591636055126,\\\\\\\"purchaseState\\\\\\\":0,\\\\\\\"developerPayload\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"developerPayload\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"is_free_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"has_introductory_price_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"is_updated\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"accountId\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}\\\\\\\",\\\\\\\"purchaseToken\\\\\\\":\\\\\\\"demcicojjookeaniabgoohda.AO-J1OzvSyKVEhrPV-sZcoNcrvRpPbhDqLmQI4RO_yEU-6pxNZy8q1MPw9Iv5V-HxpZurxIiuD6niG1Xe-rYwaOGjzd1zk16o-FHqlCNtoOEzglb3FUrFEcTTy5Bq4W1sE1ZEMGCXDFqAEgCCiSFqHgXDvDVr0k_ag\\\\\\\"}\\\",\\\"signature\\\":\\\"UzkkE9Sd\\\\/zl4l5S8uuzCnPacTreI1E1AU6qFfZEZS5wBijSyT8XycFECZCWQn4kp0uRIUkCmFLgAbChzXUBHRjllYbqBnD5uHKtpxNNBJ9ARR6\\\\/SYdBz1BiyL9lgSujYX6nJ1dRan+5HRH81FiYqjQ\\\\/0xYUJ+fsp3me917F+TpStUUKockltSszR0Vk7My2B4VFRj+ynSF2fle7bKG9y7vwb2YtdxrmIOXdIdtmDpzCkDWR5ecWEFo8XUH1eVYwsrdnLg5ZPtEr/a/A6UuUwA/5Mm1vLkNu5An4B34DgxmXVOCNrObXUxqJ8Uq1LlVn1Aldd8JFWgqExg8RQ5JE5UA==\\\"}\"}",
                "0.0.1",
                false
            ),

            //Invalid productID
            new RequestModel(
                "00000000-0000-0000-0000-000000000000",
                "00000000-0000-0000-0000-000000000000",
                "",
                "com.appquantum.idlefarmer.gold_2",
                "com.appquantum.idlefarmer",
                "{\"Store\":\"GooglePlay\",\"TransactionID\":\"GPA.3323-4503-4616-00048\",\"Payload\":\"{\\\"json\\\":\\\"{\\\\\\\"orderId\\\\\\\":\\\\\\\"GPA.3323-4503-4616-00048\\\\\\\",\\\\\\\"packageName\\\\\\\":\\\\\\\"com.appquantum.idlefarmer\\\\\\\",\\\\\\\"productId\\\\\\\":\\\\\\\"com.appquantum.idlefarmer.gold_1\\\\\\\",\\\\\\\"purchaseTime\\\\\\\":1591636055126,\\\\\\\"purchaseState\\\\\\\":0,\\\\\\\"developerPayload\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"developerPayload\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"is_free_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"has_introductory_price_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"is_updated\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"accountId\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}\\\\\\\",\\\\\\\"purchaseToken\\\\\\\":\\\\\\\"demcicojjookeaniabgoohda.AO-J1OzvSyKVEhrPV-sZcoNcrvRpPbhDqLmQI4RO_yEU-6pxNZy8q1MPw9Iv5V-HxpZurxIiuD6niG1Xe-rYwaOGjzd1zk16o-FHqlCNtoOEzglb3FUrFEcTTy5Bq4W1sE1ZEMGCXDFqAEgCCiSFqHgXDvDVr0k_ag\\\\\\\"}\\\",\\\"signature\\\":\\\"UzkkE9Sd\\\\/zl4l5S8uuzCnPacTreI1E1AU6qFfZEZS5wBijSyT8XycFECZCWQn4kp0uRIUkCmFLgAbChzXUBHRjllYbqBnD5uHKtpxNNBJ9ARR6\\\\/SYdBz1BiyL9lgSujYX6nJ1dRan+5HRH81FiYqjQ\\\\/0xYUJ+fsp3me917F+TpStUUKockltSszR0Vk7My2B4VFRj+ynSF2fle7bKG9y7vwb2YtdxrmIOXdIdtmDpzCkDWR5ecWEFo8XUH1eVYwsrdnLg5ZPtEr/a/A6UuUwA/5Mm1vLkNu5An4B34DgxmXVOCNrObXUxqJ8Uq1LlVn1Aldd8JFWgqExg8RQ5JE5UA==\\\"}\"}",
                "0.0.1",
                false
            ),

            //Invalid TransactionID
            new RequestModel(
                "00000000-0000-0000-0000-000000000000",
                "00000000-0000-0000-0000-000000000000",
                "",
                "com.appquantum.idlefarmer.gold_1",
                "com.appquantum.idlefarmer",
                "{\"Store\":\"GooglePlay\",\"TransactionID\":\"GPA.3323-4503-4616-00049\",\"Payload\":\"{\\\"json\\\":\\\"{\\\\\\\"orderId\\\\\\\":\\\\\\\"GPA.3323-4503-4616-00048\\\\\\\",\\\\\\\"packageName\\\\\\\":\\\\\\\"com.appquantum.idlefarmer\\\\\\\",\\\\\\\"productId\\\\\\\":\\\\\\\"com.appquantum.idlefarmer.gold_1\\\\\\\",\\\\\\\"purchaseTime\\\\\\\":1591636055126,\\\\\\\"purchaseState\\\\\\\":0,\\\\\\\"developerPayload\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"developerPayload\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"is_free_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"has_introductory_price_trial\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"is_updated\\\\\\\\\\\\\\\":false,\\\\\\\\\\\\\\\"accountId\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}\\\\\\\",\\\\\\\"purchaseToken\\\\\\\":\\\\\\\"demcicojjookeaniabgoohda.AO-J1OzvSyKVEhrPV-sZcoNcrvRpPbhDqLmQI4RO_yEU-6pxNZy8q1MPw9Iv5V-HxpZurxIiuD6niG1Xe-rYwaOGjzd1zk16o-FHqlCNtoOEzglb3FUrFEcTTy5Bq4W1sE1ZEMGCXDFqAEgCCiSFqHgXDvDVr0k_ag\\\\\\\"}\\\",\\\"signature\\\":\\\"UzkkE9Sd\\\\/zl4l5S8uuzCnPacTreI1E1AU6qFfZEZS5wBijSyT8XycFECZCWQn4kp0uRIUkCmFLgAbChzXUBHRjllYbqBnD5uHKtpxNNBJ9ARR6\\\\/SYdBz1BiyL9lgSujYX6nJ1dRan+5HRH81FiYqjQ\\\\/0xYUJ+fsp3me917F+TpStUUKockltSszR0Vk7My2B4VFRj+ynSF2fle7bKG9y7vwb2YtdxrmIOXdIdtmDpzCkDWR5ecWEFo8XUH1eVYwsrdnLg5ZPtEr/a/A6UuUwA/5Mm1vLkNu5An4B34DgxmXVOCNrObXUxqJ8Uq1LlVn1Aldd8JFWgqExg8RQ5JE5UA==\\\"}\"}",
                "0.0.1",
                false
            ),

            //Invalid receipt
            new RequestModel(
                "00000000-0000-0000-0000-000000000000",
                "00000000-0000-0000-0000-000000000000",
                "",
                "com.appquantum.idlefarmer.gold_1",
                "com.appquantum.idlefarmer",
                "{}",
                "0.0.1",
                false
            ),
        };

        [UnityTest]
        public IEnumerator TestValidReceipt([ValueSource(nameof(ValidRequestModels))]
            RequestModel requestModel)
        {
            var request = new ProcessServerValidationRequest(requestModel, ServerAddress);
            ResultPurchaseValidation? result = null;
            request.SendRequest(validation => { result = validation; });
            while (result == null)
            {
                yield return null;
            }

            Assert.AreEqual(ResultPurchaseValidation.VerifiedTransaction, result);
        }

        [UnityTest]
        public IEnumerator TestInvalidReceipt([ValueSource(nameof(InvalidRequestModels))]
            RequestModel requestModel)
        {
            var request = new ProcessServerValidationRequest(requestModel, ServerAddress);
            ResultPurchaseValidation? result = null;
            request.SendRequest(validation => { result = validation; });
            while (result == null)
            {
                yield return null;
            }

            Assert.AreEqual(ResultPurchaseValidation.UnverifiedTransaction, result);
        }
    }
}