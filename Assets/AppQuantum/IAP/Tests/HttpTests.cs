using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using AppQuantum.IAP.Runtime;

namespace AppQuantum.IAP.Tests
{
    [TestFixture]
    public class HttpTests
    {
        private const string ServerAddress = "http://127.0.0.1:3000/";

        private static readonly string[] WrongAddresses = {"http://wrongajhbkjghvddress.com/", "http://google.com/"};

        [UnityTest]
        public IEnumerator TestHttpError([ValueSource(nameof(WrongAddresses))] string address)
        {
            var request = new ProcessServerValidationRequest(
                new RequestModel(null, null, null, null, null, null, null, false),
                "http://wrongajhbkjghvddress.com/");
            ResultPurchaseValidation? result = null;
            request.SendRequest(validation => { result = validation; });
            while (result == null)
            {
                yield return null;
            }

            Assert.AreEqual(ResultPurchaseValidation.NetworkError, result);
        }
    }
}