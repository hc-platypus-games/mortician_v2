namespace AppQuantum.IAP.Runtime
{
    public enum ResultPurchaseValidation
    {
        VerifiedTransaction = 0,
        UnverifiedTransaction = 1,
        NetworkError = 2,
        InternalError = 3
    }
}