using System;
using AppQuantum.IAP.Runtime.Utility;
using UnityEngine;
using UnityEngine.Networking;

namespace AppQuantum.IAP.Runtime
{
    public class ProcessServerValidationRequest
    {
#if UNITY_ANDROID
        private const string Api = "api/v1/google/purchase";
#elif UNITY_IOS
		private const string Api = "api/v1/appStore/purchase";
#else
		private const string Api = "";
#endif

        private readonly string _serverAddress;
        private readonly RequestModel _requestModel;

        public ProcessServerValidationRequest(RequestModel request, string serverAddress)
        {
            _requestModel = request;
            _serverAddress = serverAddress;
        }


        public void SendRequest(Action<ResultPurchaseValidation> callback)
        {
            var www = CreateWebRequest();

            try
            {
                Debug.Log("Try send request.");
                var sendWebRequest = www.SendWebRequest();
                sendWebRequest.completed += operation => { OnSuccess(); };
            }
            catch (Exception e)
            {
                Debug.Log($"[IAPServerValidation] Failed send request (internal error) {e}");
                callback?.Invoke(ResultPurchaseValidation.InternalError);
            }


            void OnSuccess()
            {
                Debug.Log($"[IAPServerValidation] Returned text: {www.downloadHandler.text}");

                if (www.isNetworkError)
                {
                    Debug.Log($"[IAPServerValidation] NetworkError: {www.error}");
                    callback?.Invoke(ResultPurchaseValidation.NetworkError);
                }
                else if (www.isHttpError)
                {
                    Debug.Log($"[IAPServerValidation] HttpError: {www.error}");
                    callback?.Invoke(ResultPurchaseValidation.InternalError);
                }
                else if (int.TryParse(www.downloadHandler.text, out var errorCode) && errorCode > 600)
                {
                    Debug.Log($"[IAPServerValidation] unverified transaction with code: {errorCode}");
                    callback?.Invoke(ResultPurchaseValidation.UnverifiedTransaction);
                }
                else if (errorCode == 600)
                {
                    Debug.Log($"[IAPServerValidation] Success validate");
                    callback?.Invoke(ResultPurchaseValidation.VerifiedTransaction);
                }
                else
                {
                    Debug.Log($"[IAPServerValidation] NetworkError: {www.error}");
                    callback?.Invoke(ResultPurchaseValidation.NetworkError);
                }
            }
        }

        private UnityWebRequest CreateWebRequest()
        {
            var postData = GetPostData();

            Debug.Log($"[IAPServerValidation] Try validate with data: {postData}");
            var unityWebRequest = JsonContentWebRequest.CreatePostRequest(GetAddress(), postData);

            return unityWebRequest;
        }

        private string GetPostData()
        {
            return _requestModel.GetPostRequestBody();
        }

        private string GetAddress()
        {
            return $"{_serverAddress}{Api}";
        }
    }
}