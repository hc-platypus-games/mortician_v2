using System;
using UnityEngine;

namespace AppQuantum.IAP.Runtime
{
    public class RequestModel
    {
        public string idfa;
        public string idfv;
        public string appsflyer_id;
        public string reciept;
        public string productId;
        public string bundleID;
        public long timestamp;
        public string appVersion;
        public bool Subscription;

        public RequestModel(string idfa, string idfv, string appsflyer_id, string productId,
            string bundleId, string reciept, string appVersion, bool isSubscription)
        {
            this.idfa = idfa;
            this.idfv = idfv;
            this.appsflyer_id = appsflyer_id;
            this.productId = productId;
            this.bundleID = bundleId;
            this.appVersion = appVersion;
            this.reciept = reciept;
            this.Subscription = isSubscription;
        }

        public string GetPostRequestBody()
        {
            this.timestamp = DateTimeOffset.Now.ToUnixTimeSeconds();

            return JsonUtility.ToJson(this);
        }
    }
}