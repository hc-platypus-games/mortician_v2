using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace AppQuantum.IAP.Runtime.Utility
{
	public class JsonContentWebRequest
	{
		public static UnityWebRequest CreatePostRequest(string address, string postData, int timeoutSec = 5)
		{
			Debug.Log($"[IAPServerValidation] Try validate with data: {postData}");

			var bodyRaw = Encoding.UTF8.GetBytes(postData);

			var www = new UnityWebRequest(address, "POST", new DownloadHandlerBuffer(), new UploadHandlerRaw(bodyRaw)) {timeout = timeoutSec};
			www.SetRequestHeader("Content-Type", "application/json");
			return www;
		}
	}
}