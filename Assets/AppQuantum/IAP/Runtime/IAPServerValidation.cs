using System;
using UnityEngine;
using UnityEngine.Purchasing;

namespace AppQuantum.IAP.Runtime
{
    public class IAPServerValidation
    {
        private const string IdfaPrefsKey = "IDFA";
#if UNITY_DEVELOPMENT
		private const string ServerAddress = "http://localhost:3000/";
#else
        private const string ServerAddress = "https://iap.appquantum.com/";
#endif

        public static void Initialize()
        {
            Debug.Log($"[IAPServerValidation] Initialized");

            Debug.Log("[IAPServerValidation] Try get idfa");

            Application.RequestAdvertisingIdentifierAsync((string advertisingId,
                bool trackingEnabled, string error) =>
            {
                PlayerPrefs.SetString(IdfaPrefsKey, advertisingId);
                if (string.IsNullOrEmpty(error))
                {
                    Debug.Log("[IAPServerValidation] Success get idfa");
                }
                else
                {
                    Debug.Log($"[IAPServerValidation] Failed get idfa {error}");
                }
            });
        }

        public void Validate(Product product, string appsflyer_id, string appVersion, bool isSubscription,
            Action<ResultPurchaseValidation> callback)
        {
            var requestModel = new RequestModel(GetIDFA(), GetVendorId(), appsflyer_id,
                product.definition.storeSpecificId, Application.identifier,
                product.receipt, appVersion, isSubscription);

            var request = new ProcessServerValidationRequest(requestModel, ServerAddress);

            request.SendRequest(callback);
        }

        private string GetIDFA()
        {
#if UNITY_IOS
			return UnityEngine.iOS.Device.advertisingIdentifier;
#else
            return PlayerPrefs.GetString(IdfaPrefsKey);
#endif
        }

        private string GetVendorId()
        {
#if UNITY_IOS
			return UnityEngine.iOS.Device.vendorIdentifier;
#else
            return SystemInfo.deviceUniqueIdentifier;
#endif
        }
    }
}